REM get matching files for megas and remics
REM run from C:\Users\mmcdonald.ROCKPORT\projects\FM_DataTransfer\datafiles\02-atissuance

REM AtIssuanceMEGADisclosure_3138EKTH8_shortened.xml
REM egrep -1 "3138L02K6|3138L06S5"
mkdir AtIssuanceMEGADisclosure_3138EKTH8_shortened
copy AtIssuanceMBSDisclosure_CANDD_17954_AM0777.xml AtIssuanceMEGADisclosure_3138EKTH8_shortened
copy AtIssuanceMBSDisclosure_CANDD_18050_AM0880.xml AtIssuanceMEGADisclosure_3138EKTH8_shortened

REM AtIssuanceMEGADisclosure_31418M2U5.xml
REM egrep -1 "31381K3K3|31381KR27|31381L2S5|31381LV61|31381LV79|31381LWF0|31381LXQ5" *.xml

mkdir AtIssuanceMEGADisclosure_31418M2U5
copy AtIssuanceMBSDisclosure_CANDD_8333_464385.xml AtIssuanceMEGADisclosure_31418M2U5
copy AtIssuanceMBSDisclosure_CANDD_9139_463205.xml AtIssuanceMEGADisclosure_31418M2U5 
copy AtIssuanceMBSDisclosure_CANDD_9354_463502.xml AtIssuanceMEGADisclosure_31418M2U5 
copy AtIssuanceMBSDisclosure_CANDD_9924_464238.xml AtIssuanceMEGADisclosure_31418M2U5 
copy AtIssuanceMBSDisclosure_CANDD_9934_464246.xml AtIssuanceMEGADisclosure_31418M2U5 
copy AtIssuanceMBSDisclosure_CANDD_9940_464237.xml AtIssuanceMEGADisclosure_31418M2U5 
copy AtIssuanceMBSDisclosure_CANDD_9948_464287.xml AtIssuanceMEGADisclosure_31418M2U5 


REM AtIssuanceMEGADisclosure_31418MFG2.xml
REM -l egrep "31381KAV1|31381KHH5|31381KHZ5|31381KJE0|31381KJN0|31381KJQ3|31381KKF5|31381KKG3|31381KKL2|31381KL64|31381KN21|31381KN39|31381KPQ6|31381KPW3|31381KST7|31413XET6" *.xml

mkdir AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_7671_958346.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8734_462720.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8905_462932.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8923_462948.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8939_462961.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8947_462969.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8952_462971.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8965_462994.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8966_462995.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_8970_462999.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9008_463049.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9030_463110.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9050_463109.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9082_463131.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9090_463137.xml AtIssuanceMEGADisclosure_31418MFG2
copy AtIssuanceMBSDisclosure_CANDD_9153_463230.xml AtIssuanceMEGADisclosure_31418MFG2


REM AtIssuanceREMICDisclosure_2012-M07_shortened.xml
REM egrep -l "egrep -l "31381N7L1|31381P6J2|31381RKD5|31381RLL6|31381ST36|31381T2L3|31381T2Y5|31381T3Q1|31381T3V0|31381T3Y4" *.xml
mkdir AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_11653_466299.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_12482_467173.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_13658_468392.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_13704_468431.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_14771_469570.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_15850_470679.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_15884_470691.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_15904_470715.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_15908_470707.xml AtIssuanceREMICDisclosure_2012-M07_shortened
copy AtIssuanceMBSDisclosure_CANDD_15911_470712.xml AtIssuanceREMICDisclosure_2012-M07_shortened

REM AtIssuanceREMICDisclosure_2013-M10_shortened.xml
REM egrep -l "3138EKTH8|3138L32A2|3138L32E4|3138L32G9|3138L32L8|3138L32Z7|3138L33D5|3138L33Y9|3138L36L4|3138L36T7" *.xml
REM 3138EKTH8 is a mega
mkdir AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20578_AM3495.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20602_AM3472.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20607_AM3468.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20608_AM3474.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20611_AM3478.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20623_AM3491.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20638_AM3514.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20697_AM3574.xml AtIssuanceREMICDisclosure_2013-M10_shortened
copy AtIssuanceMBSDisclosure_CANDD_20704_AM3581.xml AtIssuanceREMICDisclosure_2013-M10_shortened