


SELECT DISTINCT
[Loan Number] = L.FannieMaeLoanNumber,
[Property ID] = P.PropertyIdentifier,
[Footnotes] = LA.LoanAdditionalDisclosureComment,
[Property Name] = PA.PropertyName,
[Property Address] = PAD.PropertyStreetAddressText,
[Property City] = PAD.PropertyAddressCityName,
[Property State] = PAD.PropertyAddressStateCode,
[Property Zip Code] = PAD.PropertyAddressPostalCode,
[Property County] = PAD.PropertyAddressCountyName,
[Metropolitan Statistical Area] = PAD.GeographicMetropolitanStatisticalAreaName,
[General Property Type] = CASE 
	WHEN PH.PropertyHousingType = 'Cooperative' THEN 'Cooperative Housing'
	WHEN PH.PropertyHousingType = 'Manufactured Housing' Then 'Manufactured Housing'
	ELSE 'Multifamily'
END,
[Specific Property Type] = PH.PropertyHousingType,
[Phase Year / Units] = -- allow for up to 3 phases
	CONCAT(
		(SELECT CONCAT(PropertyPhaseConstructedYearNumber,' (',PropertyPhaseConstructedUnitCount,')') FROM fmdata.PropertyPhasedConstruction WHERE PropertyId = P.Id AND PropertyPhaseNumber=1),
		(SELECT CONCAT('/',PropertyPhaseConstructedYearNumber,' (',PropertyPhaseConstructedUnitCount,')') FROM fmdata.PropertyPhasedConstruction WHERE PropertyId = P.Id AND PropertyPhaseNumber=2),
		(SELECT CONCAT('/',PropertyPhaseConstructedYearNumber,' (',PropertyPhaseConstructedUnitCount,')') FROM fmdata.PropertyPhasedConstruction WHERE PropertyId = P.Id AND PropertyPhaseNumber=3)),
--[N/A] = PP.PropertyPhaseConstructedYearNumber,
--[N/A] = PP.PropertyPhaseConstructedUnitCount,
[Total Units] = PA.PropertyTotalUnitCount,
[Unit of Measure] = '',
[Physical Occupancy (%)] = FORMAT(PO.PropertyPhysicalOccupancyPercent,'0.0#'),
[Physical Occupancy As-of-Date] = FORMAT(PO.PropertyPhysicalOccupancyAsOfDate,'M/d/yyyy'),
[Percent of Initial Pool Balance (%)] = '',
[Original UPB ($)] = FORMAT(LA.LoanOriginationUnpaidPrincipalBalanceAmount,'#,##0.00'),
[Loan Issuance UPB ($)] = FORMAT(LA.LoanContributionBalanceAmount,'#,##0.00'),
[Loan Issuance UPB / Unit($)] = '',
[Loan Seller] = LA.LoanSellerFullName,
[Loan Servicer] = LA.LoanPrimaryServicerFullName,
[Issuance Note Rate (%)] = FORMAT(LA.LoanInterestRate,'0.000##'),
[Pass-Through Rate (%)] = FORMAT(LA.LoanPassThroughRatePercent,'0.000##'),
[Interest Type] = LA.LoanInterestRateType,
[Interest Accrual Method] = LA.LoanInterestAccrualMethodType,
[Original Loan Term (months)] = LA.LoanTerm,
[Remaining Loan Term (months)] = LA.LoanRemainingTerm,
[Seasoning (months)] = LA.LoanSeasoningMonthCount,
[Original Interest Only Period (months)] = LA.LoanInterestOnlyPaymentTerm,
[Remaining Interest Only Period (months)] = LA.LoanRemainingInterestOnlyPeriod,
[Interest Only End Date] = FORMAT(LA.LoanInterestOnlyEndDate,'M/d/yyyy'),
[Amortization Type] = 
CASE WHEN LA.LoanTerm = LA.LoanOriginalAmortizationTerm THEN 'Fully Amortizing' ELSE '' END,
[Original Amortization Term (months)] = LA.LoanOriginalAmortizationTerm,
[Remaining Amortization Term (months)] = LA.LoanRemainingAmortizationTerm,
[Prepayment Provision] = '',
[Prepayment Provision End Date] = '',
[Monthly Debt Service ($)] = FORMAT(LA.LoanMonthlyDebtServiceAmount,'#,##0.00'),
[Monthly Debt Service Amount - Partial IO ($)] = FORMAT(LA.LoanMonthlyDebtServicePartialInterestOnlyAmount,'#,##0.00'),
[Note Date] = FORMAT(LA.LoanMortgageNoteDate,'M/d/yyyy'),
[First Payment Date] = FORMAT(LA.LoanFirstPaymentDueDate,'M/d/yyyy'),
[Maturity Date] = FORMAT(LA.LoanMaturityDate,'M/d/yyyy'),
[Loan Purpose] = LA.BorrowerLoanPurposeType,
[Lien Position] = LA.LoanLienPositionType,
[Ownership Interest] = PA.PropertyLandOwnershipRightType,
[Property Value ($)] = FORMAT(PV.PropertyValuationAmount,'#,##0.00'),
[Property Value As-of-Date] = FORMAT(PV.PropertyValuationAsOfDate,'M/d/yyyy'),
[Issuance LTV (%)] = LA.LoanCutOffDateLoanToValuePercent,
[All-In Issuance LTV (%)] = LA.LoanCutOffDateAllInLoanToValuePercent,
[UW NCF DSCR (x)] = LA.LoanUnderwrittenNCFDebtServiceCoverageRatioFactor,
[UW NCF DSCR (I/O)(x)] = LA.LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor,
[UW NCF DSCR All-In (x)] = LA.LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor,
[UW NCF Debt Yield (%)] = LA.LoanUnderwrittenNCFDebtYieldFactor,
[UW Economic Occupancy (%)] = PO.PropertyEconomicOccupancyPercent,
[UW Effective Gross Income ($)] = PFSA.PropertyEffectiveGrossIncomeAmount,
[UW Total Operating Expense ($)] = PFSA.PropertyTotalOperatingExpensesAmount,
[UW Replacement Reserves ($)] = PFSA.PropertyTotalOperatingCapitalExpenditureAmount,
[UW NCF ($)] = PFSA.PropertyNCFAmount,
[Cross Collateralized (Y/N)] = '',
[Cross Defaulted (Y/N)] = '',
[Crossed Transaction ID & Loan Number] = '',
[Terrorism Insurance (Y/N)] = PI.PropertyInsuranceType,
[Tax Escrow (Y/N)] = PA.PropertyTaxEscrowedIndicator,
[Tier] = LA.LoanPricingandUnderwritingTierType,
[Tier Drop Eligible (Y/N)] = LA.PricingTierDropEligibilityIndicator,
[Additional Debt Type] = '',
[Additional Debt Transaction ID & Loan Number] = '',
[Total Debt Current UPB ($)] = LA.LoanTotalDebtBalanceAmount,
[Affordable Housing Type] = PA.MultifamilyAffordableHousingProgramType,
[% of Units At or Below 50% Area Median Income] = PAF.PropertyAffordableUnitAMIPercent,
[% of Units At or Below 60% Area Median Income] = PAF.PropertyAffordableUnitAMIPercent,
[% of Units At or Below 80% Area Median Income] = PAF.PropertyAffordableUnitAMIPercent,
[% Units with Income or Rent Restrictions] = PA.PropertyUnitIncomeorRentRestrictionPercent,
[Age Restricted (Y/N)] = PA.PropertyAgeRestrictedIndicator,
[HAP Remaining Term (months)] = PA.PropertyHousingAssistanceProgramRemainingTerm,
[Green Financing Type] = LA.LoanGreenFinancingType,
[Green Building Certification] = PA.PropertyGreenBuildingCertificationType,
[Source Energy Use Intensity] = PE.PropertySourceEnergyUseIntensityRate,
[Source Energy Use Intensity Date] = PE.PropertySourceEnergyUseIntensityDate,
[Energy Star Score] = PE.PropertyEnergyStarScoreNumber,
[Energy Star Score Date] = PE.PropertyEnergyStarScoreDate

FROM FmData.Loan L
LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
LEFT JOIN FmData.Property P ON P.Id = LC.CollateralId
LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
LEFT JOIN FmData.PropertyAcquisition PA ON PA.Id = P.Id
LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
LEFT JOIN FmData.PropertyAddress PAD ON PAD.PropertyId = P.Id
LEFT JOIN FmData.PropertyHousing PH ON PH.PropertyId = P.Id
LEFT JOIN FmData.PropertyPhasedConstruction	PP ON PP.PropertyId = P.Id
LEFT JOIN FmData.PropertyOccupancy PO ON PO.PropertyId = P.Id
LEFT JOIN FmData.PropertyValuation	PV ON PV.PropertyId = P.Id
LEFT JOIN FmData.PropertyFinancialStatement PFS ON PFS.PropertyId = P.Id
LEFT JOIN FmData.PropertyFinancialSummaryActivity PFSA ON PFSA.PropertyFinancialStatementId = PFS.Id
LEFT JOIN FmData.PropertyInsurance PI ON PI.PropertyId = P.Id
LEFT JOIN FmData.PropertyAffordableUnit	PAF ON PAF.PropertyId = P.Id
LEFT JOIN FmData.PropertyEnergy	PE ON PE.PropertyId = P.Id

WHERE CP.MBSPoolIdentifier = '13170109178001'