WITH LAD As (
SELECT 
	 [LoanId] = LAD.LoanId
	,[LADCount] = COUNT(LAD.LoanAdditionalDebtType)	--TermId:17237
	,[LADType] = LAD.LoanAdditionalDebtType 
FROM [FmData].[LoanAdditionalDebt] LAD 
GROUP BY LAD.LoanId, LAD.LoanAdditionalDebtType)

SELECT 
L.Id,
[Additional Debt Count] = 
CASE
	WHEN LA.LADCount IS NULL THEN 'None'
	WHEN LA.LADCount = 1 THEN LA.LADType
	WHEN LA.LADCount > 1 THEN 'Multiple'
END
FROM FmData.Loan L
LEFT JOIN LAD LA ON L.Id = LA.LoanId
