WITH LoanAdditionalDebtCTE As (
SELECT 
	 [LoanId] = LAD.LoanId
	,[LADCount] = COUNT(LAD.LoanAdditionalDebtType)	--TermId:17237
	,[LoanAdditionalDebtType] = LAD.LoanAdditionalDebtType 
FROM [FmData].[LoanAdditionalDebt] LAD 
GROUP BY LAD.LoanId, LAD.LoanAdditionalDebtType)

SELECT
[Additional Debt Transaction ID & Loan Number] = TPD.TransactionIDLoanNumber,
[Associated Loan Number] = L.FannieMaeLoanNumber,
[Additional Debt Type] = LAD.LoanAdditionalDebtType,
[Additional Debt Lien Priority] = LAD.LoanAdditionalDebtLienPriorityType,
[Additional Debt UPB ($)] = LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount,
[Additional Debt Lien Holder] = LAD.LoanAdditionalDebtLienHolderName,
[Additional Debt Current Interest Rate (%)] = LAD.LoanAdditionalDebtCurrentInterestRate,
[Additional Debt Monthly Payment ($)] = LAD.LoanAdditionalDebtMonthlyPaymentAmount,
[Additional Debt Maximum Monthly Payment ($)] = LAD.LoanAdditionalDebtMaximumMonthlyPaymentAmount,
[Additional Debt ARM Margin (%)] = LAD.LoanAdditionalDebtAdjustableRateMortgageMarginPercent,
[Additional Debt Minimum Interest Rate (%)] = LAD.LoanAdditionalDebtMinimumInterestRatePercent,
[Additional Debt Maturity Date] = LAD.LoanAdditionalDebtMaturityDate,
[Additional Debt Original Amortization Term (months)] = LAD.LoanAdditionalDebtAmortizationTerm,
[Additional Debt Balloon (Y/N)] = LAD.LoanAdditionalDebtBalloonIndicator,
[Additional Debt Last Interest Only End Date] = LAD.LoanAdditionalDebtLastInterestOnlyEndDate,
[Additional Debt Line of Credit Full Amount ($)] = LAD.LoanAdditionalDebtLineofCreditFullAmount,
[Mezzanine Loan UPB ($)] = LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount,
[Mezzanine Loan Provider] = LAD.LoanMezzanineProviderName,
[Mezzanine Loan Interest Accrual Method] = LAD.LoanMezzanineInterestAccrualMethodType,
[Mezzanine Loan Initial Term Maturity Date] = LAD.LoanMezzanineInitialTermMaturityDate,
[Mezzanine Loan Monthly Payment Amount ($)] = LAD.LoanAdditionalDebtMonthlyPaymentAmount,
[Mezzanine Loan First Monthly Payment Due Date] = LAD.LoanMezzanineFirstPaymentDueDate,
[Mezzanine Loan Initial Term Interest Rate (%)] = LAD.LoanMezzanineInitialTermInterestRatePercent,
[Mezzanine Loan Amortization Term (months)] = LAD.LoanAdditionalDebtAmortizationTerm,
[Mezzanine Loan Interest Only End Date] = LAD.LoanMezzanineInterestOnlyEndDate,
[Mezzanine Loan Prepayment Provision] = LMP.LoanMezzaninePrepaymentProtectionType,
[Mezzanine Loan Prepayment Provision End Date] = LMP.LoanMezzaninePrepaymentProtectionEndDate,
[Mezzanine Loan Prepayment Provision Term (months)] = LMP.LoanMezzaninePrepaymentProtectionTerm

FROM FmData.Loan	L
	LEFT JOIN FmData.LoanAdditionalDebt	LAD ON LAD.LoanId = L.Id
	LEFT JOIN FmData.LoanMezzaninePrepaymentProtectionTerm LMP ON LMP.LoanAdditionalDebtId = LAD.Id
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
	LEFT JOIN LoanAdditionalDebtCTE LAD2 ON LAD2.LoanId = L.Id 
	LEFT JOIN 
		(SELECT 
			L.Id,
			[TransactionIDLoanNumber] = 
				CASE
					WHEN LA.LADCount IS NULL THEN 'None'
					WHEN LA.LADCount = 1 THEN LA.LoanAdditionalDebtType
					WHEN LA.LADCount > 1 THEN 'Multiple'
				END
		FROM FmData.Loan L
		LEFT JOIN LoanAdditionalDebtCTE LA ON L.Id = LA.LoanId) TPD ON TPD.Id = L.Id

WHERE CP.MBSPoolIdentifier = '24370094728001'