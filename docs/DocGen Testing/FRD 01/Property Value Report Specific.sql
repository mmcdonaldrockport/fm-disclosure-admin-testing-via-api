
WITH PropertyValuation AS (

SELECT 
[Ranking] = CASE 
				WHEN PV.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PV.PropertyId ORDER BY PV.PropertyActivityDatetime ASC)
			END,
PV.PropertyId,
PV.PropertyValuationAmount
FROM FmData.Property P 
	LEFT JOIN FmData.PropertyValuation PV ON PV.PropertyId = P.Id

	)

	SELECT 
	
	CP.MBSPoolIdentifier,
	L.FannieMaeLoanNumber,
	[Property Value ($)] = FORMAT(PV2.PropertyValuationAmount, '#,##0.00'),
	P.Id

	FROM FmData.Property P
		LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
		LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
		LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
		LEFT JOIN PropertyValuation PV1 ON PV1.PropertyId = P.Id
		LEFT JOIN (SELECT 
					L.Id,
					PV.PropertyId,
					[PropertyValuationAmount] = SUM(PV.PropertyValuationAmount)
				   FROM PropertyValuation PV
					LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = PV.PropertyId
					LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
					GROUP BY L.Id, PV.PropertyId) PV2 ON PV2.PropertyId = P.Id

	WHERE CP.MBSPoolIdentifier = '23470096868001'
		AND PV1.Ranking IN (0,1)
