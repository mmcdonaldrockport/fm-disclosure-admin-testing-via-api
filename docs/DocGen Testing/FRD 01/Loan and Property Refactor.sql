/*Annex A Fixed Rate MBS Loan & Property Tab*/

/*20122	Property General Property Type*/
WITH PropertyHousing AS

	(SELECT
		
		P.Id,
		[PropertyHousingType] = PH.PropertyHousingType,
		[PropertyGeneralPropertyType] = 
			CASE
				WHEN PH.PropertyHousingType = 'Cooperative' THEN 'Cooperative Housing'
				WHEN PH.PropertyHousingType = 'Manufactured Housing' THEN 'Manufactured Housing'
				ELSE 'Multifamily'
			END
	FROm FmData.Property P
		LEFT JOIN FmData.PropertyHousing PH ON PH.PropertyId = P.Id)

/*Phase Year / Units (Report Specific Field)*/
,  PropertyPhasedConstruction AS
	(SELECT
	 [PropertyId] = PPC.PropertyId, 
	 [PropertyPhaseYearUnitString] = 
		STUFF((SELECT  
					'/ ' + PPC2.PropertyPhaseConstructedYearNumber + IIF(PPC2.PropertyPhaseNumber LIKE '^[1-9][0-9]','', '(' + CAST(PropertyPhaseConstructedUnitCount AS VARCHAR(max)) + ') ' )
				FROM FmData.PropertyPhasedConstruction PPC2 
			WHERE PPC2.PropertyId = PPC.PropertyId 
			ORDER BY PPC2.PropertyPhaseNumber, PPC2.Id 
	        FOR XML PATH, TYPE).value('.[1]', 'NVARCHAR(MAX)'), 1, 2, '')
	FROM FmData.PropertyPhasedConstruction PPC 
	GROUP BY PPC.PropertyId)

/*Total Units (Report Specific Field)*/
, TotalUnits AS 

	(SELECT 
		L.Id,
		[PropertyTotalUnitCount] = SUM(PA.PropertyTotalUnitCount)
	FROM FmData.Loan L
		LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
		LEFT JOIN FmData.Collateral C ON C.Id = LC.CollateralId
		LEFT JOIN FmData.PropertyAcquisition PA ON C.Id = PA.Id
	GROUP BY L.Id)

/*Property Unit of Measure Type*/

, PropertyUnitOfMeasureType AS

	(SELECT DISTINCT
		L.Id,
		[PropertyIdentifier] = P.PropertyIdentifier,
		[PropertyHousingType] = PH.PropertyHousingType,
		[PropertyUnitofMeasureType] = 
			CASE
				WHEN PH.PropertyHousingType IN ('Cooperative', 'Multifamily', 'Seniors', 'Dedicated Student', 'Other', 'Military') THEN 'Units'
				ELSE 'Pads'
			END
	FROm FmData.Property P
		LEFT JOIN FmData.PropertyHousing PH ON PH.PropertyId = P.Id
		LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
		LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId)


SELECT 



FROM FmData.Loan L


WHERE L.FannieMaeLoanNumber = '2577006585'