
WITH PropertyOccupancy AS

	(SELECT 
		[Ranking] = 
			CASE 
				WHEN PO.PropertyId IS NULL THEN '0' 
				ELSE ROW_NUMBER() OVER(PARTITION BY PO.PropertyId ORDER BY PO.PropertyActivityDatetime ASC) 
			END,
		P.Id,
		P.PropertyTotalUnitCount,
		PO.PropertyActivityDatetime,
		PO.PropertyPhysicalOccupancyPercent,
		[Product] = iif(P.PropertyTotalUnitCount IS NULL, 0, (P.PropertyTotalUnitCount * PO.PropertyPhysicalOccupancyPercent)) 
	FROM FmData.PropertyAcquisition P
		LEFT JOIN FmData.PropertyOccupancy PO ON PO.PropertyId = P.Id)

, PropertyTotalUnitCount AS 

	(SELECT 
		L.Id,
		[PropertyTotalUnitCount] = SUM(PA.PropertyTotalUnitCount)
	FROM FmData.Loan L 
		LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
		LEFT JOIN FmData.PropertyAcquisition PA ON PA.Id = LC.CollateralId
	GROUP BY L.Id)


SELECT DISTINCT
	L.Id,
	[PropertyPhysicalOccupancyPercent] = (PO2.Product / PU.PropertyTotalUnitCount)
FROM FmData.Loan L 
	LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
	LEFT JOIN FmData.PropertyAcquisition PA ON PA.Id = LC.CollateralId 
	LEFT JOIN PropertyOccupancy PO ON PO.Id = PA.Id
	LEFT JOIN PropertyTotalUnitCount PU ON PU.Id = L.Id
	LEFT JOIN (SELECT  
				
				L.Id,
				[Product] = SUM(PO.Product)
				FROM PropertyOccupancy PO
					LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = PO.Id
					LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
				GROUP BY L.Id) PO2 ON PO2.Id = L.Id 

WHERE PO.Ranking IN (0,1)
	AND L.Id = 202167


--SELECT * FROM FmData.Loan L
--LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId WHERE LoanIdentifier = '23470096862001'
-- --809 loans
----SELECT * FROM FmData.PropertyAcquisition 4822 props

			