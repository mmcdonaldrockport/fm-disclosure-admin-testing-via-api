/*(20144) Security Minimum Cut Off Date Balance Amount*/
WITH LoanBalances AS 
	(SELECT 
		[Rank] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanContributionBalanceAmount ASC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanContributionBalanceAmount,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

SELECT DISTINCT
	[MBSPoolIdentifier] = MBSPoolIdentifier,
	[LoanIdentifier] = LoanIdentifier,
	[Security Minimum Cut Off Date Balance Amount] = LoanContributionBalanceAmount
FROM LoanBalances
WHERE [Rank] = 1 AND ProductGroupType = 'MBS';

/*(20130) Security Maximum Cut Off Date Balance Amount*/
WITH LoanBalances AS 
	(SELECT 
		[Rank] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanContributionBalanceAmount DESC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanContributionBalanceAmount,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

SELECT DISTINCT
	[MBSPoolIdentifier] = MBSPoolIdentifier,
	[LoanIdentifier] = LoanIdentifier,
	[Security Minimum Cut Off Date Balance Amount] = LoanContributionBalanceAmount
FROM LoanBalances
WHERE [Rank] = 1 AND ProductGroupType = 'MBS' AND MBSPoolIdentifier = '33263625448001';

/*(20146) Security Minimum Pass Through Rate*/
WITH LoanRates AS 
	(SELECT 
		[Rank] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanPassThroughRatePercent ASC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanPassThroughRatePercent,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

SELECT DISTINCT
	[MBS Pool Identifier] = MBSPoolIdentifier,
	[Loan Identifier] = LoanIdentifier,
	[Security Minimum Pass Through Rate] = LoanPassThroughRatePercent
FROM LoanRates
WHERE [Rank] = 1 AND ProductGroupType = 'MBS';

/*(20132) Security Maximum Pass Through Rate*/
WITH LoanRates AS 
	(SELECT 
		[Rank] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanPassThroughRatePercent DESC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanPassThroughRatePercent,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

SELECT DISTINCT
	[MBS Pool Identifier] = MBSPoolIdentifier,
	[Loan Identifier] = LoanIdentifier,
	[Security Minimum Pass Through Rate] = LoanPassThroughRatePercent
FROM LoanRates
WHERE [Rank] = 1 AND ProductGroupType = 'MBS'

/*20100	Loan Pool Balance Percent*/
SELECT 
	[MBS Pool Identifier] = CP.MBSPoolIdentifier,
	[Security Issue Amount] = S.SecurityIssueAmount,
	[Loan Contribution Balance Amount] = LA.LoanContributionBalanceAmount,
	[Loan Pool Balance Percent] = (LA.LoanContributionBalanceAmount/S.SecurityIssueAmount)
FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
WHERE ProductGroupType = 'MBS'

/*(14124) Loan Amortization Type*/

SELECT DISTINCT

[MBS Pool Identifier] = CP.MBSPoolIdentifier,
[Loan Identifier] = L.LoanIdentifier,
[Loan Term] = LA.LoanTerm,
[Loan Interest Only Payment Term] = LA.LoanInterestOnlyPaymentTerm,
[Loan Original Amortization Term] = LA.LoanOriginalAmortizationTerm,
[Loan Amortization Type] =  CASE 
					WHEN LA.LoanInterestOnlyPaymentTerm IS NOT NULL AND LA.LoanInterestOnlyPaymentTerm < LA.LoanTerm AND LoanTerm < LA.LoanOriginalAmortizationTerm THEN 'Interest Only/Amortizing/Balloon'
					WHEN LA.LoanInterestOnlyPaymentTerm IS NULL AND LA.LoanTerm <LA.LoanOriginalAmortizationTerm THEN 'Amortizing Balloon'
					WHEN LA.LoanInterestOnlyPaymentTerm = LA.LoanTerm THEN 'Interest Only/Ballon'
					WHEN LoanTerm = LA.LoanOriginalAmortizationTerm THEN 'Fully Amortizing'
					ELSE 'ERROR'
				END

FROM FmData.LoanAcquisition LA
	LEFT JOIN FmData.Loan L ON L.Id = LA.Id
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId

/*(20111) Loan Cross Collateralized Indicator*/
SELECT 
[MBS Pool Identifier] = CP.MBSPoolIdentifier,
[Loan Identifier] = L.LoanIdentifier,
[Loan Crossed Relationship Type] = LCR.LoanCrossedRelationshipType,
[Loan Cross Collateralized Indicator] = 
	CASE
		WHEN LCR.LoanCrossedRelationshipType ='Cross Collateralized'	THEN 'Y'
		WHEN LCR.LoanCrossedRelationshipType ='Cross Defaulted and Cross Collateralized' THEN 'Y'
		ELSE 'N'
	END
FROM FmData.Loan L 
LEFT JOIN FmData.LoanCrossedRelationship LCR ON LCR.LoanId = L.Id
LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId


/*(20112) Loan Cross Defaulted Indicator

IF Loan Cross Relationship Type(21149) In ('Cross Defaulted', 'Cross Defaulted and Cross Collateralized','One Way Cross Defaulted Only' ') then 'Y' Else 'N'.*/
SELECT 
[MBS Pool Identifier] = CP.MBSPoolIdentifier,
[Loan Identifier] = L.LoanIdentifier,
[Loan Crossed Relationship Type] = LCR.LoanCrossedRelationshipType,
[Loan Cross Collateralized Indicator] = 
	CASE
		WHEN LCR.LoanCrossedRelationshipType ='Cross Defaulted'	THEN 'Y'
		WHEN LCR.LoanCrossedRelationshipType ='Cross Defaulted and Cross Collateralized' THEN 'Y'
		WHEN LCR.LoanCrossedRelationshipType ='One Way Cross Defaulted Only' THEN 'Y'
		ELSE 'N'
	END
FROM FmData.Loan L 
LEFT JOIN FmData.LoanCrossedRelationship LCR ON LCR.LoanId = L.Id
LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId;

/*(20113) Loan Crossed Pool Number and Loan Number*/

WITH MBSPools AS 
	(SELECT DISTINCT 
		[MBSPoolIdentifier] = CAST(CP.MBSPoolIdentifier AS varchar(max)),
		[LoanIdentifier] = CAST(L.LoanIdentifier AS varchar(max)),
		[FannieMaeLoanCrossedNumber] = LCR.FannieMaeLoanCrossedNumber
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanCrossedRelationship LCR ON LCR.LoanId = L.Id)

/*Concatenate values using the Stuff/ xml method*/
SELECT DISTINCT
[MBS Pool Identifier] = MBSPoolIdentifier,
[Loan Identifier] = LoanIdentifier,
[Loan Crossed Pool Number and Loan Number] = 
iif(FannieMaeLoanCrossedNumber IS NULL, '',
 (CONCAT(MBSPoolIdentifier, ' / ', 
	REPLACE(
		STUFF(
			(SELECT ',' + FannieMaeLoanCrossedNumber
				FROM MBSPools AS T2
				WHERE T2.MBSPoolIdentifier = T1.MBSPoolIdentifier 
				ORDER BY FannieMaeLoanCrossedNumber
				FOR XML PATH (''), TYPE
			).value('.', 'varchar(max)'),1,1,''),
	 ',', ' / ')
	)))
FROM MBSPools AS T1;

/*(20116) Loan Contribution Balance per Unit Amount	*/
WITH CTE AS (SELECT 
[Loan Identifier] = L.LoanIdentifier,
[Property Acquisition Total Unit Count] = SUM(PA.PropertyTotalUnitCount)
FROM FmData.PropertyAcquisition PA
LEFT JOIN FmData.Property P ON P.Id = PA.Id
LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
GROUP BY L.LoanIdentifier)

SELECT 
[Loan Identifier],
[Property Acquisition Total Unit Count],
[Loan Contribution Balance per Unit Amount] = CAST(([Loan Identifier]/[Property Acquisition Total Unit Count]) AS money)
FROM CTE

/*20105	Loan Additional Debt Pool Number and Loan Number	

"If there is one Loan Additional Debt Type (17237) then Concatenate Loan Additional Debt Fannie Mae Pool Number(21120) and Loan Additional Debt Fannie Mae Loan Number(21121) separated by a forward slash, "" /  "",
If multiple types of Loan Additional Debt Type (17237) exist, then populate with �Multiple�
Else if Loan Additional Debt Type (17237) is Null, populate with �None�."
*/
SELECT * FROM FmData.LoanAdditionalDebt

/*20122	Property General Property Type*/
SELECT
[MBS Pool Identifier] =CP.MBSPoolIdentifier,
[Property Identifier] = P.PropertyIdentifier,
[Property Housing Type] = PH.PropertyHousingType,
[Property General Property Type] = 
	CASE
		WHEN PH.PropertyHousingType = 'Cooperative' THEN 'Cooperative Housing'
		WHEN PH.PropertyHousingType = 'Manufactured Housing' THEN 'Manufactured Housing'
		ELSE 'Multifamily'
	END
FROm FmData.Property P
LEFT JOIN FmData.PropertyHousing PH ON PH.PropertyId = P.Id
LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId

/*Property Unit of Measure Type*/
SELECT DISTINCT
L.Id,
[Property Identifier] = P.PropertyIdentifier,
[Property Housing Type] = PH.PropertyHousingType,
[Property Unit of Measure Type] = 
	CASE
		WHEN PH.PropertyHousingType IN ('Cooperative', 'Multifamily', 'Seniors', 'Dedicated Student', 'Other', 'Military') THEN 'Units'
		ELSE 'Pads'
	END
FROm FmData.Property P
LEFT JOIN FmData.PropertyHousing PH ON PH.PropertyId = P.Id
LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId