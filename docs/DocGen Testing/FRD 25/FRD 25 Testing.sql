/*Multifamily MBS Cover Page � Variable Rate*/

WITH SecurityInterestrate AS 

	(SELECT 
		[Ranking] = 
			CASE 
				WHEN SIR.SecurityId IS NULL THEN '0' 
				ELSE ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate ASC)
			END,
		S.Id,
		SIR.SecurityInterestRate
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityInterestRate SIR ON SIR.SecurityId = S.Id)


SELECT 

[ ] = S.SecurityIssueAmount,
[Transaction ID] = CP.MBSPoolIdentifier,
[CUSIP] = S.SecurityCUSIPIdentifier,
[Prefix] = S.MBSPoolPrefixType,
[Pass-Through Rate] = CAST(FORMAT(SIR.SecurityInterestRate, '#.000##') AS varchar(max)) + '%',
[Issue Date] = FORMAT(S.SecurityIssueDate, 'M/d/yyyy'),
[Settlement Date] = FORMAT(S.SecurityFederalBookEntryDate, 'M/d/yyyy'),
[Maturity Date] = FORMAT(S.SecurityMaturityDate, 'M/d/yyyy'),
[Principal and Interest Day of Each Month Payable] = S.SecurityPaymentDayNumber,
[Beginning] = FORMAT(S.SecurityFirstPaymentDate, 'M/d/yyyy')


FROM FmData.Security S
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id
	LEFT JOIN SecurityInterestrate SIR ON SIR.Id = S.Id

WHERE SIR.Ranking IN (0,1)

