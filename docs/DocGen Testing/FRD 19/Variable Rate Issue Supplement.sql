/*Variable Rate Issue Supplement*/


WITH SecurityInterestrate AS 

	(SELECT 
		[Ranking] = 
			CASE 
				WHEN SIR.SecurityId IS NULL THEN '0' 
				ELSE ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate ASC)
			END,
		S.Id,
		SIR.SecurityInterestRate
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityInterestRate SIR ON SIR.SecurityId = S.Id)

SELECT 

	[Transaction ID] = CP.MBSPoolIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Issue Date] = FORMAT(S.SecurityIssueDate, 'M/d/yyyy'),
	[WA Pass-Through Rate (%)] = CAST(FORMAT(SIR.SecurityInterestRate, '#.000##') AS varchar(max)) + '%',
	[Investor Security UPB] = '$' + CAST(FORMAT(S.SecurityIssueAmount, '#,###.00') AS varchar(max)),
	[Interest Type] = S.SecurityInterestRateType,
	[Settlement Date] = FORMAT(S.SecurityFederalBookEntryDate, 'M/d/yyyy'),
	[Loan Identifier] = L.FannieMaeLoanNumber,
	[Investor Loan UPB - Issuance] = '$' + CAST(FORMAT(LA.LoanContributionBalanceAmount, '#,###.00') AS varchar(max)),
	[Pass-Through Rate] = CAST(FORMAT(LA.LoanPassThroughRatePercent, '#.000##') AS varchar(max)) + '%'

FROM FmData.CollateralPool	CP
	LEFT JOIN FmData.Security S ON S.Id = CP.Id
	LEFT JOIN SecurityInterestrate SIR ON SIR.Id = S.Id 
	LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
	LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id

WHERE CP.MBSPoolIdentifier = 'AF3383' 
	AND SIR.Ranking IN (0,1)
