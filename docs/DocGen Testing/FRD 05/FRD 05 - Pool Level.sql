/*FRD 01 Pool Level*/

/*Security Interest Rate CTE to grab the at issuance value (securities can have many security rate records i.e. adjustable rate)*/
WITH SecurityInterestRateCTE AS 
	(SELECT DISTINCT
		[Ranking] = ROW_NUMBER() OVER (PARTITION BY SI.SecurityId ORDER BY SI.SecurityInterestRateEffectiveDate ASC),
		SI.SecurityId,
		SI.SecurityInterestRate,
		SI.SecurityInterestRateEffectiveDate
	FROM FmData.SecurityInterestRate SI
	)

/*Financial Instrument Collateral Group Balance CTE to grab the at issuance value*/
, FinancialInstrumentCollateralGroupBalanceCTE AS 
	(SELECT DISTINCT
		[Ranking] = ROW_NUMBER() OVER (PARTITION BY FICGB.CollateralPoolId ORDER BY FICGB.FinancialInstrumentCollateralGroupBalanceEffectiveDate),
		FICGB.CollateralPoolId,
		FICGB.CollateralPoolActiveLoanCount,
		FICGB.CollateralPoolAverageOriginalLoanSizeAmount,
		FICGB.CollateralPoolWeightedAverageOriginalTerm,
		FICGB.CollateralPoolWeightedAverageAmortizationMaturityTerm,
		FICGB.CollateralPoolWeightedAverageLoanAgeNumber,
		FICGB.CollateralPoolWeightedAverageMaturityNumber,
		FICGB.CollateralPoolWeightedAverageCouponRate,
		FICGB.CollateralPoolWeightedAverageLoanToValueRatioFactor,
		FICGB.CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor
	FROM  FmData.FinancialInstrumentCollateralGroupBalance FICGB
	)

/*(20130) Security Maximum Cut Off Date Balance Amount*/
, MaximumCutOffCTE AS 
	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanContributionBalanceAmount DESC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanContributionBalanceAmount,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

/*(20144) Security Minimum Cut Off Date Balance Amount*/
, MinimumCutOffCTE AS 
	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanContributionBalanceAmount ASC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanContributionBalanceAmount,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

/*(20132) Security Maximum Pass Through Rate*/
, MaxPassThroughRateCTE AS 
	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanPassThroughRatePercent DESC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanPassThroughRatePercent,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

/*(20146) Security Minimum Pass Through Rate*/
, MinPassThroughRateCTE AS 
	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.MBSPoolIdentifier ORDER BY LA.LoanPassThroughRatePercent ASC),
		CP.MBSPoolIdentifier,
		L.LoanIdentifier,
		LA.LoanPassThroughRatePercent,
		S.ProductGroupType
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.Security S ON S.Id = CP.Id
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

/*Select values for pool level with report labels and report formats as specified per the FRD*/
SELECT DISTINCT
	[Transaction ID] = iif (CP.MBSPoolIdentifier IS NULL, '', CP.MBSPoolIdentifier), --For the Fixed/Variable Rate MBS/Credit Facility Annex A's, this field will always be the MBS Pool Identifier
	[CUSIP] = iif (S.SecurityCUSIPIdentifier IS NULL, '', S.SecurityCUSIPIdentifier),
	[Settlement Date] = iif(S.SecurityFederalBookEntryDate IS NULL, '', FORMAT(S.SecurityFederalBookEntryDate, 'M/d/yyyy')),
	[Issue Date] = iif(S.SecurityIssueDate IS NULL, '', FORMAT(S.SecurityIssueDate, 'M/d/yyyy')),
	[MBS First Payment Date] = iif(S.SecurityFirstPaymentDate IS NULL, '', FORMAT(S.SecurityFirstPaymentDate, 'M/d/yyyy')),
	[Maturity Date] = iif(S.SecurityMaturityDate IS NULL, '', FORMAT(S.SecurityMaturityDate, 'M/d/yyyy')),
	[Prefix] =  iif(S.MBSPoolPrefixType IS NULL, '', S.MBSPoolPrefixType),
	[Number of Loans] = iif(FICGB_CTE.CollateralPoolActiveLoanCount IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolActiveLoanCount, '0')),
	[Issuance UPB ($)] = iif(S.SecurityIssueAmount IS NULL, '', FORMAT(S.SecurityIssueAmount, '#,##0.00')),
	[Maximum Issuance UPB ($)] = iif(MaxCut_CTE.LoanContributionBalanceAmount IS NULL, '', FORMAT(MaxCut_CTE.LoanContributionBalanceAmount, '#,##0.00')),
	[Minimum Issuance UPB ($)] = iif(MinCut_CTE.LoanContributionBalanceAmount IS NULL, '', FORMAT(MinCut_CTE.LoanContributionBalanceAmount, '#,##0.00')),
	[Weighted Average Issuance UPB ($)] = iif(FICGB_CTE.CollateralPoolAverageOriginalLoanSizeAmount IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolAverageOriginalLoanSizeAmount, '#,##0.00')),
	[Weighted Average Original Loan Term (months)] = iif(FICGB_CTE.CollateralPoolWeightedAverageOriginalTerm IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolWeightedAverageOriginalTerm, '0')),
	[Weighted Average Amortization Term (months)] = iif(FICGB_CTE.CollateralPoolWeightedAverageAmortizationMaturityTerm IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolWeightedAverageAmortizationMaturityTerm, '0')),
	[Weighted Average Seasoning (months)] = iif(FICGB_CTE.CollateralPoolWeightedAverageLoanAgeNumber IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolWeightedAverageLoanAgeNumber, '0')),
	[Weighted Average Remaining Term to Maturity (months)] = iif(FICGB_CTE.CollateralPoolWeightedAverageMaturityNumber IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolWeightedAverageMaturityNumber, '0')),
	[Weighted Average Accruing Note Rate (%)] = iif(FICGB_CTE.CollateralPoolWeightedAverageCouponRate IS NULL, '', FORMAT(FICGB_CTE.CollateralPoolWeightedAverageCouponRate, '0.000##')),
	[Weighted Average Pass-Through Rate (%)] = iif(SI_CTE.SecurityInterestRate IS NULL, '', FORMAT(SI_CTE.SecurityInterestRate, '0.000##')),
	[Maximum Pass-Through Rate (%)] = iif(MaxPassCTE.LoanPassThroughRatePercent IS NULL, '', FORMAT(MaxPassCTE.LoanPassThroughRatePercent, '0.000##')),
	[Minimum Pass-Through Rate (%)] = iif(MinPassCTE.LoanPassThroughRatePercent IS NULL, '', FORMAT(MinPassCTE.LoanPassThroughRatePercent, '0.000##'))

FROM FmData.Security S
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id 
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceCTE FICGB_CTE ON FICGB_CTE.CollateralPoolId = CP.Id
	LEFT JOIN SecurityInterestRateCTE SI_CTE ON SI_CTE.SecurityId = S.Id
	LEFT JOIN MaximumCutOffCTE MaxCut_CTE ON MaxCut_CTE.MBSPoolIdentifier = CP.MBSPoolIdentifier 
	LEFT JOIN MinimumCutOffCTE MinCut_CTE ON MinCut_CTE.MBSPoolIdentifier = CP.MBSPoolIdentifier
	LEFT JOIN MaxPassThroughRateCTE MaxPassCTE ON MaxPassCTE.MBSPoolIdentifier = CP.MBSPoolIdentifier
	LEFT JOIN MinPassThroughRateCTE MinPassCTE ON MinPassCTE.MBSPoolIdentifier = CP.MBSPoolIdentifier

WHERE S.ProductGroupType <> 'Mega' AND S.ProductGroupType <> 'REMIC' --filiter out Megas and REMICS
	AND SI_CTE.Ranking = 1 -- get at issuance value from security interest rate
	AND FICGB_CTE.Ranking = 1 --get at issuance value from financial instrument 
	AND MaxCut_CTE.Ranking = 1 --CTE used for TPD Field 
	AND MinCut_CTE.Ranking = 1 --CTE used for TPD Field
	AND MaxPassCTE.Ranking = 1 --CTE used for TPD Field
	AND MinPassCTE.Ranking = 1 --CTE used for TPD Field
	
	/*Transaction ID Parameter*/
	AND CP.MBSPoolIdentifier = '469620'