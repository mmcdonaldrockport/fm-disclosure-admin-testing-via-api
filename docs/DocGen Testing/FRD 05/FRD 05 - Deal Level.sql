WITH DealStructuredFacilityAtIssuance AS 

	(SELECT 
		CP.Id,
		[DealAggregateCutOffDateBalanceAmount] = CAST(FORMAT(DFSA.DealAggregateCutOffDateBalanceAmount, '#,##0.00') AS varchar(max)),
		[DealLoantoValueFactor] = CAST(FORMAT(DFSA.DealLoantoValueFactor, '0.0#') AS varchar(max)),
		[DealNumberofBorrowingsOutstandingCount] = CAST(DFSA.DealNumberofBorrowingsOutstandingCount AS varchar(max)),
		[DealPropertyCount] = CAST(DFSA.DealPropertyCount AS varchar(max)),
		[DealUnderwrittenNCFDebtServiceCoverageRatioFactor] = CAST(FORMAT(DFSA.DealUnderwrittenNCFDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor] = CAST(FORMAT(DFSA.DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor, '0.0#') AS varchar(max)),
		[DealCollateralAggregateValueAmount] = CAST(FORMAT(DFSA.DealCollateralAggregateValueAmount, '#,##0.00') AS varchar(max)),
		[DealPropertyAggregateValueAmount] = CAST(FORMAT(DFSA.DealPropertyAggregateValueAmount, '#,##0.00') AS varchar(max))
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.DealStructuredFacilityAtIssuance DFSA ON DFSA.CollateralPoolId = CP.Id)

, MBSPools AS 
	(SELECT DISTINCT
		CP.Id,
		[MBSPoolIdentifier] = CP.MBSPoolIdentifier,
		[DealIdentifier] = D.DealIdentifier, 
		[DealId] = D.Id
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.Deal D ON D.Id = L.DealId)

, NonPropertyCollateral AS

	(SELECT DISTINCT
		D.Id,
		[CollateralNonPropertyAmount] = iif(SUM(C.CollateralNonPropertyAmount) IS NULL, 0, SUM(C.CollateralNonPropertyAmount)),
		[CollateralNonPropertyType] = C.CollateralNonPropertyType
	FROM FmData.Deal D
		LEFT JOIN FmData.Loan L ON L.DealId = D.Id
		LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
		LEFT JOIN FmData.Collateral C ON C.Id = LC.CollateralId
	WHERE C.CollateralNonPropertyType IS NOT NULL
	GROUP BY D.Id, C.CollateralNonPropertyType)

SELECT DISTINCT

	[Fannie Mae Transaction ID at Facility Level] = --Concatenate values using the Stuff/ xml method
		REPLACE(
			STUFF(
				(SELECT ',' + MBSPoolIdentifier
					FROM MBSPools AS T2
					WHERE T2.DealIdentifier = T1.DealIdentifier 
					ORDER BY MBSPoolIdentifier
					FOR XML PATH (''), TYPE
				).value('.', 'varchar(max)'),1,1,''),
		 ',', ' / '),
	[Maximum Borrowing Amount ($) at Facility Level] = FORMAT(D.DealMaximumBorrowingAmount, '#,##0.00'),
	[Total Issuance UPB ($) at Facility Level] = DA.DealAggregateCutOffDateBalanceAmount,
	[Issuance LTV at Facility Level (%)] = DA.DealLoantoValueFactor,
	[UW NCF DSCR at Facility Level (x)] = DA.DealUnderwrittenNCFDebtServiceCoverageRatioFactor,
	[UW NCF DSCR (I/O) at Facility Level (x)] = DA.DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor,
	[Number of Mortgaged Properties at Facility Level] = DA.DealPropertyCount,
	[Aggregate Property Value ($) at Facility Level] = DA.DealPropertyAggregateValueAmount,
	[Value of Other Collateral ($) at Facility Level] = FORMAT(NPC.CollateralNonPropertyAmount, '#,##0.00'),
	[Other Collateral Type at Facility Level] = NPC.CollateralNonPropertyType,
	[Total Collateral Value ($) at Facility Level] = DA.DealCollateralAggregateValueAmount,
	[Additional Loans Allowed under the Credit Facility (Y/N)] = D.DealAdditionalLoanAllowedIndicator,
	[Property Releases Allowed (Y/N)] = D.DealCollateralReleaseAllowedIndicator,
	[Property Additions Allowed (Y/N)] = D.DealAdditionalCollateralAllowedIndicator,
	[Property Substitution Allowed (Y/N)] = D.DealCollateralSubstitutionAllowedIndicator,
	[Minimum Permitted DSCR of Credit Facility - Fixed Rate Loans] = FORMAT(D.DealFixedRateMinimumPermittedDebtServiceCoverageRatioFactor, '0.00'),
	[Minimum Permitted DSCR of Credit Facility - ARM Loans] = iif(D.DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor IS NULL, '', FORMAT(D.DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor, '0.00')),
	[Maximum Permitted LTV of Credit Facility (%)] = FORMAT(D.DealMaximumPermittedLoanToValuePercent, '0.0#')

FROM FmData.Deal D
	LEFT JOIN FmData.Loan L ON L.DealId = D.Id
	LEFT JOIn MBSPools T1 ON T1.DealId = D.Id
	LEFT JOIN DealStructuredFacilityAtIssuance DA ON DA.Id = T1.Id
	LEFT JOIN NonPropertyCollateral NPC ON NPC.Id = D.Id

WHERE T1.MBSPoolIdentifier = '469620'
AND D.DealMaximumBorrowingAmount IS NOT NULL
