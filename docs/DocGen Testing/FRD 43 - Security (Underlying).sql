WITH SecurityPayment AS

	(SELECT 
		[Ranking] =	
			CASE 
				WHEN SP.SecurityId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY SP.SecurityId ORDER BY SP.SecurityPaymentEffectiveDate DESC)
			END,  
		S.Id,
		SP.SecurityUnpaidPrincipalBalanceAmount
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityPayment SP ON SP.SecurityId = S.Id)

, SecurityInterestRate AS

	(SELECT 
		[Ranking] =	
			CASE 
				WHEN SIR.SecurityId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate DESC)
			END,  
		S.Id,
		SIR.SecurityInterestRate
	FROM FmData.Security S
		LEFT JOIN  FmData.SecurityInterestRate SIR ON SIR.SecurityId = S.Id)

, SecurityFactor AS  --Get the most recent record for Security Factor
	(SELECT 
		[Ranking] = iif(SF.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SF.SecurityId ORDER BY SF.SecurityFactorEffectiveDate DESC)),
		S.Id,
		[SecurityPaydownFactor] = CAST(FORMAT(SF.SecurityPaydownFactor, '0.0000000') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityFactor SF ON SF.SecurityId = S.Id)

, FinancialInstrumentCollateralGroupBalance AS -- Get the most recent Financial Instrument Collateral Group Balance
	(SELECT 
		[Ranking] = iif(FICGB.CollateralPoolId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY FICGB.CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate DESC)),
		CP.Id,
		FICGB.CollateralPoolWeightedAverageNetMarginRate,
		FICGB.CollateralPoolWeightedAverageMaturityNumber,
		FICGB.CollateralPoolWeightedAverageCouponRate
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.FinancialInstrumentCollateralGroupBalance FICGB ON FICGB.CollateralPoolId = CP.Id)

, LoanCount AS 

	(SELECT 
		CP.Id,
		[LoanCount] = COUNT(L.Id)
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = Cp.Id
	GROUP BY CP.Id)


SELECT 

[Mega Identifier] = CP1.MBSPoolIdentifier,
[Security Mega CUSIP Number] = S1.SecurityCUSIPIdentifier,
[Relationship] = SR.SecurityRelationshipType,
[Participation Percent (%)] = SR.SecurityParticipationPercent,
[Pool #] = CP2.MBSPoolIdentifier,
[CUSIP] = S2.SecurityCUSIPIdentifier,
[Current UPB ($)] = SP.SecurityUnpaidPrincipalBalanceAmount,
[Loan Count] = LCount.LoanCount, --come back to
[Net Rate (%)] = SIR.SecurityInterestRate,
[Factor] = SF.SecurityPaydownFactor,
[Participation UPB Amount ($)] = SR.SecurityCollateralParticipationUnpaidPrincipalBalanceAmount,
[Margin (%)] = FICGB.CollateralPoolWeightedAverageNetMarginRate,
[Weighted Average Remaining Maturity Term] = FICGB.CollateralPoolWeightedAverageMaturityNumber,
[Pool Weighted Average Coupon (%)] = FICGB.CollateralPoolWeightedAverageCouponRate
--[City] = PA.PropertyAddressCityName,
--[State] = PA.PropertyAddressStateCode

FROM FmData.Security S1
	LEFT JOIN FmData.CollateralPool CP1 ON CP1.Id = S1.Id
	LEFT JOIN FmData.SecurityRelationship SR ON SR.SecurityId = S1.Id
	LEFT JOIN FmData.Security S2 On S2.Id = SR.SecurityRelatedId
	LEFT JOIN FmData.CollateralPool CP2 ON CP2.Id = S2.Id
	LEFT JOIN SecurityPayment SP ON SP.Id = S2.Id
	LEFT JOIN SecurityInterestRate SIR ON SIR.Id = S2.Id
	LEFT JOIN SecurityFactor SF On SF.Id = S2.Id 
	LEFT JOIN FinancialInstrumentCollateralGroupBalance FICGB ON FICGB.Id = CP2.Id
	LEFT JOIN LoanCount LCount On LCount.Id = CP2.Id

WHERE S1.ProductGroupType = 'Mega'

