WITH SecurityStatus AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime),
		S.Id,
		SS.SecurityStatusType
	FROm FmData.Security S
		LEFT JOIN FmData.SecurityStatus SS ON SS.SecurityId = S.Id)
SELECT
	CP.LastUpdatedPayloadId, 
	[Mega Identifier] = CP.MBSPoolIdentifier,
	[Security Mega CUSIP Number] = S.SecurityCUSIPIdentifier,
	[Security Type] = S.ProductGroupType,
	[Issue Date] = S.SecurityIssueDate,
	[First Payment Date] = S.SecurityFirstPaymentDate,
	[Maturity Date] = S.SecurityMaturityDate,
	[Issuance UPB] = S.SecurityIssueAmount,
	[Interest Type] = S.SecurityInterestRateType,
	[Accrual Method] = S.SecurityDayCountConventionType,
	[Prefix Type] = S.MBSPoolPrefixType,
	[Pool Status] = SS.SecurityStatusType

FROM FmData.Security S
	LEFT JOIN SecurityStatus SS ON SS.Id = S.Id
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id

WHERE S.ProductGroupType = 'Mega'