/*Loan Crossed Relationship Tab*/

SELECT
	[Loan Identifier] = L.LoanIdentifier,
	[Loan Number] = L.FannieMaeLoanNumber,
	[Crossed Loan Number] = iif(LCR.FannieMaeLoanCrossedNumber IS NULL, '', LCR.FannieMaeLoanCrossedNumber),
	[Loan Cross Relationship Type] = iif(LCR.LoanCrossedRelationshipType IS NULL, '', LCR.LoanCrossedRelationshipType)

FROM FmData.Loan L
	LEFT JOIN FmData.LoanCrossedRelationship LCR ON LCR.LoanId = L.Id
