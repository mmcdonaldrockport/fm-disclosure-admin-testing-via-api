/*Pool Footnotes*/
WITH PoolFootnotes AS 

	(SELECT 
		[Ranking] = 
			CASE
				WHEN TF.RecordId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY TF.RecordId ORDER BY TF.RecordId)
			END,
		[MBSPoolIdentifier] = CP.MBSPoolIdentifier,
		[SecurityCUSIPIdentifier] = S.SecurityCUSIPIdentifier,
		[ReportLabel] = iif(FM.ReportLabel IS NULL, '', FM.ReportLabel),
		[Content] = iif(TF.Content IS NULL, '', TF.Content),
		[RecordType] = TF.RecordType
	FROM FmData.CollateralPool CP
		LEFT JOIN adm.TransactionFootnotes TF ON TF.RecordId = CP.Id
		LEFT JOIN adm.FilteredFieldsMetaData FM ON FM.Id = TF.FieldId
		LEFT JOIN FmData.Security S ON S.Id = CP.Id)


SELECT 

	[Pool Number] = P.MBSPoolIdentifier,
	[MBS Issuance CUSIP] = P.SecurityCUSIPIdentifier,
	[Field Name] = P.ReportLabel,
	[Comment] = P.Content

FROM PoolFootnotes P

WHERE P.RecordType = 1 OR P.RecordType IS NULL
	AND P.Ranking IN (0,1)