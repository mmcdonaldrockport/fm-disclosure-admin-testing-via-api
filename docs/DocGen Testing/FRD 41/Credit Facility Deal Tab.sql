Use DisclosureAdmin;

WITH DealMonitoringCTE AS (
  	SELECT DISTINCT
		[Ranking] = 
			iif(DM.DealId IS NULL, 0, ROW_NUMBER() OVER(PARTITION BY DM.DealId ORDER BY DM.DealActivityDatetime DESC)),
		D.Id,
		D.AgreementIdentifier,
		D.DealIdentifier,
		D.Name,
		D.DealAdditionalLoanAllowedIndicator,
		[DealMaximumBorrowingAmount] = CAST(FORMAT(D.DealMaximumBorrowingAmount, '#,##0.00') AS varchar(max)),
		[DealMaximumPermittedLoanToValuePercent] = CAST(FORMAT(D.DealMaximumPermittedLoanToValuePercent, '0.0#') AS varchar(max)),
		[DealFixedRateMinimumPermittedDebtServiceCoverageRatioFactor] = CAST(FORMAT(D.DealFixedRateMinimumPermittedDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor] = CAST(FORMAT(D.DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		D.DealAdditionalCollateralAllowedIndicator,
		D.DealCollateralReleaseAllowedIndicator,
		D.DealCollateralSubstitutionAllowedIndicator,
		DM.DealId,
		DM.DealActivityType,
		DM.DealActivityDatetime,
		DM.DealMonitoringFiscalYearNumber,
		DM.DealMonitoringFiscalPeriodType,
		DM.DealMonitoringFinancialFormType,
		[DealMonitoringEconomicOccupancyPercent] = CAST(FORMAT(DM.DealMonitoringEconomicOccupancyPercent, '0.0#') AS varchar(max)),
		[DealMonitoringLoanToValueFactor] = CAST(FORMAT(DM.DealMonitoringLoanToValueFactor, '0.0#') AS varchar(max)),
		[DealMonitoringTrailingThreeDebtServiceCoverageRatioFactor] = CAST(FORMAT(DM.DealMonitoringTrailingThreeDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[DealMonitoringTrailingThreeNCFAmount] = CAST(FORMAT(DM.DealMonitoringTrailingThreeNCFAmount, '#,##0.00') AS varchar(max)),
		[DealMonitoringTrailingTwelveDebtServiceCoverageRatioFactor] = CAST(FORMAT(DM.DealMonitoringTrailingTwelveDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[DealMonitoringTrailingTwelveNCFAmount] = CAST(FORMAT(DM.DealMonitoringTrailingTwelveNCFAmount, '#,##0.00') AS varchar(max))
	FROM FmData.Deal D
		LEFT JOIN FmData.DealMonitoring DM ON DM.DealId = D.Id)

, DealStructuredFacilityAtIssuance AS 

	(SELECT 
		CP.Id,
		[DealAggregateCutOffDateBalanceAmount] = CAST(FORMAT(DFSA.DealAggregateCutOffDateBalanceAmount, '#,##0.00') AS varchar(max)),
		[DealLoantoValueFactor] = CAST(FORMAT(DFSA.DealLoantoValueFactor, '0.0#') AS varchar(max)),
		[DealNumberofBorrowingsOutstandingCount] = CAST(DFSA.DealNumberofBorrowingsOutstandingCount AS varchar(max)),
		[DealPropertyCount] = CAST(DFSA.DealPropertyCount AS varchar(max)),
		[DealUnderwrittenNCFDebtServiceCoverageRatioFactor] = CAST(FORMAT(DFSA.DealUnderwrittenNCFDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor] = CAST(FORMAT(DFSA.DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor, '0.0#') AS varchar(max)),
		[DealCollateralAggregateValueAmount] = CAST(FORMAT(DFSA.DealCollateralAggregateValueAmount, '#,##0.00') AS varchar(max)),
		[DealPropertyAggregateValueAmount] = CAST(FORMAT(DFSA.DealPropertyAggregateValueAmount, '#,##0.00') AS varchar(max))
	FROM FmData.CollateralPool CP 
		LEFT JOIN FmData.DealStructuredFacilityAtIssuance DFSA ON DFSA.CollateralPoolId = CP.Id)

, MBSPools AS 
	(SELECT DISTINCT 
		[MBSPoolIdentifier] = CP.MBSPoolIdentifier,
		[DealIdentifier] = D.DealIdentifier, 
		[DealId] = D.Id
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.Deal D ON D.Id = L.DealId)

SELECT DISTINCT
	[Deal Identifier] = DM.DealIdentifier,
	[Deal Name] = DM.Name,
	[Pool Number] = --Concatenate values using the Stuff/ xml method
		REPLACE(
			STUFF(
				(SELECT ',' + MBSPoolIdentifier
					FROM MBSPools AS T2
					WHERE T2.DealIdentifier = T1.DealIdentifier 
					ORDER BY MBSPoolIdentifier
					FOR XML PATH (''), TYPE
				).value('.', 'varchar(max)'),1,1,''),
		 ',', ' / '),
	[Additional Loans Allowed (Y/N)] = iif(DM.DealAdditionalLoanAllowedIndicator IS NULL, '', DM.DealAdditionalLoanAllowedIndicator),
	[Property Additions Allowed (Y/N)] = iif(DM.DealAdditionalCollateralAllowedIndicator IS NULL, '', DM.DealAdditionalCollateralAllowedIndicator),
	[Total Facility Issuance UPB] = iif(DFSA.DealAggregateCutOffDateBalanceAmount IS NULL, '', DFSA.DealAggregateCutOffDateBalanceAmount),
	[Property Releases Allowed (Y/N)] = iif(DM.DealCollateralReleaseAllowedIndicator IS NULL, '', DM.DealCollateralReleaseAllowedIndicator),
	[Property Substitution Allowed (Y/N)] = iif(DM.DealCollateralSubstitutionAllowedIndicator IS NULL, '', DM.DealCollateralSubstitutionAllowedIndicator),
	[Minimum Permitted DSCR of Credit Facility - Fixed Rate Loans] = iif(DM.DealFixedRateMinimumPermittedDebtServiceCoverageRatioFactor IS NULL, '', DM.DealFixedRateMinimumPermittedDebtServiceCoverageRatioFactor),
	[LTV at the Facility Level] = iif(DFSA.DealLoantoValueFactor IS NULL, '', DFSA.DealLoantoValueFactor),
	[Maximum Borrowing Amount at the Facility Level] = iif(DM.DealMaximumBorrowingAmount IS NULL, '', DM.DealMaximumBorrowingAmount),
	[Maximum Permitted LTV of Credit Facility] = iif(DM.DealMaximumPermittedLoanToValuePercent IS NULL, '', DM.DealMaximumPermittedLoanToValuePercent),
	[Total Borrowings Outstanding at the Facility Level] = iif(DFSA.DealNumberofBorrowingsOutstandingCount IS NULL, '', DFSA.DealNumberofBorrowingsOutstandingCount),
	[Number of Mortgaged Properties at the Facility Level] = iif(DFSA.DealPropertyCount IS NULL, '', DFSA.DealPropertyCount),
	[UW NCF DSCR at the Facility Level] = iif(DFSA.DealUnderwrittenNCFDebtServiceCoverageRatioFactor IS NULL, '', DFSA.DealUnderwrittenNCFDebtServiceCoverageRatioFactor),
	[MCFA UW NCF DSCR IO at Facility Level] = iif(DFSA.DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor IS NULL, '', DFSA.DealUnderwrittenNCFDebtServicecoverageRatioInterestOnlyFactor),
	[Minimum Permitted DSCR of Credit Facility - ARM Loans] = iif(DM.DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor IS NULL, '', DM.DealVariableRateMinimumPermittedDebtServiceCoverageRatioFactor),
	[Total Collateral Value at the Facility Level] = iif(DFSA.DealCollateralAggregateValueAmount IS NULL, '', DFSA.DealCollateralAggregateValueAmount),
	[Aggregate Property Value at the Facility Level] = iif(DFSA.DealPropertyAggregateValueAmount IS NULL, '', DFSA.DealPropertyAggregateValueAmount),
	[Deal Monitoring Fiscal Period Type] = iif(DM.DealMonitoringFiscalPeriodType IS NULL, '', DM.DealMonitoringFiscalPeriodType),
	[Deal Monitoring Financial Form Type Code] = iif(DM.DealMonitoringFinancialFormType IS NULL, '', DM.DealMonitoringFinancialFormType),
	[Deal Monitoring Financial Property Statement Fiscal Year] = iif(DM.DealMonitoringFiscalYearNumber IS NULL, '', DM.DealMonitoringFiscalYearNumber),
	[Deal LTV (Approved Value)] = iif(DM.DealMonitoringLoanToValueFactor IS NULL, '', DM.DealMonitoringLoanToValueFactor),
	[Deal NCF (Trailing 3)] = iif(DM.DealMonitoringTrailingThreeNCFAmount IS NULL, '', DM.DealMonitoringTrailingThreeNCFAmount),
	[Deal DSCR (Trailing 3)] = iif(DM.DealMonitoringTrailingThreeDebtServiceCoverageRatioFactor IS NULL, '', DM.DealMonitoringTrailingThreeDebtServiceCoverageRatioFactor),
	[Deal NCF (Trailing 12)] = iif(DM.DealMonitoringTrailingTwelveNCFAmount IS NULL, '', DM.DealMonitoringTrailingTwelveNCFAmount),
	[Deal DSCR (Trailing 12)] = iif(DM.DealMonitoringTrailingTwelveDebtServiceCoverageRatioFactor IS NULL, '', DM.DealMonitoringTrailingTwelveDebtServiceCoverageRatioFactor),
	[Deal Economic Occupancy (%)] = iif(DM.DealMonitoringEconomicOccupancyPercent IS NULL, '', DM.DealMonitoringEconomicOccupancyPercent)

FROM DealMonitoringCTE DM
	LEFT JOIN FmData.Loan L ON L.DealId = DM.Id
	LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
	LEFT JOIN MBSPools T1 ON T1.DealId = DM.Id
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
	LEFT JOIN DealStructuredFacilityAtIssuance DFSA ON DFSA.Id = CP.Id

WHERE 
	LA.LoanDUSDiscloseDerivedProductType = 'Credit Facility'
	AND (DM.Ranking = 0 OR DM.Ranking = 1)
	AND T1.MBSPoolIdentifier = '34570121088001'
 