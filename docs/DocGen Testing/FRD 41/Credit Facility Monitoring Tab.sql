WITH PropertyCashFlowActivityCTE AS 
	
	(SELECT 
		[Ranking] = iif(PCFA.PropertyId IS NULL, 0, ROW_NUMBER() OVER(PARTITION BY PCFA.PropertyId ORDER BY PCFA.PropertyActivityDatetime DESC) ), 
		P.Id,
		[PropertyNCFAmount] = CAST(FORMAT(PCFA.PropertyNCFAmount, '#,##0.00') AS varchar(max)),
		[PropertyNCFAsOfDate] = CAST(FORMAT(PCFA.PropertyNCFAsOfDate, 'M/d/yyyy') AS varchar(max)) 
	FROM FmData.Property P
		LEFT JOIN FMData.PropertyCashFlowActivity PCFA ON PCFA.PropertyId = P.Id)

, PropertyValuationCTE AS 
	
	(SELECT
		[Ranking] = iif(PV.PropertyId IS NULL, 0, ROW_NUMBER() OVER(PARTITION BY PV.PropertyId ORDER BY PV.PropertyActivityDatetime DESC) ), 
		P.Id,
		[PropertyValuationAmount] = CAST(FORMAT(PV.PropertyValuationAmount, '#,##0.00') AS varchar(max)),
		[PropertyValuationAsOfDate] = CAST(FORMAT(PV.PropertyValuationAsOfDate, 'M/d/yyyy') AS varchar(max))
	FROM FmData.Property P 
		LEFT JOIN FMData.PropertyValuation PV ON PV.PropertyId = P.Id)

SELECT 
	[Deal Identifier] = D.DealIdentifier,
	[Deal Name] = D.Name,
	[Pool Number] = CP.MBSPoolIdentifier, 
	[MBS Issuance CUSIP] = S.SecurityCUSIPIdentifier,
	[Loan Identifier] = L.LoanIdentifier,
	[Property ID] = P.PropertyIdentifier,
	[Property Linkage Identifier] = iif(P.PropertyLinkageIdentifier IS NULL, '', P.PropertyLinkageIdentifier),
	[Property Net Cash Flow At Securitization Amount] = iif(PCFA_CTE.PropertyNCFAmount IS NULL, '', PCFA_CTE.PropertyNCFAmount),
	[Property Net Cash Flow At Securitization As Of Date] = iif(PCFA_CTE.PropertyNCFAsOfDate IS NULL, '', PCFA_CTE.PropertyNCFAsOfDate),
	[Property Valuation At Securitization Amount] = iif(PV_CTE.PropertyValuationAmount IS NULL, '', PV_CTE.PropertyValuationAmount),
	[Property Valuation At Securitization As Of Date] = iif(PV_CTE.PropertyValuationAsOfDate IS NULL, '', PV_CTE.PropertyValuationAsOfDate)
	
FROM FmData.Property P
	LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = P.Id
	LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
	LEFT JOIN FmData.Security S ON S.Id = CP.Id
	LEFT JOIN FmData.Deal D ON D.Id = L.DealId
	LEFT JOIN PropertyCashFlowActivityCTE PCFA_CTE ON PCFA_CTE.Id = P.Id 
	LEFT JOIN PropertyValuationCTE PV_CTE ON PV_CTE.Id = P.Id
	
WHERE PCFA_CTE.Ranking IN (0,1)
	AND PV_CTE.Ranking IN (0,1)

ORDER BY P.PropertyIdentifier
	
	
	
	
	 