/*Loan & Collateral Footnotes*/

WITH LoanFootnotes AS 

	(SELECT
		[Ranking] = 
			CASE
				WHEN TF.RecordId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY TF.RecordId ORDER BY TF.RecordId)
			END,
		[MBSPoolIdentifier] = CP.MBSPoolIdentifier,
		[LoanIdentifier] = L.LoanIdentifier,
		[FannieMaeLoanNumber] = L.FannieMaeLoanNumber,
		[ReportLabel] = iif(FM.ReportLabel IS NULL, '', FM.ReportLabel),
		[Content] = iif(TF.Content IS NULL, '', TF.Content),
		[RecordType] = TF.RecordType
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN adm.TransactionFootnotes TF ON TF.RecordId = L.Id
		LEFT JOIN adm.FilteredFieldsMetaData FM ON FM.Id = TF.FieldId
		LEFT JOIN FmData.Security S ON S.Id = CP.Id)

SELECT 

	[Pool Number] = L.MBSPoolIdentifier,
	[Loan Identifier] = L.LoanIdentifier,
	[Loan Number] = L.FannieMaeLoanNumber,
	[Field Name] = L.ReportLabel,
	[Comment] = L.Content

FROM LoanFootnotes L

WHERE L.RecordType = 3 OR L.RecordType IS NULL
	AND L.Ranking IN (0,1)