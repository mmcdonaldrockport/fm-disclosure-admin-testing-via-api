/*Additional Debt Tab*/

WITH LoanAdditionalDebt AS 

	(SELECT 
		LAD.Id,
		LAD.LoanId,
		LAD.LoanAdditionalDebtType,
		LAD.LoanAdditionalDebtRelationshipType,
		[MezzanineBalance] = CAST(FORMAT(LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount, '#,##0.00') AS varchar(max)),
		LAD.LoanAdditionalDebtFannieMaePoolNumber,
		LAD.LoanAdditionalDebtFannieMaeLoanNumber,
		LAD.LoanAdditionalDebtLienPriorityType,
		[LoanAdditionalDebtUnpaidPrincipalBalanceAmount] = CAST(FORMAT(LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanAdditionalDebtMonthlyPaymentAmount] = CAST(FORMAT(LAD.LoanAdditionalDebtMonthlyPaymentAmount, '#,##0.00') AS varchar(max)),
		LAD.LoanAdditionalDebtLienHolderName,
		[LoanAdditionalDebtCurrentInterestRate] = CAST(FORMAT(LAD.LoanAdditionalDebtCurrentInterestRate, '0.000##') AS varchar(max)),
		[LoanAdditionalDebtMaximumMonthlyPaymentAmount] = CAST(FORMAT(LAD.LoanAdditionalDebtMaximumMonthlyPaymentAmount, '#,##0.00') AS varchar(max)),
		[LoanAdditionalDebtAdjustableRateMortgageMarginPercent] = CAST(FORMAT(LAD.LoanAdditionalDebtAdjustableRateMortgageMarginPercent, '0.000##') AS varchar(max)),
		[LoanAdditionalDebtMinimumInterestRatePercent] = CAST(FORMAT(LAD.LoanAdditionalDebtMinimumInterestRatePercent, '0.000##') AS varchar(max)),
		[LoanAdditionalDebtMaturityDate] = CAST(FORMAT(LAD.LoanAdditionalDebtMaturityDate, 'M/d/yyyy') AS varchar(max)),
		LAD.LoanAdditionalDebtBalloonIndicator,
		[LoanAdditionalDebtLastInterestOnlyEndDate] = CAST(FORMAT(LAD.LoanAdditionalDebtLastInterestOnlyEndDate, 'M/d/yyyy') AS varchar(max)),
		[LoanAdditionalDebtAmortizationTerm] = CAST(LAD.LoanAdditionalDebtAmortizationTerm AS varchar(max)),
		[MezzanineLoanAmortizationTerm] = CAST(LAD.LoanAdditionalDebtAmortizationTerm AS varchar(max)),
		[LoanMezzanineInterestOnlyEndDate] = CAST(FORMAT(LAD.LoanMezzanineInterestOnlyEndDate, 'M/d/yyyy') AS varchar(max)),
		[MezzanineLoanMonthlyPayment] = CAST(FORMAT(LAD.LoanAdditionalDebtMonthlyPaymentAmount, '#,##0.00') AS varchar(max)),
		[LoanAdditionalDebtLineofCreditFullAmount] = CAST(FORMAT(LAD.LoanAdditionalDebtLineofCreditFullAmount, '0.000##') AS varchar(max)),
		LAD.LoanMezzanineFinancingType,
		[LoanMezzanineFirstPaymentDueDate] = CAST(FORMAT(LAD.LoanMezzanineFirstPaymentDueDate, 'M/d/yyyy') AS varchar(max)),
		[LoanMezzanineInitialTermInterestRatePercent] = CAST(FORMAT(LAD.LoanMezzanineInitialTermInterestRatePercent, '0.000##') AS varchar(max)),
		[LoanMezzanineInitialTermMaturityDate] = CAST(FORMAT(LAD.LoanMezzanineInitialTermMaturityDate, 'M/d/yyyy') AS varchar(max)),
		LAD.LoanMezzanineInterestAccrualMethodType,
		LAD.LoanMezzanineProviderName
	FROM FmData.LoanAdditionalDebt LAD)

, LoanMezzaninePrepaymentProtectionTerm AS
	(SELECT
		LM.LoanAdditionalDebtId,
		LM.LoanMezzanineDecliningPrepaymentPremiumType,
		[LoanMezzaninePrepaymentProtectionEndDate] = CAST(FORMAT(LM.LoanMezzaninePrepaymentProtectionEndDate, 'M/d/yyyy') AS varchar(max)),
		LM.LoanMezzanineOtherPrepaymentDescription,
		[LoanMezzaninePrepaymentProtectionTerm] = CAST(LM.LoanMezzaninePrepaymentProtectionTerm AS varchar(max)),
		LM.LoanMezzaninePrepaymentProtectionType
	FROM FmData.LoanMezzaninePrepaymentProtectionTerm LM
	)

SELECT
	[Loan Identifier] = L.LoanIdentifier,
	[Loan Number] = L.FannieMaeLoanNumber,
	[Additional Debt Type] = iif(LAD.LoanAdditionalDebtType IS NULL, '', LAD.LoanAdditionalDebtType),
	[Loan Additional Debt Relationship Type] = iif(LAD.LoanAdditionalDebtRelationshipType IS NULL, '', LAD.LoanAdditionalDebtRelationshipType),
	[Mezzanine Balance] = iif(LAD.MezzanineBalance IS NULL, '', LAD.MezzanineBalance),
	[Loan Additional Debt Fannie Mae Pool Number] = iif(LAD.LoanAdditionalDebtFannieMaePoolNumber IS NULL, '', LAD.LoanAdditionalDebtFannieMaePoolNumber),
	[Loan Additional Debt Fannie Mae Loan Number] = iif(LAD.LoanAdditionalDebtFannieMaeLoanNumber IS NULL, '', LAD.LoanAdditionalDebtFannieMaeLoanNumber),
	[Additional Debt Lien Priority] = iif(LAD.LoanAdditionalDebtLienPriorityType IS NULL, '', LAD.LoanAdditionalDebtLienPriorityType),
	[Additional Debt UPB] = iif(LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount IS NULL, '', LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount),
	[Additional Debt Monthly Payment Amount] = iif(LAD.LoanAdditionalDebtMonthlyPaymentAmount IS NULL, '', LAD.LoanAdditionalDebtMonthlyPaymentAmount),
	[Additional Debt Lien Holder] = iif(LAD.LoanAdditionalDebtLienHolderName IS NULL, '', LAD.LoanAdditionalDebtLienHolderName),
	[Additional Debt Current Interest Rate (%)] = iif(LAD.LoanAdditionalDebtCurrentInterestRate IS NULL, '', LAD.LoanAdditionalDebtCurrentInterestRate),
	[Additional Debt Maximum Monthly Payment] = iif(LAD.LoanAdditionalDebtMaximumMonthlyPaymentAmount IS NULL, '', LAD.LoanAdditionalDebtMaximumMonthlyPaymentAmount),
	[Additional Debt ARM Margin] = iif(LAD.LoanAdditionalDebtAdjustableRateMortgageMarginPercent IS NULL, '', LAD.LoanAdditionalDebtAdjustableRateMortgageMarginPercent),
	[Additional Debt Minimum Interest Rate (%)] = iif(LAD.LoanAdditionalDebtMinimumInterestRatePercent IS NULL, '', LAD.LoanAdditionalDebtMinimumInterestRatePercent),
	[Additional Debt Maturity Date] = iif(LAD.LoanAdditionalDebtMaturityDate IS NULL, '', LAD.LoanAdditionalDebtMaturityDate),
	[Additional Debt Balloon (Y/N)] = iif(LAD.LoanAdditionalDebtBalloonIndicator IS NULL, '', LAD.LoanAdditionalDebtBalloonIndicator),
	[Additional Debt Last Interest Only End Date] = iif(LAD.LoanAdditionalDebtLastInterestOnlyEndDate IS NULL, '', LAD.LoanAdditionalDebtLastInterestOnlyEndDate),
	[Additional Debt Original Amortization Term] = iif(LAD.LoanAdditionalDebtAmortizationTerm IS NULL, '', LAD.LoanAdditionalDebtAmortizationTerm),
	[Mezzanine Loan Amortization Term] = iif(LAD.MezzanineLoanAmortizationTerm IS NULL, '', LAD.MezzanineLoanAmortizationTerm),
	[Mezzanine Loan Interest Only End Date] = iif(LAD.LoanMezzanineInterestOnlyEndDate IS NULL, '', LAD.LoanMezzanineInterestOnlyEndDate),
	[Mezzanine Loan Monthly Payment] = iif(LAD.MezzanineLoanMonthlyPayment IS NULL, '', LAD.MezzanineLoanMonthlyPayment),
	[Additional Debt Line of Credit Full Amount] = iif(LAD.LoanAdditionalDebtLineofCreditFullAmount IS NULL, '', LAD.LoanAdditionalDebtLineofCreditFullAmount),
	[Loan Additional Debt Mezzanine Financing Type] = iif(LAD.LoanMezzanineFinancingType IS NULL, '', LAD.LoanMezzanineFinancingType),
	[Mezzanine Loan Declining Prepayment Premium Type] = iif(LM.LoanMezzanineDecliningPrepaymentPremiumType IS NULL, '', LM.LoanMezzanineDecliningPrepaymentPremiumType),
	[Mezzanine Loan First Monthly Payment Due Date] = iif(LAD.LoanMezzanineFirstPaymentDueDate IS NULL, '', LAD.LoanMezzanineFirstPaymentDueDate),
	[Mezzanine Loan Prepayment Provision End Date] = iif(LM.LoanMezzaninePrepaymentProtectionEndDate IS NULL, '', LM.LoanMezzaninePrepaymentProtectionEndDate),
	[Mezzanine Loan Initial Term Interest Rate (%)] = iif(LAD.LoanMezzanineInitialTermInterestRatePercent IS NULL, '', LAD.LoanMezzanineInitialTermInterestRatePercent),
	[Mezzanine Loan Initial Term Maturity Date] = iif(LAD.LoanMezzanineInitialTermMaturityDate IS NULL, '', LAD.LoanMezzanineInitialTermMaturityDate),
	[Mezzanine Loan Interest Accrual Method] = iif(LAD.LoanMezzanineInterestAccrualMethodType IS NULL, '', LAD.LoanMezzanineInterestAccrualMethodType),
	[Mezzanine Loan Other Prepayment Description] = iif(LM.LoanMezzanineOtherPrepaymentDescription IS NULL, '', LM.LoanMezzanineOtherPrepaymentDescription),
	[Mezzanine Loan Prepayment Provision Term] = iif(LM.LoanMezzaninePrepaymentProtectionTerm IS NULL, '', LM.LoanMezzaninePrepaymentProtectionTerm),
	[Mezzanine Loan Prepayment Provision] = iif(LM.LoanMezzaninePrepaymentProtectionType IS NULL, '', LM.LoanMezzaninePrepaymentProtectionType),
	[Mezzanine Loan Provider] = iif(LAD.LoanMezzanineProviderName IS NULL, '', LAD.LoanMezzanineProviderName)

FROM FmData.Loan L
	LEFT JOIN LoanAdditionalDebt LAD ON LAD.LoanId = L.Id
	LEFT JOIN LoanMezzaninePrepaymentProtectionTerm LM ON LM.LoanAdditionalDebtId = LAD.Id

WHERE L.FannieMaeLoanNumber IS NOT NULL --report does not get generated for certified transactions
