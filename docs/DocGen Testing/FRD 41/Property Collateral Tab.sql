/*Property Collateral Tab*/
WITH Loan AS 

	(SELECT
		L.Id,
		[LoanIdentifier] = L.LoanIdentifier,
		[FannieMaeLoanNumber] = L.FannieMaeLoanNumber
	FROM FmData.Loan L)

, Collateral AS

	(SELECT
		C.Id,
		[CollateralReferenceNumber] = CAST(C.CollateralReferenceNumber AS varchar(max)),
		[CollateralIdentifier] = C.CollateralIdentifier
	FROM FmData.Collateral C)

, LoanCollateral AS

	(SELECT 
		LC.CollateralId, 
		LC.LoanId, 
		[CollateralPropertySubstitutedIndicator] = CAST(LC.CollateralPropertySubstitutedIndicator AS varchar(max)) FROM FmData.LoanCollateral LC)

, PropertyAcquisition AS

	(SELECT 
		PA.ID,
		[PropertyAgeRestrictedIndicator] = CAST(PA.PropertyAgeRestrictedIndicator AS varchar(max)),
		[PropertyBuiltYearNumber] = CAST(PA.PropertyBuiltYearNumber AS varchar(max)),
		[PropertyGreenBuildingCertificationType] = CAST(PA.PropertyGreenBuildingCertificationType AS varchar(max)),
		[PropertyLandOwnershipRightType] = CAST(PA.PropertyLandOwnershipRightType AS varchar(max)),
		[PropertyTaxEscrowedIndicator] = CAST(PA.PropertyTaxEscrowedIndicator AS varchar(max)),
		[PropertyTotalUnitCount] = CAST(PA.PropertyTotalUnitCount AS varchar(max)),
		[MultifamilyAffordableHousingProgramType] = CAST(PA.MultifamilyAffordableHousingProgramType AS varchar(max)),
		[PropertyHousingAssistanceProgramRemainingTerm] = CAST(PA.PropertyHousingAssistanceProgramRemainingTerm AS varchar(max)),
		[PropertyUnitIncomeorRentRestrictionPercent] = CAST(FORMAT(PA.PropertyUnitIncomeorRentRestrictionPercent, '0.0#') AS varchar(max))
	FROM FmData.PropertyAcquisition PA)

, PropertyAddress AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PAD.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PAD.PropertyId ORDER BY PAD.LastUpdateDateTime ASC)
			END,
		PAD.PropertyId,
		[PropertyPrimaryAddressIndicator] = CAST(PAD.PropertyPrimaryAddressIndicator AS varchar(max)),
		[PropertyStreetAddressText] = CAST(PAD.PropertyStreetAddressText AS varchar(max)),
		[PropertyAddressCityName] = CAST(PAD.PropertyAddressCityName AS varchar(max)),
		[PropertyAddressStateCode] = CAST(PAD.PropertyAddressStateCode AS varchar(max)),
		[PropertyAddressPostalCode] = CAST(PAD.PropertyAddressPostalCode AS varchar(max)),
		[PropertyAddressCountyName] = CAST(PAD.PropertyAddressCountyName AS varchar(max)),
		[GeographicMetropolitanStatisticalAreaName] = CAST(PAD.GeographicMetropolitanStatisticalAreaName AS varchar(max)),
		[PropertyAddressLatitudeNumber] = CAST(FORMAT(PAD.PropertyAddressLatitudeNumber, '0.000000') AS varchar(max)),
		[PropertyAddressLongitudeNumber] = CAST(FORMAT(PAD.PropertyAddressLongitudeNumber, '0.000000') AS varchar(max))
	FROM FmData.PropertyAddress PAD)

, PropertyPropertyAddress AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PPAD.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PPAD.PropertyId ORDER BY PPAD.LastUpdateDateTime DESC)
			END,
		PPAD.PropertyId,
		[PropertyPrimaryAddressIndicator] = CAST(PPAD.PropertyPrimaryAddressIndicator AS varchar(max)),
		[PropertyStreetAddressText] = CAST(PPAD.PropertyStreetAddressText AS varchar(max)),
		[PropertyAddressCityName] = CAST(PPAD.PropertyAddressCityName AS varchar(max)),
		[PropertyAddressStateCode] = CAST(PPAD.PropertyAddressStateCode AS varchar(max)),
		[PropertyAddressPostalCode] = CAST(PPAD.PropertyAddressPostalCode AS varchar(max)),
		[PropertyAddressCountyName] = CAST(PPAD.PropertyAddressCountyName AS varchar(max)),
		[GeographicMetropolitanStatisticalAreaName] = CAST(PPAD.GeographicMetropolitanStatisticalAreaName AS varchar(max)),
		[PropertyAddressLatitudeNumber] = CAST(FORMAT(PPAD.PropertyAddressLatitudeNumber, '0.000000') AS varchar(max)),
		[PropertyAddressLongitudeNumber] = CAST(FORMAT(PPAD.PropertyAddressLongitudeNumber, '0.000000') AS varchar(max))
	FROM FmData.PropertyAddress PPAD)

, PropertyAffordableUnit AS --Sub queries?

	(SELECT 
		PAF.PropertyId,
		PAF.PropertyAffordableUnitAMIType,
		[PropertyAffordableUnitAMIPercent50] = (SELECT PropertyAffordableUnitAMIPercent FROM FmData.PropertyAffordableUnit WHERE PropertyId = P.Id and PropertyAffordableUnitAMIType = 'At or Below 50% AMI'),
		[PropertyAffordableUnitAMIPercent60] = (SELECT PropertyAffordableUnitAMIPercent FROM FmData.PropertyAffordableUnit WHERE PropertyId = P.Id and PropertyAffordableUnitAMIType = 'At or Below 60% AMI'),
		[PropertyAffordableUnitAMIPercent80] = (SELECT PropertyAffordableUnitAMIPercent FROM FmData.PropertyAffordableUnit WHERE PropertyId = P.Id and PropertyAffordableUnitAMIType = 'At or Below 80% AMI')
	FROM FmData.Property P
		LEFT JOIN FmData.PropertyAffordableUnit PAF ON PAF.PropertyId = P.Id)

, PropertyHousing AS

	(SELECT 
		[Ranking] = 
			CASE
				WHEN PAH.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PAH.PropertyId ORDER BY PAH.LastUpdateDateTime DESC)
			END,
		PAH.PropertyId, 
		[PropertyHousingType] = CAST(PAH.PropertyHousingType AS varchar(max)) 
	FROM FmData.PropertyHousing PAH)

, PropertyEnergy AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PE.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PE.PropertyId ORDER BY PE.PropertyActivityDatetime DESC)
			END,
		PE.PropertyId,
		[PropertyEnergyStarScoreDate] = CAST(FORMAT(PE.PropertyEnergyStarScoreDate, 'm/yyyy') AS varchar(max)),
		[PropertyEnergyStarScoreNumber] = CAST(PE.PropertyEnergyStarScoreNumber AS varchar(max)),
		[PropertySourceEnergyUseIntensityDate] = CAST(FORMAT(PE.PropertySourceEnergyUseIntensityDate, 'm/yyyy') AS varchar(max)),
		[PropertySourceEnergyUseIntensityRate] = CAST(PE.PropertySourceEnergyUseIntensityRate AS varchar(max))
	FROM FmData.PropertyEnergy PE
)

, PropertyFinancialExpenseActivity AS

	(SELECT 
		
		PFE.PropertyFinancialStatementId,
		[PropertyGroundRentExpenseAmount] = CAST(FORMAT(PFE.PropertyGroundRentExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyHousekeepingExpenseAmount] = CAST(FORMAT(PFE.PropertyHousekeepingExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyProfessionalFeesExpenseAmount] = CAST(FORMAT(PFE.PropertyProfessionalFeesExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyGeneralandAdministrativeExpenseAmount] = CAST(FORMAT(PFE.PropertyGeneralandAdministrativeExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyInsuranceExpenseAmount] = CAST(FORMAT(PFE.PropertyInsuranceExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyManagementFeesExpenseAmount] = CAST(FORMAT(PFE.PropertyManagementFeesExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyAdvertisingandMarketingExpenseAmount] = CAST(FORMAT(PFE.PropertyAdvertisingandMarketingExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyMealExpenseAmount] = CAST(FORMAT(PFE.PropertyMealExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyOtherExpenseAmount] = CAST(FORMAT(PFE.PropertyOtherExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyPayrollandBenefitsExpenseAmount] = CAST(FORMAT(PFE.PropertyPayrollandBenefitsExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyRealEstateTaxesExpenseAmount] = CAST(FORMAT(PFE.PropertyRealEstateTaxesExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyRepairsandMaintenanceExpenseAmount] = CAST(FORMAT(PFE.PropertyRepairsandMaintenanceExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyUtilitiesExpenseAmount] = CAST(FORMAT(PFE.PropertyUtilitiesExpenseAmount, '#,##0.00') AS varchar(max)),
		[PropertyWaterandSewerExpenseAmount] = CAST(FORMAT(PFE.PropertyWaterandSewerExpenseAmount, '#,##0.00') AS varchar(max))
	FROM FmData.PropertyFinancialExpenseActivity PFE)

, PropertyFinancialIncomeActivity AS

	(SELECT
		PFI.PropertyFinancialStatementId, 
		[PropertyGrossPotentialRentAmount] = CAST(FORMAT(PFI.PropertyGrossPotentialRentAmount, '#,##0.00') AS varchar(max)),
		[PropertyCommercialIncomeAmount] = CAST(FORMAT(PFI.PropertyCommercialIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyIncomeLessBadDebtAmount] = CAST(FORMAT(PFI.PropertyIncomeLessBadDebtAmount, '#,##0.00') AS varchar(max)),
		[PropertyLessConcessionRevenueAmount] = CAST(FORMAT(PFI.PropertyLessConcessionRevenueAmount, '#,##0.00') AS varchar(max)),
		[PropertyLaundryandVendingIncomeAmount] = CAST(FORMAT(PFI.PropertyLaundryandVendingIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyParkingIncomeAmount] = CAST(FORMAT(PFI.PropertyParkingIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyOtherIncomeAmount] = CAST(FORMAT(PFI.PropertyOtherIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyMealIncomeAmount] = CAST(FORMAT(PFI.PropertyMealIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyMedicareandMedicaidIncomeAmount] = CAST(FORMAT(PFI.PropertyMedicareandMedicaidIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyNursingMedicalIncomeAmount] = CAST(FORMAT(PFI.PropertyNursingMedicalIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertySecondaryResidentialIncomeAmount] = CAST(FORMAT(PFI.PropertySecondaryResidentialIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyVacancyandCollectionLossAmount] = CAST(FORMAT(PFI.PropertyVacancyandCollectionLossAmount, '#,##0.00') AS varchar(max))
	FROM FmData.PropertyFinancialIncomeActivity PFI)

, PropertyFinancialStatement AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PFS.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PFS.PropertyId ORDER BY PFS.PropertyActivityDatetime DESC)
			END,
		PFS.Id,
		PFS.PropertyId, 
		[PropertyFinancialStatementPeriodStartDate] = CAST(FORMAT(PFS.PropertyFinancialStatementPeriodStartDate, 'm/d/yyyy') AS varchar(max)),
		[PropertyFinancialStatementPeriodEndDate] = CAST(FORMAT(PFS.PropertyFinancialStatementPeriodEndDate, 'YYYYMMDD') AS varchar(max)),
		[EndingPropertyFinancialStatementFiscalYearNumber] = CAST(PFS.PropertyFinancialStatementFiscalYearNumber AS varchar(max)),
		[PropertyFiscalPeriodType] = CAST(PFS.PropertyFiscalPeriodType AS varchar(max)),
		[PropertyFinancialStatementFiscalYearNumber] = CAST(PFS.PropertyFinancialStatementFiscalYearNumber AS varchar(max))
	FROM FmData.PropertyFinancialStatement PFS)

, PropertyFinancialSummaryActivity AS

	(SELECT 
		PFSA.PropertyFinancialStatementId,
		[PropertyEffectiveGrossIncomeAmount] = CAST(FORMAT(PFSA.PropertyEffectiveGrossIncomeAmount, '#,##0.00') AS varchar(max)),
		[PropertyTotalOperatingExpensesAmount] = CAST(FORMAT(PFSA.PropertyTotalOperatingExpensesAmount, '#,##0.00') AS varchar(max)),
		[PropertyTotalOperatingCapitalExpenditureAmount] = CAST(FORMAT(PFSA.PropertyTotalOperatingCapitalExpenditureAmount, '#,##0.00') AS varchar(max)),
		[PropertyNCFAmount] = CAST(FORMAT(PFSA.PropertyNCFAmount, '#,##0.00') AS varchar(max))
	FROM FmData.PropertyFinancialSummaryActivity PFSA)

, PropertyInspection AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PI.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PI.PropertyId ORDER BY PI.PropertyActivityDatetime DESC)
			END, 
		PI.PropertyId,
		[PropertyInspectionEffectiveDate] = CAST(FORMAT(PI.PropertyInspectionEffectiveDate, 'm/d/yyyy') AS varchar(max)),
		[PropertyDeferredMaintenanceIndicator] = CAST(PI.PropertyDeferredMaintenanceIndicator AS varchar(max))
	FROM FmData.PropertyInspection PI)

, PropertyInsurance AS

	(SELECT PII.PropertyId, [PropertyInsuranceType] = CAST(PII.PropertyInsuranceType AS varchar(max)) FROM FmData.PropertyInsurance PII)

, PropertyOccupancy AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PO.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PO.PropertyId ORDER BY PO.PropertyActivityDatetime DESC)
			END, 
		PO.PropertyId,
		[PropertyEconomicOccupancyPercent] = CAST(FORMAT(PO.PropertyEconomicOccupancyPercent, '0.0#') AS varchar(max)),
		[PropertyPhysicalOccupancyAsOfDate] = CAST(FORMAT(PO.PropertyPhysicalOccupancyAsOfDate, 'm/d/yyyy') AS varchar(max)),
		[PropertyPhysicalOccupancyPercent] = CAST(FORMAT(PO.PropertyPhysicalOccupancyPercent, '0.0#') AS varchar(max))
	FROM FmData.PropertyOccupancy PO)

, PropertyState AS

	(SELECT 
		[Ranking] = 
			CASE
				WHEN PS.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PS.PropertyId ORDER BY PS.LastUpdateDateTime DESC)
			END,
		PS.PropertyId, 
		[PropertyStateType] = CAST(PS.PropertyStateType AS varchar(max)) 
	FROM FmData.PropertyState PS)

, PropertyValuation AS

	(SELECT
		[Ranking] = 
			CASE
				WHEN PV.PropertyId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY PV.PropertyId ORDER BY PV.PropertyActivityDatetime DESC)
			END, 
		PV.PropertyId,
		[PropertyValuationAmount] = CAST(FORMAT(PV.PropertyValuationAmount, '#,##0.00') AS varchar(max)),
		[PropertyValuationAsOfDate] = CAST(FORMAT(PV.PropertyValuationAsOfDate, 'm/d/yyyy') AS varchar(max))
	FROM FmData.PropertyValuation PV)

SELECT 
	[Loan Identifier] = iif(L.LoanIdentifier IS NULL, '', L.LoanIdentifier),
	[Loan Number] = iif(L.FannieMaeLoanNumber IS NULL, '', L.FannieMaeLoanNumber),
	[Property ID] = iif(P.PropertyIdentifier IS NULL, '', P.PropertyIdentifier),
	[Collateral Reference Number] = iif(C.CollateralReferenceNumber IS NULL, '', C.CollateralReferenceNumber),
	[Property Linkage Identifier] = iif(P.PropertyLinkageIdentifier IS NULL, '', P.PropertyLinkageIdentifier),
	[Collateral  Structured Facility Collateral Substituted Indicator] = iif(LC.CollateralPropertySubstitutedIndicator IS NULL, '', LC.CollateralPropertySubstitutedIndicator),
	[Property Name] = iif(P.PropertyName IS NULL, '', P.PropertyName),
	[Age Restricted (Y/N)] = iif(PA.PropertyAgeRestrictedIndicator IS NULL, '', PA.PropertyAgeRestrictedIndicator),
	[Year Built] = iif(PA.PropertyBuiltYearNumber IS NULL, '', PA.PropertyBuiltYearNumber),
	[Green Building Certification] = iif(PA.PropertyGreenBuildingCertificationType IS NULL, '', PA.PropertyGreenBuildingCertificationType),
	[Ownership Interest] = iif(PA.PropertyLandOwnershipRightType IS NULL, '', PA.PropertyLandOwnershipRightType),
	[Tax Escrow (Y/N)] = iif(PA.PropertyTaxEscrowedIndicator IS NULL, '', PA.PropertyTaxEscrowedIndicator),
	[Total Units] = iif(PA.PropertyTotalUnitCount IS NULL, '', PA.PropertyTotalUnitCount),
	[Energy Star Score Date] = iif(PE.PropertyEnergyStarScoreDate IS NULL, '', PE.PropertyEnergyStarScoreDate),
	[Energy Star Score] = iif(PE.PropertyEnergyStarScoreNumber IS NULL, '', PE.PropertyEnergyStarScoreNumber),
	[Source Energy Use Intensity Date] = iif(PE.PropertySourceEnergyUseIntensityDate IS NULL, '', PE.PropertySourceEnergyUseIntensityDate),
	[Source Energy Use Intensity] = iif(PE.PropertySourceEnergyUseIntensityRate IS NULL, '', PE.PropertySourceEnergyUseIntensityRate),
	[UW Economic Occupancy (%)] = iif(PO.PropertyEconomicOccupancyPercent IS NULL, '', PO.PropertyEconomicOccupancyPercent),
	[Physical Occupancy % As-of-Date] = iif(PO.PropertyPhysicalOccupancyAsOfDate IS NULL, '', PO.PropertyPhysicalOccupancyAsOfDate),
	[Physical Occupancy (%) ] = iif(PO.PropertyPhysicalOccupancyPercent IS NULL, '', PO.PropertyPhysicalOccupancyPercent),
	[Property Value] = iif(PV.PropertyValuationAmount IS NULL, '', PV.PropertyValuationAmount),
	[Property Value As-of-Date] = iif(PV.PropertyValuationAsOfDate IS NULL, '', PV.PropertyValuationAsOfDate),
	[Primary Property Indicator] = iif(PAD.PropertyPrimaryAddressIndicator IS NULL, '', PAD.PropertyPrimaryAddressIndicator),
	[Property Current Name] = iif(P.PropertyName IS NULL, '', P.PropertyName),
	[Property Address] = iif(PAD.PropertyStreetAddressText IS NULL, '', PAD.PropertyStreetAddressText),
	[Property Address] = iif(PAD.PropertyStreetAddressText IS NULL, '', PAD.PropertyStreetAddressText),
	[Property City] = iif(PAD.PropertyAddressCityName IS NULL, '', PAD.PropertyAddressCityName),
	[Property Property City] = iif(PAD.PropertyAddressCityName IS NULL, '', PAD.PropertyAddressCityName),
	[Property State] = iif(PAD.PropertyAddressStateCode IS NULL, '', PAD.PropertyAddressStateCode),
	[Property Property State] = iif(PAD.PropertyAddressStateCode IS NULL, '', PAD.PropertyAddressStateCode),
	[Property Zip Code] = iif(PAD.PropertyAddressPostalCode IS NULL, '', PAD.PropertyAddressPostalCode),
	[Property Property Zip Code] = iif(PAD.PropertyAddressPostalCode IS NULL, '', PAD.PropertyAddressPostalCode),
	[Property County] = iif(PAD.PropertyAddressCountyName IS NULL, '', PAD.PropertyAddressCountyName),
	[Metropolitan Statistical Area] = iif(PAD.GeographicMetropolitanStatisticalAreaName IS NULL, '', PAD.GeographicMetropolitanStatisticalAreaName),
	[Geocoded Latitude] = iif(PAD.PropertyAddressLatitudeNumber IS NULL, '', PAD.PropertyAddressLatitudeNumber),
	[Geocode Longitude] = iif(PAD.PropertyAddressLongitudeNumber IS NULL, '', PAD.PropertyAddressLongitudeNumber),
	[Affordability Restrictions Type] = iif(PA.MultifamilyAffordableHousingProgramType IS NULL, '', PA.MultifamilyAffordableHousingProgramType),
	[% of Units At or Below 50% Median Income] = iif(PAF.PropertyAffordableUnitAMIPercent50 IS NULL, '', PAF.PropertyAffordableUnitAMIPercent50),
	[% of Units At or Below 60% Median Income] = iif(PAF.PropertyAffordableUnitAMIPercent60 IS NULL, '', PAF.PropertyAffordableUnitAMIPercent60),
	[% of Units At or Below 80% Median Income] = iif(PAF.PropertyAffordableUnitAMIPercent80 IS NULL, '', PAF.PropertyAffordableUnitAMIPercent80),
	[HAP Remaining Term] = iif(PA.PropertyHousingAssistanceProgramRemainingTerm IS NULL, '', PA.PropertyHousingAssistanceProgramRemainingTerm),
	[Units with Income or Rent Restrictions (%)] = iif(PA.PropertyUnitIncomeorRentRestrictionPercent IS NULL, '', PA.PropertyUnitIncomeorRentRestrictionPercent),
	[Property Acquisition Property Type] = iif(PAH.PropertyHousingType IS NULL, '', PAH.PropertyHousingType),
	[Date of Last Inspection] = iif(PI.PropertyInspectionEffectiveDate IS NULL, '', PI.PropertyInspectionEffectiveDate),
	[Property Condition] = '',--iif( IS NULL, '', ),
	[Deferred Maintenance Flag (Y/N)] = iif(PI.PropertyDeferredMaintenanceIndicator IS NULL, '', PI.PropertyDeferredMaintenanceIndicator),
	[Collateral Identifier] = iif(C.CollateralIdentifier IS NULL, '', C.CollateralIdentifier),
	[Property Status] = iif(PS.PropertyStateType IS NULL, '', PS.PropertyStateType),
	[Current Number of Units / Beds / Rooms] = iif(P.PropertyTotalUnitCount IS NULL, '', P.PropertyTotalUnitCount),
	[Financial Property Statement Period Begin Date (Begin YYYYMMDD)] = iif(PFS.PropertyFinancialStatementPeriodStartDate IS NULL, '', PFS.PropertyFinancialStatementPeriodStartDate),
	[Ending YYYYMMDD] = iif(PFS.PropertyFinancialStatementPeriodEndDate IS NULL, '', PFS.PropertyFinancialStatementPeriodEndDate),
	[Property Financial Property Statement Fiscal Year] = iif(PFS.PropertyFinancialStatementFiscalYearNumber IS NULL, '', PFS.PropertyFinancialStatementFiscalYearNumber),
	[Property Fiscal Period Type] = iif(PFS.PropertyFiscalPeriodType IS NULL, '', PFS.PropertyFiscalPeriodType),
	[Terrorism Insurance (Y/N)] = iif(PII.PropertyInsuranceType IS NULL, '', PII.PropertyInsuranceType),
	[Property Financial Property Statement Fiscal Year] = iif(PFS.PropertyFinancialStatementFiscalYearNumber IS NULL, '', PFS.PropertyFinancialStatementFiscalYearNumber),
	[Gross Potential Rent] = iif(PFI.PropertyGrossPotentialRentAmount IS NULL, '', PFI.PropertyGrossPotentialRentAmount),
	[Commercial Income] = iif(PFI.PropertyCommercialIncomeAmount IS NULL, '', PFI.PropertyCommercialIncomeAmount),
	[Less: Bad Debt] = iif(PFI.PropertyIncomeLessBadDebtAmount IS NULL, '', PFI.PropertyIncomeLessBadDebtAmount),
	[Less: Concessions] = iif(PFI.PropertyLessConcessionRevenueAmount IS NULL, '', PFI.PropertyLessConcessionRevenueAmount),
	[Laundry / Vending Income] = iif(PFI.PropertyLaundryandVendingIncomeAmount IS NULL, '', PFI.PropertyLaundryandVendingIncomeAmount),
	[Parking Income] = iif(PFI.PropertyParkingIncomeAmount IS NULL, '', PFI.PropertyParkingIncomeAmount),
	[Other Income] = iif(PFI.PropertyOtherIncomeAmount IS NULL, '', PFI.PropertyOtherIncomeAmount),
	[Meals Income] = iif(PFI.PropertyMealIncomeAmount IS NULL, '', PFI.PropertyMealIncomeAmount),
	[Medicare / Medicaid] = iif(PFI.PropertyMedicareandMedicaidIncomeAmount IS NULL, '', PFI.PropertyMedicareandMedicaidIncomeAmount),
	[Nursing / Medical Income] = iif(PFI.PropertyNursingMedicalIncomeAmount IS NULL, '', PFI.PropertyNursingMedicalIncomeAmount),
	[Second Resident Income] = iif(PFI.PropertySecondaryResidentialIncomeAmount IS NULL, '', PFI.PropertySecondaryResidentialIncomeAmount),
	[Ground Rent] = iif(PFE.PropertyGroundRentExpenseAmount IS NULL, '', PFE.PropertyGroundRentExpenseAmount),
	[Room Expense - Housekeeping] = iif(PFE.PropertyHousekeepingExpenseAmount IS NULL, '', PFE.PropertyHousekeepingExpenseAmount),
	[Professional Fees] = iif(PFE.PropertyProfessionalFeesExpenseAmount IS NULL, '', PFE.PropertyProfessionalFeesExpenseAmount),
	[General / Administrative] = iif(PFE.PropertyGeneralandAdministrativeExpenseAmount IS NULL, '', PFE.PropertyGeneralandAdministrativeExpenseAmount),
	[Insurance Expense] = iif(PFE.PropertyInsuranceExpenseAmount IS NULL, '', PFE.PropertyInsuranceExpenseAmount),
	[Management Fees] = iif(PFE.PropertyManagementFeesExpenseAmount IS NULL, '', PFE.PropertyManagementFeesExpenseAmount),
	[Advertising & Marketing] = iif(PFE.PropertyAdvertisingandMarketingExpenseAmount IS NULL, '', PFE.PropertyAdvertisingandMarketingExpenseAmount),
	[Meal Expense] = iif(PFE.PropertyMealExpenseAmount IS NULL, '', PFE.PropertyMealExpenseAmount),
	[Other Expenses] = iif(PFE.PropertyOtherExpenseAmount IS NULL, '', PFE.PropertyOtherExpenseAmount),
	[Payroll / Benefits] = iif(PFE.PropertyPayrollandBenefitsExpenseAmount IS NULL, '', PFE.PropertyPayrollandBenefitsExpenseAmount),
	[Real Estate Taxes] = iif(PFE.PropertyRealEstateTaxesExpenseAmount IS NULL, '', PFE.PropertyRealEstateTaxesExpenseAmount),
	[Repairs and Maintenance] = iif(PFE.PropertyRepairsandMaintenanceExpenseAmount IS NULL, '', PFE.PropertyRepairsandMaintenanceExpenseAmount),
	[Utilities] = iif(PFE.PropertyUtilitiesExpenseAmount IS NULL, '', PFE.PropertyUtilitiesExpenseAmount),
	[Less: Vacancy Loss] = iif(PFI.PropertyVacancyandCollectionLossAmount IS NULL, '', PFI.PropertyVacancyandCollectionLossAmount),
	[Water / Sewer] = iif(PFE.PropertyWaterandSewerExpenseAmount IS NULL, '', PFE.PropertyWaterandSewerExpenseAmount),
	[UW Effective Gross Income] = iif(PFSA.PropertyEffectiveGrossIncomeAmount IS NULL, '', PFSA.PropertyEffectiveGrossIncomeAmount),
	[Total Operating Expense] = iif(PFSA.PropertyTotalOperatingExpensesAmount IS NULL, '', PFSA.PropertyTotalOperatingExpensesAmount),
	[UW Replacement Reserves] = iif(PFSA.PropertyTotalOperatingCapitalExpenditureAmount IS NULL, '', PFSA.PropertyTotalOperatingCapitalExpenditureAmount),
	[UW NCF] = iif(PFSA.PropertyNCFAmount IS NULL, '', PFSA.PropertyNCFAmount)

FROM FmData.Property P
	LEFT JOIN Collateral C ON C.Id =P.Id
	LEFT JOIN LoanCollateral LC ON LC.CollateralId = C.Id
	LEFT JOIN Loan L ON L.Id = LC.LoanId
	LEFT JOIN PropertyAcquisition PA ON PA.Id = P.Id
	LEFT JOIN PropertyAddress PAD ON PAD.PropertyId = P.Id
	LEFT JOIN PropertyPropertyAddress PPAD ON PPAD.PropertyId  = P.Id
	LEFT JOIN PropertyAffordableUnit PAF ON PAF.PropertyId = P.Id
	LEFT JOIN PropertyEnergy PE ON PE.PropertyId = P.Id
	LEFT JOIN PropertyHousing PAH ON PAH.PropertyId = P.Id
	LEFT JOIN PropertyOccupancy PO ON PO.PropertyId = P.Id
	LEFT JOIN PropertyValuation PV ON PV.PropertyId = P.Id
	LEFT JOIN PropertyInspection PI ON PI.PropertyId = P.Id
	LEFT JOIN PropertyInsurance PII ON PII.PropertyId = P.Id
	LEFT JOIN PropertyState PS ON PS.PropertyId = P.Id
	LEFT JOIN PropertyFinancialStatement PFS ON PFS.PropertyId = P.Id
	LEFT JOIN PropertyFinancialExpenseActivity PFE ON PFE.PropertyFinancialStatementId = PFS.Id
	LEFT JOIN PropertyFinancialIncomeActivity PFI ON  PFI.PropertyFinancialStatementId = PFS.Id
	LEFT JOIN PropertyFinancialSummaryActivity PFSA ON PFSA.PropertyFinancialStatementId = PFS.Id


