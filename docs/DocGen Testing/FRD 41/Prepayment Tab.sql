/*Prepayment Tab*/

WITH LoanPrepaymentProtectionTerm AS
	(SELECT
		LP.Id,
		LP.LoanId, 
		LP.LoanPrepaymentProtectionType,
		[LoanPrepaymentProtectionTerm] = CAST(LP.LoanPrepaymentProtectionTerm AS varchar(max)),
		[LoanPrepaymentProtectionEndDate] = CAST(FORMAT(LP.LoanPrepaymentProtectionEndDate, 'M/d/yyyy') AS varchar(max))
	FROM FmData.LoanPrepaymentProtectionTerm LP
	)
, LoanPrepaymentPremiumTerm AS
	(SELECT
		LPP.Id,
		LPP.LoanPrepaymentProtectionTermId,
		LPP.LoanPrepaymentPremiumType,
		[LoanPrepaymentPremiumTerm] = CAST(LPP.LoanPrepaymentPremiumTerm AS varchar(max)),
		[LoanPrepaymentPremiumEndDate] = CAST(FORMAT(LPP.LoanPrepaymentPremiumEndDate, 'M/d/yyyy') AS varchar(max)),
		LPP.LoanPrepaymentDecliningPremiumType,
		LPP.LoanOtherPrepaymentPremiumDescription	
	FROM FMData.LoanPrepaymentPremiumTerm LPP
	)
, LoanOtherDecliningPrepaymentSchedule AS 
	(SELECT 
		LO.LoanPrepaymentPremiumTermId,
		[LoanOtherDecliningPremiumStartMonthNumber] = CAST(LO.LoanOtherDecliningPremiumStartMonthNumber AS varchar(max)),
		[LoanOtherDecliningPremiumEndMonthNumber] = CAST(LO.LoanOtherDecliningPremiumEndMonthNumber AS varchar(max)),
		[LoanOtherDecliningPremiumPercent] = CAST(LO.LoanOtherDecliningPremiumPercent AS varchar(max))
	FROM FmData.LoanOtherDecliningPrepaymentSchedule LO )

SELECT
	[Loan Identifier] = L.LoanIdentifier,
	[Loan Number] = L.FannieMaeLoanNumber,
	[Prepayment Protection Type] = iif(LP.LoanPrepaymentProtectionType IS NULL, '', LP.LoanPrepaymentProtectionType),
	[Prepayment Protection Term] = iif(LP.LoanPrepaymentProtectionTerm IS NULL, '', LP.LoanPrepaymentProtectionTerm),
	[Prepayment Protection End Date] = iif(LP.LoanPrepaymentProtectionEndDate IS NULL, '', LP.LoanPrepaymentProtectionEndDate),
	[Prepayment Premium Type] = iif(LPP.LoanPrepaymentPremiumType IS NULL, '', LPP.LoanPrepaymentPremiumType),
	[Loan Prepayment Premium Term] = iif(LPP.LoanPrepaymentPremiumTerm IS NULL, '', LPP.LoanPrepaymentPremiumTerm),
	[Loan Prepayment Premium End Date] = iif(LPP.LoanPrepaymentPremiumEndDate IS NULL, '', LPP.LoanPrepaymentPremiumEndDate),
	[Declining Prepayment Premium Type] = iif(LPP.LoanPrepaymentDecliningPremiumType IS NULL, '', LPP.LoanPrepaymentDecliningPremiumType),
	[Declining Premium Start Date] = iif(LO.LoanOtherDecliningPremiumStartMonthNumber IS NULL, '', LO.LoanOtherDecliningPremiumStartMonthNumber),
	[Declining Premium End Date] = iif(LO.LoanOtherDecliningPremiumEndMonthNumber IS NULL, '', LO.LoanOtherDecliningPremiumEndMonthNumber),
	[Declining Premium Percentage] = iif(LO.LoanOtherDecliningPremiumPercent IS NULL, '', LO.LoanOtherDecliningPremiumPercent),
	[Declining Premium Description] = iif(LPP.LoanOtherPrepaymentPremiumDescription IS NULL, '', LPP.LoanOtherPrepaymentPremiumDescription)

FROM FmData.Loan L
	LEFT JOIN LoanPrepaymentProtectionTerm LP ON L.Id = LP.LoanId
	LEFT JOIN LoanPrepaymentPremiumTerm LPP ON LPP.LoanPrepaymentProtectionTermId = LP.Id 
	LEFT JOIN LoanOtherDecliningPrepaymentSchedule LO ON LO.LoanPrepaymentPremiumTermId = LPP.Id

	