/*Loan Tab*/

WITH LoanAcquisition AS 
	
	(SELECT 
		LA.Id,
		LA.LoanDUSDiscloseDerivedExecutionType,
		LA.LoanDUSDiscloseDerivedProductType,
		LA.LoanAdditionalDebtIndicator,
		LA.LoanDUSSplitIndicator,
		[LoanActualUnpaidPrincipalBalanceAmount] = CAST(FORMAT(LA.LoanActualUnpaidPrincipalBalanceAmount, '#,###0.00') AS varchar(max)),
		[LoanContributionBalanceAmount] = CAST(FORMAT(LA.LoanContributionBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanFannieMaeParticipationPercent] = CAST(FORMAT(LA.LoanFannieMaeParticipationPercent, '0.00') AS varchar(max)),
		[LoanFixedPrincipalPaymentAmount] = CAST(FORMAT(LA.LoanFixedPrincipalPaymentAmount, '#,##0.00') AS varchar(max)),
		LA.LoanInterestAccrualMethodType,
		[LoanFirstPaymentDueDate] = CAST(FORMAT(LA.LoanFirstPaymentDueDate, 'M/d/yyyy') AS varchar(max)),
		[LoanInterestRate] = CAST(FORMAT(LA.LoanInterestRate, '0.000##') AS varchar(max)),
		LA.LoanInterestRateType,
		[LoanMonthlyDebtServiceAmount] = CAST(FORMAT(LA.LoanMonthlyDebtServiceAmount, '#,##0.00') AS varchar(max)),
		[LoanMonthlyDebtServicePartialInterestOnlyAmount] = CAST(FORMAT(LA.LoanMonthlyDebtServicePartialInterestOnlyAmount, '#,##0.00') AS varchar(max)),
		[LoanOriginationUnpaidPrincipalBalanceAmount] = CAST(FORMAT(LA.LoanOriginationUnpaidPrincipalBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanPassThroughRatePercent] = CAST(FORMAT(LA.LoanPassThroughRatePercent, '') AS varchar(max)),
		[LoanTotalDebtBalanceAmount] = CAST(FORMAT(LA.LoanTotalDebtBalanceAmount, '#,##0.00') AS varchar(max)),
		LA.BorrowerLoanPurposeType,
		LA.LoanAdditionalDisclosureIndicator,
		LA.LoanAdditionalDisclosureComment,
		LA.LoanBifurcatedStructureIndicator,
		[LoanCutOffDateAllInLoanToValuePercent] = CAST(FORMAT(LA.LoanCutOffDateAllInLoanToValuePercent, '0.0#') AS varchar(max)),
		[LoanCutOffDateLoanToValuePercent] = CAST(FORMAT(LA.LoanCutOffDateLoanToValuePercent, '0.0#') AS varchar(max)),
		LA.LoanDeliveryType,
		LA.LoanExecutionType,
		LA.LoanGreenFinancingType,
		LA.LoanLienPositionType,
		[LoanMortgageNoteDate] = CAST(FORMAT(LA.LoanMortgageNoteDate, 'M/d/yyyy') AS varchar(max)),
		LA.LoanNegotiatedTransactionType,
		LA.LoanPricingandUnderwritingTierType,
		[LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor] = CAST(FORMAT(LA.LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[LoanUnderwrittenNCFDebtYieldFactor] = CAST(FORMAT(LA.LoanUnderwrittenNCFDebtYieldFactor, '0.000') AS varchar(max)),
		[LoanUnderwrittenNCFDebtServiceCoverageRatioFactor] = CAST(FORMAT(LA.LoanUnderwrittenNCFDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor] = CAST(FORMAT(LA.LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[LoanUnderwrittenNCFCapRateDebtServiceCoverageRatioFactor] = CAST(FORMAT(LA.LoanUnderwrittenNCFCapRateDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		LA.PricingTierDropEligibilityIndicator,
		[LoanInterestOnlyEndDate] = CAST(FORMAT(LA.LoanInterestOnlyEndDate, 'M/d/yyyy') AS varchar(max)),
		[LoanInterestOnlyPaymentTerm] = CAST(LA.LoanInterestOnlyPaymentTerm AS varchar(max)),
		LA.LoanInterestOnlyType,
		[LoanMaturityDate] = CAST(FORMAT(LA.LoanMaturityDate, 'M/d/yyyy') AS varchar(max)),
		[LoanRemainingInterestOnlyPeriod] = CAST(LA.LoanRemainingInterestOnlyPeriod AS varchar(max)),
		[LoanSeasoningMonthCount] = CAST(LA.LoanSeasoningMonthCount AS varchar(max)),
		[LoanTerm] = CAST(LA.LoanTerm AS varchar(max)),
		[LoanRemainingTerm] = CAST(LA.LoanRemainingTerm AS varchar(max)),
		[LoanNextInterestRateChangeDate] = CAST(FORMAT(LA.LoanNextInterestRateChangeDate, 'M/d/yyyy') AS varchar(max)),
		[LoanAdjustableRateMortgageRateChangeIndexLookbackDayCount] = CAST(LA.LoanAdjustableRateMortgageRateChangeIndexLookbackDayCount AS varchar(max)),
		[LoanFirstInterestRateChangeDate] = CAST(FORMAT(LA.LoanFirstInterestRateChangeDate, 'M/d/yyyy') AS varchar(max)),
		[LoanFirstPaymentChangeDate] = CAST(FORMAT(LA.LoanFirstPaymentChangeDate, 'M/d/yyyy') AS varchar(max)),
		LA.LoanInterestRateRoundingMethodType,
		[LoanLifetimeCapInterestRate] = CAST(FORMAT(LA.LoanLifetimeCapInterestRate, '0.000##') AS varchar(max)),
		[LoanLifetimeFloorInterestRate] = CAST(FORMAT(LA.LoanLifetimeFloorInterestRate, '0.000##') AS varchar(max)),
		[LoanMortgageMarginPercent] = CAST(FORMAT(LA.LoanMortgageMarginPercent, '0.000##') AS varchar(max)),
		[LoanOriginationInterestRate] = CAST(FORMAT(LA.LoanOriginationInterestRate, '0.000##') AS varchar(max)),
		[LoanPaymentResetFrequencyTerm] = CAST(LA.LoanPaymentResetFrequencyTerm AS varchar(max)),
		[LoanPeriodicPaymentChangeDecreasePercent] = CAST(FORMAT(LA.LoanPeriodicPaymentChangeDecreasePercent, '0.000##') AS varchar(max)),
		[LoanPeriodicPaymentChangeMaximumIncreasePercent] = CAST(FORMAT(LA.LoanPeriodicPaymentChangeMaximumIncreasePercent, '0.000##') AS varchar(max)),
		[LoanPeriodicRateChangeMaximumDecreasePercent] = CAST(FORMAT(LA.LoanPeriodicRateChangeMaximumDecreasePercent, '0.000##') AS varchar(max)),
		[LoanPeriodicRateChangeMaximumIncreasePercent] = CAST(FORMAT(LA.LoanPeriodicRateChangeMaximumIncreasePercent, '0.000##') AS varchar(max)),
		[LoanRateResetFrequencyTerm] = CAST(LA.LoanRateResetFrequencyTerm AS varchar(max))
	FROM FmData.LoanAcquisition LA)

, LoanFinancialActivity AS 

	(SELECT 
		[Ranking] = 
			CASE
				WHEN LFA.LoanId IS NULL THEN '0' 
				ELSE ROW_NUMBER() OVER(PARTITION BY LFA.LoanId ORDER BY LFA.LoanActivityDatetime DESC)
			END, 
		L.Id,
		[LoanNoteABCNCFDebtServiceCoverageRatioFactor] = CAST(FORMAT(LFA.LoanNoteABCNCFDebtServiceCoverageRatioFactor, '0.0#') AS varchar(max)),
		[LoanNCFAmount] = CAST(FORMAT(LFA.LoanNCFAmount, '#,##0.00') AS varchar(max)),
		[LoanNoteABCTotalDebtServiceAmount] = CAST(FORMAT(LFA.LoanNoteABCTotalDebtServiceAmount, '#,##0.00') AS varchar(max)),
		[LoanNCFDebtServiceCoverageRatioAllInFactor] = CAST(FORMAT(LFA.LoanNCFDebtServiceCoverageRatioAllInFactor, '0.0#') AS varchar(max))
	FROM FmData.Loan L 
		LEFT JOIN FmData.LoanFinancialActivity LFA ON LFA.LoanId = L.Id)

, LoanServicingActivity AS (SELECT
		[Ranking] = 
				CASE
					WHEN LSA.LoanId IS NULL THEN '0'
					ELSE ROW_NUMBER() OVER(PARTITION BY LSA.LoanId ORDER BY LSA.LoanActivityDatetime)
				END,
		L.Id, 
		[LoanActualUnpaidPrincipalBalanceAmount] = CAST(FORMAT(LSA.LoanActualUnpaidPrincipalBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanInterestRate] = CAST(FORMAT(LSA.LoanInterestRate, '0.000##') AS varchar(max)),
		[LoanLastPaidInstallmentDate] = CAST(FORMAT(LSA.LoanLastPaidInstallmentDate, 'M/d/yyyy') AS varchar(max)),
		[LoanBeginningScheduledBalanceAmount] = CAST(FORMAT(LSA.LoanBeginningScheduledBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanDefeasanceClosingDate] = CAST(FORMAT(LSA.LoanDefeasanceClosingDate, 'M/d/yyyy') AS varchar(max)),
		[LoanEndingScheduledBalanceAmount] = CAST(FORMAT(LSA.LoanEndingScheduledBalanceAmount, '#,##0.00') AS varchar(max)),
		[LoanPassThroughRate] = CAST(FORMAT(LSA.LoanPassThroughRate, '') AS varchar(max)),
		[LoanPaymentDueDate] = CAST(FORMAT(LSA.LoanPaymentDueDate, 'M/d/yyyy') AS varchar(max)),
		LSA.LoanPaymentStatusType,
		[LoanPrepaidPrincipalAmount] = CAST(FORMAT(LSA.LoanPrepaidPrincipalAmount, '#,##0.00') AS varchar(max)),
		[LoanPrepaymentPremiumPaidAmount] = CAST(FORMAT(LSA.LoanPrepaymentPremiumPaidAmount, '#,##0.00') AS varchar(max)),
		LSA.LoanReportingPeriodName,
		[LoanScheduledInterestPaymentAmount] = CAST(FORMAT(LSA.LoanScheduledInterestPaymentAmount, '#,##0.00') AS varchar(max)),
		[LoanScheduledPrincipalAmount] = CAST(FORMAT(LSA.LoanScheduledPrincipalAmount, '#,##0.00') AS varchar(max)),
		[LoanIndexRate] = CAST(FORMAT(LSA.LoanIndexRate, '0.000##') AS varchar(max)),
		[LoanNextIndexRate] = CAST(FORMAT(LSA.LoanNextIndexRate, '0.000##') AS varchar(max)),
		[LoanNextInterestRate] = CAST(FORMAT(LSA.LoanNextInterestRate, '0.000##') AS varchar(max)),
		[LoanNextInterestRateChangeDate] = CAST(FORMAT(LSA.LoanNextInterestRateChangeDate, 'M/d/yyyy') AS varchar(max)),
		[LoanNextPaymentChangeDate] = CAST(FORMAT(LSA.LoanNextPaymentChangeDate, 'M/d/yyyy') AS varchar(max)),
		[LoanSeasoningMonthCount] = CAST(LSA.LoanSeasoningMonthCount AS varchar(max)),
		[LoanRemainingTerm] = CAST(LSA.LoanRemainingTerm AS varchar(max))
	FROM FmData.Loan L
		LEFT JOIN FmData.LoanServicingActivity LSA ON LSA.LoanId = L.Id)

, LoanServicingReclassificationActivity AS 

	(SELECT 
		[Ranking] = 
			CASE
				WHEN LSAR.LoanId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY LSAR.LoanId ORDER BY LSAR.LoanActivityDatetime DESC) 
			END,
		L.Id,
		[LoanReclassificationAmount] = CAST(FORMAT(LSAR.LoanReclassificationAmount, '#,##0.00') AS varchar(max)),
		[LoanReclassificationEffectiveDate] = CAST(FORMAT(LSAR.LoanReclassificationEffectiveDate, 'M/d/yyyy') AS varchar(max))
	FROM FMData.Loan L 
		LEFT JOIN FMData.LoanServicingReclassificationActivity LSAR ON LSAR.LoanId = L.Id)

SELECT
	[Transaction ID] = iif(CP.MBSPoolIdentifier IS NULL, '', CP.MBSPoolIdentifier),
	[Loan Identifier] = iif(L.LoanIdentifier IS NULL, '', L.LoanIdentifier),
	[Loan Number] = iif(L.FannieMaeLoanNumber IS NULL, '', L.FannieMaeLoanNumber),
	[Loan DUS Disclose Derived Execution Type] = iif(LA.LoanDUSDiscloseDerivedExecutionType IS NULL, '', LA.LoanDUSDiscloseDerivedExecutionType),
	[Loan DUS Disclose Derived Product Type] = iif(LA.LoanDUSDiscloseDerivedProductType IS NULL, '', LA.LoanDUSDiscloseDerivedProductType),
	[Loan Additional Debt Indicator] = iif(LA.LoanAdditionalDebtIndicator IS NULL, '', LA.LoanAdditionalDebtIndicator),
	[Loan DUS Split Indicator] = iif(LA.LoanDUSSplitIndicator IS NULL, '', LA.LoanDUSSplitIndicator),
	[Loan Actual UPB at Acquisition Amount] = iif(LA.LoanActualUnpaidPrincipalBalanceAmount IS NULL, '', LA.LoanActualUnpaidPrincipalBalanceAmount),
	[Loan Issuance UPB ] = iif(LA.LoanContributionBalanceAmount IS NULL, '', LA.LoanContributionBalanceAmount),
	[Loan Fannie Mae Participation Percent] = iif(LA.LoanFannieMaeParticipationPercent IS NULL, '', LA.LoanFannieMaeParticipationPercent),
	[Fixed Principal Payment Amount] = iif(LA.LoanFixedPrincipalPaymentAmount IS NULL, '', LA.LoanFixedPrincipalPaymentAmount),
	[Interest Accrual Method] = iif(LA.LoanInterestAccrualMethodType IS NULL, '', LA.LoanInterestAccrualMethodType),
	[First Payment Date] = iif(LA.LoanFirstPaymentDueDate IS NULL, '', LA.LoanFirstPaymentDueDate),
	[Interest Rate (%)] = iif(LA.LoanInterestRate IS NULL, '', LA.LoanInterestRate),
	[Interest Type] = iif(LA.LoanInterestRateType IS NULL, '', LA.LoanInterestRateType),
	[Monthly Debt Service] = iif(LA.LoanMonthlyDebtServiceAmount IS NULL, '', LA.LoanMonthlyDebtServiceAmount),
	[Monthly Debt Service Amount - Partial IO] = iif(LA.LoanMonthlyDebtServicePartialInterestOnlyAmount IS NULL, '', LA.LoanMonthlyDebtServicePartialInterestOnlyAmount),
	[Original UPB] = iif(LA.LoanOriginationUnpaidPrincipalBalanceAmount IS NULL, '', LA.LoanOriginationUnpaidPrincipalBalanceAmount),
	[Net Interest Rate (%)] = iif(LA.LoanPassThroughRatePercent IS NULL, '', LA.LoanPassThroughRatePercent),
	[Total Debt Current UPB] = iif(LA.LoanTotalDebtBalanceAmount IS NULL, '', LA.LoanTotalDebtBalanceAmount),
	[Loan Purpose] = iif(LA.BorrowerLoanPurposeType IS NULL, '', LA.BorrowerLoanPurposeType),
	[Additional Disclosure Indicator] = iif(LA.LoanAdditionalDisclosureIndicator IS NULL, '', LA.LoanAdditionalDisclosureIndicator),
	[Additional Disclosure Comment] = iif(LA.LoanAdditionalDisclosureComment IS NULL, '', LA.LoanAdditionalDisclosureComment),
	[Loan Bifurcated Structure Indicator] = iif(LA.LoanBifurcatedStructureIndicator IS NULL, '', LA.LoanBifurcatedStructureIndicator),
	[All-In Issuance LTV] = iif(LA.LoanCutOffDateAllInLoanToValuePercent IS NULL, '', LA.LoanCutOffDateAllInLoanToValuePercent),
	[Issuance LTV] = iif(LA.LoanCutOffDateLoanToValuePercent IS NULL, '', LA.LoanCutOffDateLoanToValuePercent),
	[Loan Delivery Type] = iif(LA.LoanDeliveryType IS NULL, '', LA.LoanDeliveryType),
	[Loan Acquisition Execution Type] = iif(LA.LoanExecutionType IS NULL, '', LA.LoanExecutionType),
	[Green Financing Type] = iif(LA.LoanGreenFinancingType IS NULL, '', LA.LoanGreenFinancingType),
	[Lien Position] = iif(LA.LoanLienPositionType IS NULL, '', LA.LoanLienPositionType),
	[Note Date] = iif(LA.LoanMortgageNoteDate IS NULL, '', LA.LoanMortgageNoteDate),
	[Loan Negotiated Transaction Type] = iif(LA.LoanNegotiatedTransactionType IS NULL, '', LA.LoanNegotiatedTransactionType),
	[Tier] = iif(LA.LoanPricingandUnderwritingTierType IS NULL, '', LA.LoanPricingandUnderwritingTierType),
	[UW NCF DSCR - All In] = iif(LA.LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor IS NULL, '', LA.LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor),
	[UW NCF Debt Yield] = iif(LA.LoanUnderwrittenNCFDebtYieldFactor IS NULL, '', LA.LoanUnderwrittenNCFDebtYieldFactor),
	[UW NCF DSCR] = iif(LA.LoanUnderwrittenNCFDebtServiceCoverageRatioFactor IS NULL, '', LA.LoanUnderwrittenNCFDebtServiceCoverageRatioFactor),
	[UW NCF DSCR (IO)] = iif(LA.LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor IS NULL, '', LA.LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor),
	[UW NCF DSCR at Cap] = iif(LA.LoanUnderwrittenNCFCapRateDebtServiceCoverageRatioFactor IS NULL, '', LA.LoanUnderwrittenNCFCapRateDebtServiceCoverageRatioFactor),
	[Tier Drop Eligible (Y/N)] = iif(LA.PricingTierDropEligibilityIndicator IS NULL, '', LA.PricingTierDropEligibilityIndicator),
	[Interest Only End Date] = iif(LA.LoanInterestOnlyEndDate IS NULL, '', LA.LoanInterestOnlyEndDate),
	[Original IO Period (months)] = iif(LA.LoanInterestOnlyPaymentTerm IS NULL, '', LA.LoanInterestOnlyPaymentTerm),
	[Interest Only (Y/N)] = iif(LA.LoanInterestOnlyType IS NULL, '', LA.LoanInterestOnlyType),
	[Maturity Date] = iif(LA.LoanMaturityDate IS NULL, '', LA.LoanMaturityDate),
	[Remaining IO Period (months)] = iif(LA.LoanRemainingInterestOnlyPeriod IS NULL, '', LA.LoanRemainingInterestOnlyPeriod),
	[Seasoning (months)] = iif(LA.LoanSeasoningMonthCount IS NULL, '', LA.LoanSeasoningMonthCount),
	[Original Loan Term (months)] = iif(LA.LoanTerm IS NULL, '', LA.LoanTerm),
	[Remaining Loan Term (months)] = iif(LA.LoanRemainingTerm IS NULL, '', LA.LoanRemainingTerm),
	[Loan Acquisition Next Interest Rate Change Date] = iif(LA.LoanNextInterestRateChangeDate IS NULL, '', LA.LoanNextInterestRateChangeDate),
	[Interest Rate Lookback] = iif(LA.LoanAdjustableRateMortgageRateChangeIndexLookbackDayCount IS NULL, '', LA.LoanAdjustableRateMortgageRateChangeIndexLookbackDayCount),
	[Loan First Interest Rate Change Date] = iif(LA.LoanFirstInterestRateChangeDate IS NULL, '', LA.LoanFirstInterestRateChangeDate),
	[Loan First Payment Change Date] = iif(LA.LoanFirstPaymentChangeDate IS NULL, '', LA.LoanFirstPaymentChangeDate),
	[Interest Rate Rounding Methodology (%)] = iif(LA.LoanInterestRateRoundingMethodType IS NULL, '', LA.LoanInterestRateRoundingMethodType),
	[Interest Rate Cap (%)] = iif(LA.LoanLifetimeCapInterestRate IS NULL, '', LA.LoanLifetimeCapInterestRate),
	[Interest Rate Floor (%)] = iif(LA.LoanLifetimeFloorInterestRate IS NULL, '', LA.LoanLifetimeFloorInterestRate),
	[ARM Margin (%)] = iif(LA.LoanMortgageMarginPercent IS NULL, '', LA.LoanMortgageMarginPercent),
	[Original Interest Rate (%)] = iif(LA.LoanOriginationInterestRate IS NULL, '', LA.LoanOriginationInterestRate),
	[Payment Reset Frequency (months)] = iif(LA.LoanPaymentResetFrequencyTerm IS NULL, '', LA.LoanPaymentResetFrequencyTerm),
	[Periodic Payment Change Decrease (%)] = iif(LA.LoanPeriodicPaymentChangeDecreasePercent IS NULL, '', LA.LoanPeriodicPaymentChangeDecreasePercent),
	[Periodic Payment Change Increase (%)] = iif(LA.LoanPeriodicPaymentChangeMaximumIncreasePercent IS NULL, '', LA.LoanPeriodicPaymentChangeMaximumIncreasePercent),
	[Periodic Rate Change Decrease (%)] = iif(LA.LoanPeriodicRateChangeMaximumDecreasePercent IS NULL, '', LA.LoanPeriodicRateChangeMaximumDecreasePercent),
	[Periodic Rate Change Increase (%)] = iif(LA.LoanPeriodicRateChangeMaximumIncreasePercent IS NULL, '', LA.LoanPeriodicRateChangeMaximumIncreasePercent),
	[Rate Reset Frequency (months)] = iif(LA.LoanRateResetFrequencyTerm IS NULL, '', LA.LoanRateResetFrequencyTerm)

FROM FmData.Loan L
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
	LEFT JOIN LoanAcquisition LA ON LA.Id = L.Id
	LEFT JOIN LoanFinancialActivity LFA ON LFA.Id = L.Id
	LEFT JOIN LoanServicingActivity LSA ON LSA.Id = L.Id
	LEFT JOIN LoanServicingReclassificationActivity LSAR ON LSAR.Id = L.Id

WHERE LFA.Ranking IN (0,1)
	AND LSA.Ranking IN (0,1)
	AND LSAR.Ranking IN (0,1)
	AND L.FannieMaeLoanNumber IS NOT NULL -- Report is not generated for certified transactions


