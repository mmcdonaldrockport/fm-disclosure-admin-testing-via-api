/*Deal Footnotes*/
WITH DealFootnotes AS 

	(SELECT
		[Ranking] = 
			CASE
				WHEN TF.RecordId IS NULL THEN '0'
				ELSE ROW_NUMBER() OVER(PARTITION BY TF.RecordId ORDER BY TF.RecordId)
			END,
		[DealIdentifier] = D.DealIdentifier,
		[Name] = D.Name,
		[FieldName] = iif(FM.ReportLabel IS NULL, '', FM.ReportLabel),
		[Content] = iif(TF.Content IS NULL, '', TF.Content),
		[RecordType] = TF.RecordType
	FROM FmData.Deal D
		LEFT JOIN FmData.Loan L ON L.DealId = D.Id 
		LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.Id
		LEFT JOIN adm.TransactionFootnotes TF ON TF.RecordId = D.Id
		LEFT JOIN adm.FilteredFieldsMetaData FM ON FM.Id = TF.FieldId)

SELECT

	[Deal Identifier] = D.DealIdentifier,
	[Deal Name] = D.Name,
	[Field Name] = D.FieldName,
	[Comment] = D.Content

FROM DealFootnotes D

WHERE D.RecordType = 2 OR D.RecordType IS NULL
	AND D.Ranking IN (0,1)
	


