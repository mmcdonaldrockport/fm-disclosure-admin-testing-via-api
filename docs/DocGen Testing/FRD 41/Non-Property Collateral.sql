/*Non-Property Collateral Tab*/
WITH CollateralCTE AS
	
	(SELECT 
		C. Id, 
		[CollateralNonPropertyAmount] = CAST(FORMAT(C.CollateralNonPropertyAmount, '#,##0.00') AS varchar(max)) 
	FROM FmData.Collateral C)

SELECT
	[Loan Identifier] = L.LoanIdentifier,
	[Loan Number] = L.FannieMaeLoanNumber,
	[Collateral Identifier] = C.CollateralIdentifier,
	[Collateral Type] = C.CollateralType,
	[Collateral Non Property Collateral Type] = iif(C.CollateralNonPropertyType IS NULL, '', C.CollateralNonPropertyType),
	[Collateral Non Property Collateral Value Amount] = iif(CC.CollateralNonPropertyAmount IS NULL, '', CC.CollateralNonPropertyAmount)

FROM FmData.Collateral C
	LEFT JOIN FmData.LoanCollateral LC ON LC.CollateralId = C.Id
	LEFT JOIN FmData.Loan L ON L.Id = LC.LoanId
	LEFT JOIN CollateralCTE CC ON CC.Id = C.Id
WHERE L.FannieMaeLoanNumber IS NOT NULL -- Report is not generated for certified transactions. Loan number is required in IMP 02
	