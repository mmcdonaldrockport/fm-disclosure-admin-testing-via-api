SELECT 
	[Property ID] = iif(P.PropertyIdentifier IS NULL, '', P.PropertyIdentifier),
	[Property Name] = iif(PA.PropertyName IS NULL, '', PA.PropertyName)	,
	[Property Financial Property Statement Fiscal Year] = iif(PFS.PropertyFinancialStatementFiscalYearNumber IS NULL, '', PFS.PropertyFinancialStatementFiscalYearNumber),
	[Property Fiscal Period Type] = iif(PFS.PropertyFiscalPeriodType IS NULL, '', PFS.PropertyFiscalPeriodType),
	[Property Financial Property Statement Type Code] = iif(PFS.PropertyFinancialStatementType IS NULL, '', PFS.PropertyFinancialStatementType),
	[Property Financial Form Type Code] = iif(PFS.PropertyFinancialFormType IS NULL, '', PFS.PropertyFinancialFormType),
	[Financial Property Statement Period Begin Date (Begin YYYYMMDD)] = iif(PFS.PropertyFinancialStatementPeriodStartDate IS NULL, '', PFS.PropertyFinancialStatementPeriodStartDate),
	[Ending YYYYMMDD] = iif(PFS.PropertyFinancialStatementPeriodEndDate IS NULL, '', PFS.PropertyFinancialStatementPeriodEndDate),
	[Number of Months Covered] = iif(PFS.PropertyFinancialNumberofMonthsCoveredCount IS NULL, '', PFS.PropertyFinancialNumberofMonthsCoveredCount),
	[Fiscal Year End Month] = iif(PFS.PropertyFinancialFiscalYearEndMonthDayName IS NULL, '', 
		DateName( month , DateAdd( month, CONVERT(numeric, LEFT(PFS.PropertyFinancialFiscalYearEndMonthDayName,2)), 0 ) - 1 )),
	[Source of Financial Data] = iif(PFS.PropertyFinancialDataSourceType IS NULL, '', PFS.PropertyFinancialDataSourceType),
	[Data Type] = iif(PFS.PropertyFinancialDataType IS NULL, '', PFS.PropertyFinancialDataType),
	[Property Financial Submission Status Code] = iif(PFS.PropertyFinancialSubmissionStatusType IS NULL, '', PFS.PropertyFinancialSubmissionStatusType),
	[Financial Indicator] = PFS.PropertyFinancialAnnualizedIndicator,
	[Comments - Capital Items / DSCR / Income / Expenses] = '',--iif( IS NULL, '', ),
	[Commercial Income] = iif(PFI.PropertyCommercialIncomeAmount IS NULL, '', PFI.PropertyCommercialIncomeAmount),
	[Gross Potential Rent] = iif(PFI.PropertyGrossPotentialRentAmount IS NULL, '', PFI.PropertyGrossPotentialRentAmount),
	[Other Income] = iif(PFI.PropertyIncomeLessBadDebtAmount IS NULL, '', PFI.PropertyIncomeLessBadDebtAmount),
	[Laundry / Vending Income] = iif(PFI.PropertyLaundryandVendingIncomeAmount IS NULL, '', PFI.PropertyLaundryandVendingIncomeAmount),
	[Less: Vacancy Loss] = iif(PFI.PropertyLessConcessionRevenueAmount IS NULL, '', PFI.PropertyLessConcessionRevenueAmount),
	[Meals Income] = iif(PFI.PropertyMealIncomeAmount IS NULL, '', PFI.PropertyMealIncomeAmount),
	[Medicare / Medicaid] = iif(PFI.PropertyMedicareandMedicaidIncomeAmount IS NULL, '', PFI.PropertyMedicareandMedicaidIncomeAmount),
	[Nursing / Medical Income] = iif(PFI.PropertyNursingMedicalIncomeAmount IS NULL, '', PFI.PropertyNursingMedicalIncomeAmount),
	[Other Income] = iif(PFI.PropertyOtherIncomeAmount IS NULL, '', PFI.PropertyOtherIncomeAmount),
	[Parking Income] = iif(PFI.PropertyParkingIncomeAmount IS NULL, '', PFI.PropertyParkingIncomeAmount),
	[Second Resident Income] = iif(PFI.PropertySecondaryResidentialIncomeAmount IS NULL, '', PFI.PropertySecondaryResidentialIncomeAmount),
	[Less: Vacancy Loss] = iif(PFI.PropertyVacancyandCollectionLossAmount IS NULL, '', PFI.PropertyVacancyandCollectionLossAmount),
	[Advertising & Marketing] = iif(PFE.PropertyAdvertisingandMarketingExpenseAmount IS NULL, '', PFE.PropertyAdvertisingandMarketingExpenseAmount),
	[Operating Expenses: General & Administrative] = iif(PFE.PropertyGeneralandAdministrativeExpenseAmount IS NULL, '', PFE.PropertyGeneralandAdministrativeExpenseAmount),
	[Ground Rent] = iif(PFE.PropertyGroundRentExpenseAmount IS NULL, '', PFE.PropertyGroundRentExpenseAmount),
	[Room Expense - Housekeeping] = iif(PFE.PropertyHousekeepingExpenseAmount IS NULL, '', PFE.PropertyHousekeepingExpenseAmount),
	[Property Insurance] = iif(PFE.PropertyInsuranceExpenseAmount IS NULL, '', PFE.PropertyInsuranceExpenseAmount),
	[Management Fees] = iif(PFE.PropertyManagementFeesExpenseAmount IS NULL, '', PFE.PropertyManagementFeesExpenseAmount),
	[Meal Expense] = iif(PFE.PropertyMealExpenseAmount IS NULL, '', PFE.PropertyMealExpenseAmount),
	[Other Expenses] = iif(PFE.PropertyOtherExpenseAmount IS NULL, '', PFE.PropertyOtherExpenseAmount),
	[Payroll / Benefits] = iif(PFE.PropertyPayrollandBenefitsExpenseAmount IS NULL, '', PFE.PropertyPayrollandBenefitsExpenseAmount),
	[Professional Fees] = iif(PFE.PropertyProfessionalFeesExpenseAmount IS NULL, '', PFE.PropertyProfessionalFeesExpenseAmount),
	[Real Estate Taxes] = iif(PFE.PropertyRealEstateTaxesExpenseAmount IS NULL, '', PFE.PropertyRealEstateTaxesExpenseAmount),
	[Repairs and Maintenance] = iif(PFE.PropertyRepairsandMaintenanceExpenseAmount IS NULL, '', PFE.PropertyRepairsandMaintenanceExpenseAmount),
	[Utilities] = iif(PFE.PropertyUtilitiesExpenseAmount IS NULL, '', PFE.PropertyUtilitiesExpenseAmount),
	[Water & Sewer] = iif(PFE.PropertyWaterandSewerExpenseAmount IS NULL, '', PFE.PropertyWaterandSewerExpenseAmount),
	[Effective Gross Income] = iif(PFSA.PropertyEffectiveGrossIncomeAmount IS NULL, '', PFSA.PropertyEffectiveGrossIncomeAmount),
	[Net Cash Flow] = iif(PFSA.PropertyNCFAmount IS NULL, '', PFSA.PropertyNCFAmount),
	[Debt Service - A Note] = iif(PFSA.PropertyNoteAAllocatedDebtServiceAmount IS NULL, '', PFSA.PropertyNoteAAllocatedDebtServiceAmount),
	[DSCR: (NCF / Debt Service) - A, B & C Note] = iif(PFSA.PropertyNoteABCNCFDebtServiceCoverageRatioFactor IS NULL, '', FORMAT(PFSA.PropertyNoteABCNCFDebtServiceCoverageRatioFactor,'0.0#')),
	[Total Debt Service A B C Note Amount] = iif(PFSA.PropertyNoteABCTotalDebtServiceAmount IS NULL, '', FORMAT(PFSA.PropertyNoteABCTotalDebtServiceAmount,'#,##0.00')),
	[DSCR: (NCF / Debt Service) - A & B Note] = iif(PFSA.PropertyNoteABNCFDebtServiceCoverageRatioFactor IS NULL, '', FORMAT(PFSA.PropertyNoteABNCFDebtServiceCoverageRatioFactor,'0.0#')),
	[DSCR: (NCF / Debt Service) - A Note] = iif(PFSA.PropertyNoteANCFDebtServiceCoverageRatioFactor IS NULL, '', FORMAT(PFSA.PropertyNoteANCFDebtServiceCoverageRatioFactor,'0.0#')),
	[Debt Service - B Note] = iif(PFSA.PropertyNoteBAllocatedDebtServiceAmount IS NULL, '', FORMAT(PFSA.PropertyNoteBAllocatedDebtServiceAmount,'#,##0.00')),
	[Debt Service - C Note] = iif(PFSA.PropertyNoteCAllocatedDebtServiceAmount IS NULL, '', FORMAT(PFSA.PropertyNoteCAllocatedDebtServiceAmount,'#,##0.00')),
	[Operating Expense Ratio] = iif(PFSA.PropertyOperatingExpenseRatioPercent IS NULL, '', FORMAT(PFSA.PropertyOperatingExpenseRatioPercent,'0.0#')),
	[Capital Expenditures] = iif(PFSA.PropertyTotalOperatingCapitalExpenditureAmount IS NULL, '', FORMAT(PFSA.PropertyTotalOperatingCapitalExpenditureAmount,'#,##0.00')),
	[Total Operating Expenses] = iif(PFSA.PropertyTotalOperatingExpensesAmount IS NULL, '', FORMAT(PFSA.PropertyTotalOperatingExpensesAmount,'#,##0.00')),
	[Occupancy Date] = (SELECT TOP 1 FORMAT(PropertyPhysicalOccupancyAsOfDate,'M/d/yyyy') FROM fmdata.PropertyOccupancy WHERE PropertyId = P.Id ORDER BY PropertyActivityDatetime DESC),
	[Physical Occupancy (%)] = (SELECT TOP 1 FORMAT(PropertyPhysicalOccupancyPercent,'0.0#') FROM fmdata.PropertyOccupancy WHERE PropertyId = P.Id ORDER BY PropertyActivityDatetime DESC)
 
FROM FmData.Property P
	LEFT OUTER JOIN 
		(SELECT 
			[Ranking] = ROW_NUMBER() OVER(PARTITION BY PropertyId ORDER BY PropertyActivityDatetime DESC, Id DESC) ,
			PFS.Id,
			PFS.PropertyId,
			PropertyFinancialAnnualizedIndicator,
			[PropertyFinancialStatementFiscalYearNumber] = CAST(PFS.PropertyFinancialStatementFiscalYearNumber AS varchar(max)),
			[PropertyFiscalPeriodType] = CAST(PFS.PropertyFiscalPeriodType AS varchar(max)),
			[PropertyFinancialStatementType] = CAST(PFS.PropertyFinancialStatementType AS varchar(max)),
			[PropertyFinancialFormType] = CAST(PFS.PropertyFinancialFormType AS varchar(max)),
			[PropertyFinancialStatementPeriodStartDate] = FORMAT(PFS.PropertyFinancialStatementPeriodStartDate, 'yyyyMMdd'),
			[PropertyFinancialStatementPeriodEndDate] = FORMAT(PFS.PropertyFinancialStatementPeriodEndDate, 'yyyyMMdd'),
			[PropertyFinancialNumberofMonthsCoveredCount] = CAST(PFS.PropertyFinancialNumberofMonthsCoveredCount AS varchar(max)),
			[PropertyFinancialFiscalYearEndMonthDayName] = CAST(PFS.PropertyFinancialFiscalYearEndMonthDayName AS varchar(max)),
			[PropertyFinancialDataSourceType] = CAST(PFS.PropertyFinancialDataSourceType AS varchar(max)),
			[PropertyFinancialDataType] = CAST(PFS.PropertyFinancialDataType AS varchar(max)),
			[PropertyFinancialSubmissionStatusType] = CAST(PFS.PropertyFinancialSubmissionStatusType AS varchar(max))

		FROM FmData.PropertyFinancialStatement PFS) PFS  ON P.Id = PFS.PropertyId AND PFS.Ranking = 1
	LEFT JOIN FmData.PropertyFinancialIncomeActivity PFI ON PFI.PropertyFinancialStatementId = PFS.Id AND PFS.Ranking = 1
	LEFT JOIN FmData.PropertyFinancialExpenseActivity PFE ON PFE.PropertyFinancialStatementId = PFS.Id AND PFS.Ranking = 1
	LEFT JOIN FmData.PropertyFinancialSummaryActivity PFSA ON PFSA.PropertyFinancialStatementId = PFS.Id AND PFS.Ranking = 1
	LEFT JOIN FmData.PropertyAcquisition PA ON P.Id = PA.Id AND PFS.Ranking = 1

	WHERE P.PropertyIdentifier IN (43470133747001,43470133747003,43470133747004)


	--34570121087003
	--34570121087003


--SELECT * from fmdata.PropertyOccupancy WHERE PropertyId in (204518,204520,204521)


select * from adm.Users


--SELECT LEFT(PropertyFinancialFiscalYearEndMonthDayName,2) from fmdata.PropertyFinancialStatement pfs JOIN fmdata.Property p ON p.id = pfs.PropertyId WHERE p.PropertyIdentifier = 34570121087003


--SELECT DISTINCT PropertyFinancialActivitySourceType from fmdata.PropertyFinancialExpenseActivity

--SELECT DateName( month , DateAdd( month, CONVERT(numeric, LEFT(PropertyFinancialFiscalYearEndMonthDayName,2)), 0 ) - 1 ) from fmdata.PropertyFinancialStatement pfs JOIN fmdata.Property p ON p.id = pfs.PropertyId WHERE p.PropertyIdentifier = 34570121087003

--SELECT PropertyFinancialAnnualizedIndicator FROM fmdata.PropertyFinancialStatement where PropertyFinancialAnnualizedIndicator is not null
--select * from fmdata.PropertyFinancialStatement 


