Use DisclosureAdmin;

/*Security Tab*/

WITH CurrentSecurityFactorCTE AS  --Get the most recent record for Security Factor
	(SELECT 
		[Ranking] = iif(SF.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SF.SecurityId ORDER BY SF.SecurityFactorEffectiveDate DESC)),
		S.Id,
		[SecurityPaydownFactor] = CAST(FORMAT(SF.SecurityPaydownFactor, '0.0000000') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityFactor SF ON SF.SecurityId = S.Id)

, AtIssuanceSecurityFactorCTE AS  --Get the first reocrd for Security Factor
	(SELECT 
		[Ranking] = iif(SF.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SF.SecurityId ORDER BY SF.SecurityFactorEffectiveDate ASC)),
		S.Id,
		[SecurityPaydownFactor] = CAST(FORMAT(SF.SecurityPaydownFactor, '0.0000000') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityFactor SF ON SF.SecurityId = S.Id)

, SecurityStatusCTE AS -- Get the most recent Security Status
	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityID ORDER BY SS.SecurityStatusDatetime DESC), --Since there is no reporting for certified messages, each security should have a status. It is a required field in IMP 02
		SS.SecurityId,
		SS.SecurityStatusType
	FROM FMData.SecurityStatus SS)

, SecurityPaymentCTE AS --Get the most recent Security Payment
	(SELECT 
		[Ranking] = iif(SP.SecurityID IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SP.SecurityID ORDER BY SP.SecurityPaymentEffectiveDate DESC)),
		S.Id,
		[SecurityPaymentDistributionDate] = CAST(FORMAT(SP.SecurityPaymentDistributionDate, 'M/d/yyyy') AS varchar(max)),
		[SecurityInterestAccruedAmount] = CAST(FORMAT(SP.SecurityInterestAccruedAmount, '#,##0.00') AS varchar(max)),
		[SecurityInterestPaymentAmount] = CAST(FORMAT(SP.SecurityInterestPaymentAmount, '#,##0.00') AS varchar(max)),
		[SecurityPrincipalAmount] = CAST(FORMAT(SP.SecurityPrincipalAmount, '#,##0.00') AS varchar(max)),
		[SecurityScheduledPrincipalAmount] = CAST(FORMAT(SP.SecurityScheduledPrincipalAmount, '#,##0.00') AS varchar(max)),
		[SecurityUnpaidInterestBalanceAmount] = CAST(FORMAT(SP.SecurityUnpaidInterestBalanceAmount, '#,##0.00') AS varchar(max)),
		[SecurityUnpaidPrincipalBalanceAmount] = CAST(FORMAT(SP.SecurityUnpaidPrincipalBalanceAmount, '#,##0.00') AS varchar(max)),
		[SecurityUnscheduledPrincipalAmount] = CAST(FORMAT(SP.SecurityUnscheduledPrincipalAmount, '#,##0.00') AS varchar(max)),
		[SecurityYieldMaintenanceAmount] = CAST(FORMAT(SP.SecurityYieldMaintenanceAmount, '#,##0.00') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityPayment SP ON SP.SecurityId = S.Id)

, SecurityInterestAccrualScheduleCTE AS -- Get the most recent Security Interest Accrual Schedule
	(SELECT 
		[Ranking] = iif(SIA.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SIA.SecurityId ORDER BY SIA.SecurityInterestAccrualScheduleDate DESC)),
		S.Id,
		[SecurityInterestAccruedRate] = CAST(FORMAT(SIA.SecurityInterestAccruedRate, '0.000##') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityInterestAccrualSchedule SIA ON SIA.SecurityId = S.Id)

, SecurityInterestRateCTE AS -- Get the most recent Security Interest Rate
	(SELECT	
		[Ranking] = iif(SIR.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate DESC)),
		S.Id,
		[SecurityInterestRate] = CAST(FORMAT(SIR.SecurityInterestRate, '0.000##') AS varchar(max)),
		[FinancialIndexRate] = CAST(FORMAT(SIR.FinancialIndexRate, '0.000##') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityInterestRate SIR ON SIR.SecurityId = S.Id)

, SecurityInterestRateAdjustmentLifetimeTermCTE AS -- Get the most recent Security Interest Rate Adjustment Lifetime Term
	(SELECT 
		[Ranking] = iif(SIRA.SecurityId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY SIRA.SecurityId ORDER BY SIRA.SecurityInterestRateAdjustmentLifetimeEffectiveDate DESC)),
		S.Id,
		[SecurityLifetimeCapRate] = CAST(FORMAT(SIRA.SecurityLifetimeCapRate, '0.000##') AS varchar(max)),
		[SecurityLifetimeFloorRate] = CAST(FORMAT(SIRA.SecurityLifetimeFloorRate, '0.000##') AS varchar(max))
	FROM FmData.Security S
		LEFT JOIN FmData.SecurityInterestRateAdjustmentLifetimeTerm SIRA ON SIRA.SecurityId = S.Id)

, FinancialInstrumentCollateralGroupBalanceCTE AS -- Get the most recent Financial Instrument Collateral Group Balance
	(SELECT 
		[Ranking] = iif(FICGB.CollateralPoolId IS NULL, 0 , ROW_NUMBER() OVER(PARTITION BY FICGB.CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate DESC)),
		CP.Id,
		[CollateralPoolActiveLoanCount] = CAST(FICGB.CollateralPoolActiveLoanCount AS varchar(max)),
		[CollateralPoolWeightedAverageCouponRate] = CAST(FORMAT(FICGB.CollateralPoolWeightedAverageCouponRate, '0.000##') AS varchar(max)),
		[CollateralPoolWeightedAverageLoanAgeNumber] = CAST(FICGB.CollateralPoolWeightedAverageLoanAgeNumber AS varchar(max)),
		[CollateralPoolWeightedAverageMaturityNumber] = CAST(FICGB.CollateralPoolWeightedAverageMaturityNumber AS varchar(max)),
		[CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor] = CAST(FORMAT(FICGB.CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor, '0.00') AS varchar(max))
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.FinancialInstrumentCollateralGroupBalance FICGB ON FICGB.CollateralPoolId = CP.Id)

, IndexCTE AS --Lookup value for index, need to confirm if bad data or need to account for the 0's in the join
	(SELECT 
		S.Id,
		CP.MBSPoolIdentifier,
		S.ProductARMPlanNumber,
		AV.Value 
	FROM FmData.Security S 
		LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id
		LEFT JOIN FmData.AllowableValue AV ON AV.Code = S.ProductARMPlanNumber
	WHERE S.ProductARMPlanNumber IS NOT NULL)

, SecurityCTE AS

	(SELECT
		S.Id, 
		S.SecurityCUSIPIdentifier,
		S.MBSPoolPrefixType,
		S.ResecuritizationRestrictedIndicator,
		S.MBSSecurityARMSubType,
		[ProductARMPlanNumber] = CAST(S.ProductARMPlanNumber AS varchar(max)),
		[SecurityMaturityDate] = CAST(FORMAT(S.SecurityMaturityDate , 'M/d/yyyy') AS varchar(max)),
		S.ProductGroupType,
		[SecurityFederalBookEntryDate] = CAST(FORMAT(S.SecurityFederalBookEntryDate , 'M/d/yyyy') AS varchar(max)),
		[SecurityRateLookbackPeriod] = CAST(S.SecurityRateLookbackPeriod AS varchar(max)),
		[SecurityIssueAmount] = CAST(FORMAT(S.SecurityIssueAmount , '#,##0.00') AS varchar(max)),
		[SecurityIssueDate] = CAST(FORMAT(S.SecurityIssueDate , 'M/d/yyyy') AS varchar(max)),
		[SecurityFirstPaymentDate] = CAST(FORMAT(S.SecurityFirstPaymentDate , 'M/d/yyyy') AS varchar(max)),
		S.SecurityDayCountConventionType,
		S.SecurityInterestRateType,
		[SecurityMaturityTerm] = CAST(S.SecurityMaturityTerm AS varchar(max)),
		[SecurityPaymentDayNumber] = CAST(S.SecurityPaymentDayNumber AS varchar(max)),
		[SecurityPaymentLookbackPeriod] = CAST(S.SecurityPaymentLookbackPeriod AS varchar(max))
	FROM FmData.Security S)

SELECT DISTINCT
	[Transaction ID] = CP.MBSPoolIdentifier,
	[MBS Issuance CUSIP] = S.SecurityCUSIPIdentifier,
	[Pool Prefix] = S.MBSPoolPrefixType,
	[Security Resecuritization Restricted Indicator] = iif(S.ResecuritizationRestrictedIndicator IS NULL, '', S.ResecuritizationRestrictedIndicator),
	[ARM Subtype] = iif(S.MBSSecurityARMSubType IS NULL, '', S.MBSSecurityARMSubType),
	[ARM Plan Number] = iif(S.ProductARMPlanNumber IS NULL, '', S.ProductARMPlanNumber),
	[Maturity Date] = iif(S.SecurityMaturityDate IS NULL, '', S.SecurityMaturityDate),
	[Security Type] = iif(S.ProductGroupType IS NULL, '', S.ProductGroupType),
	[Factor] = iif(ASF.SecurityPaydownFactor IS NULL, '', ASF.SecurityPaydownFactor),
	[Settlement Date] = iif(S.SecurityFederalBookEntryDate IS NULL, '', S.SecurityFederalBookEntryDate),
	[Lookback Days] = iif(S.SecurityRateLookbackPeriod IS NULL, '', S.SecurityRateLookbackPeriod),
	[Issuance UPB] = iif(S.SecurityIssueAmount IS NULL, '', S.SecurityIssueAmount),
	[Issue Date] = iif(S.SecurityIssueDate IS NULL, '', S.SecurityIssueDate),
	[MBS First Payment Date] = iif(S.SecurityFirstPaymentDate IS NULL, '', S.SecurityFirstPaymentDate),
	[Accrual Basis] = iif(S.SecurityDayCountConventionType IS NULL, '', S.SecurityDayCountConventionType),
	[Interest Type] = iif(S.SecurityInterestRateType IS NULL, '', S.SecurityInterestRateType),
	[Pool-Status] = iif(SS.SecurityStatusType IS NULL, '', SS.SecurityStatusType),
	[Distribution Date] = iif(SP.SecurityPaymentDistributionDate IS NULL, '', SP.SecurityPaymentDistributionDate),
	[Maturity Term] = iif(S.SecurityMaturityTerm IS NULL, '', S.SecurityMaturityTerm),
	[Distribution Day Number] = iif(S.SecurityPaymentDayNumber IS NULL, '', S.SecurityPaymentDayNumber),
	[Prepayment Penalty / Premium Allocation] = '', --Field not sent in XML imports
	[Current Factor] = iif(CSF.SecurityPaydownFactor IS NULL, '', CSF.SecurityPaydownFactor),
	[Accrual Rate (%)] = iif(SIA.SecurityInterestAccruedRate IS NULL, '', SIA.SecurityInterestAccruedRate),
	[Remittance Rate / Pass Through Rate (%)] = iif(SIR.SecurityInterestRate IS NULL, '', SIR.SecurityInterestRate),
	[Current Index Rate (%)] = iif(SIR.FinancialIndexRate IS NULL, '', SIR.FinancialIndexRate),
	[Index Name] = iif(I.Value IS NULL, '', I.Value),
	[Weighted Average Loan Pass-Through Lifetime Cap] = iif(SIRA.SecurityLifetimeCapRate IS NULL, '', SIRA.SecurityLifetimeCapRate),
	[Weighted Average Loan Pass-Through Lifetime Floor] = iif(SIRA.SecurityLifetimeFloorRate IS NULL, '', SIRA.SecurityLifetimeFloorRate),
	[Accrued Interest] = iif(SP.SecurityInterestAccruedAmount IS NULL, '', SP.SecurityInterestAccruedAmount),
	[Total Interest Distribution] = iif(SP.SecurityInterestPaymentAmount IS NULL, '', SP.SecurityInterestPaymentAmount),
	[Total Principal Distribution] = iif(SP.SecurityPrincipalAmount IS NULL, '', SP.SecurityPrincipalAmount),
	[Scheduled Principal Amount] = iif(SP.SecurityScheduledPrincipalAmount IS NULL, '', SP.SecurityScheduledPrincipalAmount),
	[Ending Unpaid Interest Balance] = iif(SP.SecurityUnpaidInterestBalanceAmount IS NULL, '', SP.SecurityUnpaidInterestBalanceAmount),
	[Current UPB] = iif(SP.SecurityUnpaidPrincipalBalanceAmount IS NULL, '', SP.SecurityUnpaidPrincipalBalanceAmount),
	[Unscheduled Principal Collections] = iif(SP.SecurityUnscheduledPrincipalAmount IS NULL, '', SP.SecurityUnscheduledPrincipalAmount),
	[Yield Maintenance Allocation] = iif(SP.SecurityYieldMaintenanceAmount IS NULL, '', SP.SecurityYieldMaintenanceAmount),
	[Payment Lookback] = iif(S.SecurityPaymentLookbackPeriod IS NULL, '', S.SecurityPaymentLookbackPeriod),
	[Number of Pools] = '', --Field not sent in XML imports, can hard code 1 for MBS if we'd like
	[Ending Current Period Loan Count] = iif(FICGB.CollateralPoolActiveLoanCount IS NULL, '', FICGB.CollateralPoolActiveLoanCount),
	[Calculated WAC] = iif(FICGB.CollateralPoolWeightedAverageCouponRate IS NULL, '', FICGB.CollateralPoolWeightedAverageCouponRate),
	[Current Weighted Average Original Loan Term] = iif(FICGB.CollateralPoolWeightedAverageLoanAgeNumber IS NULL, '', FICGB.CollateralPoolWeightedAverageLoanAgeNumber),
	[Maturity WAM] = iif(FICGB.CollateralPoolWeightedAverageMaturityNumber IS NULL, '', FICGB.CollateralPoolWeightedAverageMaturityNumber),
	[Security Current Weighted Average Debt Service Coverage Ratio] = iif(FICGB.CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor IS NULL, '', FICGB.CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor)

FROM SecurityCTE S
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id
	LEFT JOIN CurrentSecurityFactorCTE CSF ON CSF.Id = S.Id 
	LEFT JOIN AtIssuanceSecurityFactorCTE ASF ON ASF.Id = S.Id 
	LEFT JOIN SecurityStatusCTE SS ON SS.SecurityId = S.Id
	LEFT JOIN SecurityPaymentCTE SP ON SP.Id = S.Id
	LEFT JOIN SecurityInterestAccrualScheduleCTE SIA ON SIA.Id = S.Id
	LEFT JOIN SecurityInterestRateCTE SIR ON SIR.Id = S.Id
	LEFT JOIN SecurityInterestRateAdjustmentLifetimeTermCTE SIRA ON SIRA.Id = S.Id
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceCTE FICGB ON FICGB.Id = CP.Id
	LEFT JOIN IndexCTE I ON I.Id = S.Id

WHERE CSF.Ranking IN (0,1)
	AND ASF.Ranking IN (0,1)
	AND SS.Ranking IN (0,1)
	AND SP.Ranking IN (0,1)
	AND SIA.Ranking IN (0,1)
	AND SIR.Ranking IN (0,1)
	AND SIRA.Ranking IN (0,1)
	AND FICGB.Ranking IN (0,1)
	AND S.ProductGroupType = 'MBS'
	AND CP.MBSPoolIdentifier = '25470119028001'

	