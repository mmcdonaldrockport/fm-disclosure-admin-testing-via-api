/*Property Phases Tab*/

--Cast PropertyPhaseConstructedUnitCount to string to avoid 0's in the resulting queries with data is null
WITH PropertyPhaseConstruction As 

	(SELECT		
		[PropertyId] = PC.PropertyId, 
		[PropertyPhaseConstructedUnitCount] = CAST(PC.PropertyPhaseConstructedUnitCount AS varchar(500)) 
	FROM FmData.PropertyPhasedConstruction PC)

SELECT 
	[Property ID] = P.PropertyIdentifier,
	[Property Name] = PA.PropertyName,
	[Property Phase Number] = iif(PC.PropertyPhaseNumber IS NULL, '', PC.PropertyPhaseNumber),
	[Phase Year] = iif(PC.PropertyPhaseConstructedYearNumber IS NULL, '', PC.PropertyPhaseConstructedYearNumber),
	[Total Number of Units per Phase Year] = iif(PCC.PropertyPhaseConstructedUnitCount IS NULL, '', PCC.PropertyPhaseConstructedUnitCount)

FROM FmData.Property P
	LEFT JOIN FmData.PropertyAcquisition PA ON PA.Id = P.Id
	LEFT JOIN FmData.PropertyPhasedConstruction	PC ON PC.PropertyId = P.Id
	LEFT JOIN PropertyPhaseConstruction PCC ON PCC.PropertyId = P.Id

ORDER BY [Property ID] DESC, [Property Phase Number] ASC