﻿'use strict';

angular.module('disclosure').value('AppConstants', {
    DayOfWeekEnums : {
        MONDAY: 0,
        TUESDAY: 1,
        WEDNESDAY: 2,
        THURSDAY: 3,
        FRIDAY: 4,
        SATURDAY: 5,
        SUNDAY: 6        
    },

    WeekDays: [
        'MONDAYS',
        'TUESDAYS',
        'WEDNESDAYS',
        'THURSDAYS',
        'FRIDAYS',
        'SATURDAYS',
        'SUNDAYS'
    ],

    TransactionWaiverRequestors: [
        "Acquisition",
        "Certification",
        "Legal",
        "Other"
    ],

    BannerEndBasis: {
        None: 0,
        Date: 1,
        PoolTermination: 2
    },
	
    HighlightEffectiveDatePeriods: [
        1,
        2,
        3,
        4
    ],

    PublishingScheduleStatuses: {
        Pending: 'Pending',
        Cancelled: 'Cancelled'
    },

    InputType: {
        Dropdown: 1,
        Date: 2,
        Text: 3
    },

    TransactionType: {
        Pool: 1,
        Deal: 2,
        Remic: 3,
        Mega: 4
    },

    FilterType: {
        Regular: 1,
        Criteria: 2
    },

    FootnoteType: {
        Pool: 1,
        Deal: 2,
        Loan: 3
    },

    ResourceTypes: {
        learningcenter: { name: 'learningcenter', value: 0, label: 'Learning Center', pageCode: 8 },
        productinformation: { name: 'productinformation', value: 1, label: 'Product Information', pageCode: 9 },
        datacollections: { name: 'datacollections', value: 2, label: 'Data Collections', pageCode: 11 }
    },

    DataFormats: {
        Url: /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
    },

    PageFunctionCode: {
        //TransactionPage: 1,
        PendingPublishing: 2,
        StickeringApproval: 3,
        ManageDocumentHolds: 4,
        IssuancePublicationHolds: 5,
        Banners: 6,
        Carousel: 7,
        LearningCenter: 8,
        ProductInformation: 9,
        UsageAgreement: 10,
        DataCollections: 11,
        DataCollectionsSystemDocuments: 12,
        DataTransfers: 13,
        PublishingSchedule: 14,
        HolidayCalendar: 15,
        UserRoles: 16,
        DisclosureOperations: 17,
        AdditionalDisclosure: 18,
        SpecializedTransactions: 19,
        Users: 20,
        AdHocReporting: 21
    },

    AllowedAction: {
        View: 1,
        ViewEdit: 2,
        Create: 3
    },

    QueueAccess: {
        DisclosureOperations: 17,
        AdditionalDisclosure: 18,
        SpecializedTransactions: 19
    },
    QueueTypes: {
        disclosureoperations: { label: 'Disclosure Operations', code: 17 },
        additionaldisclosure: { label: 'Additional Disclosure', code: 18 },
        specializedtransactions: { label: 'Specialized Transactions', code: 19 }
    },
});