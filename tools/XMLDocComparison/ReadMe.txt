﻿Objective: XMLDocComparison is to make sure Document Generated (Excel) has matched the XML Message sent From Fannie.

Setup in XMLDocComparison.exe.config 
1. Update Database connection(s) Name "DisclosureAdmin" to desired database server
2. Update saved file location "SavedFileLocation" to desired folder path

Command line usage: XMLDocComparison.exe [Transfer Id] [Document Id] [ConnectionString]
Ex. XMLDocComparison.exe 34770231558001 4 DisclosureAdminDev

DocumentsInfoConfig.xlsx has each data point information ( Format, Xpath, DataType, IsDerived and ConvertValue(bit) ). 
The data point is group by document for by worksheet and tab within each section (ex.0, 1, 2).
