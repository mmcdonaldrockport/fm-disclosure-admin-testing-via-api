﻿# Function to simplify setting values in AppSettings
function Set-AppSettingValue {
	Param([xml]$config, [string]$key, [string]$value, [bool]$create = $FALSE)
	
	$setting = $config.configuration.appSettings.add | where {$_.key -eq $key}
	
	if ($setting -eq $null) {
	
		if ($create -eq $TRUE) {
			$setting = $config.CreateElement("add")
			$attr = $config.CreateAttribute("key")
			$attr.psbase.value = $key
			
			$setting.SetAttributeNode($attr)
			$setting.SetAttributeNode($config.CreateAttribute("value"))
			
			$config.configuration.appSettings.AppendChild($setting)
			Write-Host "Application Setting $key was created!!!" -foregroundcolor "green"
		} else {
			Write-Host "Application Setting $key was not found!!!" -foregroundcolor "yellow"
			Return
		}
	}

	Write-Host "Setting $key to $value"
	$setting.value = $value
}

$SMTP_HOST = $env:SMTP_HOST
$SMTP_PORT = $env:SMTP_PORT
$DATA_PROVIDER = $env:DATA_PROVIDER
$DROP_STAGING_TABLES = $env:DROP_STAGING_TABLES

$APP_CONFIG_FILE_PATH = "$($env:APPLICATION_PATH)\ImportEngine.exe.config"

if (Test-Path $APP_CONFIG_FILE_PATH)
{
	$APP_CONFIG_CONTENT = [xml](Get-Content $APP_CONFIG_FILE_PATH)
	$CONNECTION_STRING = "$($env:CONNECTION_STRING)"
	
	if (![String]::IsNullOrEmpty($CONNECTION_STRING)) {

		$CONFIG_CONNECTION_STRING = $APP_CONFIG_CONTENT.configuration.connectionstrings.add | ? { $_.name -eq "ConfigDatabase" }
		$CONFIG_CONNECTION_STRING.connectionString = "$CONNECTION_STRING"

		Write-Host "Connection String was set to $CONNECTION_STRING"
	}
}
else
{
	Write-Host "Connection String >> Update Process Skipped." -foregroundcolor "yellow"
}

if ($SMTP_HOST.length -gt 0 -And $SMTP_PORT -gt 0) {
		
	$SMTP = $APP_CONFIG_CONTENT.configuration.'system.net'.mailSettings.smtp.network
	$SMTP.host = "$SMTP_HOST"
	$SMTP.port = "$SMTP_PORT"
		
} else {
	Write-Host "Environment variables SMTP_HOST or SMTP_PORT not found!!!" -foregroundcolor "yellow"
}

if ($DATA_PROVIDER.length -gt 0) {
	Set-AppSettingValue -config $APP_CONFIG_CONTENT -key "provider" -value $DATA_PROVIDER
} else {
	Write-Host "Environment variables $DATA_PROVIDER not found!!!" -foregroundcolor "yellow"
}

if ($DROP_STAGING_TABLES.length -gt 0) {
	Set-AppSettingValue -config $APP_CONFIG_CONTENT -key "DropStagingTables" -value $DROP_STAGING_TABLES -create $TRUE
}

Write-Host "Started saving Config File"
$APP_CONFIG_CONTENT.Save("$APP_CONFIG_FILE_PATH")

Write-Host "Configuration Saved Successfully!!!" -foregroundcolor "green"