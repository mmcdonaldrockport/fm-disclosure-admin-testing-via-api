﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>Rockport Import Report</title>
      </head>
      <body style="margin:0;padding:0;background:#fff;">
        <table width="98%" style="margin:0px auto;" border="0" cellpadding="20">
          <tr>
            <td style="width:600px;background:#fff;font-family:helvetica">
              <table cellpadding="0" cellspacing="0" width="600" align="center">
                <tr>
                  <td style="padding-bottom:10px;">
                    <table cellpadding="0" cellspacing="0" width="600" align="center" style="border-collapse:collapse;">
                      <tr>
                        <td style="background:#fff;">
                          <img src="http://www.therockportgroup.com/images/email/logo_rockport.png" alt="Rockport" border="0" width="180" height="37" />
                        </td>
                        <td style="background:#fff;text-align:right;font-family:helvetica,arial,sans-serif;font-weight:bold;font-size:18px;padding:10px 0px 2px 0px;">
                          <xsl:apply-templates select="job/summary"/> Report
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" style="padding-top:10px;">
                          <table cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;">
                            <tr>
                              <td style="padding:8px 0px 16px 0px;background:#fff;font-family:helvetica;font-size:11px;line-height:1.2em;font-style:normal;">
                                <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;">
                                  <tr valign="top">
                                    <td>
                                      <xsl:apply-templates select="job"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr valign="top">
                              <td style="padding:4px 0px 8px 0px;font-family:georgia;font-size:18px;">
                                <font face="georgia" color="#000000">
                                  <xsl:apply-templates select="job/summary"/> Summary</font>
                              </td>
                            </tr>
                            <!--TRANSFER REPEAT BEGINS-->
                            <xsl:apply-templates select="job/summary/transfer"/>
                            <!--TRANSFER REPEAT ENDS-->

                            <tr>
                              <td style="padding:0px 0px 8px 0px;font-size:11px;line-height:1.2em;font-style:normal;">
                                <font face="helvetica" size="2" color="#666666">For more information, please see the attached log file.</font>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="background:#e6e6e6;padding:8px 10px;">
                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td style="color:#999;font-size:10px;font-family:helvetica">
                          DISCLAIMER: This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Any unauthorized copying, disclosure, or distribution of the material in this e-mail is strictly forbidden.
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="job">
    <table cellpadding="4" cellspacing="0" style="border-collapse:collapse;">
      <tr valign="top">
        <td>
          <font face="helvetica" size="2" color="#666666">Client:</font>
        </td>
        <td>
          <font face="helvetica" size="2">
            <xsl:value-of select="@client"/>
          </font>
        </td>
      </tr>
      <tr valign="top">
        <td style="color:#666;">
          <font face="helvetica" size="2" color="#666666">Date/Time:</font>
        </td>
        <td>
          <font face="helvetica" size="2">
            <xsl:value-of select="@started"/>
          </font>
        </td>
      </tr>
      <tr valign="top">
        <td style="color:#666;">
          <font face="helvetica" size="2" color="#666666">Job:</font>
        </td>
        <td>
          <font face="helvetica" size="2">
            <xsl:value-of select="@name"/>
          </font>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="transfer">
    <tr>
      <td style="padding:8px 16px 0px 16px;margin:0px 0px 20px 0px;background:#eee;border:1px solid #ddd;font-size:12px;line-height:1.2em;font-style:normal;">
        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;">
          <tr>
            <td colspan="8" style="border-bottom:1px solid #cccccc;padding:8px 0px 4px 0px;font-size:12px;">
              <font face="helvetica" color="#333333">
                <b>
                  <xsl:value-of select="@name"/>
                </b>
              </font>
              <br />
              <font face="helvetica" color="#333333">
                <xsl:value-of select="@filename"/>
              </font>
            </td>
          </tr>
          <tr>
            <td colspan="8" style="font-size:8px;">&#160;</td>
          </tr>
          <xsl:apply-templates select="stats[@type='Source']"/>
          <xsl:apply-templates select="stats[@type='Lookup']"/>
          <xsl:apply-templates select="stats[@type='Destination']"/>
        </table>
      </td>
    </tr>
    <tr>
      <td>&#160;</td>
    </tr>

  </xsl:template>
  
  <xsl:template match="summary">
      <xsl:choose>
        <xsl:when test="@type = 'export'">
          Export
      </xsl:when>
        <xsl:otherwise>
          Import
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>

  <xsl:template name="stats_header">
    <xsl:if test="@type != 'Source'">
      <tr>
        <td colspan="8" style="background:#dddddd;padding:4px;font-size:12px;line-height:1.4em;font-style:normal;">
          <nobr>
            <font face="helvetica" color="#333333">
              <b>
                <xsl:value-of select="@type"/>
              </b>
            </font>
          </nobr>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="stats">
    <xsl:if test="position() = 1">
      <xsl:call-template name="stats_header"/>
    </xsl:if>
    <tr>
      <td colspan="8" style="font-size:4px;">&#160;</td>
    </tr>
    <tr>
      <td style="padding:0px 0px 4px 0px;font-size:12px;line-height:1.4em;font-style:normal;">
        <nobr>
          <font face="helvetica" color="#333333">
            <b>
              &#160;<xsl:value-of select="@name"/>
            </b>
          </font>
        </nobr>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Loaded</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Rejected</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Inserted</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Updated</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Deleted</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Errors</font>
      </td>
      <td align="right" style="padding:0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">Saved</font>
      </td>
    </tr>
    <tr>
      <td>&#160;</td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@loaded"/>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@rejected"/>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@inserted"/>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@updated"/>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@deleted"/>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;color:#990000;">
        <font face="helvetica">
          <b>
            <xsl:value-of select="@errors"/>
          </b>
        </font>
      </td>
      <td align="right" style="0px 4px 4px 4px;font-size:12px;font-style:normal;">
        <font face="helvetica" color="#333333">
          <xsl:value-of select="@saved"/>
        </font>
      </td>
    </tr>
    <tr>
      <td colspan="8">&#160;</td>
    </tr>
  </xsl:template>
  
</xsl:stylesheet>
