﻿<?xml version="1.0" encoding="utf-8"?>
<job type="Components.Job" id="20170308" name="SecurityConversion" description="Import Security Source Data" useTransaction="true" note="V1.0.10" note1="Add refreshinfo, edit source table name and loader to test." note2="Build up more actions, move child action to separate imp." note3="Review for FRD v3.0." note5="Correct refreshinfo, add writer to Security source table, add primaryKey to SecurityCUSIPNumber, and CollateralPoolId to staging table _Source1." note6="Edit context database to QA3" note7="Revise to handle conditional required FirstPaymentDate, use separate actions" note8="Add validation action for MBSPoolARMSubType when SecurityInterestRate = &quot;A&quot;, and exclude from source query in Load_Security actions." note9="Revamp to add SecurityAtIssuance destination and insert At Issuance values for certain fields." noe10="Add Refresh operation for SecurityId after Load Security action.">
  <context>
    <database server="DVFM01-D03\DVFM01_D03" dataDbName="DEV_LL_DisclosureAdmin_QA" server-QA="DVFM01-D03\QAFM01_D04" dataDbName-QA="DisclosureAdmin3" />
    <log>
      <appender name="console" level="INFO" />
      <appender name="file" level="DEBUG" filePath="Clients\FNMA\Logs\{1}_SecurityConversion_{0}.log" />
      <appender name="database" level="OFF" />
    </log>
    <summary sendEmail="false" mailTo="llee@rockportllc.com" />
  </context>
  <actions>
    <action type="Components.DataTransferAction" name="Load_CollateralPool" dateFormats="M/d/yyyy h:mm:ss tt" forwardExecOnFail="true" saveData="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name="import.Security_Source1">
          <loader type="Components.SqlDbLoader" dataSource="context:data" />
          <fields>
            <field name="MBSPoolIdentifier" dataType="string" required="true" />
            <field name="SecurityId" dataType="int?" note="use refreshinto to populate if doable, otherwise use sql query to update" />
            <fieldx name="CollateralPoolId" dataType="int?" expressionx="GENERICID(&quot;CollateralPoolIDLookup&quot;,&quot;MBSPoolIdentifier&quot;,MBSPoolIdentifier)" note="use refreshinto to populate if doable, otherwise use sql query to update" />
          </fields>
        </table>
      </source>
      <destinations>
        <destination type="Components.Destination" name="CollateralPool_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.CollateralPool">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="MBSPoolIdentifier" dataType="string" expression="MBSPoolIdentifier" />
            </fields>
          </table>
          <matchmappings>
            <mapping sourceField="MBSPoolIdentifier" destinationField="MBSPoolIdentifier" destinationTable="import.CollateralPool" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.SqlDbAction" name="RefreshCollateralPoolId" forwardExecOnFail="false">
      <operations>
        <operation commandText="UPDATE import.Security_Source1 &#xD;&#xA;SET CollateralPoolId = CP.Id, SecurityId = CP.Id &#xD;&#xA;FROM import.CollateralPool CP &#xD;&#xA;WHERE CP.MBSPoolIdentifier = import.Security_Source1.MBSPoolIdentifier ;" operationType="CommandText" dataSource="context:data" />
      </operations>
    </action>
    <action type="Components.DataTransferAction" name="MBSPoolARMSubType_Validation" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" saveData="false" dataRejectLevel="Row">
        <table name="Security_ARMSubTypeValidation">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT MBSPoolIdentifier, MBSPoolARMSubType, SecurityInterestRateType &#xD;&#xA;FROM [import].[Security_Source1] &#xD;&#xA;WHERE [SecurityInterestRateType] = 'A' AND MBSPoolARMSubType IS NULL " />
          <fields>
            <field name="MBSPoolIdentifier" dataType="long?" required="true" primaryValue="true" />
            <field name="SecurityInterestRateType" dataType="string" primaryValue="true" />
            <field name="MBSPoolARMSubType" dataType="string">
              <validators>
                <validator type="Components.DynamicValidator" expression="IsNotNull(MBSPoolARMSubType)" generateError="true" message="MBSPoolARMSubType is required for this ARM Security" />
              </validators>
            </field>
          </fields>
        </table>
      </source>
    </action>
    <action type="Components.DataTransferAction" name="Load_SecurityMBS" dateFormats="M/d/yyyy h:mm:ss tt" forwardExecOnFail="true" saveData="true">
      <source type="Components.Source" saveData="true" dataRejectLevel="Row">
        <table name="import.Security_Source1">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT CPSecurityId = CollateralPoolId, * &#xD;&#xA;FROM [import].[Security_Source1] &#xD;&#xA;WHERE SecurityType &lt;&gt; 'DMBS' &#xD;&#xA;&#x9;AND (SecurityInterestRateType &lt;&gt; 'A' OR &#xD;&#xA;&#x9;(MBSPoolARMSubType IS NOT NULL AND SecurityInterestRateType = 'A')) "></loader>
          <writer type="Components.SqlDbWriter" dataSource="context:data" />
          <fields>
            <field name="CPSecurityId" dataType="long" required="true" />
            <field name="DealIdentifier" dataType="long" required="true" />
            <field name="MBSPoolIdentifier" dataType="string" required="true" primaryValue="true" />
            <field name="SecurityCUSIPNumber" dataType="string" required="true" primaryKey="true" />
            <field name="SecurityType" dataType="string" required="true" />
            <field name="MBSPoolARMSubType" dataType="string" required-cond="unknown" />
            <field name="SecuritySettlementDate" dataType="date" required="true" />
            <field name="SecurityCutOffDateBalanceAmount" dataType="money?" />
            <field name="SecurityFirstPaymentDate" dataType="date?" required="true" note="Required except for DMBS" />
            <field name="SecurityAtIssuanceWeightedAverageAmortizationTerm" dataType="int?" />
            <field name="SecurityCurrentCalculatedWeightedAverageCouponRate" dataType="decimal?" />
            <field name="SecurityEndingPeriodLoanCount" dataType="int?" />
            <field name="MBSPoolCurrentWeightedAverageRemainingMaturityTerm" dataType="int?" />
            <field name="SecurityIssueDate" dataType="date" required="true" />
            <field name="PoolIssuanceLoanCount" dataType="int?" />
            <field name="MBSPoolAtIssuanceWeightedAverageCutOffDateBalanceAmount" dataType="money?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetInterestRateCap" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetInterestRateFloor" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetMBSMarginRate" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageOriginalTerm" dataType="int?" />
            <field name="MBSPoolAtIssuanceWeightedAveragePassThroughRate" dataType="decimal?" />
            <field name="PoolAtIssuanceWeightedAverageLoanAgeNumber" dataType="int?" />
            <field name="SecurityPrefixType" dataType="string" />
            <field name="SecurityMaturityDate" dataType="date?" />
            <field name="DealStructuredTransactionType" dataType="string" />
            <field name="SecurityAtIssuanceCollateralPoolWeightedAverageCouponRate" dataType="decimal?" />
            <field name="SecurityAtIssuanceProductARMPlanNumber" dataType="string" />
            <field name="SecurityAtIssuanceLookbackPeriod" dataType="int?" />
            <field name="SecurityCurrentPoolStatusType" dataType="string" />
            <field name="SecurityAccrualRateMethodType" dataType="string" />
            <field name="SecurityPaymentLookbackPeriod" dataType="string" />
            <field name="MBSPoolCurrentWeightedAverageLifetimePassThroughRateCap" dataType="decimal?" />
            <field name="MBSPoolCurrentWeightedAverageLifetimePassThroughRateFloor" dataType="decimal?" />
            <field name="MBSPoolCurrentWeightedAverageMonthstoRateChangeCount" dataType="string" />
            <field name="PoolCurrentAccrualRate" dataType="decimal?" />
            <field name="SecurityDeferredInterestAllowedIndicator" dataType="string" />
            <field name="SecurityNextRateChangeDate" dataType="string" />
            <field name="SecurityConvertibleIndicator" dataType="string" />
            <field name="SecurityFirstRateChangeDate" dataType="string" />
            <field name="SecurityPaymentatIssuanceCapRate" dataType="string" />
            <field name="MBSPoolAtIssuanceWeightedAverageMonthstoRateChangeCount" dataType="string" />
            <field name="MBSPoolCurrentWeightedAverageLoanAgeNumber" dataType="int?" />
            <field name="SecurityAtIssuanceWeightedAverageLoanToValueRatio" dataType="decimal?" />
            <field name="SecurityInterestRateType" dataType="string" required="true" />
            <field name="SecurityAtIssuanceWeightedAveragePrepaymentProtectionTerm" dataType="int?" />
            <field name="SecurityAtIssuanceRemainingMaturityTerm" dataType="string" />
            <field name="SecurityCurrentRemainingMaturityTerm" dataType="int" requiredx="true" />
            <field name="CDSSecurityIdentifier" dataType="long?" />
            <field name="FinancialInstrumentCollateralGroupBalanceEffectiveDate" dataType="date?" required-cond="If adding to destination table, no need for here." />
            <field name="SecurityInterestAccrualScheduleDate" dataType="date?" />
            <field name="SecurityInterestRateEffectiveDate" dataType="date" required="true" />
            <field name="SecurityInterestRateAdjustmentLifetimeEffectiveDate" dataType="date?" />
            <fieldx name="SecurityStatusDatetime" dataType="datetimeoffset" required="true" />
            <fieldx name="SecurityId" dataType="int?" expressionx="GENERICID(&quot;SecurityIDLookup&quot;,&quot;SecurityCUSIPIdentifier&quot;,SecurityCUSIPNumber)" loadFromSource="false" note="use refreshInfo to populate, not Generic Lookup." />
            <fieldx name="CollateralPoolId" dataType="int?" expression="GENERICID(&quot;CollateralPoolIDLookup&quot;,&quot;MBSPoolIdentifier&quot;,MBSPoolIdentifier)" loadFromSource="false" />
          </fields>
        </table>
      </source>
      <lookupsx>
        <lookup type="Components.GenericLookup" name="CollateralPoolIDLookup" dbName="import.CollateralPool" indexColumns="MBSPoolIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="MBSPoolIdentifier" dataType="string" />
          </fields>
          <matchmappings>
            <mapping sourceField="MBSPoolIdentifier" destinationField="MBSPoolIdentifier" />
          </matchmappings>
        </lookup>
        <lookup type="Components.GenericLookup" name="SecurityIDLookup" dbName="import.Security" indexColumns="SecurityCUSIPIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="SecurityCUSIPIdentifier" dataType="string" />
          </fields>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </lookup>
      </lookupsx>
      <destinations>
        <destination type="Components.Destination" name="Security_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.Security">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="Id" dataType="int" expression="CPSecurityId" note="The FmData.CollateralPool and FmData.Security tables have a special one-to-one relationship, where the CollateralPool is parent to the Security, except for Megas and Remics where a Security does not have a corresponding CollateralPool parent. Since this is not the case here, Security.Id must be populated with the same value as its CollateralPool.Id." />
              <field name="MBSSecurityARMSubType" dataType="string" expression="MBSPoolARMSubType"></field>
              <field name="SecurityDayCountConventionType" dataType="string" expression="SecurityAccrualRateMethodType" />
              <field name="SecurityRateLookbackPeriod" dataType="long?" expression="SecurityAtIssuanceLookbackPeriod" />
              <field name="ProductARMPlanNumber" dataType="string" expression="SecurityAtIssuanceProductARMPlanNumber" />
              <field name="SecurityMaturityTerm" dataType="int?" expression="SecurityCurrentRemainingMaturityTerm" />
              <field name="SecurityCUSIPIdentifier" dataType="string" expression="SecurityCUSIPNumber" />
              <field name="SecurityIssueAmount" dataType="money?" expression="SecurityCutOffDateBalanceAmount" />
              <field name="SecurityFirstPaymentDate" dataType="date?" expression="SecurityFirstPaymentDate" />
              <field name="SecurityInterestRateType" dataType="string" expression="SecurityInterestRateType" />
              <field name="SecurityIssueDate" dataType="date?" expression="SecurityIssueDate" />
              <field name="SecurityMaturityDate" dataType="date?" expression="SecurityMaturityDate" />
              <field name="SecurityPaymentLookbackPeriod" dataType="long?" expression="SecurityPaymentLookbackPeriod" />
              <field name="MBSPoolPrefixType" dataType="string" expression="SecurityPrefixType" />
              <field name="SecurityFederalBookEntryDate" dataType="date?" expression="SecuritySettlementDate" />
              <field name="ProductGroupType" dataType="string" expression="SecurityType" />
            </fields>
          </table>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </destination>
        <destination type="Components.Destination" name="SecurityAtIssuance_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.SecurityAtIssuance">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="Id" dataType="int" expression="CPSecurityId" note="The FmData.CollateralPool and FmData.Security tables have a special one-to-one relationship, where the CollateralPool is parent to the Security, except for Megas and Remics where a Security does not have a corresponding CollateralPool parent. Since this is not the case here, Security.Id must be populated with the same value as its CollateralPool.Id." />
              <field name="MBSSecurityARMSubType" dataType="string" expression="MBSPoolARMSubType"></field>
              <field name="SecurityDayCountConventionType" dataType="string" expression="SecurityAccrualRateMethodType" />
              <field name="SecurityRateLookbackPeriod" dataType="long?" expression="SecurityAtIssuanceLookbackPeriod" />
              <field name="ProductARMPlanNumber" dataType="string" expression="SecurityAtIssuanceProductARMPlanNumber" />
              <field name="SecurityMaturityTerm" dataType="int?" expression="SecurityAtIssuanceRemainingMaturityTerm" />
              <field name="SecurityCUSIPIdentifier" dataType="string" expression="SecurityCUSIPNumber" />
              <field name="SecurityIssueAmount" dataType="money?" expression="SecurityCutOffDateBalanceAmount" />
              <field name="SecurityFirstPaymentDate" dataType="date?" expression="SecurityFirstPaymentDate" />
              <field name="SecurityInterestRateType" dataType="string" expression="SecurityInterestRateType" />
              <field name="SecurityIssueDate" dataType="date?" expression="SecurityIssueDate" />
              <field name="SecurityMaturityDate" dataType="date?" expression="SecurityMaturityDate" />
              <field name="SecurityPaymentLookbackPeriod" dataType="long?" expression="SecurityPaymentLookbackPeriod" />
              <field name="MBSPoolPrefixType" dataType="string" expression="SecurityPrefixType" />
              <field name="SecurityFederalBookEntryDate" dataType="date?" expression="SecuritySettlementDate" />
              <field name="ProductGroupType" dataType="string" expression="SecurityType" />
            </fields>
          </table>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.DataTransferAction" name="Load_SecurityDMBS" dateFormats="M/d/yyyy h:mm:ss tt" forwardExecOnFail="true" saveData="true">
      <source type="Components.Source" saveData="true" dataRejectLevel="Row">
        <table name="import.Security_Source1">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT CPSecurityId = CollateralPoolId, * &#xD;&#xA;FROM [import].[Security_Source1] &#xD;&#xA;WHERE SecurityType = 'DMBS'&#xD;&#xA;&#x9;AND (SecurityInterestRateType &lt;&gt; 'A' OR &#xD;&#xA;&#x9;(MBSPoolARMSubType IS NOT NULL AND SecurityInterestRateType = 'A')) "></loader>
          <writer type="Components.SqlDbWriter" dataSource="context:data" />
          <fields>
            <field name="CPSecurityId" dataType="long" required="true" />
            <field name="DealIdentifier" dataType="long" required="true" />
            <field name="MBSPoolIdentifier" dataType="string" required="true" />
            <field name="SecurityCUSIPNumber" dataType="string" required="true" primaryKey="true" />
            <field name="SecurityType" dataType="string" required="true" />
            <field name="MBSPoolARMSubType" dataType="string" required-cond="unknown" />
            <field name="SecuritySettlementDate" dataType="date" required="true" />
            <field name="SecurityCutOffDateBalanceAmount" dataType="money?" />
            <field name="SecurityFirstPaymentDate" dataType="date?" required="false" note="Required except for DMBS" />
            <field name="SecurityAtIssuanceWeightedAverageAmortizationTerm" dataType="int?" />
            <field name="SecurityCurrentCalculatedWeightedAverageCouponRate" dataType="decimal?" />
            <field name="SecurityEndingPeriodLoanCount" dataType="int?" />
            <field name="MBSPoolCurrentWeightedAverageRemainingMaturityTerm" dataType="int?" />
            <field name="SecurityIssueDate" dataType="date" required="true" />
            <field name="PoolIssuanceLoanCount" dataType="int?" />
            <field name="MBSPoolAtIssuanceWeightedAverageCutOffDateBalanceAmount" dataType="money?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetInterestRateCap" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetInterestRateFloor" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageNetMBSMarginRate" dataType="decimal?" />
            <field name="MBSPoolAtIssuanceWeightedAverageOriginalTerm" dataType="int?" />
            <field name="MBSPoolAtIssuanceWeightedAveragePassThroughRate" dataType="decimal?" />
            <field name="PoolAtIssuanceWeightedAverageLoanAgeNumber" dataType="int?" />
            <field name="SecurityPrefixType" dataType="string" />
            <field name="SecurityMaturityDate" dataType="date?" />
            <field name="DealStructuredTransactionType" dataType="string" />
            <field name="SecurityAtIssuanceCollateralPoolWeightedAverageCouponRate" dataType="decimal?" />
            <field name="SecurityAtIssuanceProductARMPlanNumber" dataType="string" />
            <field name="SecurityAtIssuanceLookbackPeriod" dataType="int?" />
            <field name="SecurityCurrentPoolStatusType" dataType="string" />
            <field name="SecurityAccrualRateMethodType" dataType="string" />
            <field name="SecurityPaymentLookbackPeriod" dataType="string" />
            <field name="MBSPoolCurrentWeightedAverageLifetimePassThroughRateCap" dataType="decimal?" />
            <field name="MBSPoolCurrentWeightedAverageLifetimePassThroughRateFloor" dataType="decimal?" />
            <field name="MBSPoolCurrentWeightedAverageMonthstoRateChangeCount" dataType="string" />
            <field name="PoolCurrentAccrualRate" dataType="decimal?" />
            <field name="SecurityDeferredInterestAllowedIndicator" dataType="string" />
            <field name="SecurityNextRateChangeDate" dataType="string" />
            <field name="SecurityConvertibleIndicator" dataType="string" />
            <field name="SecurityFirstRateChangeDate" dataType="string" />
            <field name="SecurityPaymentatIssuanceCapRate" dataType="string" />
            <field name="MBSPoolAtIssuanceWeightedAverageMonthstoRateChangeCount" dataType="string" />
            <field name="MBSPoolCurrentWeightedAverageLoanAgeNumber" dataType="int?" />
            <field name="SecurityAtIssuanceWeightedAverageLoanToValueRatio" dataType="decimal?" />
            <field name="SecurityInterestRateType" dataType="string" required="true" />
            <field name="SecurityAtIssuanceWeightedAveragePrepaymentProtectionTerm" dataType="int?" />
            <field name="SecurityAtIssuanceRemainingMaturityTerm" dataType="string" />
            <field name="SecurityCurrentRemainingMaturityTerm" dataType="int" requiredx="true" />
            <field name="CDSSecurityIdentifier" dataType="long?" />
            <field name="FinancialInstrumentCollateralGroupBalanceEffectiveDate" dataType="date?" required-cond="If adding to destination table, no need for here." />
            <field name="SecurityInterestAccrualScheduleDate" dataType="date?" />
            <field name="SecurityInterestRateEffectiveDate" dataType="date" required="true" />
            <field name="SecurityInterestRateAdjustmentLifetimeEffectiveDate" dataType="date?" />
            <fieldx name="SecurityStatusDatetime" dataType="datetimeoffset" required="true" />
            <fieldx name="SecurityId" dataType="int?" expressionx="GENERICID(&quot;SecurityIDLookup&quot;,&quot;SecurityCUSIPIdentifier&quot;,SecurityCUSIPNumber)" loadFromSource="false" note="use refreshInfo to populate, not Generic Lookup." />
            <fieldx name="CollateralPoolId" dataType="int?" expression="GENERICID(&quot;CollateralPoolIDLookup&quot;,&quot;MBSPoolIdentifier&quot;,MBSPoolIdentifier)" loadFromSource="false" />
          </fields>
        </table>
      </source>
      <lookupsx>
        <lookup type="Components.GenericLookup" name="CollateralPoolIDLookup" dbName="import.CollateralPool" indexColumns="MBSPoolIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="MBSPoolIdentifier" dataType="string" />
          </fields>
          <matchmappings>
            <mapping sourceField="MBSPoolIdentifier" destinationField="MBSPoolIdentifier" />
          </matchmappings>
        </lookup>
        <lookup type="Components.GenericLookup" name="SecurityIDLookup" dbName="import.Security" indexColumns="SecurityCUSIPIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="SecurityCUSIPIdentifier" dataType="string" />
          </fields>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </lookup>
      </lookupsx>
      <destinations>
        <destination type="Components.Destination" name="SecurityDMBS_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.Security">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="Id" dataType="int" expression="CPSecurityId" note="The FmData.CollateralPool and FmData.Security tables have a special one-to-one relationship, where the CollateralPool is parent to the Security, except for Megas and Remics where a Security does not have a corresponding CollateralPool parent. Since this is not the case here, Security.Id must be populated with the same value as its CollateralPool.Id." />
              <field name="MBSSecurityARMSubType" dataType="string" expression="MBSPoolARMSubType"></field>
              <field name="SecurityDayCountConventionType" dataType="string" expression="SecurityAccrualRateMethodType" />
              <field name="SecurityRateLookbackPeriod" dataType="long?" expression="SecurityAtIssuanceLookbackPeriod" />
              <field name="ProductARMPlanNumber" dataType="string" expression="SecurityAtIssuanceProductARMPlanNumber" />
              <field name="SecurityMaturityTerm" dataType="int?" expression="SecurityCurrentRemainingMaturityTerm" />
              <field name="SecurityCUSIPIdentifier" dataType="string" expression="SecurityCUSIPNumber" />
              <field name="SecurityIssueAmount" dataType="money?" expression="SecurityCutOffDateBalanceAmount" />
              <field name="SecurityFirstPaymentDate" dataType="date?" expression="SecurityFirstPaymentDate" />
              <field name="SecurityInterestRateType" dataType="string" expression="SecurityInterestRateType" />
              <field name="SecurityIssueDate" dataType="date?" expression="SecurityIssueDate" />
              <field name="SecurityMaturityDate" dataType="date?" expression="SecurityMaturityDate" />
              <field name="SecurityPaymentLookbackPeriod" dataType="long?" expression="SecurityPaymentLookbackPeriod" />
              <field name="MBSPoolPrefixType" dataType="string" expression="SecurityPrefixType" />
              <field name="SecurityFederalBookEntryDate" dataType="date?" expression="SecuritySettlementDate" />
              <field name="ProductGroupType" dataType="string" expression="SecurityType" />
            </fields>
          </table>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </destination>
        <destination type="Components.Destination" name="SecurityDMBSAtIssuance_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.SecurityAtIssuance">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="Id" dataType="int" expression="CPSecurityId" note="The FmData.CollateralPool and FmData.Security tables have a special one-to-one relationship, where the CollateralPool is parent to the Security, except for Megas and Remics where a Security does not have a corresponding CollateralPool parent. Since this is not the case here, Security.Id must be populated with the same value as its CollateralPool.Id." />
              <field name="MBSSecurityARMSubType" dataType="string" expression="MBSPoolARMSubType"></field>
              <field name="SecurityDayCountConventionType" dataType="string" expression="SecurityAccrualRateMethodType" />
              <field name="SecurityRateLookbackPeriod" dataType="long?" expression="SecurityAtIssuanceLookbackPeriod" />
              <field name="ProductARMPlanNumber" dataType="string" expression="SecurityAtIssuanceProductARMPlanNumber" />
              <field name="SecurityMaturityTerm" dataType="int?" expression="SecurityAtIssuanceRemainingMaturityTerm" />
              <field name="SecurityCUSIPIdentifier" dataType="string" expression="SecurityCUSIPNumber" />
              <field name="SecurityIssueAmount" dataType="money?" expression="SecurityCutOffDateBalanceAmount" />
              <field name="SecurityFirstPaymentDate" dataType="date?" expression="SecurityFirstPaymentDate" />
              <field name="SecurityInterestRateType" dataType="string" expression="SecurityInterestRateType" />
              <field name="SecurityIssueDate" dataType="date?" expression="SecurityIssueDate" />
              <field name="SecurityMaturityDate" dataType="date?" expression="SecurityMaturityDate" />
              <field name="SecurityPaymentLookbackPeriod" dataType="long?" expression="SecurityPaymentLookbackPeriod" />
              <field name="MBSPoolPrefixType" dataType="string" expression="SecurityPrefixType" />
              <field name="SecurityFederalBookEntryDate" dataType="date?" expression="SecuritySettlementDate" />
              <field name="ProductGroupType" dataType="string" expression="SecurityType" />
            </fields>
          </table>
          <matchmappings>
            <mapping sourceField="SecurityCUSIPNumber" destinationField="SecurityCUSIPIdentifier" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.SqlDbAction" name="RefreshSecurityId" forwardExecOnFail="false">
      <operations>
        <operation commandText="UPDATE import.Security_Source1 &#xD;&#xA;SET SecurityId = NULL  &#xD;&#xA;FROM &#xD;&#xA;&#x9;(&#xD;&#xA;&#x9;SELECT SS.SecurityCUSIPNumber, SS.SecurityId, SE.Id AS Sec_Id &#xD;&#xA;&#x9;FROM import.Security_Source1 SS &#xD;&#xA;&#x9;LEFT JOIN import.Security SE ON SE.SecurityCUSIPIdentifier = SS.SecurityCUSIPNumber&#xD;&#xA;&#x9;WHERE SE.Id IS NULL &#xD;&#xA;&#x9;) SE&#xD;&#xA;WHERE SE.SecurityCUSIPNumber =  import.Security_Source1.SecurityCUSIPNumber; " operationType="CommandText" dataSource="context:data" />
      </operations>
    </action>
  </actions>
</job>