﻿<?xml version="1.0" encoding="utf-8"?>
<job type="Components.Job" id="20170306" name="PropertyFinancialTerminatedConversion" description="Import PropertyFinancialTerminated Source Data" useTransaction="true" note="V1.0.2" note1="Initial version" note2="Changed destination table to newly created import.ConvertedTerminatedPropertyFinancialStatement.">
  <context>
    <database server="DVFM01-D03\DVFM01_D03" dataDbName="DEV_LL_DisclosureAdmin_QA" server-QA="DVFM01-D03\QAFM01_D04" dataDbName-QA="DisclosureAdmin3" />
    <log>
      <appender name="console" level="INFO" />
      <appender name="file" level="DEBUG" filePath="Clients\FNMA\Logs\{1}_PropertyFinancialTerminatedConversion_{0}.log" />
      <appender name="database" level="OFF" />
    </log>
    <summary sendEmail="false" mailTo="llee@rockportllc.com" />
  </context>
  <actions>
    <action-Validate type="Components.DataTransferAction" name="Validate_PropertyFinancialTerminated" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" saveData="false" dataRejectLevel="Row">
        <table name="PropertyMonitoring_Validation">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT PropertyIdentifier &#xD;&#xA;,[PropertyActivityType],[PropertyActivityDatetime],[PropertyActivityAcquisitionIndicator]&#xD;&#xA;FROM import.PropertyFinancialsTerminated_Source1&#xD;&#xA;WHERE &#xD;&#xA;(&#xD;&#xA;[PropertyIdentifier] IS NULL OR &#xD;&#xA;[PropertyFinancialStatementPeriodEndDate]  IS NULL OR &#xD;&#xA;[PropertyFiscalPeriodType] IS NULL OR &#xD;&#xA;[PropertyActivityType] IS NULL OR &#xD;&#xA;[PropertyActivityDatetime] IS NULL OR &#xD;&#xA;[PropertyActivityAcquisitionIndicator] IS NULL &#xD;&#xA;) ;" />
          <fields>
            <field name="PropertyIdentifier" dataType="long?" required="true" primaryValue="true" />
            <field name="PropertyFinancialStatementPeriodEndDate" dataType="date?" required="true"></field>
            <field name="PropertyFiscalPeriodType" dataType="string" required="true"></field>
            <field name="PropertyActivityType" dataType="string" required="true"></field>
            <field name="PropertyActivityDatetime" dataType="datetimeoffset" required="true"></field>
            <field name="PropertyActivityAcquisitionIndicator" dataType="string" required="true"></field>
          </fields>
        </table>
      </source>
    </action-Validate>
    <action type="Components.DataTransferAction" name="Load_PropertyFinancialStatement" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name="import.PropertyFinancialsTerminated_Source1">
          <loader type="Components.SqlDbLoader" dataSource="context:data" />
          <fields>
            <field name="PropertyIdentifier" dataType="int" required="true" primaryValue="true">
              <validator type="Components.LookupValidator" expression="GENERICID(&quot;PropertyIdLookup&quot;,&quot;PropertyIdentifier&quot;,PropertyIdentifier)" generateError="true" message="&quot;'{1}' was not found. {0} does not match in Property Table&quot;" />
            </field>
            <field name="CollateralIdentifier" dataType="long?" required-off="true" />
            <field name="PropertyMostRecentNCFAmount" dataType="money?"></field>
            <field-TBA name="PropertyFinancialStatemenrPeriodStartDate" dataType="date?" />
            <field name="PropertyPrecedingFiscalYearPhysicalOccupancyPercent" dataType="decimal?"></field>
            <field name="PropertyMostRecentNCFtoDSCRFactor" dataType="decimal?"></field>
            <field name="PropertyPrecedingFiscalYearDSCRNCFFactor" dataType="decimal?"></field>
            <field name="PropertyPrecedingFiscalYearNCFAmount" dataType="money?" />
            <field name="PropertyFinancialSecondPrecedingFiscalYearDSCRNCFFactor" dataType="decimal?"></field>
            <field name="PropertyFinancialSecondPrecedingFiscalYearNCFAmount" dataType="money?" />
            <field name="PropertyFinancialSecondPrecedingFiscalYearPhysicalOccupancyPercent" dataType="decimal?"></field>
            <field name="PropertyFinancialStatementPeriodEndDate" dataType="date?" required="true"></field>
            <field name="PropertyFinancialStatementFiscalYear" dataType="string"></field>
            <field name="PropertyFiscalPeriodType" dataType="string"></field>
            <field name="PropertyFinancialFormTypeCode" dataType="string"></field>
            <field name="PropertyCurrentPhysicalOccupancyPercent" dataType="decimal?" />
            <field name="PropertyActivityType" dataType="string" required="true"></field>
            <field name="PropertyActivityDatetime" dataType="datetimeoffset" required="true"></field>
            <field name="PropertyActivityAcquisitionIndicator" dataType="string" required="true"></field>
            <field name="PropertyId" dataType="int?" expression="GENERICID(&quot;PropertyIdLookup&quot;,&quot;PropertyIdentifier&quot;,PropertyIdentifier)" loadFromSource="false"></field>
          </fields>
        </table>
      </source>
      <lookups>
        <lookup type="Components.GenericLookup" name="PropertyIdLookup" dbName="import.Property" indexColumns="PropertyIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="PropertyIdentifier" dataType="int" />
          </fields>
        </lookup>
      </lookups>
      <destinations>
        <destination type="Components.Destination" name="ConvertedTerminatedPropetyFinancials_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.ConvertedTerminatedPropertyFinancialStatement">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="PropertyId" dataType="int" expression="PropertyId"></field>
              <field name="PropertyIdentifier" dataType="int" expression="PropertyIdentifier"></field>
              <field name="PropertyMostRecentNCFAmount" dataType="money?" expression="PropertyMostRecentNCFAmount" />
              <field-TBA name="PropertyFinancialStatementPeriodStartDate" dataType="date?" expression="PropertyFinancialStatementPeriodStartDate" />
              <field name="PropertyPrecedingFiscalYearPhysicalOccupancyPercent" dataType="decimal?" expression="PropertyPrecedingFiscalYearPhysicalOccupancyPercent" />
              <field name="PropertyMostRecentNCFToDSCRFactor" dataType="decimal?" expression="PropertyMostRecentNCFToDSCRFactor" />
              <field name="PropertyPrecedingFiscalYearDSCRNCFFactor" dataType="decimal?" expression="PropertyPrecedingFiscalYearDSCRNCFFactor" />
              <field name="PropertyPrecedingFiscalYearNCFAmount" dataType="money?" expression="PropertyPrecedingFiscalYearNCFAmount" />
              <field name="PropertyFinancialSecondPrecedingFiscalYearDSCRNCFFactor" dataType="decimal?" expression="PropertyFinancialSecondPrecedingFiscalYearDSCRNCFFactor" />
              <field name="PropertyFinancialSecondPrecedingFiscalYearNCFAmount" dataType="money?" expression="PropertyFinancialSecondPrecedingFiscalYearNCFAmount" />
              <field name="PropertyFinancialSecondPrecedingFiscalYearPhysicalOccupancyPercent" dataType="decimal?" expression="PropertyFinancialSecondPrecedingFiscalYearPhysicalOccupancyPercent" />
              <field name="PropertyFinancialStatementPeriodEndDate" dataType="date?" expression="PropertyFinancialStatementPeriodEndDate" />
              <field name="PropertyFinancialStatementFiscalYear" dataType="string" expression="PropertyFinancialStatementFiscalYear" />
              <field name="PropertyFiscalPeriodType" dataType="string" expression="PropertyFiscalPeriodType" />
              <field name="PropertyFinancialFormTypeCode" dataType="string" expression="PropertyFinancialFormTypeCode" />
              <field name="PropertyCurrentPhysicalOccupancyPercent" dataType="decimal?" expression="PropertyCurrentPhysicalOccupancyPercent" />
              <field name="PropertyActivityType" dataType="string" expression="PropertyActivityType" />
              <field name="PropertyActivityDatetime" dataType="datetimeoffset" expression="PropertyActivityDatetime" />
              <field name="PropertyActivityAcquisitionIndicator" dataType="string" expression="PropertyActivityAcquisitionIndicator"></field>
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(PropertyId)" />
            <condition expression="IsNotNull(PropertyActivityType)" />
            <condition expression="IsNotNull(PropertyActivityDatetime)" />
            <condition expression="IsNotNull(PropertyActivityAcquisitionIndicator)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="PropertyId" destinationField="PropertyId" destinationTable="import.PropertyFinancialStatement" />
            <mapping sourceField="PropertyActivityType" destinationField="PropertyActivityType" destinationTable="import.PropertyFinancialStatement" />
            <mapping sourceField="PropertyActivityDatetime" destinationField="PropertyActivityDatetime" destinationTable="import.PropertyFinancialStatement" />
            <mapping sourceField="PropertyActivityAcquisitionIndicator" destinationField="PropertyActivityAcquisitionIndicator" destinationTable="import.PropertyFinancialStatement" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <actionx type="Components.SqlDbAction" name="RefreshPropertyId" forwardExecOnFail="false">
      <operations>
        <operation commandText="UPDATE import.PropertyFinancialsTerminated_Source1 &#xD;&#xA;SET PropertyId = P.Id&#xD;&#xA;FROM import.Property P &#xD;&#xA;WHERE P.PropertyIdentifier = PropertyFinancialsTerminated_Source1.PropertyIdentifier ;" operationType="CommandText" dataSource="context:data" />
      </operations>
    </actionx>
    <actionx type="Components.DataTransferAction" name="Load_PropertyOccupancy" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name="import.PropertyFinancialsTerminated_Source1">
          <loader type="Components.SqlDbLoader" dataSource="context:data" />
          <fields>
            <field name="PropertyId" dataType="int" required="true" primaryValue="true"></field>
            <field name="PropertyIdentifier" dataType="long?" required="true" primaryValue="true" />
            <field name="PropertyCurrentPhysicalOccupancyPercent" dataType="decimal?"></field>
            <field name="PropertyActivityType" dataType="string" required="true" />
            <field name="PropertyActivityDatetime" dataType="datetimeoffset" required="true" />
            <field name="PropertyActivityAcquisitionIndicator" dataType="string" required="true" />
          </fields>
        </table>
      </source>
      <destinations>
        <destination type="Components.Destination" name="PropertyOccupancy_Destination" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.PropertyOccupancy">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="PropertyId" dataType="int" expression="PropertyId" />
              <fieldx name="PropertyPhysicalOccupancyAsOfDate" dataType="date?" expression="PropertyPhysicalOccupancyAsOfDate" />
              <field name="PropertyPhysicalOccupancyPercent" dataType="decimal?" expression="PropertyCurrentPhysicalOccupancyPercent" />
              <field name="PropertyActivityType" dataType="string" expression="PropertyActivityType" />
              <field name="PropertyActivityDatetime" dataType="datetimeoffset" expression="PropertyActivityDatetime" />
              <field name="PropertyActivityAcquisitionIndicator" dataType="string" expression="PropertyActivityAcquisitionIndicator" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(PropertyId)" />
            <condition expression="IsNotNull(PropertyActivityType)" />
            <condition expression="IsNotNull(PropertyActivityDatetime)" />
            <condition expression="IsNotNull(PropertyActivityAcquisitionIndicator)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="PropertyId" destinationField="PropertyId" destinationTable="import.PropertyOccupancy" />
            <mapping sourceField="PropertyActivityType" destinationField="PropertyActivityType" destinationTable="import.PropertyOccupancy" />
            <mapping sourceField="PropertyActivityDatetime" destinationField="PropertyActivityDatetime" destinationTable="import.PropertyOccupancy" />
            <mapping sourceField="PropertyActivityAcquisitionIndicator" destinationField="PropertyActivityAcquisitionIndicator" destinationTable="import.PropertyOccupancy" />
          </matchmappings>
        </destination>
      </destinations>
    </actionx>
  </actions>
</job>