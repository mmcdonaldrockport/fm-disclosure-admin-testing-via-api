﻿<?xml version="1.0" encoding="utf-8"?>
<job type="Components.Job" id="20170301" name="LoanPrepaymmentConversion" description="Import LoanPrepaymment Conversion Data" useTransaction="true" note="V1.0.5" note1="Edit generated shell imp; add Lookups and validators." note2="Add action for Protection Type = &quot;Prepayment Premium&quot; having different required fields" note3="Add actions for Premium Types having different required fields" note4="Ready version 1.0.4" note5="Add post-process operation to delete from PrepaymentProtectionTerm if the record has error and not inserted to PrepaymentPremiumTerm">
  <context>
    <database server="DVFM01-D03\DVFM01_D03" dataDbName="DEV_LL_DisclosureAdmin_QA" />
    <log>
      <appender name="console" level="INFO" />
      <appender name="file" level="DEBUG" filePath="Clients\FNMA\Logs\{1}_LoanPrepaymmentConversion_{0}.log" />
      <appender name="database" level="OFF" />
    </log>
    <summary sendEmail="false" mailTo="llee@rockportllc.com" />
  </context>
  <actions>
    <action type="Components.DataTransferAction" name="Load_LoanPrepaymentProtection" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name-StgTable="import.LoanPrepayment_Source1" name="LoanPrepaymentProtection_Source">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT FannieMaeLoanIdentifier, LoanIdentifier&#xD;&#xA;, LoanPrepaymentProtectionType&#xD;&#xA;, LoanPrepaymentProtectionTerm, LoanPrepaymentProtectionEndDate&#xD;&#xA;FROM [import].[LoanPrepayment_Source1] &#xD;&#xA;WHERE LoanPrepaymentProtectionType NOT IN ('Prepayment Premium') " />
          <fields>
            <field name="FannieMaeLoanIdentifier" dataType="string" required="true"></field>
            <field name="LoanIdentifier" dataType="long" required="true" primaryValue="true">
              <validators>
                <validator type="Components.LookupValidator" expression="GENERICID(&quot;LoanIdLookup&quot;,&quot;LoanIdentifier&quot;,LoanIdentifier)" generateError="true" message="&quot;'{1}' was not found. {0} does not match in Loan Table&quot;" />
              </validators>
            </field>
            <field name="LoanPrepaymentProtectionType" dataType="string" required="true"></field>
            <field name="LoanPrepaymentProtectionEndDate" dataType="date?" required="true" note="conditional except for &quot;Prepayment Premium&quot;" />
            <field name="LoanPrepaymentProtectionTerm" dataType="long?" required="true" note="conditional except for &quot;Prepayment Premium&quot;" />
            <field name="LoanId" dataType="int?" expression="GENERICID(&quot;LoanIdLookup&quot;,&quot;LoanIdentifier&quot;,LoanIdentifier)" loadFromSource="false"></field>
          </fields>
        </table>
      </source>
      <lookups>
        <lookup type="Components.GenericLookup" name="LoanIdLookup" dbName="import.Loan" indexColumns="LoanIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="LoanIdentifier" dataType="long" />
          </fields>
        </lookup>
      </lookups>
      <destinations>
        <destination type="Components.Destination" name="LoanPrepaymentProtectionTerm_Destination1" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.LoanPrepaymentProtectionTerm">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="LoanId" dataType="int" expression="LoanId"></field>
              <field name="LoanPrepaymentProtectionType" dataType="string" expression="LoanPrepaymentProtectionType" />
              <field name="LoanPrepaymentProtectionTerm" dataType="long?" expression="LoanPrepaymentProtectionTerm" />
              <field name="LoanPrepaymentProtectionEndDate" dataType="date?" expression="LoanPrepaymentProtectionEndDate" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(LoanId)" />
            <condition expression="IsNotNull(LoanPrepaymentProtectionType)" />
            <condition expression="IsNotNull(LoanPrepaymentProtectionTerm)" />
            <condition expression="IsNotNull(LoanPrepaymentProtectionEndDate)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="LoanId" destinationField="LoanId" destinationTable="import.LoanPrepaymentProtectionTerm" />
            <mapping sourceField="LoanPrepaymentProtectionType" destinationField="LoanPrepaymentProtectionType" destinationTable="import.LoanPrepaymentProtectionTerm" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.DataTransferAction" name="Load_LoanPrepaymentProtectionPremium" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name-StgTable="import.LoanPrepayment_Source1" name="LoanPrepaymentProtectionPremium_Source">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT FannieMaeLoanIdentifier, LoanIdentifier&#xD;&#xA;, LoanPrepaymentProtectionType&#xD;&#xA;, LoanPrepaymentProtectionTerm, LoanPrepaymentProtectionEndDate&#xD;&#xA;FROM [import].[LoanPrepayment_Source1] &#xD;&#xA;WHERE LoanPrepaymentProtectionType IN ('Prepayment Premium') " />
          <fields>
            <field name="FannieMaeLoanIdentifier" dataType="string" required="true"></field>
            <field name="LoanIdentifier" dataType="long" required="true" primaryValue="true">
              <validators>
                <validator type="Components.LookupValidator" expression="GENERICID(&quot;LoanIdLookup&quot;,&quot;LoanIdentifier&quot;,LoanIdentifier)" generateError="true" message="&quot;'{1}' was not found. {0} does not match in Loan Table&quot;" />
              </validators>
            </field>
            <field name="LoanPrepaymentProtectionType" dataType="string" required="true"></field>
            <field name="LoanPrepaymentProtectionEndDate" dataType="date?" requiredx="true" note="conditional except for &quot;Prepayment Premium&quot;" />
            <field name="LoanPrepaymentProtectionTerm" dataType="long?" requiredx="true" note="conditional except for &quot;Prepayment Premium&quot;" />
            <field name="LoanId" dataType="int?" expression="GENERICID(&quot;LoanIdLookup&quot;,&quot;LoanIdentifier&quot;,LoanIdentifier)" loadFromSource="false"></field>
          </fields>
        </table>
      </source>
      <lookups>
        <lookup type="Components.GenericLookup" name="LoanIdLookup" dbName="import.Loan" indexColumns="LoanIdentifier">
          <fields>
            <field name="Id" dataType="int" primaryKey="true" />
            <field name="LoanIdentifier" dataType="long" />
          </fields>
        </lookup>
      </lookups>
      <destinations>
        <destination type="Components.Destination" name="LoanPrepaymentProtectionTerm_Destination2" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.LoanPrepaymentProtectionTerm">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="LoanId" dataType="int" expression="LoanId"></field>
              <field name="LoanPrepaymentProtectionType" dataType="string" expression="LoanPrepaymentProtectionType" />
              <field name="LoanPrepaymentProtectionTerm" dataType="long?" expression="LoanPrepaymentProtectionTerm" />
              <field name="LoanPrepaymentProtectionEndDate" dataType="date?" expression="LoanPrepaymentProtectionEndDate" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(LoanId)" />
            <condition expression="IsNotNull(LoanPrepaymentProtectionType)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="LoanId" destinationField="LoanId" destinationTable="import.LoanPrepaymentProtectionTerm" />
            <mapping sourceField="LoanPrepaymentProtectionType" destinationField="LoanPrepaymentProtectionType" destinationTable="import.LoanPrepaymentProtectionTerm" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.DataTransferAction" name="Load_LoanPrepaymentPremiumTerm1" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name-StgTable="import.LoanPrepayment_Source1" name="LoanPrepaymentPremiumTerm_Source">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT FannieMaeLoanIdentifier&#xD;&#xA;, LP.LoanIdentifier&#xD;&#xA;, LoanDecliningPrepaymentPremiumType&#xD;&#xA;, LoanPrepaymentPremiumType&#xD;&#xA;, LoanOtherPrepaymentPremiumDescription&#xD;&#xA;, LoanPrepaymentPremiumTerm&#xD;&#xA;, LoanPrepaymentPremiumEndDate&#xD;&#xA;, L.Id AS LoanId&#xD;&#xA;, LPPT.Id AS LoanPrepaymentProtectionTermId&#xD;&#xA;FROM [import].[LoanPrepayment_Source1] LP&#xD;&#xA;LEFT JOIN import.Loan L ON L.LoanIdentifier = LP.LoanIdentifier&#xD;&#xA;LEFT JOIN import.LoanPrepaymentProtectionTerm LPPT ON LPPT.LoanId = L.Id&#xD;&#xA;WHERE LP.LoanPrepaymentProtectionType IN ('Prepayment Premium') -- 18744 &#xD;&#xA;&#x9;AND LP.LoanPrepaymentPremiumType NOT IN ('Declining Premium','Other Prepayment')" />
          <fields>
            <field name="FannieMaeLoanIdentifier" dataType="string" required="true"></field>
            <field name="LoanIdentifier" dataType="long" required="true" primaryValue="true">
              <validatorsx>
                <validator type="Components.LookupValidator" expression="GENERICID(&quot;LoanIdLookup&quot;,&quot;LoanIdentifier&quot;,LoanIdentifier)" generateError="true" message="&quot;'{1}' was not found. {0} does not match in Loan Table&quot;" />
              </validatorsx>
            </field>
            <field name="LoanDecliningPrepaymentPremiumType" dataType="string" required-cond="true" note="If LoanPrepaymentPremiumType = &quot;Declining Premium&quot; then required"></field>
            <field name="LoanPrepaymentPremiumType" dataType="string" required="true" />
            <field name="LoanOtherPrepaymentPremiumDescription" dataType="string" required-cond="true" note="If LoanPrepaymentPremiumType = &quot;Other Prepayment&quot; then required" />
            <field name="LoanPrepaymentPremiumTerm" dataType="long?" required="true" />
            <field name="LoanPrepaymentPremiumEndDate" dataType="date?" required="true" />
            <field name="LoanId" dataType="int?" />
            <field name="LoanPrepaymentProtectionTermId" dataType="int?" />
          </fields>
        </table>
      </source>
      <destinations>
        <destination type="Components.Destination" name="LoanPrepaymentPremiumTerm_Destination1" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.LoanPrepaymentPremiumTerm">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="LoanPrepaymentProtectionTermId" dataType="int?" expression="LoanPrepaymentProtectionTermId" />
              <field name="LoanPrepaymentDecliningPremiumType" dataType="string" expression="LoanDecliningPrepaymentPremiumType"></field>
              <fieldx name="LoanOtherPrepaymentPremiumDescription" dataType="string" expression="LoanOtherPrepaymentPremiumDescription" />
              <field name="LoanPrepaymentPremiumEndDate" dataType="date?" expression="LoanPrepaymentPremiumEndDate" />
              <field name="LoanPrepaymentPremiumTerm" dataType="long?" expression="LoanPrepaymentPremiumTerm" />
              <field name="LoanPrepaymentPremiumType" dataType="string" expression="LoanPrepaymentPremiumType" />
              <field name="LoanOtherPrepaymentPremiumDescription" dataType="string" expression="LoanOtherPrepaymentPremiumDescription" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(LoanPrepaymentProtectionTermId)" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumType)" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumTerm)" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumEndDate)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="LoanPrepaymentProtectionTermId" destinationField="LoanPrepaymentProtectionTermId" destinationTable="import.LoanPrepaymentPremiumTerm" />
            <mapping sourceField="LoanPrepaymentPremiumType" destinationField="LoanPrepaymentPremiumType" destinationTable="import.LoanPrepaymentPremiumTerm" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.DataTransferAction" name="Load_LoanPrepaymentPremiumTerm2" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name-StgTable="import.LoanPrepayment_Source1" name="LoanPrepaymentDecliningPremium_Source">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT FannieMaeLoanIdentifier&#xD;&#xA;, LP.LoanIdentifier&#xD;&#xA;, LoanDecliningPrepaymentPremiumType&#xD;&#xA;, LoanPrepaymentPremiumType&#xD;&#xA;, LoanPrepaymentPremiumTerm&#xD;&#xA;, LoanPrepaymentPremiumEndDate&#xD;&#xA;, L.Id AS LoanId&#xD;&#xA;, LPPT.Id AS LoanPrepaymentProtectionTermId&#xD;&#xA;FROM [import].[LoanPrepayment_Source1] LP&#xD;&#xA;LEFT JOIN import.Loan L ON L.LoanIdentifier = LP.LoanIdentifier&#xD;&#xA;LEFT JOIN import.LoanPrepaymentProtectionTerm LPPT ON LPPT.LoanId = L.Id&#xD;&#xA;WHERE  LP.LoanPrepaymentProtectionType IN ('Prepayment Premium')&#xD;&#xA;&#x9;AND LP.LoanPrepaymentPremiumType = 'Declining Premium' " />
          <fields>
            <field name="FannieMaeLoanIdentifier" dataType="string" required="true"></field>
            <field name="LoanIdentifier" dataType="long" required="true" primaryValue="true"></field>
            <field name="LoanDecliningPrepaymentPremiumType" dataType="string" required="true" note="If LoanPrepaymentPremiumType = &quot;Declining Premium&quot; then required"></field>
            <field name="LoanPrepaymentPremiumType" dataType="string" required="true" />
            <field name="LoanPrepaymentPremiumTerm" dataType="long?" required="true" />
            <field name="LoanPrepaymentPremiumEndDate" dataType="date?" required="true" />
            <field name="LoanId" dataType="int?" />
            <field name="LoanPrepaymentProtectionTermId" dataType="int?" />
          </fields>
        </table>
      </source>
      <destinations>
        <destination type="Components.Destination" name="LoanPrepaymentPremiumTerm_Destination2" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.LoanPrepaymentPremiumTerm">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="LoanPrepaymentProtectionTermId" dataType="int?" expression="LoanPrepaymentProtectionTermId" />
              <field name="LoanPrepaymentDecliningPremiumType" dataType="string" expression="LoanDecliningPrepaymentPremiumType"></field>
              <field name="LoanPrepaymentPremiumEndDate" dataType="date?" expression="LoanPrepaymentPremiumEndDate" />
              <field name="LoanPrepaymentPremiumTerm" dataType="long?" expression="LoanPrepaymentPremiumTerm" />
              <field name="LoanPrepaymentPremiumType" dataType="string" expression="LoanPrepaymentPremiumType" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(LoanPrepaymentProtectionTermId)" />
            <conditionx expression="IsNotNull(LoanPrepaymentPremiumType)" note="not needed since source query includes this value for condition" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumTerm)" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumEndDate)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="LoanPrepaymentProtectionTermId" destinationField="LoanPrepaymentProtectionTermId" destinationTable="import.LoanPrepaymentPremiumTerm" />
            <mapping sourceField="LoanPrepaymentPremiumType" destinationField="LoanPrepaymentPremiumType" destinationTable="import.LoanPrepaymentPremiumTerm" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.DataTransferAction" name="Load_LoanPrepaymentPremiumTerm3" dateFormats="M/d/yyyy h:mm:ss tt" saveData="true" forwardExecOnFail="true">
      <source type="Components.Source" dataRejectLevel="Row">
        <table name-StgTable="import.LoanPrepayment_Source1" name="LoanPrepaymentOtherPrepayment_Source">
          <loader type="Components.SqlDbLoader" dataSource="context:data" query="SELECT FannieMaeLoanIdentifier&#xD;&#xA;, LP.LoanIdentifier&#xD;&#xA;--, LoanDecliningPrepaymentPremiumType&#xD;&#xA;, LoanPrepaymentPremiumType&#xD;&#xA;, LoanOtherPrepaymentPremiumDescription&#xD;&#xA;, LoanPrepaymentPremiumTerm&#xD;&#xA;, LoanPrepaymentPremiumEndDate&#xD;&#xA;, L.Id AS LoanId&#xD;&#xA;, LPPT.Id AS LoanPrepaymentProtectionTermId&#xD;&#xA;FROM [import].[LoanPrepayment_Source1] LP&#xD;&#xA;LEFT JOIN import.Loan L ON L.LoanIdentifier = LP.LoanIdentifier&#xD;&#xA;LEFT JOIN import.LoanPrepaymentProtectionTerm LPPT ON LPPT.LoanId = L.Id&#xD;&#xA;WHERE LP.LoanPrepaymentProtectionType IN ('Prepayment Premium') &#xD;&#xA;&#x9;AND LP.LoanPrepaymentPremiumType = 'Other Prepayment' " />
          <fields>
            <field name="FannieMaeLoanIdentifier" dataType="string" required="true"></field>
            <field name="LoanIdentifier" dataType="long" required="true" primaryValue="true"></field>
            <field name="LoanPrepaymentPremiumType" dataType="string" required="true" />
            <field name="LoanOtherPrepaymentPremiumDescription" dataType="string" required="true" />
            <field name="LoanPrepaymentPremiumTerm" dataType="long?" required="true" />
            <field name="LoanPrepaymentPremiumEndDate" dataType="date?" required="true" />
            <field name="LoanId" dataType="int?" />
            <field name="LoanPrepaymentProtectionTermId" dataType="int?" />
          </fields>
        </table>
      </source>
      <destinations>
        <destination type="Components.Destination" name="LoanPrepaymentPremiumTerm_Destination3" mode="Upsert" indexedSearch="true" sourceDataUnique="false">
          <table name="import.LoanPrepaymentPremiumTerm">
            <loader type="Components.SqlDbLoader" dataSource="context:data" />
            <writer type="Components.SqlDbWriter" dataSource="context:data" />
            <fields>
              <field name="LoanPrepaymentProtectionTermId" dataType="int?" expression="LoanPrepaymentProtectionTermId" />
              <field name="LoanOtherPrepaymentPremiumDescription" dataType="string" expression="LoanOtherPrepaymentPremiumDescription"></field>
              <field name="LoanPrepaymentPremiumEndDate" dataType="date?" expression="LoanPrepaymentPremiumEndDate" />
              <field name="LoanPrepaymentPremiumTerm" dataType="long?" expression="LoanPrepaymentPremiumTerm" />
              <field name="LoanPrepaymentPremiumType" dataType="string" expression="LoanPrepaymentPremiumType" />
            </fields>
          </table>
          <processconditions>
            <condition expression="IsNotNull(LoanPrepaymentProtectionTermId)" />
            <conditionx expression="IsNotNull(LoanPrepaymentPremiumType)" note="not needed since source query includes this value for condition" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumTerm)" />
            <condition expression="IsNotNull(LoanPrepaymentPremiumEndDate)" />
          </processconditions>
          <matchmappings>
            <mapping sourceField="LoanPrepaymentProtectionTermId" destinationField="LoanPrepaymentProtectionTermId" destinationTable="import.LoanPrepaymentPremiumTerm" />
            <mapping sourceField="LoanPrepaymentPremiumType" destinationField="LoanPrepaymentPremiumType" destinationTable="import.LoanPrepaymentPremiumTerm" />
          </matchmappings>
        </destination>
      </destinations>
    </action>
    <action type="Components.SqlDbAction" name="PostProcess_LoanPrepaymentProtectionTerm" forwardExecOnFail="false">
      <operations>
        <operation commandText="DELETE import.LoanPrepaymentProtectionTerm &#xD;&#xA;FROM (&#xD;&#xA;SELECT LPP.Id, LoanId, LoanPrepaymentProtectionType &#xD;&#xA;FROM import.LoanPrepaymentProtectionTerm LPP&#xD;&#xA;LEFT JOIN import.LoanPrepaymentPremiumTerm LPPT ON LPPT.[LoanPrepaymentProtectionTermId] = LPP.Id&#xD;&#xA;WHERE LoanPrepaymentProtectionType = 'Prepayment Premium'&#xD;&#xA;&#x9;AND LPPT.Id IS NULL &#xD;&#xA;&#x9;) LPPT&#xD;&#xA;WHERE LPPT.LoanId = import.LoanPrepaymentProtectionTerm.LoanId;" operationType="CommandText" dataSource="context:data" />
      </operations>
    </action>
  </actions>
</job>