exec dbo.execute_ssis_package_sample
	'Load Deal Source.dtsx',
	'C:\DataConversion\DealDataFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (2) Deal Monitoring
exec dbo.execute_ssis_package_sample
	'Load DealMonitoring Source.dtsx',
	'C:\DataConversion\DealMonitoringFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (3) Mega 
exec dbo.execute_ssis_package_sample
	'Load Mega Source.dtsx',
	'C:\DataConversion\MegaFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (4) Mega Collateral
exec dbo.execute_ssis_package_sample
	'Load MegaCollateral Source.dtsx',
	'C:\DataConversion\MegaCollateralFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (5) Pool Factor Plus
exec dbo.execute_ssis_package_sample
	'Load PoolFactorPlus Source.dtsx',
	'C:\DataConversion\PoolFactorPlusFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (6) Remic
exec dbo.execute_ssis_package_sample
	'Load REMIC Source.dtsx',
	'C:\DataConversion\RemicFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (7) Remic Collateral
exec dbo.execute_ssis_package_sample
	'Load RemicCollateral Source.dtsx',
	'C:\DataConversion\RemicCollateralFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

-- (8) Security
exec dbo.execute_ssis_package_sample
	'Load Security Source.dtsx',
	'C:\DataConversion\SecurityFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO
