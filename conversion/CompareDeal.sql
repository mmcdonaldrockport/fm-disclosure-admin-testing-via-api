SELECT 'source', s.* FROM source.Deal s 
	FULL OUTER 
	JOIN extract.Deal e ON
	s.[Column 0] = e.[Column 0] AND
	s.[Column 1] = e.[Column 1] AND
	s.[Column 2] = e.[Column 2] AND
	s.[Column 3] = e.[Column 3] AND
	s.[Column 4] = e.[Column 4] AND
	s.[Column 5] = e.[Column 5] AND
	s.[Column 6] = e.[Column 6]
WHERE 
	--s.[Column 0] IS NULL
	e.[Column 0] IS NULL
UNION ALL
SELECT 'extract', e.* FROM source.Deal s 
	FULL OUTER 
	JOIN extract.Deal e ON
	s.[Column 0] = e.[Column 0] AND
	s.[Column 1] = e.[Column 1] AND
	s.[Column 2] = e.[Column 2] AND
	s.[Column 3] = e.[Column 3] AND
	s.[Column 4] = e.[Column 4] AND
	s.[Column 5] = e.[Column 5] AND
	s.[Column 6] = e.[Column 6]
WHERE 
	s.[Column 0] IS NULL 
ORDER BY 2, 3, 4, 5, 6, 7, 1 

SELECT * FROM DisclosureAdmin3.adm.TransferMessageDetail
