exec dbo.execute_ssis_package_sample
	'Load LoanAdditionalDebt Source.dtsx',
	'C:\DataConversion\LoanAdditionalDebtFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

exec dbo.execute_ssis_package_sample
	'Load Loan Source.dtsx',
	'C:\DataConversion\LoanDataFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

exec dbo.execute_ssis_package_sample
	'Load Loan DMBS Source.dtsx',
	'C:\DataConversion\LoanDataFileDMBS.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

exec dbo.execute_ssis_package_sample
	'Load LoanDelinquency Source.dtsx',
	'C:\DataConversion\LoanDelinquencyFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO

exec dbo.execute_ssis_package_sample
	'Load LoanPrepayment Source.dtsx',
	'C:\DataConversion\LoanPrepaymentFile.csv',
	'DVFM01-D03\QAFM01_D04',
	'DisclosureAdmin3'

GO


