SELECT LoanAdditionalDebtBalloonIndicator, LoanIdentifier,
       LoanAdditionalDebtBalanceAmount, 
       LoanMezzanineAcquisitionUPBAmount,
       LoanAdditionalDebtType, LoanAdditionalDebtMezzanineFinancingType, LoanAdditionalDebtLienPriorityType
FROM import.LoanAdditionalDebt_Source
WHERE LoanAdditionalDebtType NOT LIKE '%Mezzanine%'
AND LoanMezzanineAcquisitionUPBAmount IS NOT NULL
/*
20867-LoanMezzanineAcquisitionUPBAmount missing from extract   (2)
21200-LoanMezzanineAmortizationTerm  missing from extract  (1)
21202-LoanMezzanineDecliningPrepaymentPremiumType missing from extract (4)
21203-LoanMezzaninePrepaymentEndDate missing from extract (1)
21204-LoanMezzaninePrepaymentTerm missing from extract (2)
*/
------------------------------------------------------------------------
SELECT L.LoanIdentifier,
       LAD.LoanAdditionalDebtIdentifier,
       LAD.LoanAdditionalDebtType,
       [17240_LoanAdditionalDebtBalanceAmount] = CASE WHEN LAD.LoanAdditionalDebtType NOT LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount ELSE NULL END,
       [20867_LoanMezzanineAcquisitionUPBAmount] = CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount ELSE NULL END,
       [21191_LoanAdditionalDebtOriginalAmortizationTerm] = CASE WHEN LAD.LoanAdditionalDebtType NOT LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END,
       [21200_LoanMezzanineAmortizationTerm] = CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END         
FROM FmData.LoanAdditionalDebt LAD
     INNER JOIN FmData.Loan L ON L.Id = LAD.LoanId
WHERE L.LoanIdentifier IN (495, 27080, 524, 611, 667)
ORDER BY LAD.LoanAdditionalDebtIdentifier
------------------------------------------------------------------------
SELECT  LAD.LoanId,
        [21200_LoanMezzanineAmortizationTerm] = NULL,
        [21202_LoanMezzanineDecliningPrepaymentPremiumType] = LMT.LoanMezzanineDecliningPrepaymentPremiumType,
        [00000_LoanMezzanineDecliningPrepaymentPremiumType] = LMT1.LoanMezzanineDecliningPrepaymentPremiumType,
        [21203_LoanMezzaninePrepaymentEndDate] = LMT.LoanMezzaninePrepaymentProtectionEndDate,
        [00000_LoanMezzaninePrepaymentEndDate] = LMT1.LoanMezzaninePrepaymentProtectionEndDate,
        [21204_LoanMezzaninePrepaymentTerm] = LMT.LoanMezzaninePrepaymentProtectionTerm,
        [00000_LoanMezzaninePrepaymentTerm] = LMT1.LoanMezzaninePrepaymentProtectionTerm,
        LAD.LoanAdditionalDebtType, LAD.LoanMezzanineFinancingType
FROM    FmData.LoanAdditionalDebt LAD
        LEFT JOIN FmData.LoanMezzaninePrepaymentProtectionTerm LMT ON (LMT.LoanAdditionalDebtId = LAD.Id)
        LEFT JOIN import.LoanMezzaninePrepaymentProtectionTerm LMT1 ON (LMT1.LoanAdditionalDebtId = LAD.Id)
WHERE LAD.LoanAdditionalDebtIdentifier IN ( 2243, 1972, 1971, 1970)
------------------------------------------------------------------------
SELECT L.LoanIdentifier,
       LAD.LoanAdditionalDebtIdentifier,
       LAD.LoanAdditionalDebtType,
       [17240_LoanAdditionalDebtBalanceAmount] = CASE WHEN LAD.LoanAdditionalDebtType NOT LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount ELSE NULL END,
       [20867_LoanMezzanineAcquisitionUPBAmount] = CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount ELSE NULL END,
       [21191_LoanAdditionalDebtOriginalAmortizationTerm] = CASE WHEN LAD.LoanAdditionalDebtType NOT LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END,
       [21200_LoanMezzanineAmortizationTerm] = CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END         
FROM FmData.LoanAdditionalDebt LAD
     INNER JOIN FmData.Loan L ON L.Id = LAD.LoanId
WHERE L.LoanIdentifier IN (495, 611, 667, 27080, 524)
ORDER BY LAD.LoanAdditionalDebtIdentifier
---------------------------------------------------------------------
SELECT  LAD.LoanId, l.LoanIdentifier, LAD.LoanAdditionalDebtIdentifier,
		LAD.LoanAdditionalDebtType, LAD.LoanMezzanineFinancingType,
		[20867_LoanMezzanineAcquisitionUPBAmount] = LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount,
			--CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtUnpaidPrincipalBalanceAmount ELSE NULL END,
        [21200_LoanMezzanineAmortizationTerm] = LAD.LoanAdditionalDebtAmortizationTerm,
			--CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END,
		--[00000_LoanMezzanineAmortizationTerm] = CASE WHEN LAD.LoanAdditionalDebtType LIKE '%Mezzanine%' THEN LAD.LoanAdditionalDebtAmortizationTerm ELSE NULL END,
        [21202_LoanMezzanineDecliningPrepaymentPremiumType] = LMT.LoanMezzanineDecliningPrepaymentPremiumType,
        --[00000_LoanMezzanineDecliningPrepaymentPremiumType] = LMT1.LoanMezzanineDecliningPrepaymentPremiumType,
        [21203_LoanMezzaninePrepaymentEndDate] = LMT.LoanMezzaninePrepaymentProtectionEndDate,
        --[00000_LoanMezzaninePrepaymentEndDate] = LMT1.LoanMezzaninePrepaymentProtectionEndDate,
        [21204_LoanMezzaninePrepaymentTerm] = LMT.LoanMezzaninePrepaymentProtectionTerm
        --[00000_LoanMezzaninePrepaymentTerm] = LMT1.LoanMezzaninePrepaymentProtectionTerm,
        
FROM    FmData.LoanAdditionalDebt LAD
        LEFT JOIN FmData.LoanMezzaninePrepaymentProtectionTerm LMT ON (LMT.LoanAdditionalDebtId = LAD.Id)
        --LEFT JOIN import.LoanMezzaninePrepaymentProtectionTerm LMT1 ON (LMT1.LoanAdditionalDebtId = LAD.Id)
		JOIN FmData.Loan l ON l.id = LAD.LoanId
WHERE LAD.LoanAdditionalDebtIdentifier IN (1972, 1971, 1970, 2243, 73) --LoanIdentifier: 524;611;667;27080;495;
Order By LoanIdentifier
------------------------------------------------------------------------