# README #

### What is this repository for? ###

* Files for testing FM Disclosure Admin via API and DB with SoapUI
* Files for NeoLoad load testing
* Version 0.1
[comment]: # ([Learn Markdown](https://bitbucket.org/tutorials/markdowndemo))

### How do I get set up? ###

* Summary of set up
    * Ready!API 1.9 with SoapUI NG (paid version) installed
    * HyperSQL database (included in these files)
    * Read/update access to FM Disclosure Admin DB for the environment under test
* Source Tree
    * **/** - contains SoapUI project file
    * **/IMP** - data transfer implementation files
    * **/SQL scripts** - DB query and manipulation scripts
    * **/config**
    * **/datafiles** - data transfer files
    * **/db** - contains local DB (HSQLDB) to hold test state and DB manager utility (DBMgr.bat)
    * **/docs**
    * **/example reports/Bulk Import Report**
    * **/example transfer files**
    * **/reports** - destination for data transfer test result reports
    * **/scenarios** - data transfer scenarios
    * **/scripts** - generic scripts and utilities
    * **/soapui_dependencies/jars** - additional jar files for SoapUI project
    * **/temp**
    * **/templates**
    * **/zippeddata**
* Configuration
* Dependencies
* Database configuration
* How to run tests
[comment]: # (* Deployment instructions)

### Contribution guidelines ###

* Writing tests
[comment]: # (* Code review)
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact