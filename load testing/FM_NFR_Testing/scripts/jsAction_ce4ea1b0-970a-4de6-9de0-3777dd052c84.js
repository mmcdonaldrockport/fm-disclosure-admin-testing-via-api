﻿var serviceUser = context.variableManager.parseString("${config_${env.Env}.serviceUser}");
var servicePassword = context.variableManager.parseString("${config_${env.Env}.servicePassword}");
var str = new java.lang.String(serviceUser + ':' + servicePassword);
logger.info("user + password = " + str);

var basicAuth = SecurityUtils.getBasicAuth(serviceUser, servicePassword);
logger.info("basic auth = " + basicAuth);
//var authDigest = "Basic " + org.apache.commons.codec.binary.Base64.encodeBase64(token);
//context.variableManager.setValue("BasicAuthDigest",authDigest);
context.variableManager.setValue("BasicAuth",basicAuth);