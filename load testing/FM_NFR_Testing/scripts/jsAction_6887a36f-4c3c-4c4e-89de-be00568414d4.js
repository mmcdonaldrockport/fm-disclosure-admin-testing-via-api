﻿var qSize = context.variableManager.getSharedQueueSize('ReturnToHoldSQ')
//context.variableManager.setValue("ReturnToHoldSQSize",size);
if (qSize < 0 ) {
        context.fail("TransactionsSQ does not exist.");
}

if (qSize == 0) { // queue empty
    var dbServer = context.variableManager.getValue('config_${env.Env}.adminDBServer');
    var dbInstance = context.variableManager.getValue('config_${env.Env}.adminDBInstance');
    var db = context.variableManager.getValue('config_${env.Env}.adminDB');

    var tList = com.rockportllc.neoload.utils.DBUtils.getReturnToHoldIds(dbServer, dbInstance, db);
    
    var transCount = tList.size();
    for (t = 0; t < transCount; t++) {
        context.variableManager.addSharedValue("ReturnToHoldSQ", tList.get(t));
        //logger.info(tList.get(t));
    }
}