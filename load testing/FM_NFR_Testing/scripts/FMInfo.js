﻿if (typeof FMInfo !== "object") {
    FMInfo = {};
};

FMInfo.QueueIds = {
    "OPS":17,
    "AD":18,
    "ST": 19
}

FMInfo.serviceLookup = {
    "01":"01-PreCloseMBSAdminSetup",
    "02":"02-AtIssuanceMBSDisclosure",
    "03":"03-AtIssuanceMBSDisclosureCorrection",
    "04":"04-AtIssuanceMegaDisclosure",
    "09":"09-SecurityCollapse",
    "10":"10-PropertyInspectionData",
    "11":"11-PropertyFinancialData",
    "12":"12-CreditFacilityPropertyMonitoringFinancialData",
    "13":"13-CreditFacilityDealMonitoringFinancialData",
    "14":"14-CollateralChange",
    "16":"16-LoanServicingData",
    "17":"17-SecurityServicingData",
    "18":"18-SecurityDissolve",
    "19":"19-SecurityPaidinFullMatured",
    "20":"20-PropertyIdentifierUpdated",
    "21":"21-AtIssuanceREMICDisclosure",
    "23":"23-CreditFacilitiesDealUpdate",
    "24":"24-CreditFacilitiesDealUpdateCorrection",
    "25":"25-CreditFacilitiesPropertyUpdate"
    
};



FMInfo.issuanceDocTypes= {
    1:{typeName:"AnnexA", filetype:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",ext:"xlsx"},
    2:{typeName:"AnnexACorrected", filetype:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",ext:"xlsx"},
    3:{typeName:"AnnexAFinal", filetype:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",ext:"xlsx"},
    4:{typeName:"UnitaryProspectusCoverPage", filetype:"application/pdf",ext:"pdf"},
    5:{typeName:"UnitaryProspectusCoverPageCorrected", filetype:"application/pdf",ext:"pdf"},
    6:{typeName:"UnitaryProspectusCoverPageFinal", filetype:"application/pdf",ext:"pdf"},
    7:{typeName:"UnitaryProspectusTemplate", filetype:"application/pdf",ext:"pdf"},
    8:{typeName:"UnitaryProspectus", filetype:"application/pdf",ext:"pdf"},
    9:{typeName:"UnitaryProspectus-Corrected", filetype:"application/pdf",ext:"pdf"},
    10:{typeName:"UnitaryProspectus-Final", filetype:"application/pdf",ext:"pdf"},
    11:{typeName:"IssueSupplement", filetype:"application/pdf",ext:"pdf"},
    12:{typeName:"TrustAgreement", filetype:"application/pdf",ext:"pdf"},
    13:{typeName:"Prospectus", filetype:"application/pdf",ext:"pdf"},
    14:{typeName:"ProspectusSupplementNarrative", filetype:"application/pdf",ext:"pdf"},
    15:{typeName:"ScheduleA", filetype:"application/pdf",ext:"pdf"},
    16:{typeName:"ScheduleofMortgage", filetype:"application/pdf",ext:"pdf"},
    17:{typeName:"ScheduleofPoolandLoanInformation", filetype:"application/pdf",ext:"pdf"},
    18:{typeName:"PoolStatistics", filetype:"application/pdf",ext:"pdf"},
    19:{typeName:"CurrentScheduleA", filetype:"application/pdf",ext:"pdf"},
   
};