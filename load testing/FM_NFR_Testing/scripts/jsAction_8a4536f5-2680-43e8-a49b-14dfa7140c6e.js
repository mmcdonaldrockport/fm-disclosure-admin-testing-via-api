﻿var vuserId = context.variableManager.getValue("VUserId");
var userRoleName = "Role for user " + vuserId;
context.variableManager.setValue("UserRoleName",userRoleName);

// Get response content  from VariableManager
var resp = context.variableManager.getValue("UserRoleResponse");

// convert to JSON object
var jsonObj = JSON.parse(resp);

var userRoleId = JSONUtils.jsonPath(jsonObj,"$.[?(@.Name == '" + userRoleName + "')][Id]");
context.variableManager.setValue("UserRoleId",userRoleId);


var accessRoleId = JSONUtils.jsonPath(jsonObj, "$.[?(@.Name == 'Admin')].[AccessRights][0].[RoleId]");
var pageFunction = JSONUtils.jsonPath(jsonObj, "$.[?(@.Name == 'Admin')].[AccessRights][0].[PageFunctionCode]");
var allowedAction =JSONUtils.jsonPath(jsonObj, "$.[?(@.Name == 'Admin')].[AccessRights][0].[AllowedAction]");
var accessId = 	JSONUtils.jsonPath(jsonObj, "$.[?(@.Name == 'Admin')].[AccessRights][0].[Id]");

context.variableManager.setValue("AccessRoleId", accessRoleId);
context.variableManager.setValue("PageFunction", pageFunction);
context.variableManager.setValue("AllowedAction", allowedAction);
context.variableManager.setValue("AccessId", accessId);

logger.info("UserRoleName = " + userRoleName);
logger.info("UserRoleId = " + userRoleId);