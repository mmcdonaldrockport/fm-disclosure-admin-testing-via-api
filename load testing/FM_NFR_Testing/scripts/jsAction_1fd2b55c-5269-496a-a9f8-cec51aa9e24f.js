﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("LearningCenterResponse");

// convert to JSON object
var jsonObj = null;
var learningCtrCount = 0;
var learningCtrEntryId = -1;
var learningCtrEntry = "";
if (resp != "" && (jsonObj = JSON.parse(resp))) {
    var learnCtrNames = JSONUtils.jsonPath(jsonObj,"$.[Name]")
    if (learnCtrNames) {
        learningCtrCount = learnCtrNames.length;
    }
    if (learningCtrCount > 5) {
        var idx = RandomUtils.getRandomInt(0,learningCtrCount);
        learningCtrEntryId  = JSONUtils.jsonPath(jsonObj,"$.[Id]")[idx];
        learningCtrEntry = JSON.stringify(JSONUtils.jsonPath(jsonObj,"$.[" + idx + "]")[0]);
        
    }
}
//context.variableManager.setValue("CarouselJson",resp);
context.variableManager.setValue("LearningCenterCount", learningCtrCount);
context.variableManager.setValue("LearningCenterEntryId", learningCtrEntryId);
context.variableManager.setValue("LearningCenterEntry", learningCtrEntry);
logger.info("LearningCenterCount = " + learningCtrCount);