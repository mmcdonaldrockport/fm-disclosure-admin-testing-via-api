﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var userId = context.variableManager.parseString("${ExternalUsers_${env.Env}.UserId}");
logger.info("UserId="+userId);

// Inject the computed value in a runtime variable
context.variableManager.setValue("UserId",userId);

var vuserId = context.currentVU.id.split("#")[1];
context.variableManager.setValue("VUserId",vuserId);
logger.info("VUserId="+vuserId);