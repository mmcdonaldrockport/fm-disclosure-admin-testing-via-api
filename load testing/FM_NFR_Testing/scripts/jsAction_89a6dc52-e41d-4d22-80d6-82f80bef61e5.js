﻿var qSize = context.variableManager.getSharedQueueSize('StickeringSQ')
//context.variableManager.setValue("TranasactionsSQSize",size);
if (qSize < 0 ) {
        context.fail("StickeringSQ does not exist.");
}

if (qSize == 0) { // queue empty
    var dbServer = context.variableManager.getValue('config_${env.Env}.adminDBServer');
    var dbInstance = context.variableManager.getValue('config_${env.Env}.adminDBInstance');
    var db = context.variableManager.getValue('config_${env.Env}.adminDB');

    var tList = com.rockportllc.neoload.utils.DBUtils.getAvailableStickeringTransactions(dbServer, dbInstance, db);
    var transCount = tList.size();
    for (t = 0; t < transCount; t++) {
        //logger.info(tList.get(t));
        context.variableManager.addSharedValue("StickeringSQ", tList.get(t))
    }
}