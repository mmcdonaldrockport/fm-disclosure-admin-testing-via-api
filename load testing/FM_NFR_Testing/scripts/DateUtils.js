﻿if (typeof DateUtils !== "object") {
    DateUtils = {};
};

DateUtils.dateFromDay = function(year, day) {
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}

DateUtils.holidayDateForVUser = function(vuserId) {
    if (vuserId < 0 || vuserId > 365) { return null; }

    var year = new Date().getFullYear()+2;
    var date = DateUtils.dateFromDay(year, vuserId);

    var fmtDate = date.toISOString().slice(0, 10);
    return fmtDate;
} 