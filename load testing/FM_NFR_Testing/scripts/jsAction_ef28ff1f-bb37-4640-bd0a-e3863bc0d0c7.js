﻿var transSharedQueue = "TransactionsSQ";
var transInfo = context.variableManager.pollSharedValue(transSharedQueue);
if (transInfo == null || transInfo == "${" + transSharedQueue + "}") {
    transInfo = "";
}
var intTransId = transInfo.split('|')[0]; // get transaction ID
var transStatus = transInfo.split('|')[1]; // Issuance or Stickering
logger.info(transInfo);
context.variableManager.setValue("InternalTransactionId", intTransId);
context.variableManager.setValue("TransactionStatus", transStatus);