﻿// Select environment to execute against
var envChoices = [ "DEV", "QA1", "QA2" ];
var env = javax.swing.JOptionPane.showInputDialog(null, "Choose now...", "Environment Selection", javax.swing.JOptionPane.QUESTION_MESSAGE, null,  envChoices, envChoices[0]);
logger.info("env = " + env);

if (env==null) {
        context.fail("Environment (env) not selected)");
}

//overwriteFile("C:\\local\\env.cfg", env);
context.variableManager.setValue("env", env);
context.variableManager.getValue("env");