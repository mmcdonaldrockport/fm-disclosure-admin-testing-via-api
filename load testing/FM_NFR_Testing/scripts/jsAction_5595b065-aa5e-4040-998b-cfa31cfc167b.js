﻿var dbServer = context.variableManager.getValue('config_${env.Env}.adminDBServer');
var dbInstance = context.variableManager.getValue('config_${env.Env}.adminDBInstance');
var db = context.variableManager.getValue('config_${env.Env}.adminDB');

var tList = com.rockportllc.neoload.utils.DBUtils.getAvailableAdminTransactions(dbServer, dbInstance, db);
//logger.info(tList.size());
if (tList.size() < 50) {
    var rList = com.rockportllc.neoload.utils.DBUtils.getReturnToHoldIds(dbServer, dbInstance, db);
    
    var transCount = Math.min(rList.size(),50);
    for (r = 0; r < transCount; r++) {
        context.variableManager.addSharedValue("ReturnToHoldSQ", rList.get(r));
        //logger.info(rList.get(r));
    }    

}