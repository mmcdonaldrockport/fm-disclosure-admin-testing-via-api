﻿if (typeof ConversionUtils !== "object") {
    ConversionUtils = {};
};

ConversionUtils.arraysToObject = function(keyArr, valueArr) {
    // convert 2 arrays to object (assoc array)
    var obj = {};
    
    keyArr.forEach( function(key, idx) {
        obj[key] = valueArr[idx];
    });
    
    return obj;
    
}


ConversionUtils.arraysToArrayOfObjects = function(mapping, arrayList) {
    // (mapping, arrayList:[])
    // convert  multiple arrays to array of objects
    // use shortest array to loop, discard remaining
    // must have same number of arrays as mapping items 
    var shortestArrayLen = 1000000;
    
    for (a = 0; a < arrayList.length; a++)   {
        if (arrayList[a].length < shortestArrayLen) { shortestArrayLen = arrayList[a].length; }
    }

    var arrayOfObjs = [];
    
    for (var i = 0; i < shortestArrayLen; i++) {
        var obj = {};
        
        for (m = 0; m < mapping.length; m++) {
            obj[mapping[m]] = arrayList[m][i];
        }
        arrayOfObjs.push(obj);
    }
        
    return arrayOfObjs;
    
};