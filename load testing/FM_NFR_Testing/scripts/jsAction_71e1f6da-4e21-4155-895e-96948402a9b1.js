﻿var vuserId = context.variableManager.getValue("VUserId");
var issHoldName = "Issuance Hold for user " + vuserId;
context.variableManager.setValue("IssuanceHoldName",issHoldName);

// Get response content  from VariableManager
var resp = context.variableManager.getValue("IssuanceHoldResponse");

// convert to JSON object
var jsonObj = JSON.parse(resp);

var issHoldId = JSONUtils.jsonPath(jsonObj,"$.[?(@.Name == '" + issHoldName + "')][Id]");
context.variableManager.setValue("IssuanceHoldId",issHoldId);

var issHoldJson = JSONUtils.jsonPath(jsonObj,"$.[?(@.Name == '" + issHoldName + "')]")[0];
context.variableManager.setValue("IssuanceHoldJson", JSON.stringify(issHoldJson));

logger.info("IssuanceHoldName= " + issHoldName);
logger.info("IssuanceHoldId = "+ issHoldId);
logger.info("IssuanceHoldJson = " + issHoldJson);