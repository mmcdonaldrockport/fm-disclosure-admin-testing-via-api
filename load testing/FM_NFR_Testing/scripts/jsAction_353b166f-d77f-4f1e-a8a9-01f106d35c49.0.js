﻿// log info about the transaction

var currentQueueType = 'NONE';
if (context.variableManager.getValue("ADStatus") == 1) { currentQueueType = 'AD'; }
else if (context.variableManager.getValue("OpsStatus") == 1) { currentQueueType = 'OPS'; }
else if (context.variableManager.getValue("STStatus") == 1) { currentQueueType = 'ST'; }
context.variableManager.setValue('CurrentQueueType', currentQueueType);
context.variableManager.setValue('CurrentQueueId', FMInfo.QueueIds[currentQueueType]);

var poolId = context.variableManager.getValue("PoolId");
logger.info("PoolId = " + poolId);
logger.info("TransactionType = " + context.variableManager.getValue("TransactionType"));
logger.info("Waiver = " + context.variableManager.getValue("Waiver"));
logger.info("Eligible = " + context.variableManager.getValue("Eligible"));
logger.info("ADStatus = " + context.variableManager.getValue("ADStatus"));
logger.info("OpsStatus = " + context.variableManager.getValue("OpsStatus"));
logger.info("STStatus = " + context.variableManager.getValue("STStatus"));
logger.info("ProductType = " + context.variableManager.getValue("ProductType"));
logger.info("DealId = " + context.variableManager.getValue("DealId"));
logger.info("ExecutionType = " + context.variableManager.getValue("ExecutionType"));
logger.info("CurrentQueueType = " + context.variableManager.getValue("currentQueueType"));
logger.info("Transaction Response = " + context.variableManager.getValue("TransactionResponse"));