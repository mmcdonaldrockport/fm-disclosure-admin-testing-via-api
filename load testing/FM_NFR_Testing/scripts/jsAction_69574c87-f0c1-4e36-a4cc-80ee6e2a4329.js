﻿var json = {
	"MBS": {
		"IssuanceUPB": 2.3680249224495E11,
		"CurrentUPB": 2.3680249224495E11,
		"NumberOfLoans": 25359,
		"NumberOfSecurities": 21405,
		"CurrentNoteRate": 4.120559633962129,
		"PayingPTR": 0.28527434549922714,
		"CurrentNCFDSCR": 0.0012000017382054336,
		"LTV": 62.52858552694846,
		"MBSTableResultList": [{
			"SecurityStatus": "Active",
			"PoolId": "467654",
			"TransactionId": 3,
			"Cusip": "31381QQF6",
			"IssueDate": "2011-04-01T00:00:00",
			"IssuanceUPB": 6200000.0,
			"Prefix": "HX",
			"InterestType": "F",
			"MaturityDate": "2018-04-01T00:00:00",
			"CurrentUPB": 6200000.0,
			"PayingPTR": 3.864,
			"ProductType": "DUS",
			"IssuancePTR": "0.0000000000",
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2018-04-01T00:00:00",
			"IssuanceInterestRate": "0.0000000000",
			"NumberOfLoans": 1,
			"CurrentNoteRate": 3.255E7,
			"CurrentNCFDSCR": 0.0,
			"LTV": 4.805E8,
			"ARMSubType": null,
			"SecurityType": "MBS",
			"SecurityNoteRate": 3.255E7,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 4.805E8,
			"SecurityUPB": 6200000.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "470424",
			"TransactionId": 4,
			"Cusip": "31381TSM3",
			"IssueDate": "2012-03-01T00:00:00",
			"IssuanceUPB": 8951066.0,
			"Prefix": "HY",
			"InterestType": "F",
			"MaturityDate": "2022-02-01T00:00:00",
			"CurrentUPB": 8951066.0,
			"PayingPTR": 3.193,
			"ProductType": "DUS",
			"IssuancePTR": "0.0000000000",
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2022-02-01T00:00:00",
			"IssuanceInterestRate": "0.0000000000",
			"NumberOfLoans": 1,
			"CurrentNoteRate": 3.57147548961E7,
			"CurrentNCFDSCR": 0.0,
			"LTV": 4.9230865145E8,
			"ARMSubType": null,
			"SecurityType": "MBS",
			"SecurityNoteRate": 3.57147548961E7,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 4.9230865145E8,
			"SecurityUPB": 8951066.39
		}]
	},
	"DMBS": {
		"DMBSTableResultList": [{
			"SecurityStatus": "Active",
			"PoolId": "AN4153",
			"TransactionId": 1842,
			"Cusip": "3138LGTK2",
			"IssueDate": "2017-01-01T00:00:00",
			"IssuanceUPB": 2.6E7,
			"Prefix": "MD",
			"InterestType": "F",
			"MaturityDate": "2017-04-03T00:00:00",
			"CurrentUPB": 2.6E7,
			"PayingPTR": 0.0,
			"ProductType": "Credit Facility",
			"IssuancePTR": null,
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2017-04-03T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "DMBS",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "AN4575",
			"TransactionId": 2528,
			"Cusip": "3138LHCM4",
			"IssueDate": "2017-02-01T00:00:00",
			"IssuanceUPB": 1.01918E8,
			"Prefix": "MD",
			"InterestType": "F",
			"MaturityDate": "2017-05-01T00:00:00",
			"CurrentUPB": 1.01918E8,
			"PayingPTR": 0.0,
			"ProductType": "Credit Facility",
			"IssuancePTR": null,
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2017-05-01T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "DMBS",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		}]
	},
	"Mega": {
		"IssuanceUPB": 2.5861899693E10,
		"NumberOfSecurities": 465,
		"MegaTableResultList": [{
			"SecurityStatus": "Active",
			"PoolId": "AL6829",
			"TransactionId": 379469,
			"Cusip": "3138EPST2",
			"IssueDate": "2015-05-01T00:00:00",
			"IssuanceUPB": 9.7761854E7,
			"Prefix": "XY",
			"InterestType": null,
			"MaturityDate": "2027-05-01T00:00:00",
			"CurrentUPB": 9.7761854E7,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2027-05-01T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "MEGA",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "AL6920",
			"TransactionId": 379470,
			"Cusip": "3138EPVN1",
			"IssueDate": "2015-06-01T00:00:00",
			"IssuanceUPB": 1.2597059E7,
			"Prefix": "XY",
			"InterestType": null,
			"MaturityDate": "2025-05-01T00:00:00",
			"CurrentUPB": 1.2597059E7,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2025-05-01T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "MEGA",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "AL6921",
			"TransactionId": 379471,
			"Cusip": "3138EPVP6",
			"IssueDate": "2015-06-01T00:00:00",
			"IssuanceUPB": 4.9735E7,
			"Prefix": "XX",
			"InterestType": null,
			"MaturityDate": "2022-05-01T00:00:00",
			"CurrentUPB": 4.9735E7,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": null,
			"Class": null,
			"FinalDistributionDate": "2022-05-01T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "MEGA",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		}]
	},
	"Remic": {
		"IssuanceUPB": 1.56311425079E11,
		"NumberOfSecurities": 717,
		"RemicTableResultList": [{
			"SecurityStatus": "Active",
			"PoolId": "09-M01",
			"TransactionId": 386795,
			"Cusip": "31398F2R1",
			"IssueDate": "2009-10-01T00:00:00",
			"IssuanceUPB": 0.0,
			"Prefix": null,
			"InterestType": null,
			"MaturityDate": "2019-08-25T00:00:00",
			"CurrentUPB": 0.0,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": "0",
			"Class": "RL",
			"FinalDistributionDate": "2019-08-25T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "REMIC",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "09-M01",
			"TransactionId": 386795,
			"Cusip": "31398F2Q3",
			"IssueDate": "2009-10-01T00:00:00",
			"IssuanceUPB": 0.0,
			"Prefix": null,
			"InterestType": null,
			"MaturityDate": "2019-08-25T00:00:00",
			"CurrentUPB": 0.0,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": "0",
			"Class": "R",
			"FinalDistributionDate": "2019-08-25T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "REMIC",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		},
		{
			"SecurityStatus": "Active",
			"PoolId": "09-M01",
			"TransactionId": 386795,
			"Cusip": "31398F2P5",
			"IssueDate": "2009-10-01T00:00:00",
			"IssuanceUPB": 5.03816029E8,
			"Prefix": null,
			"InterestType": null,
			"MaturityDate": "2019-07-25T00:00:00",
			"CurrentUPB": 5.03816029E8,
			"PayingPTR": 0.0,
			"ProductType": null,
			"IssuancePTR": null,
			"Group": "0",
			"Class": "X",
			"FinalDistributionDate": "2019-07-25T00:00:00",
			"IssuanceInterestRate": null,
			"NumberOfLoans": 0,
			"CurrentNoteRate": 0.0,
			"CurrentNCFDSCR": 0.0,
			"LTV": 0.0,
			"ARMSubType": null,
			"SecurityType": "REMIC",
			"SecurityNoteRate": 0.0,
			"SecurityPayingPTR": 0.0,
			"SecurityNCFDSCR": 0.0,
			"SecurityLTV": 0.0,
			"SecurityUPB": 0.0
		}]
	}
};

var transIdArray = JSON.stringify(JSONUtils.jsonPath(json, '$.MBS.MBSTableResultList[0].TransactionId')); 
var transId = transIdArray.substring(1,transIdArray.length-1)
logger.info(transId);