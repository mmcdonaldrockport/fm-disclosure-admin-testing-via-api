﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("CarouselResponse");

// convert to JSON object
var jsonObj = null;
var carouselCount = 0;
var carouselEntryId = -1;
var carouselEntry = "";
if (resp != "" && (jsonObj = JSON.parse(resp))) {
    var carouselNames = JSONUtils.jsonPath(jsonObj,"$.[Name]")
    if (carouselNames) {
        carouselCount = carouselNames.length;
    }
    if (carouselCount > 5) {
        var idx = RandomUtils.getRandomInt(0,carouselCount);
        carouselEntryId  = JSONUtils.jsonPath(jsonObj,"$.[Id]")[idx];
        carouselEntry = JSON.stringify(JSONUtils.jsonPath(jsonObj,"$.[" + idx + "]")[0]);
        
    }
}
//context.variableManager.setValue("CarouselJson",resp);
context.variableManager.setValue("CarouselCount", carouselCount);
context.variableManager.setValue("CarouselEntryId", carouselEntryId);
context.variableManager.setValue("CarouselEntry", carouselEntry);
logger.info("CarouselCount = " + carouselCount);