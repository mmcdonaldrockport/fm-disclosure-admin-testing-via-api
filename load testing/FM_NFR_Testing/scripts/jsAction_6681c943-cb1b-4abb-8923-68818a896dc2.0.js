﻿var vuserId = context.variableManager.getValue("VUserId");
var bannerName = "Banner for user " + vuserId;
context.variableManager.setValue("BannerName",bannerName);

// Get response content  from VariableManager
var resp = context.variableManager.getValue("BannerResponse");

// convert to JSON object
var bannerId = false;
var bannerJson = false;
var jsonObj = null;
if (resp != "" && (jsonObj = JSON.parse(resp))) {
    var bannerJson = JSONUtils.jsonPath(jsonObj,"$.[?(@.Name == '" + bannerName +"')]")[0];
    var bannerId = JSONUtils.jsonPath(jsonObj,"$.[?(@.Name == '" + bannerName + "')][Id]");
}
context.variableManager.setValue("BannerJson",bannerJson);
context.variableManager.setValue("BannerId",bannerId);
logger.info("BannerName = " + bannerName);
logger.info("BannerId = " + bannerId);
logger.info("BannerJson = " + bannerJson);