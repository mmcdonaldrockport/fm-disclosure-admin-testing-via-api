﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var vuser = context.variableManager.getValue("VUserId");
var waitTime = Number(vuser) *1000;

// Do some computation using the methods
// you defined in the JS Library

// Inject the computed value in a runtime variable
context.variableManager.setValue("WaitTime",waitTime);
logger.info("Wait Time = " + waitTime);