﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.




// Get variable value from VariableManager
var myVar = context.variableManager.getValue("transactionId.Product");
if (myVar==null) {
        context.fail("Variable 'myVar' not found");
}

// Inject the computed value in a runtime variable
logger.info("Product="+myVar);