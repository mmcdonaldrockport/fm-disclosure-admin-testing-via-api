﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("TransactionResponse");
//logger.info(resp);

// convert to JSON object
var jsonObj = JSON.parse(resp);
//var docTypeArray = JSONUtils.jsonPath(jsonObj,"$.[IssuanceDocuments][?(@.Id != null)].[TypeName]"); 
//var docIdArray = JSONUtils.jsonPath(jsonObj,"$.[IssuanceDocuments][?(@.Id != null)].[Id]"); 

var uploadDocList = [];
var allDocs = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][*]");
if (allDocs) {
    var numDocs = Object.keys(allDocs).length;
    for (i = 0; i < numDocs; i++) {
        var docObj = {};
                   var id  = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][" + i + "].Id")[0];
                   if (id == null) { id = -1; }
    	docObj.id = id;
    	docObj.typeName = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][" + i + "].TypeName")[0].replace(/\W/g, "");
    	docObj.issuanceDocumentType = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][" + i + "].IssuanceDocumentType")[0];
    	uploadDocList.push(docObj)
    }
}

logger.info("first upload Id  = " + uploadDocList[0]['id']);
logger.info("first upload file type  = " + uploadDocList[0]['typeName']);

// copy upload list to download list where doc id != -1
var downloadDocList = [];
for (i = 0; i < uploadDocList.length; i++) { 
    if ( uploadDocList[i]['id'] !== -1) {
        var downloadDoc = JSON.parse(JSON.stringify(uploadDocList[i]));
        downloadDocList.push(downloadDoc);
    }
}

// serialize for storage 
var serUploadDocList = JSON.stringify(uploadDocList);
var serDownloadDocList = JSON.stringify(downloadDocList);

logger.info('serDocList = ' + serUploadDocList);
logger.info('serDocList = ' + serDownloadDocList);

// and store
context.variableManager.setValue("UploadDocList",serUploadDocList);
context.variableManager.setValue("DownloadDocList",serDownloadDocList);

var downloadDocId = downloadDocList[RandomUtils.getRandomInt(0,downloadDocList.length)].id;

context.variableManager.setValue("DownloadDocId", downloadDocId);
logger.info("DownloadDocId " + downloadDocId);
logger.info("DownloadDocId " + context.variableManager.getValue("DownloadDocId"));

var randomDocInstance = RandomUtils.getRandomInt(0,uploadDocList.length);

var uploadDocId = uploadDocList[randomDocInstance].id;
var uploadDocName = uploadDocList[randomDocInstance].typeName;
var uploadDocType = uploadDocList[randomDocInstance].issuanceDocumentType;
var uploadDocExt = FMInfo.issuanceDocTypes[uploadDocList[randomDocInstance].issuanceDocumentType].ext;
var uploadDocFileType = FMInfo.issuanceDocTypes[uploadDocList[randomDocInstance].issuanceDocumentType].filetype;

context.variableManager.setValue("UploadDocId", uploadDocId);
context.variableManager.setValue("UploadDocName", uploadDocName);
context.variableManager.setValue("UploadDocType", uploadDocType);
context.variableManager.setValue("UploadDocExt", uploadDocExt);
context.variableManager.setValue("UploadDocFileType", uploadDocFileType);

logger.info("UploadDocId " + uploadDocId);
logger.info("UploadDocName " + uploadDocName);
logger.info("UploadDocType " + uploadDocType);
logger.info("UploadDocExt " + uploadDocExt);
logger.info("UploadDocFileType " + uploadDocFileType);

var irpId = JSONUtils.jsonPath(jsonObj,"$.['IRPDocuments'][0].[Id]");
var irpDate = JSONUtils.jsonPath(jsonObj,"$.['IRPDocuments'][0].[PublicationDate]");

logger.info("IRPDate = " + irpDate);
logger.info("IRPId = " + irpId);

context.variableManager.setValue("IRPId", irpId ? irpId : false);
context.variableManager.setValue("IRPDate", irpDate ? irpDate.toString().split('T')[0] : false);