﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var externalId = context.variableManager.getValue("ExternalTransactionId");

logger.debug("ExternalTransactionId = "+externalId);