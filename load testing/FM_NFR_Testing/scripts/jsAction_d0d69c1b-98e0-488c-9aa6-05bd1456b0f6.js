﻿// update
var correctionCount = context.variableManager.getValue("CorrectionCount");
logger.info("CorrectionCount = " + correctionCount);
var correctionDataRow = context.variableManager.pollSharedValue("CorrectionsSQ");
logger.info(correctionDataRow);
var correctionData = correctionDataRow.split("|");
context.variableManager.setValue("DealAgreementIdentifier", correctionData[0]);
context.variableManager.setValue("DealIdentifier", correctionData[1]);
context.variableManager.setValue("MBSPoolIdentifier", correctionData[2]);
context.variableManager.setValue("SecurityCUSIPIdentifier", correctionData[3]);
logger.info(correctionData[4]);
var securityIssueAmount = Number(correctionData[4]) + 1;
context.variableManager.setValue("SecurityIssueAmount", securityIssueAmount);

correctionCount -= 1;
context.variableManager.setValue("CorrectionCount", correctionCount);