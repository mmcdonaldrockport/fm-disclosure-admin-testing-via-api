﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var filename = context.variableManager.getValue("TransferMessageList.Filename");
if (filename == null) {
        context.fail("Variable 'filename' not found");
}
var prefix = filename.substring(0,2);
var serviceName = FMInfo.serviceLookup[prefix];
logger.info(filename + ":" + prefix + ":" + serviceName);
 context.variableManager.getValue("ServiceName",serviceName);
// Do some computation using the methods
// you defined in the JS Library