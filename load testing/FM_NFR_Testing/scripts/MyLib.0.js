﻿function writeFileByLine(filepath, text) {
    var lock = new java.util.concurrent.locks.ReentrantLock();
    lock.lock();
    var writer = new java.io.FileWriter(filepath, true);
    writer.write(text);
    writer.write("\r\n");
    writer.close();
 
lock.unlock();
}

function sleep(ms) {
    var now = new Date().getTime();
    var millisecondsToWait = ms;
    logger.debug("Sleeping for " + ms + " milliseconds.");
    while ( new Date().getTime() < now + millisecondsToWait )  {}   
}