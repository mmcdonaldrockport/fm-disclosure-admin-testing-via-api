﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("DataCollectionsResponse");

// convert to JSON object
var jsonObj = null;
var dataCollCount = 0;
var dataCollEntryId = -1;
var dataCollEntry = "";
if (resp != "" && (jsonObj = JSON.parse(resp))) {
    var learnCtrNames = JSONUtils.jsonPath(jsonObj,"$.[Name]")
    if (learnCtrNames) {
        dataCollCount = learnCtrNames.length;
    }
    if (dataCollCount > 5) {
        var idx = RandomUtils.getRandomInt(0,dataCollCount);
        dataCollEntryId  = JSONUtils.jsonPath(jsonObj,"$.[Id]")[idx];
        dataCollEntry = JSON.stringify(JSONUtils.jsonPath(jsonObj,"$.[" + idx + "]")[0]);
        
    }
}
//context.variableManager.setValue("CarouselJson",resp);
context.variableManager.setValue("DataCollectionsCount", dataCollCount);
context.variableManager.setValue("DataCollectionsEntryId", dataCollEntryId);
context.variableManager.setValue("DataCollectionsEntry", dataCollEntry);
logger.info("DataCollectionsCount = " + dataCollCount);