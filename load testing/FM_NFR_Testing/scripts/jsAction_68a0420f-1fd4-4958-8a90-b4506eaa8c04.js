﻿var qSize = context.variableManager.getSharedQueueSize('TransactionsSQ')
//context.variableManager.setValue("TranasactionsSQSize",size);
var refillSize = 50;
if (qSize < 0 ) {
        context.fail("TransactionsSQ does not exist.");
}

if (qSize == 0) { // queue empty
    var dbServer = context.variableManager.getValue('config_${env.Env}.adminDBServer');
    var dbInstance = context.variableManager.getValue('config_${env.Env}.adminDBInstance');
    var db = context.variableManager.getValue('config_${env.Env}.adminDB');

    var tList = com.rockportllc.neoload.utils.DBUtils.getAvailableAdminTransactions(dbServer, dbInstance, db);
    var transCount = tList.size();
    if (transCount > 0) {
        transCount = Math.min(refillSize, transCount);
        for (t = 0; t < transCount; t++) {
            //logger.info(tList.get(t));
            context.variableManager.addSharedValue("TransactionsSQ", tList.get(t))
        }
     }
}