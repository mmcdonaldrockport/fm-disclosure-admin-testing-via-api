﻿var filename = context.variableManager.getValue("TransferMessageList.Filename");
if (filename == null) {
        context.fail("Variable 'filename' not found");
}
var prefix = filename.substring(0,2);
var serviceName = FMInfo.serviceLookup[prefix];
logger.info(filename + ":" + prefix + ":" + serviceName);
context.variableManager.setValue("ServiceName",serviceName);

var resourceDir = context.variableManager.getValue("NL-CustomResources");
var transferFile = FileUtils.readFile(resourceDir + "\\" + filename);
context.variableManager.setValue("TransferFile",transferFile);