﻿var qSize = context.variableManager.getSharedQueueSize('ExternalTransactionsSQ')
//context.variableManager.setValue("TranasactionsSQSize",size);
if (qSize < 0 ) {
        context.fail("ExternalTransactionsSQ does not exist.");
}

if (qSize == 0) { // queue empty
    logger.info("ExternalSQ is empty");
    var dbServer = context.variableManager.getValue("config_${env.Env}.extDBServer");
    var dbInstance = context.variableManager.getValue("config_${env.Env}.extDBInstance");
    var db = context.variableManager.getValue("config_${env.Env}.extDB");

    logger.info(dbServer + "\\" + dbInstance + ";" + db);

    var tList = com.rockportllc.neoload.utils.DBUtils.getAvailableExternalTransactions(dbServer, dbInstance, db);
    var transCount = tList.size();
    //transCount = Math.min(transCount);
    for (t = 0; t < transCount; t++) {
        //logger.info(tList.get(t));
        context.variableManager.addSharedValue("ExternalTransactionsSQ", tList.get(t))
    }
} else {
    logger.info("ExternalSQ size = " + qSize); 
}