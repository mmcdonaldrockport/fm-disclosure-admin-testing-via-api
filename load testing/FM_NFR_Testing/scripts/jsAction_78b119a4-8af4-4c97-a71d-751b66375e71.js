﻿Cross-site scripting protection

- Returned in redirect from DisclosureAdmin/Internal/Login page as dus-request-verification-token:

<div 
    ng-controller="NavBarController as navbar" 
    ng-include="navbar.template" 
    dus-request-verification-token=cP6z9XUfuKUsza9yWmER4uQXTdhuqVb3sYq7w8JsOK_OQOxRdYjS3U-8bCf5dc9qqlWZrou8z8AKFouTOLVKR7G8q3k1:RtoQ53PBe8ujeAuF5R0w3Ye0Xy6vDEieGc_Km68CHzI97YBffNCUHgECzVwoptGtzabsCTW_euyPXsxeesDT7RDWtlHcmgLBJV_3KigbotvArfNI0>
</div>

- Captured to variable XSRFToken

- Returned in request header as necessary in XSRF-HTTP-Token