﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager

context.variableManager.addSharedValue("TestQ", context.variableManager.getValue("result_Id_1") + ":" + context.variableManager.getValue("result_Time_1"));
context.variableManager.addSharedValue("TestQ", context.variableManager.getValue("result_Id_2") + ":" + context.variableManager.getValue("result_Time_2"));
context.variableManager.addSharedValue("TestQ", context.variableManager.getValue("result_Id_3") + ":" + context.variableManager.getValue("result_Time_3"));
logger.info(context.variableManager.getValue("result_Id_1") + ":" + context.variableManager.getValue("result_Time_1"));
logger.info(context.variableManager.getValue("result_Id_2") + ":" + context.variableManager.getValue("result_Time_2"));
logger.info(context.variableManager.getValue("result_Id_3") + ":" + context.variableManager.getValue("result_Time_3"));