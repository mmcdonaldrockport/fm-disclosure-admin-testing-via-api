﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var userCount = context.variableManager.getValue("users_userId_count");
var matchNr = context.variableManager.getValue("users_userId_matchNr");
//var userId = context.variableManager.getValue("creds_userId_1");
// Do some computation using the methods
// you defined in the JS Library

logger.info(userCount);
logger.info(matchNr);
context.variableManager.setValue("testVar", [1,2,3]);
var testVar2 = context.variableManager.getValue("testVar");
logger.info(testVar2);
//logger.info(userId);

// Inject the computed value in a runtime variable
//context.variableManager.setValue("computedVar",computedValue);
var fileLocation =  context.variableManager.getValue("adminUsersFile");
var csvFileFormat = org.apache.commons.csv.CSVFormat.DEFAULT.withRecordSeparator("\n");
var fileWriter = new java.io.FileWriter(fileLocation);
var csvPrinter = new org.apache.commons.csv.CSVPrinter(fileWriter, csvFileFormat);

var header = new java.util.ArrayList();
header.add("userId");
header.add("password")
csvPrinter.printRecord(header);
adminPassword = context.variableManager.getValue("adminPassword");
logger.info("adminPassword=" + adminPassword);

for (var i = 1; i <= userCount; i++ ) {
    var record = new java.util.ArrayList();
    record.add(context.variableManager.getValue("users_userId_" + i))
    
    record.add(adminPassword);
    csvPrinter.printRecord(record);
}

fileWriter.flush();
fileWriter.close();
csvPrinter.close();
/*
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Delimiter used in CSV file
18
    private static final String NEW_LINE_SEPARATOR = "\n";
19
     
20
    //CSV file header
21
    private static final Object [] FILE_HEADER = {"id","firstName","lastName","gender","age"};


 
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
fileWriter = new FileWriter(fileName);
53
             
54
            //initialize CSVPrinter object
55
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
56
             
57
            //Create CSV file header
58
            csvFilePrinter.printRecord(FILE_HEADER);
59
             
60
            //Write a new student object list to the CSV file
61
            for (Student student : students) {
62
                List studentDataRecord = new ArrayList();
63
                studentDataRecord.add(String.valueOf(student.getId()));
64
                studentDataRecord.add(student.getFirstName());
65
                studentDataRecord.add(student.getLastName());
66
                studentDataRecord.add(student.getGender());
67
                studentDataRecord.add(String.valueOf(student.getAge()));
68
                csvFilePrinter.printRecord(studentDataRecord);
69
            }
70
 




*/