﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("ProductInfoResponse");

// convert to JSON object
var jsonObj = null;
var prodInfoCount = 0;
var prodInfoEntryId = -1;
var prodInfoEntry = "";
if (resp != "" && (jsonObj = JSON.parse(resp))) {
    var learnCtrNames = JSONUtils.jsonPath(jsonObj,"$.[Name]")
    if (learnCtrNames) {
        prodInfoCount = learnCtrNames.length;
    }
    if (prodInfoCount > 5) {
        var idx = RandomUtils.getRandomInt(0,prodInfoCount);
        prodInfoEntryId  = JSONUtils.jsonPath(jsonObj,"$.[Id]")[idx];
        prodInfoEntry = JSON.stringify(JSONUtils.jsonPath(jsonObj,"$.[" + idx + "]")[0]);
        
    }
}
//context.variableManager.setValue("CarouselJson",resp);
context.variableManager.setValue("ProductInfoCount", prodInfoCount);
context.variableManager.setValue("ProductInfoEntryId", prodInfoEntryId);
context.variableManager.setValue("ProductInfoEntry", prodInfoEntry);
logger.info("ProductInfoCount = " + prodInfoCount);