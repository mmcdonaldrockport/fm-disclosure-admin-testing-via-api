SELECT 
	D.AgreementIdentifier AS DealAgreementIdentifier,
	D.DealIdentifier,
	L.LoanIdentifier,
	L.FannieMaeLoanNumber,
	C.CollateralIdentifier,
	P.PropertyIdentifier,
	CP.MBSPoolIdentifier,
	S.SecurityCUSIPIdentifier
FROM FmData.CollateralPool CP
	INNER JOIN adm.PublishedTransactions PT ON PT.RecordId = CP.Id
	LEFT JOIN FmData.Security S ON S.Id = CP.Id
	LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
	LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
	LEFT JOIN FmData.Collateral C ON C.Id = LC.CollateralId
	LEFT JOIN FmData.Property P ON P.Id = LC.CollateralId
	LEFT JOIN FmData.Deal d ON d.Id = l.DealId
WHERE S.ProductGroupType <> 'Mega'
	AND S.SecurityIssueDate >= DATEADD(DAY,-90,GETDATE())