﻿// Get response content  from VariableManager
var resp = context.variableManager.getValue("TransactionResponse");
//logger.info(resp);

// convert to JSON object
var jsonObj = JSON.parse(resp);
//var docTypeArray = JSONUtils.jsonPath(jsonObj,"$.[IssuanceDocuments][?(@.Id != null)].[TypeName]"); 
//var docIdArray = JSONUtils.jsonPath(jsonObj,"$.[IssuanceDocuments][?(@.Id != null)].[Id]"); 

var issuanceDocs = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][*]");
if (issuanceDocs) {
    var numDocs = Object.keys(issuanceDocs).length;
    var issuanceDocList = [];
    for (i = 0; i < numDocs; i++) {
        var id  = JSONUtils.jsonPath(jsonObj,"$.['IssuanceDocuments'][" + i + "].Id")[0];
        issuanceDocList.push(id);
    }
    context.variableManager.setValue("IssuanceDocList",issuanceDocList.join(","));
    context.variableManager.setValue("RandomIssuanceDocId", issuanceDocList[RandomUtils.getRandomInt(0,issuanceDocList.length)]);
    
} else {
    context.variableManager.setValue("IssuanceDocList","");
}



var ongoingResp  = context.variableManager.getValue("OngoingDocTypesResponse");
var ongoingJsonObj = JSON.parse(ongoingResp);

var ongoingDocList = JSONUtils.jsonPath(ongoingJsonObj,"$.[Id]");
/*
numDocs = Object.keys(ongoingDocs).length;
var ongoingDocList = [];
for (i = 0; i < numDocs; i++) {
    var id  = JSONUtils.jsonPath(jsonObj,"$.['OngoingDocuments'][" + i + "].Id")[0];
    ongoingDocList.push(id);
}
*/
context.variableManager.setValue("OngoingDocList",ongoingDocList);

// get oldest date
var ongoingDate = JSONUtils.jsonPath(jsonObj, "$.[OngoingDocumentDates][(@.length-1)].[PublicationDate]");
if (ongoingDate) {
        ongoingDate  = ongoingDate.split("T")[0];
}

context.variableManager.setValue("OngoingDate",ongoingDate);
var ongoingDocIds = ongoingDocList.toString().split(',') 

context.variableManager.setValue("RandomOngoingDocId",ongoingDocIds[RandomUtils.getRandomInt(0,ongoingDocIds.length)]);

// get first Loan id
var loanId = JSONUtils.jsonPath(jsonObj,"$.['Loans'][0].Id");
context.variableManager.setValue("LoanId",loanId);

var propertyId = JSONUtils.jsonPath(jsonObj,"$.['Loans'][0].['properties'][0].Id");
context.variableManager.setValue("PropertyId",propertyId);