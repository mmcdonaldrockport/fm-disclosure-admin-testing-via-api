﻿if (typeof FileUtils !== "object") {
    FileUtils = {};
};


FileUtils.overwriteFile = function (filePath, text) 
{
    var lock = new java.util.concurrent.locks.ReentrantLock();
    lock.lock();
    var writer = new java.io.FileWriter(filePath,false);
    writer.write(text);
    //writer.write("\r\n");
    writer.close();
     
    lock.unlock();
}

FileUtils.appendToFile = function (filePath, text) 
{
    var lock = new java.util.concurrent.locks.ReentrantLock();
    lock.lock();
    var writer = new java.io.FileWriter(filePath,true);
    writer.write(text);
    writer.write("\r\n");
    writer.close();
     
    lock.unlock();
}

FileUtils.readFile = function (filePath) {
    var lock = new java.util.concurrent.locks.ReentrantLock();
    lock.lock();
    var reader = new java.io.BufferedReader(new java.io.FileReader(filePath));
    var text = ""
    var line = "";
    while((line = reader.readLine()) != null) {
                text += line;
     }
     reader.close();   
    lock.unlock();
    return text;
}