﻿var qSize = context.variableManager.getSharedQueueSize('StickeringSQ')
var restickerCount = 20;
var correctionCount = 0;
//context.variableManager.setValue("TranasactionsSQSize",size);
if (qSize < 0 ) {
        context.fail("StickeringSQ does not exist.");
} else {
    logger.info("StickeringSQ size " + context.variableManager.getSharedQueueSize('StickeringSQ'));
    if (qSize < restickerCount) { 
    //if (qSize < restickerCount) { 
        var dbServer = context.variableManager.getValue('config_${env.Env}.adminDBServer');
        var dbInstance = context.variableManager.getValue('config_${env.Env}.adminDBInstance');
        var db = context.variableManager.getValue('config_${env.Env}.adminDB');
    
        var tList = com.rockportllc.neoload.utils.DBUtils. getStickeringEligibleTransactions(dbServer, dbInstance, db);
        var transCount = tList.size();

        if (transCount > 0) {
            transCount = Math.min(transCount, restickerCount);
            context.variableManager.setValue("CorrectionCount", transCount)
            for (t = 0; t < transCount; t++) {
                var correctionDataRow = "";
                logger.info("DealAgreementIdentifier " + tList.get(t).get('DealAgreementIdentifier'));
                
                correctionDataRow = 
                    tList.get(t).get('DealAgreementIdentifier') + "|" +
                    tList.get(t).get('DealIdentifier') + "|" +
                    tList.get(t).get('MBSPoolIdentifier') + "|" +
                    tList.get(t).get('SecurityCUSIPIdentifier') + "|" +
                    tList.get(t).get('SecurityIssueAmount');
                context.variableManager.addSharedValue("CorrectionsSQ", correctionDataRow);
                logger.info("CorrectionDataRow =" + correctionDataRow);
                correctionCount++;
            }
        }
        logger.info("CorrectionsSQ size " + context.variableManager.getSharedQueueSize('CorrectionsSQ'));
    }
}
logger.info("CorrectionsSQ size " + context.variableManager.getSharedQueueSize('CorrectionsSQ'));
context.variableManager.setValue("CorrectionCount", correctionCount);