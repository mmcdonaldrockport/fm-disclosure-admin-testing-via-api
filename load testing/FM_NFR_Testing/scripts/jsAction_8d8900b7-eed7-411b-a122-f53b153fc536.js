﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
//var userEmail = context.variableManager.parseString("${AdminUsers_${env.Env}.Email}");
//logger.info("UserEmail="+userEmail);

// Inject the computed value in a runtime variable
//context.variableManager.setValue("UserEmail",userEmail);

var vuserId = context.currentVU.id.split("#")[1];
context.variableManager.setValue("VUserId",vuserId);
logger.info("VUserId="+vuserId);