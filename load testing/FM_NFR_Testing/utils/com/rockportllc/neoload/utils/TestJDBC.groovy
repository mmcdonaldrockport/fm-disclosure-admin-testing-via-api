package com.rockportllc.neoload.utils;

import groovy.sql.Sql;
import java.sql.ResultSet;
import com.opencsv.CSVWriter;

public class DBUtils {
    public static void main(String[] args) {
        println populateExternalSearchFile("C:\\local\\ids.csv", "DVFM01-D03", "QAFM01_D04", "DisclosureAdmin2");
    }

    public static boolean populateExternalSearchFile (String filepath, String server, String instance, String database, String username=null, String password=null) {
        // erase file first so no old data if query fails
        def sql = null;
        
        try {
            def fileDeleted = new File(filepath).delete();
            if (! fileDeleted) {
                return false;
            }
        } catch (Exception e) {
            println e;
            return false;
        }
        
        try {
        
            if (username == null) {
                // if username is null or empty assume integrated security
                sql = Sql.newInstance(
                    "jdbc:sqlserver://" + server + "\\" + instance + ";databaseName=" + database + ";integratedSecurity=true", 
                    "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } else {
                sql = Sql.newInstance(
                    "jdbc:sqlserver://" + server + "\\" + instance + ";databaseName=" + database + ";integratedSecurity=true", 
                    username, password, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            }

            /*  //jtds version
                sql = Sql.newInstance("jdbc:sqlserver://DVFM01-D03\\QAFM01_D04;databaseName=DisclosureAdmin2;integratedSecurity=true", 
               "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            */

        } catch (Exception e) {
            println e;
            return false;
        }

        try {
            sql.query('''
                SET NOCOUNT ON -- prevents sql.query from throwing up over CTE's
                Use ''' + database + ''';

                DECLARE @ProductType TABLE (value NVARCHAR(100) PRIMARY KEY); -- DUS
                DECLARE @ProductGroupType TABLE (value NVARCHAR(100) PRIMARY KEY); -- MBS
                DECLARE @SecurityStatusType TABLE (value NVARCHAR(100) PRIMARY KEY); -- Active

                INSERT INTO @ProductType (Value) VALUES
                ('Negotiated Transaction'),
                ('Non-DUS'),
                ('DUS'),
                ('Credit Facility'),
                ('Bulk Delivery');


                INSERT INTO @ProductGroupType SELECT 'MBS';
                INSERT INTO @SecurityStatusType SELECT 'Active';

                WITH Transactions AS 
                    
                    (SELECT DISTINCT
                        [RecordId] = COALESCE(S.Id, D.Id), 
                        [TransactionId] = COALESCE(CP.MBSPoolIdentifier, D.Name)
                            
                    FROM FmData.Security S
                         -- REMIC Security Joins
                            LEFT JOIN FmData.CollateralPool CP ON Cp.Id = S.Id
                            LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
                            LEFT JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.StructuredTransactionCollateralGroupId = STCG.Id
                            LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId)

                , SecurityPayment As 

                    (SELECT
                        [Ranking] = ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY SP.SecurityPaymentEffectiveDate DESC),
                        S.Id,
                        [CurrentUPB] = COALESCE(SP.SecurityUnpaidPrincipalBalanceAmount, S.SecurityIssueAmount)
                    FROM FmData.Security S 
                        LEFT JOIN FmData.SecurityPayment SP ON SP.SecurityId = S.Id)

                , SecurityStatus AS
                    
                    (SELECT
                        [Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime DESC),
                        SS.SecurityId,
                        SS.SecurityStatusType
                    FROM FmData.Security S 
                        LEFT JOIN FmData.SecurityStatus SS ON SS.SecurityId = S.Id)

                , SecurityInterestRateON AS 

                    (SELECT 
                        [Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate DESC),
                        SIR.SecurityId,
                        SIR.SecurityInterestRate
                    FROM FMData.SecurityInterestRate SIR)

                , SecurityInterestRateAI AS 

                    (SELECT 
                        [Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate ASC),
                        SIR.SecurityId,
                        SIR.SecurityInterestRate
                    FROM FMData.SecurityInterestRate SIR)

                , LoanAcquisition AS 

                    (SELECT 
                        [Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.Id ORDER BY L.Id),
                        CP.Id,
                        LA.LoanDUSDiscloseDerivedProductType
                    FROM FmData.CollateralPool CP
                        LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
                        LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

                SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS RowNumber, * FROM (
                SELECT  DISTINCT
                    S.Id, -- Security table Id
                    [SecurityStatus] = SS.SecurityStatusType,
                    [SecurityType] = S.ProductGroupType, 
                    [ProductType] = LA.LoanDUSDiscloseDerivedProductType,
                    [Prefix] = S.MBSPoolPrefixType,
                    [InterestType] = S.SecurityInterestRateType,
                    [ARMSubtype] =  S.MBSSecurityARMSubType,
                    [IssueDate] = S.SecurityIssueDate,
                    [PayingPass-Through Rate] = COALESCE(SIRON.SecurityInterestRate, SIRAI.SecurityInterestRate),
                    [CUSIP] = S.SecurityCUSIPIdentifier,
                    [IssuanceUPB] = S.SecurityIssueAmount,
                    [TransactionID] = T.TransactionId,
                    PT.RecordId

                FROM FMData.Security S  
                    
                    LEFT JOIN Transactions T ON T.RecordId = S.Id
                    LEFT JOIN FmData.CollateralPool CP ON S.Id = CP.Id
                    LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
                    LEFT JOIN LoanAcquisition LA ON LA.Id = CP.Id AND LA.Ranking = 1
                     LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1
                    LEFT JOIN SecurityPayment SP ON SP.Id = CP.Id AND SP.Ranking = 1
                    LEFT JOIN SecurityInterestRateAI SIRAI ON SIRAI.SecurityId  = CP.Id AND SIRAI.Ranking = 1
                    LEFT JOIN SecurityInterestRateON SIRON ON SIRON.SecurityId  = CP.Id AND SIRON.Ranking = 1
                    LEFT JOIN adm.PublishedTransactions PT ON PT.RecordId = T.RecordId

                WHERE PT.RecordId IS NOT NULL
                    --AND T.TransactionId IN ('33670521938001', '27954857')
                    AND SS.SecurityStatusType IN (SELECT value FROM @SecurityStatusType)
                    AND S.ProductGroupType IN (SELECT value FROM @ProductGroupType)
                    AND LA.LoanDUSDiscloseDerivedProductType IN (SELECT value FROM @ProductType) -- e.g. DUS
                ) A
            ''') { ResultSet resultSet ->
            CSVWriter writer = new CSVWriter(new FileWriter(filepath));
            Boolean includeHeaders = true;
            writer.writeAll(resultSet, includeHeaders);
            writer.close();
            }
            sql.close();
            return true;
        } catch (Exception e) {
            println e;    
            return false;
        }
    }
}