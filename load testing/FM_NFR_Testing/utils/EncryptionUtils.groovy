package com.rockportllc.neoload.utils;

import groovy.io.FileType;
import org.apache.commons.lang3.RandomUtils;


public class EncryptionUtils {
	public static void main(String[] args) {
		
		String sapisid = "b4qUZKO4943exo9W/AmP2OAZLWGDwTsuh1";
        String origin = "https://hangouts.google.com";
		long time = System.currentTimeMillis() 

		System.out.println(getSAPISIDHash(sapisid, origin, time);
	
	}

    public static String getSAPISIDHash(String sapisid, String origin, long time) {
        
		String sapisidhash = Long.toString(time) + " " + sapisid + " " + origin;
        return sapisidhash;
    }

    private static String hashString(String password)
    {
        String sha1 = "";
        try
        {
            java.security.MessageDigest crypt = java.security.MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        }
        catch(NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash)
    {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
	
