/* Create FM Admin users for load testing */
DECLARE @Env VARCHAR(100);
DECLARE @EnvNum Varchar(1);
DECLARE @AdminId Integer;
DECLARE @NextUserId Integer;
DECLARE @CurrentUserId Integer;
DECLARE @UsersToCreate Integer;
DECLARE @count INT = 1;
DECLARE @PassHash VARBINARY(100);
DECLARE @UserSuffix VARCHAR(100);
DECLARE @UserName VARCHAR(100);

DECLARE @UsersAffected TABLE (
	UserId INTEGER,
	Action VARCHAR(20)
);

SET @Env = 'QA'; -- 'QA'
SET @EnvNum = '4'; -- '3'
-- Hash is for 'r0ckp0rt#!'
SET @PassHash = 0x22FA651BE3280A4A06F5A54586C1AD4CEE5D84F8182286A2517E8896BDA084515316BDB8436DF3626EA47C404BF051DD7A0E8A486DD1F8384DF5BF8E53625290;
SET @UsersToCreate = 10;

BEGIN TRAN;
BEGIN TRY
	SELECT @AdminId = Id FROM adm.Roles WHERE Name = 'Admin';
	IF @AdminId IS NULL 
		THROW 51000, 'NO Admin role defined', 1;

	SELECT @NextUserId = IDENT_CURRENT('adm.Users') + 1

	WHILE @count <= @UsersToCreate
	BEGIN
		SET @UserSuffix = CONCAT(@env, @envNum, RIGHT('000'+ISNULL(CONVERT(VARCHAR,@count),''),3));
		SET @UserName = CONCAT('Admin', @UserSuffix);
		SET @CurrentUserId = (SELECT Id FROM adm.Users WHERE UserId = @UserName);
		IF @CurrentUserId IS NOT NULL
			-- update existing user
			BEGIN
				UPDATE adm.USERS SET
					FirstName = CONCAT('FirstAdmin', @UserSuffix), 
					LastName = CONCAT('LastAdmin', @UserSuffix), 
					--LastAccess = null, 
					Email = CONCAT('Admin', @UserSuffix, '@rockportllc.com'), 
					IsInternal = 1,
					PasswordHash = @PassHash
				WHERE
					Id = @CurrentUserId;
				-- add Admin role if not already assigned
				IF NOT EXISTS(SELECT Id FROM adm.UserRoles 
						WHERE UserId = @CurrentUserId AND RoleId = @AdminId)
					INSERT INTO adm.UserRoles (UserId, RoleId) VALUES (
						@CurrentUserId, @AdminId);

				INSERT INTO @UsersAffected (UserId, Action) VALUES (@CurrentUserId, 'UPDATED');
			END;
		ELSE
			-- insert new user
			BEGIN
				INSERT INTO adm.USERS (FirstName, LastName, UserId, LastAccess, Email, IsInternal, PasswordHash) VALUES (
					CONCAT('FirstAdmin', @UserSuffix), 
					CONCAT('LastAdmin', @UserSuffix), 
					@UserName,
					null, 
					CONCAT('Admin', @UserSuffix, '@rockportllc.com'), 
					1, 
					@PassHash
				);
					
				--SELECT CONCAT(@env, @envNum, RIGHT('000'+ISNULL(CONVERT(VARCHAR,@count),''),3))
				INSERT INTO adm.UserRoles (UserId, RoleId) VALUES (
					@NextUserId, 
					@AdminId
				);

				INSERT INTO @UsersAffected (UserId, Action) VALUES (@NextUserId, 'INSERTED');
				SET @NextUserId = @NextUserId + 1;
			END;
		SET @count = @count + 1;
	END;
	COMMIT TRAN;
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		'FAILED' as Result, 
		ERROR_NUMBER() AS ErrorNumber, 
		ERROR_MESSAGE() as ErrorMessage, 
		ERROR_LINE() as ErrorLine;
END CATCH

/*
SELECT * FROM adm.Users u JOIN adm.UserRoles ur ON u.Id = ur.UserId WHERE u.Id > @LastUserId;
SELECT COUNT(*) FROM adm.Users u JOIN adm.UserRoles ur ON u.Id = ur.UserId WHERE u.Id > @LastUserId;
*/

SELECT * 
FROM @UsersAffected ua
	JOIN adm.Users u ON u.Id = ua.UserId
	JOIN adm.UserRoles ur ON u.Id = ur.UserId;