/*Advanced Search Mega Tab*/

WITH SecurityStatus AS
	
	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime DESC),
		SS.SecurityId,
		SS.SecurityStatusType
	FROM FmData.SecurityStatus SS )

, SecurityInterestRate AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate ASC),
		SIR.SecurityId,
		SIR.SecurityInterestRate
	FROM FMData.SecurityInterestRate SIR)

SELECT  DISTINCT

	[Status] = SS.SecurityStatusType,
	[Transaction ID] = CP.MBSPoolIdentifier,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[Interest Type] = S.SecurityInterestRateType,
	[Issue Date] = S.SecurityIssueDate,
	[Maturity Date] = S.SecurityMaturityDate,
	[Issuance UPB] = S.SecurityIssueAmount,
	[Paying PTR] = SIR.SecurityInterestRate

FROM FmData.CollateralPool CP
	LEFT JOIN FmData.Security S ON S.Id = CP.Id
 	LEFT JOIN SecurityStatus SS ON SS.SecurityId = CP.Id AND SS.Ranking = 1
	LEFT JOIN SecurityInterestRate SIR ON SIR.SecurityId  = CP.Id AND SIR.Ranking = 1

WHERE S.ProductGroupType = 'Mega'



