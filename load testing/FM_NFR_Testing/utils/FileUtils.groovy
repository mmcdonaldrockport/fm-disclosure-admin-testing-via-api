package com.rockportllc.neoload.utils;

import groovy.io.FileType;
import org.apache.commons.lang3.RandomUtils;


public class FileUtils {
    public static String getRandomFilename(String filePath) {
        
		def list = []

        def dir = new File(filePath);
        dir.eachFileRecurse (FileType.FILES) { file ->
            list << file.name
        }
		
		def filenameIndex = RandomUtils.nextInt(0,list.size());
        return list[filenameIndex];
		
		return filePath;
    }

}
