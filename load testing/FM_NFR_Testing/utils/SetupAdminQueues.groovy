import groovy.sql.Sql;
import java.sql.ResultSet;
import com.opencsv.CSVWriter;
import com.opencsv.CSVReader;
import DBUtils;

String NL_VARS_DIR = 'C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/load testing/FM_NFR_Testing/variables';

return setupAdminQueues(NL_VARS_DIR);

boolean setupAdminQueues(def varsDir) {
     def env = getEnv(varsDir);
     println 'Packing queues for ' + env
     
     def adminDBInfo = getAdminDBInfo(env, varsDir);
     def extDBInfo = getExtDBInfo(env, varsDir);
     println adminDBInfo;
     
     return populateFMAdminQueues(varsDir, adminDBInfo['server'], adminDBInfo['instance'], adminDBInfo['database']);
}    


String getEnv(def varsDir) {
    def env = "";
    
    CSVReader envReader = new CSVReader(new FileReader(varsDir + "/env.csv"), (char)',' , (char)'"', 1);
    String[] nextLine;
    if ((nextLine = envReader.readNext()) != null) {
         env = nextLine[0];
    }
    
    return env;
} 

Map configToMap(String filepath) {
    def configMap = [:]
    def headerLine = [];
    def dataLine = [];
    //def dbInfo = [:];

    CSVReader cfgReader = new CSVReader(new FileReader(filepath), (char)',' , (char)'"', 0);
    if ((headerLine = cfgReader.readNext()) != null && (dataLine = cfgReader.readNext()) != null) {
         headerLine.eachWithIndex { header, index ->
            configMap.put(header.toUpperCase(), dataLine[index]);
        }
    } else {
        //println "No config";
    }
    return configMap;
} 

def getAdminDBInfo(String env, String varsDir) {
    def config = configToMap(varsDir + "/config_${env}.csv");
    //println config;
    def dbInfo = [:];
    
    dbInfo.put('database', config['ADMINDB']);
    dbInfo.put('server', config['ADMINDBSERVER']);
    dbInfo.put('instance', config['ADMINDBINSTANCE']);
    dbInfo.put('username', config['ADMINDBUSERNAME']);
    dbInfo.put('password', config['ADMINDBPASWORD']);
    
    return dbInfo;
}

 def getExtDBInfo(String env, String varsDir) {
    def config = configToMap(varsDir + "/config_${env}.csv");
    //println config;
    def dbInfo = [:];
    
    dbInfo.put('database', config['EXTDB']);
    dbInfo.put('server', config['EXTDBSERVER']);
    dbInfo.put('instance', config['EXTDBINSTANCE']);
    dbInfo.put('username', config['EXTDBUSERNAME']);
    dbInfo.put('password', config['EXTDBPASWORD']);
    
    return dbInfo;
}


def populateFMAdminQueues(String dirPath, String server, String instance, String database, String username=null, String password=null) {
    def adminQueueFiles = [
        'AdditionalDisclosure':null,
        'DisclosureOperations':null,
        'SpecializedTransactions':null
    ];
    
    //prepare files
    adminQueueFiles.each { qName, qFile ->
        try {
            qFile = new File(dirPath + '/' + qName + 'SQ.csv');
            adminQueueFiles[qName] = qFile;
            if (qFile.exists() && !qFile.delete()) {
                println qName ' file could not be processed.';
                return false;
            } else {
                qFile.createNewFile();
                //qFile << qName + '\r\n';
                //println qFile
            }
        } catch (Exception e) {
            println e;
            return false;
        }
    }
    
    def sql = getNewSqlInstance(server, instance, database, username, password);
    
    sql.eachRow('''
        SELECT 
            Id, 
            OpsStatus as 'DisclosureOperations',
            ADStatus as 'AdditionalDisclosure', 
            STStatus as 'SpecializedTransactions'
        FROM 
            [adm].[v_DisclosureOperations];
    
    ''') { row ->
        adminQueueFiles.each { qName, qFile ->
            if (row[qName] == 1) {
                qFile.append(row['Id'] + '\r\n');
            }
        }
    }
}



def getNewSqlInstance(String serverInstance, String database, String username, String password) {
    return getNewSqlInstance(serverInstance, null, database, username, password);
}

def getNewSqlInstance(String server, String instance, String database, String username, String password) {
    def sql = null;
    def serverInstance = server;
    if (instance != null && instance != '') {
        serverInstance += '\\' + instance;
    }
    
    try {
    
        if (username == null) {
            // if username is null or empty assume integrated security
            sql = Sql.newInstance(
                "jdbc:sqlserver://" + serverInstance + ";databaseName=" + database + ";integratedSecurity=true", 
                "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } else {
            sql = Sql.newInstance(
                "jdbc:sqlserver://" + serverInstance + ";databaseName=" + database + ";integratedSecurity=true", 
                username, password, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }

        /*  //jtds version
            sql = Sql.newInstance("jdbc:sqlserver://DVFM01-D03\\QAFM01_D04;databaseName=DisclosureAdmin2;integratedSecurity=true", 
           "com.microsoft.sqlserver.jdbc.SQLServerDriver")
        */
        return sql;
    } catch (Exception e) {
        println e;
        return null;
    }
}
