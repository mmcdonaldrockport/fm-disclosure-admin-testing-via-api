package com.rockportllc.neoload.utils;

import groovy.sql.Sql;
import java.sql.ResultSet;

public class DBUtils {
    public static void main(String[] args) {
        //getReturnToHoldIds('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2');
       //println(getTransactionInfo('747532','DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2'));
       //println(getAvailableAdminTransactions('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2'));
       //println(getStickeringEligibleTransactions('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2'));
       //println(getAvailableAdminTransactions('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2'));
       //println(getAvailableStickeringTransactions('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin2'));
       println(getStickeringEligibleTransactions('DVFM01-D03', 'QAFM01_D04', 'DisclosureAdmin'));
       //println(getAvailableExternalTransactions('172.18.10.86', 'LIVESQLDB101', 'DisclosureExternal'));
    }
   
   
    public static getStickeringEligibleTransactions(String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        def result = [];
        
        if (sql) {
            sql.eachRow('''   
            SELECT DISTINCT 
                D.AgreementIdentifier AS DealAgreementIdentifier,
                D.DealIdentifier,
                --L.LoanIdentifier,
                --L.FannieMaeLoanNumber,
                --C.CollateralIdentifier,
                --P.PropertyIdentifier,
                CP.MBSPoolIdentifier,
                S.SecurityCUSIPIdentifier,
                S.SecurityIssueAmount
            FROM FmData.CollateralPool CP
                INNER JOIN adm.PublishedTransactions PT ON PT.RecordId = CP.Id
                LEFT JOIN FmData.Security S ON S.Id = CP.Id
                LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
                LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
                LEFT JOIN FmData.Collateral C ON C.Id = LC.CollateralId
                LEFT JOIN FmData.Property P ON P.Id = LC.CollateralId
                LEFT JOIN FmData.Deal d ON d.Id = l.DealId
            WHERE S.ProductGroupType <> 'Mega'
                AND S.SecurityIssueDate >= DATEADD(DAY,-90,GETDATE())
            ''' ) { row ->
                def resultRow = [:];
                   
                resultRow.put('DealAgreementIdentifier',          row['DealAgreementIdentifier']);
                resultRow.put('DealIdentifier',                   row['DealIdentifier']);
                //resultRow.put('LoanIdentifier',                   row['LoanIdentifier']);
                //resultRow.put('FannieMaeLoanNumber',              row['FannieMaeLoanNumber']);
                //resultRow.put('CollateralIdentifier',             row['CollateralIdentifier']);
                //resultRow.put('PropertyIdentifier',               row['PropertyIdentifier']);
                resultRow.put('MBSPoolIdentifier',                row['MBSPoolIdentifier']);
                resultRow.put('SecurityCUSIPIdentifier',          row['SecurityCUSIPIdentifier']);
                resultRow.put('SecurityIssueAmount',              row['SecurityIssueAmount']);
                
                result.push(resultRow);
            }
            sql.close();
        }
        return result;
    }
    
    
    public static getReturnToHoldIds(String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        def transIds = [];
        
        if (sql) {
            sql.eachRow('''
               SELECT [TransactionId] 
                FROM [adm].[v_PendingPublishing]
                WHERE PublicationType = 'Issuance'
            ''') { row ->
                        transIds.push(row['TransactionId']);
                };
            //println(transIds);
            sql.close();
        }
        return transIds;
    }
    
    public static getAvailableExternalTransactions(String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        def ids = [];
    
        if (sql) {
            sql.eachRow('''
                SELECT TOP 1000 [TransactionId] AS Id FROM [ext].[v_AdvancedSearch]
                 WHERE SecurityType = 'MBS'
                ORDER BY NEWID()
            ''') { row ->
                        ids.push(row['Id']);
                };
            //println(transIds);
            sql.close();
        }
        return ids;
    }
    
    
    
    
    public static getAvailableAdminTransactions(String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        def ids = [];
    
        if (sql) {
            sql.eachRow('''
                SELECT [Id] FROM [adm].[v_DisclosureOperations]
                ORDER BY NEWID()
            ''') { row ->
                        ids.push(row['Id']);
                };
            //println(transIds);
            sql.close();
        }
        return ids;
    }
    
    public static getAvailableStickeringTransactions(String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        def ids = [];
    
        if (sql) {
            sql.eachRow('''
                SELECT [Id] FROM [adm].[v_StickeringApprovals] 
                ORDER BY NEWID()
            ''') { row ->
                        ids.push(row['Id']);
                };
            //println(transIds);
            sql.close();
        } 
        return ids;
    }

    public static getTransactionInfo(String transId, String server, String instance, String database, String username=null, String password=null) {
        def sql = getNewSqlInstance(server, instance, database, username, password);
        
        def result = [:];
        
        if (sql) {
            sql.eachRow('''
               SELECT [Id]
          ,[TransactionId]
          ,[Product]
          ,[ExecutionType]
          ,[SettlementDate]
          ,[HoldReason]
          ,[OpsStatus]
          ,[ADStatus]
          ,[STStatus]
          ,[DocStatus]
          ,[LastUpdateDate]
          ,[LastUpdateUser]
          ,[Status]
          ,[DaysToSettlement]
          ,[Waiver]
      FROM [adm].[v_DisclosureOperations]
      WHERE Id = 747532;'''
            ) { row ->
                    result.put('Id', row['Id']);
                    result.put('TransactionId'      ,row['TransactionId'   ]);
                    result.put('Product'            ,row['Product'         ]);
                    result.put('ExecutionType'      ,row['ExecutionType'   ]);
                    result.put('SettlementDate'     ,row['SettlementDate'  ]);
                    result.put('HoldReason'         ,row['HoldReason'      ]);
                    result.put('OpsStatus'          ,row['OpsStatus'       ]);
                    result.put('ADStatus'           ,row['ADStatus'        ]);
                    result.put('STStatus'           ,row['STStatus'        ]);
                    result.put('DocStatus'          ,row['DocStatus'       ]);
                    result.put('LastUpdateDate'     ,row['LastUpdateDate'  ]);
                    result.put('LastUpdateUser'     ,row['LastUpdateUser'  ]);
                    result.put('Status'             ,row['Status'          ]);
                    result.put('DaysToSettlement'   ,row['DaysToSettlement']);
                    result.put('Waiver'             ,row['Waiver'          ]);    
                                   
                };
            //println(transIds);
            sql.close();
        }
        return result;
    
    }


    public static getNewSqlInstance(String serverInstance, String database, String username, String password) {
        return getNewSqlInstance(serverInstance, null, database, username, password);
    }


    public static getNewSqlInstance(String server, String instance, String database, String username, String password) {
        def sql = null;
        def serverInstance = server;
        if (instance != null && instance != '') {
            serverInstance += '\\' + instance;
        }
        
        try {
        
            if (username == null) {
                // if username is null or empty assume integrated security
                sql = Sql.newInstance(
                    "jdbc:sqlserver://" + serverInstance + ";databaseName=" + database + ";integratedSecurity=true", 
                    "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } else {
                sql = Sql.newInstance(
                    "jdbc:sqlserver://" + serverInstance + ";databaseName=" + database + ";integratedSecurity=true", 
                    username, password, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            }
    
            /*  //jtds version
                sql = Sql.newInstance("jdbc:sqlserver://DVFM01-D03\\QAFM01_D04;databaseName=DisclosureAdmin2;integratedSecurity=true", 
               "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            */
            return sql;
        } catch (Exception e) {
            println e;
            return null;
        }
    }

    public static Map rowAsMap(groovy.sql.GroovyRowResult row) {
        def rowMap = [:]
        row.keySet().each {column ->
            rowMap[column] = row[column]
        }
        return rowMap;
    }

}