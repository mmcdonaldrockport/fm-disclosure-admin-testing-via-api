/* Create FM Admin users for load testing */
DECLARE @Env VARCHAR(100);
DECLARE @EnvNum Varchar(1);
DECLARE @ExtId Integer;
--DECLARE @LastUserId Integer;
DECLARE @NextUserId Integer;
DECLARE @CurrentUserId Integer;
DECLARE @UsersToCreate Integer;
DECLARE @count INT = 1;
DECLARE @PassHash VARBINARY(100);
DECLARE @UserSuffix VARCHAR(100);
DECLARE @RegOn DateTime;
DECLARE @ActiveOn DateTime;
DECLARE @AgreedOn DateTime;
DECLARE @MaxUsageAgreementId Integer;
DECLARE @EMailAddress varchar(200);

DECLARE @UsersAffected TABLE (
	UserId INTEGER,
	Action VARCHAR(20)
);


SET @Env = 'QA'; -- 'QA'
SET @EnvNum = '4'; -- '3'
-- Hash is for 'P@ssw0rd'
SET @PassHash = 0x705BE5ED3B9081855B53E91D4617B60F7709F47A2A8C21E09004ACB22C72FE3C2C4BBD31ACC04EEAF20B27FAF89C53E3A846E1EB920FDFA30FCEC6F6B19D6EE3;
SET @UsersToCreate = 100;
SET @RegOn = GETDATE();
SET @ActiveOn = GETDATE() + DATEPART(MINUTE,1);
SET @AgreedOn = GETDATE() + DATEPART(MINUTE,2);

--SELECT @Id = Id FROM adm.Roles WHERE Name = 'Admin';
BEGIN TRAN;
BEGIN TRY
	SELECT @MaxUsageAgreementId = MAX(Id) FROM ext.UsageAgreements;	
	SELECT @NextUserId = IDENT_CURRENT('ext.Users') + 1;

	WHILE @count <= @UsersToCreate
	BEGIN
		SET @UserSuffix = CONCAT(@env, @envNum, RIGHT('000'+ISNULL(CONVERT(VARCHAR,@count),''),3));
		SET @EMailAddress = CONCAT('Ext', @UserSuffix, '@rockportllc.com');
		IF EXISTS (SELECT Id FROM ext.Users WHERE EmailAddress = @EmailAddress)
			-- update existing user
			BEGIN
				SELECT @CurrentUserId = Id FROM ext.Users WHERE EmailAddress = @EmailAddress;
				UPDATE ext.Users SET 
					FirstName = CONCAT('FirstExt', @UserSuffix),
					LastName = CONCAT('LastExt', @UserSuffix),
					Company = CONCAT('Test Company ', @UserSuffix), 
					CompanyType =1, -- companyType 
					CompanyTypeOther = NULL, -- companyothertype
					--RegisteredOn = @RegOn,
					--ActivatedOn = @ActiveOn,
					IsLocked = 0, --isLocked
					Phone = CONCAT('123-456-', @EnvNum, RIGHT('000'+ISNULL(CONVERT(VARCHAR,@count),''),3))
				WHERE 
					Id =  @CurrentUserId;

				UPDATE ext.UserPassword
					SET PasswordHash = @PassHash
				WHERE 
					Id =  @CurrentUserId;

				UPDATE ext.UserUsageAgreements SET
					UsageAgreementId = @MaxUsageAgreementId,
					AgreedOn = @AgreedOn
				WHERE 
					UserId =  @CurrentUserId;
				
				INSERT INTO @UsersAffected (UserId, Action) VALUES (@CurrentUserId, 'UPDATED');
			END;
		ELSE
			-- insert new user
			BEGIN
				INSERT INTO ext.Users (EmailAddress, FirstName, LastName, Company, CompanyType, CompanyTypeOther,RegisteredOn, ActivatedOn, IsLocked, Phone) VALUES (
					@EMailAddress, 
					CONCAT('FirstExt', @UserSuffix), 
					CONCAT('LastExt', @UserSuffix), 
					CONCAT('Test Company ', @UserSuffix),
					1, -- companyType
					null, -- companyothertype
					@RegOn,
					@ActiveOn,
					0, --isLocked
					CONCAT('123-456-', @EnvNum, RIGHT('000'+ISNULL(CONVERT(VARCHAR,@count),''),3))
				);
			
				INSERT INTO ext.UserPassword (Id, PasswordHash) VALUES (
					@NextUserId, 
					@PassHash
				);
				INSERT INTO ext.UserUsageAgreements (UserId, UsageAgreementId, AgreedOn) VALUES (
					@NextUserId, 
					@MaxUsageAgreementId,
					@AgreedOn
				);

				INSERT INTO @UsersAffected (UserId, Action) VALUES (@NextUserId, 'INSERTED');
				SET @NextUserId = @NextUserId + 1;
			END;
		SET @count = @count + 1;
	END;
	COMMIT TRAN;
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		'FAILED' as Result, 
		ERROR_NUMBER() AS ErrorNumber, 
		ERROR_MESSAGE() as ErrorMessage, 
		ERROR_LINE() as ErrorLine;
END CATCH
/*
SELECT * 
FROM ext.Users u 
	JOIN ext.UserPassword up ON u.Id = up.Id 
	JOIN ext.UserUsageAgreements uua ON u.Id = uua.UserId
WHERE u.Id > @LastUserId;
*/

SELECT * 
FROM @UsersAffected ua
	JOIN ext.Users u ON u.Id = ua.UserId
	JOIN ext.UserPassword up ON u.Id = up.Id 
	JOIN ext.UserUsageAgreements uua ON u.Id = uua.UserId

