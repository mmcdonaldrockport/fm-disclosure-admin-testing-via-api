/*Advanced Search DMBS Tab*/

WITH SecurityStatus AS
	
	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime DESC),
		SS.SecurityId,
		SS.SecurityStatusType
	FROM FmData.SecurityStatus SS )

SELECT  DISTINCT

	[Status] = SS.SecurityStatusType,
	[Transaction ID] = CP.MBSPoolIdentifier,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[Issue Date] = S.SecurityIssueDate,
	[Maturity Date] = S.SecurityMaturityDate,
	[Issuance UPB] = S.SecurityIssueAmount

FROM FmData.Security S
	LEFT JOIN FmData.CollateralPool CP ON Cp.Id = S.Id
	LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
	LEFT JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.StructuredTransactionCollateralGroupId = STCG.Id
	LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId
 	LEFT JOIN SecurityStatus SS ON SS.SecurityId = CP.Id AND SS.Ranking = 1

WHERE S.ProductGroupType = 'REMIC'



