/*Advanced Search Criteria*/
WITH Transactions AS 
	
	(SELECT DISTINCT
		[RecordId] = COALESCE(S.Id, D.Id), 
		[TransactionId] = COALESCE(CP.MBSPoolIdentifier, D.Name)
			
	FROM FmData.Security S
		 -- REMIC Security Joins
			LEFT JOIN FmData.CollateralPool CP ON Cp.Id = S.Id
			LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
			LEFT JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.StructuredTransactionCollateralGroupId = STCG.Id
			LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId)

, SecurityPayment As 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY SP.SecurityPaymentEffectiveDate DESC),
		S.Id,
		[CurrentUPB] = COALESCE(SP.SecurityUnpaidPrincipalBalanceAmount, S.SecurityIssueAmount)
	FROM FmData.Security S 
		LEFT JOIN FmData.SecurityPayment SP ON SP.SecurityId = S.Id)

, SecurityStatus AS
	
	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime DESC),
		SS.SecurityId,
		SS.SecurityStatusType
	FROM FmData.Security S 
		LEFT JOIN FmData.SecurityStatus SS ON SS.SecurityId = S.Id)

, SecurityInterestRateON AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate DESC),
		SIR.SecurityId,
		SIR.SecurityInterestRate
	FROM FMData.SecurityInterestRate SIR)

, SecurityInterestRateAI AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate ASC),
		SIR.SecurityId,
		SIR.SecurityInterestRate
	FROM FMData.SecurityInterestRate SIR)

, LoanAcquisition AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CP.Id ORDER BY L.Id),
		CP.Id,
		LA.LoanDUSDiscloseDerivedProductType
	FROM FmData.CollateralPool CP
		LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
		LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id)

SELECT  DISTINCT

	[Security Status] = SS.SecurityStatusType,
	[Security Type] = S.ProductGroupType, 
	[Product Type] = LA.LoanDUSDiscloseDerivedProductType,
	[Prefix] = S.MBSPoolPrefixType,
	[Interest Type] = S.SecurityInterestRateType,
	[ARM Subtype] =  S.MBSSecurityARMSubType,
	[Issue Date] = S.SecurityIssueDate,
	[Paying Pass-Through Rate] = COALESCE(SIRON.SecurityInterestRate, SIRAI.SecurityInterestRate),
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Issuance UPB] = S.SecurityIssueAmount,
	[Transaction ID] = T.TransactionId,
	PT.RecordId

FROM FMData.Security S  
	
	LEFT JOIN Transactions T ON T.RecordId = S.Id
	LEFT JOIN FmData.CollateralPool CP ON S.Id = CP.Id
	LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
	LEFT JOIN LoanAcquisition LA ON LA.Id = CP.Id AND LA.Ranking = 1
 	LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1
	LEFT JOIN SecurityPayment SP ON SP.Id = CP.Id AND SP.Ranking = 1
	LEFT JOIN SecurityInterestRateAI SIRAI ON SIRAI.SecurityId  = CP.Id AND SIRAI.Ranking = 1
	LEFT JOIN SecurityInterestRateON SIRON ON SIRON.SecurityId  = CP.Id AND SIRON.Ranking = 1
	LEFT JOIN adm.PublishedTransactions PT ON PT.RecordId = T.RecordId

WHERE PT.RecordId IS NOT NULL
	AND SS.SecurityStatusType = 'Active'
	AND CP.MBSPoolIdentifier = 'AM1856'





