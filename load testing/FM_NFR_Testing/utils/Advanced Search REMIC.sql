/*Advanced Search DMBS Tab*/

WITH SecurityStatus AS
	
	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SS.SecurityId ORDER BY SS.SecurityStatusDatetime DESC),
		SS.SecurityId,
		SS.SecurityStatusType
	FROM FmData.SecurityStatus SS )

, SecurityInterestRate AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SIR.SecurityId ORDER BY SIR.SecurityInterestRateEffectiveDate DESC),
		SIR.SecurityId,
		SIR.SecurityInterestRate
	FROM FMData.SecurityInterestRate SIR)

SELECT  DISTINCT

	[Status] = SS.SecurityStatusType,
	[Transaction ID] = D.Name,
	[Group] = STCG.DealGroupNumber , 
	[Class] = S.SecurityClassName,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Issue Date] = S.SecurityIssueDate,
	[Final Distribution Date] = --TPD Field: Use Maturity Date but swap out the day component for the Payment Day Number
		
		(
			(CAST(FORMAT(S.SecurityMaturityDate, 'MM') AS varchar(max))) + '/' +
			(CAST(FORMAT(S.SecurityPaymentDayNumber, 'D') AS varchar(max))) + '/' +
			(CAST(FORMAT(S.SecurityMaturityDate, 'yyyy') AS varchar(max)))
		),
	[Issuance UPB] = S.SecurityIssueAmount,
	[Issuance Interest Rate] = SIR.SecurityInterestRate

FROM FmData.Security S
	LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id
	LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
	LEFT JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.StructuredTransactionCollateralGroupId = STCG.Id
	LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId
 	LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1
	LEFT JOIN SecurityInterestRate SIR ON SIR.SecurityId = S.Id AND SIR.Ranking = 1

WHERE S.ProductGroupType = 'REMIC'
