import groovy.sql.Sql;
import java.sql.ResultSet;
import com.opencsv.CSVWriter;
import com.opencsv.CSVReader;
import DBUtils;

String NL_VARS_DIR = 'C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/load testing/FM_NFR_Testing/variables';

return setupAdminQueues(NL_VARS_DIR);

boolean setupAdminQueues(def varsDir) {
     def env = DBUtils.getEnv(varsDir);
     println 'Packing queues for ' + env
     
     def adminDBInfo = DBUtils.getAdminDBInfo(env, varsDir);
     //def extDBInfo = DBUtils.getExtDBInfo(env, varsDir);
     println adminDBInfo;
     
     return populateFMAdminQueues(varsDir, adminDBInfo['server'], adminDBInfo['instance'], adminDBInfo['database']);
}    

def populateFMAdminQueues(String dirPath, String server, String instance, String database, String username=null, String password=null) {
    def adminQueueFiles = [
        'AdditionalDisclosure':null,
        'DisclosureOperations':null,
        'SpecializedTransactions':null
    ];
    
    //delete files
    adminQueueFiles.each { qName, qFile ->
        try {
            qFile = new File(dirPath + '/' + qName + 'SQ.csv');
            adminQueueFiles[qName] = qFile;
            if (qFile.exists() && !qFile.delete()) {
                //println qName ' file could not be processed.';
                throw new IOException(qName + ' file could not be deleted.');;
            } 
        } catch (Exception e) {
            println e;
            return false;
        }
     }

     // create new files
     adminQueueFiles.each { qName, qFile ->
        try {    
            qFile.createNewFile();
        } catch (Exception e) {
            println e;
            return false;
        }
    }
    
    def sql = DBUtils.getNewSqlInstance(server, instance, database, username, password);
    
    sql.eachRow('''
        SELECT 
            Id, 
            OpsStatus as 'DisclosureOperations',
            ADStatus as 'AdditionalDisclosure', 
            STStatus as 'SpecializedTransactions'
        FROM 
            [adm].[v_DisclosureOperations];
    
    ''') { row ->
        adminQueueFiles.each { qName, qFile ->
            if (row[qName] == 1) {
                qFile.append(row['Id'] + '\r\n');
            }
        }
    }
    
    return true;
}
