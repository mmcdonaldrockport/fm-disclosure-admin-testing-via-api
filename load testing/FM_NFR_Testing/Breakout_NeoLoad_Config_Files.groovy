// NeoLoad
// breakout config_master.csv to individual files for each environment

@Grab('org.apache.commons:commons-csv:1.2')
import org.apache.commons.csv.CSVParser;
import static org.apache.commons.csv.CSVFormat.*;

import java.nio.file.Paths

def master = 'C:/Users/mmcdonald.ROCKPORT/projects/FM_NFR_Testing/FM_NFR_Testing/config_master.csv';
def targetDir = 'C:/Users/mmcdonald.ROCKPORT/projects/FM_NFR_Testing/FM_NFR_Testing';

Paths.get(master).withReader { reader ->
    CSVParser csv = new CSVParser(reader, DEFAULT.withHeader())
    
    def hdr = ""; // header for each file (fixed)
    def line = ""; // line for each file
    
    csv.getHeaderMap().each { k, v -> 
        hdr += '"' + k.replace('"','""') + '",';
    };
    
    for (record in csv.iterator()) {
         line = "";
         csv.getHeaderMap().each { k, v -> 
            line += '"' + record[v].replace('"','""') + '",';
         };
         def f = new File( targetDir, 'config_' + record[0] + '.csv');
         f.setText(hdr[0..-2] + '\n');
         f.append(line[0..-2]);
         println("wrote " + f.path); 
    }
}