'use strict'

angular.module('dusdisclose').directive('dusTransactionDocuments', [function () {
    return {
        restrict: 'AE',
        templateUrl: window.globals.extViews.get('client/transaction/documents/dus-transaction-documents.html'),
        scope: {
            additionalDisclosure: '<',
            issuanceDocuments: "<",
            ongoingDocumentDates: "<",
            transactionId: "<",
            transactionIdentifier: "<"
        },
        replace: true,
        controller: ['$scope', 'WebService', function ($scope, WebService) {

            $scope.ongoingDocuments = [];
            $scope.ongoingIds = [];

            $scope.versionChecks = {};

            $scope.initOngoingMonth = function (date) {
                if (!date) return false;

                $scope.ongoingMonth = date;

                WebService.getPromise('transaction/ongoing/types', null, { useMoments: true }).then(function (response) {
                    $scope.ongoingDocuments = response.data;
                });
            };

            $scope.updateOngoingMonth = function (date) {
                $scope.ongoingMonth = date;
            };

            $scope.issuanceDocumentTriage = function (documents) {
                if (!documents || documents.length == 0) return false;
                $scope.issuanceDocumentsByVersion = {};

                for (var i = 3; i >= 1; i--) {
                    var list = documents.filter(function (doc) { return doc.VersionId === i });
                    if (list.length > 0)
                        $scope.issuanceDocumentsByVersion[list[0].VersionName] = list;
                }
            };

            $scope.selectAllOngoing = function () {
                $scope.ongoingDocuments.forEach(function (doc) {
                    doc.Selected = true;
                });
            };

            $scope.selectAllOfVersion = function (version) {
                $scope.issuanceDocumentsByVersion[version].forEach(function (doc) {
                    doc.Selected = $scope.versionChecks[version];
                });
            };

            $scope.selectAllIssuance = function () {
                Object.keys($scope.issuanceDocumentsByVersion).forEach(function (version) {
                    $scope.versionChecks[version] = true;
                    $scope.selectAllOfVersion(version);
                });
            };

            $scope.setVersionCheck = function (version) {
                var allSelected = $scope.issuanceDocumentsByVersion[version].every(function (doc) { return doc.Selected });
                if (allSelected)
                    $scope.versionChecks[version] = true;
                else
                    $scope.versionChecks[version] = false;
            };

            $scope.getSelectedIssuance = function () {
                var issuanceIds = [];

                if (!$scope.issuanceDocumentsByVersion) return issuanceIds;

                Object.keys($scope.issuanceDocumentsByVersion).forEach(function (version) {
                    $scope.issuanceDocumentsByVersion[version].forEach(function (doc) {
                        if (doc.Selected)
                            issuanceIds.push(doc.Id);
                    });
                });

                return issuanceIds;
            };

            $scope.getSelectedOngoing = function () {
                var selectedIds = [];

                $scope.ongoingDocuments.forEach(function (doc) {
                    if (doc.Selected)
                        selectedIds.push(doc.Id);
                });

                return selectedIds;
            };

            $scope.getSelectedDocumentZip = function () {
                var selectedOngoing = $scope.getSelectedOngoing();
                var selectedIssuance = $scope.getSelectedIssuance();

                if (selectedOngoing.length == 0 && selectedIssuance.length == 0) {
                    $scope.documentZipRequest = "";
                    return;
                }

                var month = $scope.ongoingMonth ? $scope.ongoingMonth.format('YYYY-MM-DD') : null;

                $scope.documentZipRequest = "service/transaction/" + $scope.transactionId + "/documents/?month=" + month + "&ongoingIds=" + selectedOngoing.join(',') + "&issuanceIds=" + selectedIssuance.join(',') + "&transactionIdentifier=" + $scope.transactionIdentifier;
            };

        }]
    }
}]);