﻿'use strict';

var app = angular.module('app', ['ui.bootstrap', 'ui.router', 'ngAnimate', 'angularytics', 'ui.tree', 'ui.select']);

app.config(['$stateProvider', '$httpProvider', '$provide', '$urlRouterProvider', 'AngularyticsProvider', function ($stateProvider, $httpProvider, $provide, $urlRouterProvider, AngularyticsProvider) {
    //$urlRouterProvider.otherwise("/dashboard/0");
    $httpProvider.interceptors.push('httpInterceptor');

    AngularyticsProvider.setEventHandlers(['GoogleUniversal']);

    var defaultOnEnter = function () {
        return;
    };
    var defaultOnExit = function () {
        return
    };
    var viewDirectory = './app/views/';

    var defineState = function (name, state) {
        if (!state.onEnter) state.onEnter = defaultOnEnter;
        if (!state.onExit) state.onExit = defaultOnExit;
        //if (!state.templateFile && !state.template && !state.templateUrl) state.templateFile = name + ".html";
        //if (!state.controller) state.controller = name;
        if (state.templateFile) state.templateUrl = viewDirectory + state.templateFile;
        $stateProvider.state(name, state);
    };

    defineState('main', {
        url: '',
        templateFile: 'main.html',
        controller: 'RootController',
        controllerAs: 'rootctrl',
        onEnter: ['AppLoader', '$location', 'AppConstants', function (AppLoader, $location, AppConstants, $state) {
            if ($location.absUrl().toLowerCase().indexOf('login') == -1) {
                AppLoader.load().then(function(){
                    if($location.url()==="") $location.url("/dashboard/0");
                });
            }
        }
        ]
    });

    defineState('main.dashboard', {
        url: '/dashboard/:tab',
        templateFile: 'dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'tabctrl',
        data: {
            title: 'Dashboard',
            showHeader: true
        }
    });

    defineState('main.buildreport', {
        url: '/build-report?reportId',
        template: '<div ui-view></div>',
        controller: 'BuildReportController',
        controllerAs: 'buildctrl',
        abstract: true,
        resolve: {
            configs: ['$stateParams', 'ReportsService',
                function ($stateParams, ReportsService) {
                    if ($stateParams.reportId) {
                        return ReportsService.getReportById($stateParams.reportId).then(function (report) {
                            return ReportsService.getDataSetById(report.DataSetId, true).then(function (dataset) {
                                return { report: report, dataset: dataset };
                            })
                        });
                    }
                    else
                        return {};
                }
            ]
        },
        data: {
            showHeader: false
        }
    });

    defineState('main.buildreport.datasets', {
        url: '/datasets',
        templateFile: 'buildreport-datasets.html',
        controller: 'DataSetsController',
        controllerAs: 'ctrl',
        resolve: {
            datasets: ['ReportsService',
                function (ReportsService) {
                    return ReportsService.getDataSets(true);
                }
            ]
        },
        data: {
            nextStep: 'Fields',
            title: 'Data Set'
        }
    });

    defineState('main.buildreport.fields', {
        url: '/fields',
        templateFile: 'buildreport-fields.html',
        controller: 'ReportFieldsController',
        controllerAs: 'ctrl',
        data: {
            beforeStep: 'DataSets',
            nextStep: 'Filters',
            title: 'Fields'
        }
    });

    defineState('main.buildreport.filters', {
        url: '/filters',
        templateFile: 'buildreport-filters.html',
        controller: 'ReportFiltersController',
        controllerAs: 'ctrl',
        data: {
            beforeStep: 'Fields',
            nextStep: 'Groups',
            title: 'Filters'
        }
    });

    defineState('main.buildreport.groupings', {
        url: '/groupings',
        templateFile: 'buildreport-groupings.html',
        controller: 'ReportGroupingsController',
        controllerAs: 'ctrl',
        data: {
            beforeStep: 'Filters',
            nextStep: 'Display',
            title: 'Groups'
        }
    });

    defineState('main.buildreport.display', {
        url: '/display',
        templateFile: 'buildreport-display.html',
        controller: 'ReportDisplayController',
        controllerAs: 'ctrl',
        data: {
            beforeStep: 'Groups',
            nextStep: 'Results',
            title: 'Display'
        }
    });

    defineState('main.buildreport.results', {
        url: '/results?page',
        templateFile: 'buildreport-results.html',
        controller: 'ReportResultsController',
        controllerAs: 'ctrl',
        reloadOnSearch : false,
        data: {
            nextStep: '',
            title: 'Results'
        },
        onExit: ['$rootScope', function ($rootScope) {
            $rootScope.showSpinner = false;
        }]
    });

    defineState('timeout', {
        url: '/timeout',
        templateFile: 'timeout.html'
    });
}]);

app.config(['$provide', function ($provide) {
    $provide.decorator('AppConstants', ['$delegate', function ($delegate) {
        var constants = document.getElementById('constants');
        if (constants)
            $delegate = angular.extend($delegate, JSON.parse(constants.innerHTML));
        return $delegate;
    }]);
}]);

app.config(['$animateProvider', function ($animateProvider) {
    $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
}]);

app.config(['$provide', function ($provide) {
    $provide.decorator('Angularytics', ['$delegate', function ($delegate) {
        var set = function (key, value) {
            ga('set', key, value);
        }
        $delegate = angular.extend($delegate, { set: set });
        return $delegate;
    }]);
}]);

app.run(['$rootScope', 'RouteService', 'Angularytics', '$location', 'AppConstants', function ($rootScope, RouteService, Angularytics, $location, AppConstants) {
    $rootScope.showLoading = false;
    $rootScope.systemInfo = AppConstants.SYSTEM_INFO;
    Angularytics.init();
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        RouteService.routeChangeEvent(event, toState, toParams, fromState, fromParams);
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.title = RouteService.getBrowserTitle(toState);
        $rootScope.showHeader = toState.data ? toState.data.showHeader : false;
    });
}]);


app.factory('httpInterceptor', ['$q', '$window', '$injector', 'AppConstants', function ($q, $window, $injector, AppConstants) {
    return {
        responseError: function (response) {
            //console.log(response);
            if (response.status === 401) {
                if (!AppConstants.SYSTEM_INFO.Integrated) {
                    $window.location.assign("Login");
                }
                else {
                    $injector.get('$state').go('timeout');
                }
                return $q.reject(response);
            }
            else {
                var ModalService = $injector.get('ModalService');

                if (response.data.Error == undefined)
                    ModalService.showMessageModal('An issue has occurred with your request.', 'If the problem persists, please contact Customer Support.  HTTP status code: ' + response.status, undefined, 'Cancel');
                else
                    ModalService.showMessageModal('An issue has occurred with your request.', 'If the problem persists, please contact Customer Support.  Reference Number:' + response.data.Error.Id + '\n' + response.data.Error.Message, undefined, 'Cancel');

                return $q.reject(response);
            }
        }
    };
}]);