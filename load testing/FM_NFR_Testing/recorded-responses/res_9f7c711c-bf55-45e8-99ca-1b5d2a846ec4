﻿'use strict';

angular.module('dusdisclose').factory('WebService', ['$http', 'Notification', 'ModalService', '$state',
    function ($http, Notification, ModalService, $state) {
        var prefix = "service/";
        var useCustomErrorHandler = false;

        var transformResponse = function (obj, config) {
            var dateRegEx = /^\d{4}-\d{2}-\d{2}$/;
            var dateTimeRegEx = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?([+,-]\d{2}:\d{2})?$/;
            var timeRegEx = /^\d{2}:\d{2}:\d{2}(\.\d+)?([+,-]\d{2}:\d{2})?$/;

            for (var property in obj) {
                if (obj.hasOwnProperty(property)) {
                    var type = typeof obj[property];

                    if (type === 'object') {
                        transformResponse(obj[property], config);
                    }

                    if (type === 'string' && (dateTimeRegEx.test(obj[property]) || dateRegEx.test(obj[property]))) {
                        var isDateTime = dateTimeRegEx.test(obj[property]);
                        var momentObj = moment(obj[property]);
                        obj[property] = config.useMoments ? momentObj
                            : new Date(momentObj.year(), momentObj.month(), momentObj.date(), momentObj.hour(), momentObj.minute(), momentObj.second());
                        obj[property].isDateTime = isDateTime;
                    }
                    else if (type === 'string' && timeRegEx.test(obj[property])) {
                        if (!config.useMoments)
                            throw new Error('Not implemented');

                        obj[property] = moment(obj[property], 'HH:mm:ss');
                        obj[property].isTime = true;
                    }
                }
            }

            return obj;
        }

        var transformRequest = function (obj) {
            for (var property in obj) {
                if (obj.hasOwnProperty(property)) {
                    var type = typeof obj[property];

                    if (type === 'object') {
                        if (obj[property] instanceof moment || obj[property] instanceof Date) {
                            var momentObj = moment(obj[property]);
                            if (obj[property].hasOwnProperty('isTime') && obj[property].isTime) {
                                obj[property] = momentObj.format('HH:mm:ss');
                            } else if (obj[property].hasOwnProperty('isDateTime') && obj[property].isDateTime)
                                obj[property] = momentObj.format('YYYY-MM-DDTHH:mm:ssZ');
                            else
                                obj[property] = momentObj.format('YYYY-MM-DD');
                        } else {
                            transformRequest(obj[property]);
                        }
                    }
                }
            }

            return obj;
        }

        var appendTransform = function (defaults, transform) {

            // We can't guarantee that the default transformation is an array
            defaults = angular.isArray(defaults) ? defaults : [defaults];

            // Append the new transformation to the defaults
            return defaults.concat(transform);
        }

        var reqPromise = function (req) {
            req.url = prefix + req.url;
            req.config = angular.extend({ useMoments: false, transform: true, customErrorHandler: useCustomErrorHandler }, req.config);

            if (req.config.transform) {
                req.transformRequest = appendTransform(function (value) {
                    return transformRequest(value);
                }, $http.defaults.transformRequest);

                req.transformResponse = appendTransform($http.defaults.transformResponse, function (value) {
                    return transformResponse(value, req.config);
                });
            }

            Notification.showLoader(true);

            var promise = $http(req);
            promise.then(
                function (res) {
                    Notification.showLoader(false);
                },
                function (res) {

                    Notification.showLoader(false);
                    if (res.status == 429) {  // handle too many requests
                        
                        var instance = ModalService.open({
                            templateUrl: window.globals.extViews.get('client/shared/views/reset-requestcount-modal.html'),
                            controller: 'ResetRequestCountController',
                            controllerAs: 'modal',
                            backdrop: 'static',
                            size: 'md',
                            keyboard: false,
                            resolve: {
                                throttleId: function () {
                                    return res.headers()["Retry-Request-Verification-Id"];
                                }
                            }
                        });

                        instance.result.then(function (emailAddress) {
                            //$timeout(function () {
                            //    Notification.show('', 'warning', 8000);
                            //}, 20);
                        });
                    }
                });
            console.log('Server Request (' + req.method + ') -> ' + req.url);
            useCustomErrorHandler = false;
            return promise;
        }

        var service = {
            getPromise: function (route, params, config) {
                var promise = reqPromise({
                    method: 'GET',
                    url: route + (params ? '?' + params : ''),
                    config: config
                });
                return promise;
            },

            postPromise: function (route, data, config) {
                var promise = reqPromise({
                    method: 'POST', url: route, data: angular.extend({}, data),
                    config: config
                });
                return promise;
            },

            putPromise: function (route, data, config) {
                return reqPromise({
                    method: 'PUT', url: route, data: angular.extend({}, data),
                    config: config
                });
            },

            savePromise: function (route, data, recId, config) {
                if (recId <= 0)
                    return this.postPromise(route, data, config);

                return this.putPromise(route + '/' + recId, data, config);
            },

            deletePromise: function (route, config) {
                var promise = reqPromise({
                    method: 'DELETE', url: route,
                    config: config
                });
                return promise;
            },

            uploadPromise: function (route, data, config) {
                return reqPromise({ method: 'POST', url: route, data: data, headers: { 'Content-Type': undefined }, config: config });
            },

            customErrorHandler: function (setVal) {
                useCustomErrorHandler = setVal;
                return this;
            }
        };

        return service;
    }]);