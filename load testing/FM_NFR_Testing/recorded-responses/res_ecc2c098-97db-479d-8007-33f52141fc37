﻿'use strict'

angular.module('dusdisclose').directive('dusPropertyDetails', [function () {
    return {
        restrict: 'E',
        templateUrl: window.globals.extViews.get('client/transaction/properties/dus-property-details.html'),
        scope: {
            property : '<'
        },
        replace: true,
        controller: ['$scope', '$sce', '$filter', '$timeout', 'WebService', 'KeyService', 'TransactionService', function ($scope, $sce, $filter, $timeout, WebService, KeyService, TransactionService) {
            
            $scope.transaction = TransactionService.getTransaction();

            $scope.tabs = [
                { id: 0, caption: 'Summary' },
                { id: 1, caption: 'Operating Statements' }
            ];

            $scope.financialColumns = ['Underwritten', 'YTDPeriod', 'PrecedingFiscalYear', 'SecondPrecedingFiscalYear', 'ThirdPrecedingFiscalYear'];

            $scope.creditFacilityFinancialColumns =
                $scope.property.TransactionProduct == 'Credit Facility' ? ['Underwritten', 'Trailing3Months', 'Trailing12Months'] : $scope.financialColumns;

            $scope.activeTab = $scope.tabs[0];

            $scope.setActiveTab = function (tab) {
                if ($scope.activeTab.id == tab.id) return;
                if (tab.id == 1 && $scope.operatingStatements.Income.length == 0)
                    getOperatingStatements();
                $scope.activeTab = tab;
            };

            $scope.propertyHasMapData = typeof $scope.property.Latitude == "number" && typeof $scope.property.Longitude == "number";

            var getOperatingStatements = function () {

                var operatingStatements = [];

                WebService.getPromise('transaction/' + $scope.transaction.Id + '/property/' + $scope.property.Id + '/financials/' + $scope.property.SpecificPropertyType, null, { useMoments: true }).then(function (response) {
                    operatingStatements = response.data;

                    $scope.operatingStatements.Income = operatingStatements.filter(function (item) {
                        return $scope.operatingLabels[item.Field].tableSection == 'Income';
                    });

                    $scope.operatingStatements.Income.forEach(function (item) {
                        item.Order = incomeOrder[item.Field] || 0;
                    });

                    $scope.operatingStatements.OperatingExpenses = operatingStatements.filter(function (item) {
                        return $scope.operatingLabels[item.Field].tableSection == 'OperatingExpenses';
                    });

                    $scope.operatingStatements.OperatingExpenses.forEach(function (item) {
                        item.Order = operatingExpensesOrder[item.Field] || 0;
                    });

                    $scope.operatingStatements.Other = operatingStatements.filter(function (item) {
                        return $scope.operatingLabels[item.Field].tableSection == 'Other';
                    });

                    $scope.operatingStatements.Other.forEach(function (item) {
                        item.Order = otherOperatingExpensesOrder[item.Field] || 0;
                    });
                });
            }

            var incomeOrder = {
                'GrossPotentialRent': 0,
                'VacancyLoss': 1,
                'LaundryVendingIncome': 2,
                'ParkingIncome': 3,
                'Medicare': 4,
                'MedicalIncome': 5,
                'MealsIncome': 6,
                'OtherIncome': 7,
                'EffectiveGrossIncome': 8
            };

            var operatingExpensesOrder = {
                'RealEstateTaxes': 0,
                'PropertyInsurance': 1,
                'Utilities': 2,
                'RepairAndMaintenance': 3,
                'ManagementFees': 4,
                'PayrollAndBenefits': 5,
                'AdvertisingAndMarketing': 6,
                'ProfessionalFees': 7,
                'GeneralAndAdministrative': 8,
                'RoomExpense': 9,
                'MealExpense': 10,
                'OtherExpenses': 11,
                'GroundRent': 12,
                'TotalOperatingExpenses': 13
            };

            var otherOperatingExpensesOrder = {
                'CapitalExpendituresReplacementReserves': 0,
                'NetCashFlow': 1,
                'PhysicalOccupancy': 2,
                'OperatingExpenseRatio': 3,
                'CapitalExReserve': 4
            };

            $scope.shouldDisplayBrackets = function (fieldKey, value) {
                if (fieldKey === 'VacancyLoss')
                    return value >= 0;

                return value < 0;
            };

            $scope.negatedValue = function (value) {
                if (value < 0)
                    return -1 * value;

                return value;
            };

            $scope.operatingLabels = {
                GrossPotentialRent: { label: 'Gross Potential Rent', tableSection: 'Income' },
                VacancyLoss: { label: 'Less: Vacancy Loss', tableSection: 'Income'},
                LaundryVendingIncome: { label: 'Laundry/Vending Income', tableSection: 'Income'},
                ParkingIncome: { label: 'Parking Income', tableSection: 'Income' },
                Medicare: { label: 'Medicare/Medicaid', tableSection: 'Income'},
                MedicalIncome: { label: 'Nursing/Medical Income', tableSection: 'Income' },
                MealsIncome: { label: 'Meals Income', tableSection: 'Income'},
                OtherIncome: { label: 'Other Income', tableSection: 'Income'},
                EffectiveGrossIncome: { label: 'Effective Gross Income (EGI)', tableSection: 'Income' },
                RealEstateTaxes: { label: 'Real Estate Taxes', tableSection: 'OperatingExpenses' },
                PropertyInsurance: { label: 'Property Insurance', tableSection: 'OperatingExpenses' },
                Utilities: { label: 'Utilities', tableSection: 'OperatingExpenses' },
                RepairAndMaintenance: { label: 'Repairs and Maintenance', tableSection: 'OperatingExpenses' },
                ManagementFees: { label: 'Management Fees', tableSection: 'OperatingExpenses' },
                PayrollAndBenefits: { label: 'Payroll & Benefits', tableSection: 'OperatingExpenses' },
                AdvertisingAndMarketing: { label: 'Advertising & Marketing', tableSection: 'OperatingExpenses' },
                ProfessionalFees: { label: 'Professional Fees', tableSection: 'OperatingExpenses' },
                GeneralAndAdministrative: { label: 'General and Administrative', tableSection: 'OperatingExpenses' },
                RoomExpense: { label: 'Room Expense - Housekeeping', tableSection: 'OperatingExpenses' },
                MealExpense: { label: 'Meal Expense', tableSection: 'OperatingExpenses' },
                OtherExpenses: { label: 'Other Expenses', tableSection: 'OperatingExpenses' },
                GroundRent: { label: 'Ground Rent', tableSection: 'OperatingExpenses' },
                TotalOperatingExpenses: { label: 'Total Operating Expenses', tableSection: 'OperatingExpenses' },
                CapitalExpendituresReplacementReserves: { label: 'Capital Expenditures/Replacement Reserves', tableSection: 'Other' },
                NetCashFlow: { label: 'Net Cash Flow (NCF)', tableSection: 'Other' },
                PhysicalOccupancy: { label: 'Physical Occupancy', tableSection: 'Other' },
                OperatingExpenseRatio: { label: 'Operating Expense Ratio', tableSection: 'Other' },
                CapitalExReserve: { label: 'Capital Ex Reserve (/per Unit)', tableSection: 'Other' }
            };

            $scope.summaryLabels = {
                EffectiveGrossIncome: { label: 'EGI' },
                TotalOperatingExpenses: { label: 'Operating Expenses' },
                NetCashFlow: { label: 'NCF' },
                PhysicalOccupancy: { label: 'Physical Occupancy' }
            };

            $scope.financialColumns = ['Underwritten', 'YTDPeriod', 'PrecedingFiscalYear', 'SecondPrecedingFiscalYear', 'ThirdPrecedingFiscalYear'];

            $scope.operatingStatements = { Income: [], OperatingExpenses: [], Other: [] };

            $scope.phasesHeaders = [{ label: 'Year', attribute: 'PhaseYear' }, { label: 'Total Units per Phase Year', attribute: 'TotalUnits' }];

            $scope.showMapOverlay = true;

            $scope.propertySummary = {
                YearBuilt: 'Year Built',
                TotalUnits: 'Total Units',
                OwnershipInterest: 'Ownership Interest',
                GeneralPropertyType: 'General Property Type',
                SpecificPropertyType: 'Specific Property Type',
                AffordableHousingType: 'Affordable Housing Type'
            };

            var getGoogleString = function (property, key) {

                var latitude = property.Latitude;
                var longitude = property.Longitude;

                return [
                    "https://www.google.com/maps/embed/v1/place?key=" + key,
                    "&q=" + [latitude, longitude].join(','),
                    "&center=" + [latitude+0.002, longitude+0.002].join(','),
                    "&zoom=14"
                ].join("");

            };

            var isScrolledIntoView = function (elem) {

                if (!elem.offset()) return false;

                var wnd = $(window);

                var viewport = {
                    top: wnd.scrollTop()
                };

                var bounds = {
                    top: elem.offset().top
                };

                viewport.bottom = viewport.top + wnd.height();                
                bounds.bottom = bounds.top + elem.height();

                return ((bounds.top <= (viewport.bottom + 200)) && (bounds.bottom >= viewport.top)) || ((bounds.top <= viewport.bottom) && (bounds.bottom >= (viewport.top - 200)));
            }

            $scope.loadMap = function () {

                var thisPropertyElement = $('#property-map-' + $scope.property.Id);

                if (!thisPropertyElement) return;

                if (isScrolledIntoView(thisPropertyElement) && !$scope.googleString) {

                    $(window).off("scroll", $scope.loadMap);

                    KeyService.getGoogleMapsKey().then(function (response) {
                        $scope.googleString = $sce.trustAsResourceUrl(getGoogleString($scope.property, response.data));
                    });
                }
            };

            if ($scope.propertyHasMapData) {
                $(window).on("scroll", $scope.loadMap);
                $timeout($scope.loadMap, 100);
            }

        }]
    }
}]);