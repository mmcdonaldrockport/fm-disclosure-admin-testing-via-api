﻿'use strict'

angular.module('dusdisclose').directive('dusTransactionHeader', [function () {
    return {
        restrict: 'E',
        templateUrl: window.globals.extViews.get('client/transaction/header/dus-transaction-header.html'),
        scope: {
            transaction: '=',
            goToDocuments: '&documentAction'
        },
        replace: true,
        controller: ['$scope', '$location', 'ModalService', 'AccountService', 'PortfolioService', 'WebService', 'AppConstants', 'Notification',
            function ($scope, $location, ModalService, AccountService, PortfolioService, WebService, AppConstants, Notification) {

            $('.transaction-header').scrollToFixed();

            $scope.userPortfolios = [];

            PortfolioService.getPortfolios().then(function (response) {
                $scope.userPortfolios = response.data;
            });

            $scope.userAuthenticated = function () {
                return AccountService.getLoggedIn().val;
            }

            $scope.getGALabel = function () {                
                return 'TransactionID: ' + $scope.transaction.TransactionIdentifier;
            };
            
            $scope.getGACategory = function () {
                return 'Transaction: ' + $scope.transaction.ExecutionType;
            }

            switch ($scope.transaction.ExecutionType) {
                case 'REMIC':
                    $scope.labels = {
                        IssueDate: 'Issue Date'
                    };
                    break;
                case 'MEGA':
                    $scope.labels = {
                        TransactionIdentifier: 'Transaction ID',
                        CUSIP: 'CUSIP',
                        InterestType: 'Int. Type',
                        SecurityStatusType: 'Status',
                        IssuanceUPB: 'Issuance UPB',
                        AtIssuancePayingPTR: 'Issuance PTR',
                        Resecuritization: 'Resecuritization'
                    };
                    break;
                case 'DMBS':
                    $scope.labels = {
                        TransactionIdentifier: 'Transaction ID',
                        CUSIP: 'CUSIP',
                        Product: 'Product',
                        SecurityStatusType: 'Status',
                        IssuanceUPB: 'Issuance UPB'
                    };
                    break;
                default:
                    if ($scope.transaction.Product == 'Credit Facility') {
                        $scope.labels = {
                            TransactionIdentifier: 'Transaction ID',
                            CUSIP: 'CUSIP',
                            Product: 'Product',
                            InterestType: 'Int. Type',
                            SecurityStatusType: 'Status',
                            IssuanceUPB: 'Issuance UPB',
                            Resecuritization: 'Resecuritization'
                        };
                    }
                    else {
                        $scope.labels = {
                            TransactionIdentifier: 'Transaction ID',
                            CUSIP: 'CUSIP',
                            Product: 'Product',
                            InterestType: 'Int. Type',
                            SecurityStatusType: 'Status',
                            IssuanceUPB: 'Issuance UPB',
                            LoanCount: 'Loans',
                            PropertyCount: 'Properties',
                            Resecuritization: 'Resecuritization'
                        };
                    }
                    
                    break;
            }

            $scope.displayNotifications = function () {
                var instance = ModalService.open({
                    templateUrl: window.globals.extViews.get('client/transaction/notification-modal/notification-modal.html'),
                    controller: 'NotificationsModalController',
                    controllerAs: 'modal',
                    backdrop: 'static',
                    size: 'md',
                    keyboard: false,
                    resolve: {
                        record: function () {
                            return $scope.transaction;
                        }
                    }
                });
            };

            $scope.addTransactionToPortfolio = function (portfolio) {
                WebService.postPromise('portfolio/transaction/' + $scope.transaction.TransactionIdentifier, portfolio).then(function (response) {
                    Notification.show(AppConstants.Template.ConfirmAddedToPortfolio.format($scope.transaction.TransactionIdentifier, portfolio.Name), 'warning');
                });
            }

        }]
    }
}]);