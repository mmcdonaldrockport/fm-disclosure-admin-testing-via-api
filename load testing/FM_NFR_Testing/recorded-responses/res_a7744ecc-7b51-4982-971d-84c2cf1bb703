﻿'use strict';

angular.module('dusdisclose',
    ['ui.router', 'ui.bootstrap', 'ui.select', 'ngSanitize', 'ngMessages', 'vcRecaptcha', 'angulartics',
        'angulartics.google.analytics', 'ui.toggle', 'ui.scroll', 'duScroll'])
    .config(['$provide', '$stateProvider', '$urlRouterProvider', '$httpProvider',
        function ($provide, $stateProvider, $urlRouterProvider, $httpProvider) {

            $httpProvider.interceptors.push('WebInterceptor');
                        
            // Sets the default time zone for moment object to Amercia/New_York
            // Otherwise every moment object will be created with local time zone of the user
            //moment.tz.setDefault('America/New_York');

            // Set the fallback url
            $urlRouterProvider.otherwise('/home');

            var defineState = function (name, spec) {
                spec.templateUrl = window.globals.extViews.get('client/' + spec.templateUrl);
                $stateProvider.state(name, spec);
            };

            defineState('home', {
                url: '/home',
                templateUrl: 'home/home.html',
                controller: 'HomeController',
                controllerAs: 'home',
                data: { stateTitle: 'Home' }
            });

            defineState('search', {
                url: '/search/:portfolioId',
                params: { searchOptions: null },
                templateUrl: 'search/search-results.html',
                controller: 'SearchController',
                controllerAs: 'search',
                data: { stateTitle: 'Search Results' }
            });

            defineState('transaction', {
                url: '/transaction/:id',
                templateUrl: 'transaction/transaction.html',
                controller: 'TransactionController',
                controllerAs: 'trans',
                data: { stateTitle: 'Transaction' },
                params: { scrollToSection: null, loanId: null },
                onEnter: ['$state', '$stateParams', function ($state, $stateParams) {
                    if (!$stateParams.id)
                        $state.go('home');
                }]
            });

            defineState('resources', {
                url: '/resources/:resourcetype',
                templateUrl: 'resources/resources.html',
                controller: 'ResourcesController',
                controllerAs: 'resources',
                data: { stateTitle: 'Resources' },
                onEnter: ['$state', '$stateParams', function ($state, $stateParams) {
                    if (!$stateParams.resourcetype)
                        $state.go('home');
                }]
            });

            defineState('registration', {
                url: '/registration',
                templateUrl: 'registration/registration.html',
                controller: 'RegistrationController',
                controllerAs: 'register',
                data: { stateTitle: 'Registration' },
            });

            defineState('account', {
                url: '/account/:accttype/:token',
                templateUrl: 'shared/views/account-info.html',
                controller: 'AccountInfoController',
                controllerAs: 'acct',
                data: { stateTitle: 'Account' },
                onEnter: ['$state', '$stateParams', 'AccountService', function ($state, $stateParams, AccountService) {                    
                    if (!$stateParams.accttype && !$stateParams.token)
                        $state.go('home');
                    else
                        AccountService.setActivateStatus(false);
                }]
            });

            defineState('expired', {
                url: '/expired/:accttype/:token',
                templateUrl: 'shared/views/account-expired.html',
                controller: 'AccountExpiredController',
                controllerAs: 'AcctExp',
                data: { stateTitle: 'Expired' },
                onEnter: ['$state', '$stateParams', 'AccountService', function ($state, $stateParams, AccountService) {
                    if (!$stateParams.accttype && !$stateParams.token)
                        $state.go('home');
                    else
                        AccountService.setActivateStatus(false);
                }]
            });
        }
    ]).run(['$rootScope', '$state', '$location', '$analytics', 'KeyService', 'AccountService', 'AppConstants', 'LookupService',
        function ($rootScope, $state, $location, $analytics, KeyService, AccountService, AppConstants, LookupService) {

        $rootScope.requestInProgress = false;

        if (ga) {
            KeyService.getGoogleAnalyticsKey().then(function (response) {
                var gaData = response.data;
                ga('create', gaData.Key, null);
                $analytics.setUserProperties({ 'dimension1': AppConstants.Guid.Empty }); //User.TrackingId
                $analytics.setUserProperties({ 'dimension2': gaData.ClientIp });        //User.ClientIp
                $analytics.setUserProperties({ 'dimension3': gaData.InstanceId });      //User.InstanceId
            });
        }

        LookupService.LoadDropDownLookups();

        AccountService.isLoggedInUser().then(function (response) {

            if (response.data) {
                AccountService.getUserInfo(true).then(function (response) {
                    var userData = response.data;

                    if (userData.EmailAddress) {
                        AccountService.setUserInfo(userData);
                        AccountService.setLoggedIn(true);
                    } else {
                        AccountService.setLoggedIn(false);
                    }
                });
            }
        }, function () {
            AccountService.setLoggedIn(false);
        });

        $rootScope.$on('$stateChangeSuccess', function (event, next, current) {
            $analytics.pageTrack($location.$$path);
        });

        $rootScope.$on('$viewContentLoaded', function (event, next, current) {
            $analytics.pageTrack($location.$$path);
        });

    }]);