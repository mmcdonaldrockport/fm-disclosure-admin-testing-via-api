﻿'use strict';

angular.module('dusdisclose').factory('AccountExpiredService', ['AppConstants', function (AppConstants) {

    var accttype, title, content, buttonLabel, actionType

    var init = function (type) {

        accttype = type.toLowerCase();
        if (accttype == AppConstants.ActionType.Registration.toLowerCase()) {
            title = 'REGISTRATION LINK EXPIRED';
            content = 'Registration confirmation links expire 24 hours after registration.';
            buttonLabel = 'Request a new registration confirmation link...';
            actionType = AppConstants.ActionType.Registration;
        } else if (accttype == AppConstants.ActionType.ResetPassword.toLowerCase()) {
            title = 'RESET PASSWORD EMAIL LINK EXPIRED';
            content = 'Reset password email links expire 24 hours after the initial request.';
            buttonLabel = 'Request a new reset password link...';
            actionType = AppConstants.ActionType.ResetPassword;
        } else if (accttype == AppConstants.ActionType.AccountHasActivated.toLowerCase()) {
            title = 'Account already confirmed';
            content = 'This account has already been registered and confirmed. Go to the DUS Disclose home page and login.';
            buttonLabel = 'Continue';
            actionType = AppConstants.ActionType.AccountHasActivated;
        }
    }

    var getTitle = function () {
        return title;
    }

    var getContent = function () {
        return content;
    }

    var getButtonLabel = function () {
        return buttonLabel;
    }

    var getActionType = function () {
        return actionType;
    }

    var getNotificationMessage = function () {

        var firstRepl = '';
        var thirdRepl = '';

        if (accttype == AppConstants.ActionType.ResetPassword.toLowerCase()) {
            firstRepl = 'new password reset';
            thirdRepl = 'complete your registration';
        } else if (accttype == AppConstants.ActionType.Registration.toLowerCase()) {
            firstRepl = 'new registration confirmation';
            thirdRepl = 'reset your password';
        }

        // leave email to be replace from the caller
        return AppConstants.Template.ConfirmEmailMessage.format(firstRepl, '{0}', thirdRepl);
    }

    var service = {
        init: init,
        getTitle: getTitle,
        getContent: getContent,
        getButtonLabel: getButtonLabel,
        getActionType: getActionType,
        getNotificationMessage: getNotificationMessage
    };

    return service;
}]);