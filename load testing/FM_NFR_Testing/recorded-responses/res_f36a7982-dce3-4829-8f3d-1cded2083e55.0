'use strict';

angular.module('dusdisclose').controller('SearchController', ['$state', 'AccountService', 'PortfolioService', 'WebService', '$window', 'ModalService',
    '$stateParams', 'AppConstants', '$scope', '$rootScope', 'Notification', 'CacheService', '$filter',
    function ($state, AccountService, PortfolioService, WebService, $window, ModalService, $stateParams, AppConstants,
        $scope, $rootScope,Notification, CacheService, $filter) {
    var ctrl = this;

        // Inherit data to be used in StratController: allStrats, stratDatasources
    $scope.allStrats = { show: false };
    $scope.stratDatasources = [];

    $scope.getStratsCount = function (items) {

        if (items.length == 0)
            return 0;

        var aggregate = items.reduce(function (item1, item2) {
            return { 
                numberOfLoans: (item1.numberOfLoans + item2.numberOfLoans)
            };
        });

        return aggregate.numberOfLoans;
    }

    $scope.getStratsUpbSum = function (items) {

        if (items.length == 0)
            return 0;

        var aggregate = items.reduce(function (item1, item2) {
            return { //label: '',  numberOfLoans: 0, percentBalance: 0,
                totalUPB: (item1.totalUPB + item2.totalUPB)
            };
        });

        return aggregate.totalUPB;
    }

    $scope.getStratsPerctBalanceTotal = function (items) {

        if (items.length == 0)
            return 0;

        var aggregate = items.reduce(function (item1, item2) {
            return {
                percentBalance: item1.percentBalance + item2.percentBalance
            };
        });

        return aggregate.percentBalance;
    }

    ctrl.resultSet = [
        { poolId: 1 },
        { poolId: 2 }
    ];
        
    ctrl.sort = { on: 'PoolId', desc: false };

    ctrl.searchResults = {};
    ctrl.portfolios = [];

    ctrl.searchModel = $state.params.searchOptions;
    ctrl.encodedModel = '';

    ctrl.portfolioId = ((ctrl.searchModel != null && ctrl.searchModel.PortfolioId > 0) ? ctrl.searchModel.PortfolioId : undefined) || $stateParams.portfolioId;

    ctrl.alertTypes = [];

    ctrl.editingPortfolio = false;

    ctrl.onEditPortfolioAlerts = false;
     
    function setAlertStatus(alert) {
        
        if(ctrl.alertTypes.length > 0){
            var result = $.grep(ctrl.alertTypes, function (item) {
                return item.AlertType == alert.AlertType;
            });
            if (result.length > 0) {
                alert.PortfolioId = result[0].PortfolioId;
                alert.Active = true;
            }
        }

        return alert;
    }

    function getAlert(alertType) { 
        var alert = $.grep(ctrl.alertTypes, function (item) {
            return item.AlertType == alertType;
        });
        if (alert.length > 0) {
            return alert[0];
        }
        else {
            return new PortfolioAlert(0, alertType);
        }
    }

    ctrl.createPortfolioBtnClicked = false;

    ctrl.editPortfolioAlerts = function () {
        ctrl.onEditPortfolioAlerts = !ctrl.onEditPortfolioAlerts;
        ctrl.viewAdvSearch = false;
        $rootScope.$broadcast("updatePortfolios");
    }

    ctrl.editPortfolioCriterias = function () {
        ctrl.onEditPortfolioAlerts = false;
        ctrl.editingPortfolio = true;
        ctrl.viewAdvSearch = !ctrl.viewAdvSearch;
    }

    ctrl.createNewPortfolio = function () {
        $rootScope.$broadcast("displayAdvSearch");
    }

    ctrl.updatePortfolio = function (name) {
        var portfolio = new Portfolio();
        portfolio.Name = name;
        portfolio.Id = ctrl.portfolioId;
        portfolio.PortfolioCriterias = createCriteriaList(portfolio.PortfolioCriterias);

        //update fields
        WebService.postPromise('portfolio/update', portfolio)
            .then(function (response) {
                $rootScope.$broadcast("updatePortfolios");

                //refresh search
                reload(null, portfolio.Id);
                
            });
    }

    ctrl.getAlerts = function () {
        ctrl.alerts =
        {
            newIssuance: getAlert(1),
            dealPaydown: getAlert(2),
            collateralChange: getAlert(3),
            prepayment: getAlert(4),
            delinquency: getAlert(5),
            updatedIssuanceDoc: getAlert(6)
        };
    }

    ctrl.portfolio = {};

    var reload = function (searchOptions, portfolioId) {
        $state.go('search', { searchOptions: searchOptions, portfolioId: portfolioId }, { reload: true });
    }

    function GetDateWithoutTime(date) {
        if (date)
            return (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear()
        return null;
    }

    ctrl.load = function () {

        if (ctrl.searchModel || ctrl.portfolioId) {

            if (ctrl.portfolioId > 0) {

                //get the alerts for a portfolioId details (CRITERIA AND ALERTS)
                WebService.getPromise('portfolio/params/' + ctrl.portfolioId)
                .then(function (response) {
                    ctrl.searchModel = {};
                    ctrl.portfolio = response.data;

                    ctrl.alertTypes = ctrl.portfolio.PortfolioAlerts;
                    ctrl.portfolio.PortfolioCriterias.forEach(function (el) {
                        ctrl.searchModel[el.Name] = el.Value;
                    });

                    ctrl.searchModel.PortfolioId = ctrl.portfolioId;

                    ctrl.getAlerts();

                    ctrl.loaded = true;

                });
            }
            else {

                ctrl.alerts =
                {
                    newIssuance: setAlertStatus(new PortfolioAlert(0, 1)),
                    dealPaydown: setAlertStatus(new PortfolioAlert(0, 2)),
                    collateralChange: setAlertStatus(new PortfolioAlert(0, 3)),
                    prepayment: setAlertStatus(new PortfolioAlert(0, 4)),
                    delinquency: setAlertStatus(new PortfolioAlert(0, 5)),
                    updatedIssuanceDoc: setAlertStatus(new PortfolioAlert(0, 6))
                }
                

            }

            if (ctrl.searchModel && !ctrl.searchModel.PortfolioId && !ctrl.portfolioId) {
                
                CacheService.put("advSearchInput", ctrl.searchModel);

                var input = angular.extend({}, ctrl.searchModel);

                if (input.IssueDateFrom) {
                    input.IssueDateFrom = GetDateWithoutTime(input.IssueDateFrom);
                }

                if (input.IssueDateTo) {
                    input.IssueDateTo = GetDateWithoutTime(input.IssueDateTo);
                }

                if (input.TransactionIdsCusips) {
                    input.TransactionIdsCusips = formatIdsCusips(input.TransactionIdsCusips, ',');
                }
                
                WebService.getPromise('search/advanced/' + btoa(encodeURIComponent(JSON.stringify(input || new AdvancedSearch()).replace(/\./g, '|'))))
                .then(function (response) {
                    ctrl.searchResults = response.data;
                    
                    if (ctrl.searchResults && Object.keys(ctrl.searchResults).length > 0) {

                        for (var key in ctrl.searchResults) {

                            if (ctrl.searchResults.hasOwnProperty(key)) {
                                ctrl.searchResults[key].sorted = [];
                                ctrl.searchResults[key].paged = [];

                                if (key === 'MBS') {

                                    if (ctrl.searchResults[key].StratTypeList != null) {
                                        $scope.stratDatasources = AppendChartOptionsToDataSource(ctrl.searchResults[key].StratTypeList);
                                        ctrl.selectedStratType = $scope.stratDatasources ? $scope.stratDatasources[0] : null;
                                    }
                                }
                            }
                        }
                    }

                    getPortfolios();
                    ctrl.setDefaultTab();
                    ctrl.loaded = true;
                });
            }
            else if (ctrl.portfolioId || ctrl.searchModel.PortfolioId) {

                WebService.getPromise('portfolio/advancedsearch/' + (ctrl.portfolioId || ctrl.searchModel.PortfolioId))
                .then(function (response) {
                    ctrl.searchResults = response.data;
                    if (ctrl.searchResults.MBS && ctrl.searchResults.MBS.StratTypeList && ctrl.searchResults.MBS.StratTypeList.length > 0) {
                        $scope.stratDatasources = AppendChartOptionsToDataSource(ctrl.searchResults.MBS.StratTypeList);
                        ctrl.selectedStratType = $scope.stratDatasources ? $scope.stratDatasources[0] : null;
                    }

                    getPortfolios();
                    ctrl.setDefaultTab();
                    ctrl.loaded = true;
                });
            }
            
            getUnfoundRecords();
        }
        else {
            reload((CacheService.get('advSearchInput') || { SecurityType: 'reload' }), '');
        }
    }


    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    function getUnfoundRecords() {

        ctrl.unfoundRecords = [];

        if (ctrl.searchModel && ctrl.searchModel.TransactionIdsCusips) {
            
            WebService.getPromise('search/notexist/' + formatIdsCusips(ctrl.searchModel.TransactionIdsCusips, '|'))
                      .then(function (response) {                          
                          ctrl.unfoundRecords = response.data;
                          
                          ctrl.unFoundRecordTemplate = "<div class='scroll'>";
                          ctrl.unfoundRecords.forEach(function (record) {                            
                              ctrl.unFoundRecordTemplate = ctrl.unFoundRecordTemplate + "<div class='list-group-item no-border'>" + record + "</div>";
                          })
                          ctrl.unFoundRecordTemplate = ctrl.unFoundRecordTemplate + "</div>";
                      });
        }
    }

    function AppendChartOptionsToDataSource(dataSource) {

        return dataSource.map(function (data) {

            data.stratType = AppConstants.StratChartType[data.title.replaceAll(" ", "").replace("-", "")];
            data.chartOptions = AppConstants.chartOptions[data.title];
            
            return data;
        })
    }

    ctrl.activeTab = 'mbs';

    ctrl.setDefaultTab = function () {
        if (ctrl.searchResults && Object.keys(ctrl.searchResults).length > 0) {

            for (var key in ctrl.searchResults) {
                ctrl.activeTab = key.toLowerCase();
                return;
            }
        }
    }

    ctrl.containsTab = function (key) {

        if (ctrl.searchResults && Object.keys(ctrl.searchResults).length > 0) {

            if (ctrl.searchResults.hasOwnProperty(key)) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    ctrl.setActiveTab = function (name) {
        ctrl.activeTab = name;
    }

    ctrl.isLoggedIn = function () {
        return AccountService.getLoggedIn().val;
    }

    ctrl.toggleCreatePortfolio = function () {
        ctrl.viewAdvSearch = false;
        ctrl.createPortfolioBtnClicked = !ctrl.createPortfolioBtnClicked;
    }

    var getPortfolios = function () {
        if (ctrl.isLoggedIn()) {

            PortfolioService.getPortfolios().then(function (response) {
                ctrl.portfolios = response.data;
            });
        }
    }

    ctrl.updateSearch = function () {
        ctrl.refineSearch();
        // Only pass in non-empty property value of searchModel object
        reload(copyNonEmptySearchModelValue(ctrl.searchModel));
    }

    ctrl.onClickedOutside = function () {
        ctrl.viewAdvSearch = false; 
    }

    ctrl.refineSearch = function () {
        ctrl.viewAdvSearch = !ctrl.viewAdvSearch;
        ctrl.createPortfolioBtnClicked = false;
    }

    function getAlertIndex(alertType) {
        for (var i = 0; i < ctrl.alertTypes.length; i++) {
            if (ctrl.alertTypes[i].AlertType == alertType) {
                return i;
            }
        }
        return -1;
    }

    function copyNonEmptySearchModelValue(searchModel) {

        var copyObj = {};

        for (var key in searchModel) {
            if (searchModel.hasOwnProperty(key)) {
                
                if (searchModel[key] != '')
                    copyObj[key] = searchModel[key];                
            }
        }

        return copyObj;
    }

    ctrl.portfolioName = '';
    ctrl.toggleAlert = function (alert) {

        var alertTypesClone = ctrl.alertTypes.slice();

        if (!alert.Active) {
            //remove from list
            var index = getAlertIndex(alert.AlertType);

            if (index >= 0) {
                alertTypesClone.splice(index, 1);

                if (ctrl.onEditPortfolioAlerts) {

                    WebService.deletePromise('portfolio/delete/alert/' + alert.Id)
                        .then(function (response) {
                            ctrl.alertTypes = alertTypesClone;
                            ctrl.getAlerts();                            
                    });
                }
            }

        }
        else {
                
            if (ctrl.onEditPortfolioAlerts) {
                alert.PortfolioId = ctrl.portfolioId;
                WebService.postPromise('portfolio/insert/alert', alert)
                    .then(function (response) {
                        alertTypesClone.splice(0, 0, response.data);
                        ctrl.alertTypes = alertTypesClone;
                        ctrl.getAlerts();                        
                });
            }
            else
                //add alert to list
                alertTypesClone.splice(0, 0, alert);


        }

        ctrl.alertTypes = alertTypesClone;
    }

    ctrl.disableSavePortfolio = function () {
        if (ctrl.portfolioName)
            return false;
        return true;
    }

    var formatIdsCusips = function (input,separator) {
        var idArray = input.split('\n');
        var noWhiteSpace = idArray.map(function (item) { return item.replace(/\s/g, ''); });
        return noWhiteSpace.join(separator ? separator : '\n');
    };

    var createCriteriaList = function (list) {

        for (var key in ctrl.searchModel) {
            if (ctrl.searchModel.hasOwnProperty(key)) {
                if (key === "TransactionIdsCusips") ctrl.searchModel.TransactionIdsCusips = formatIdsCusips(ctrl.searchModel.TransactionIdsCusips);
                list.splice(0, 0, new PortfolioCriteria(key, ctrl.searchModel[key]))
            }
        }

        return list;
    }

    var checkIfNameExist = function (name) {
        var result = $.grep(ctrl.portfolios, function (item) {
            return item.Name.toLowerCase() == name.toLowerCase()
        });
        if (result.length > 0)
            return true;
        return false;
    }

    ctrl.createPortfolio = function () {

        var userInfo = AccountService.getUserInfo();
        var portfolio = new Portfolio(ctrl.portfolioName, userInfo.Id);

        if (!checkIfNameExist(ctrl.portfolioName)) {

            portfolio.PortfolioAlerts = ctrl.alertTypes;

            portfolio.PortfolioCriterias = createCriteriaList(portfolio.PortfolioCriterias);

            WebService.postPromise('portfolio/create', portfolio).then(function (response) {
                portfolio = response.data;
                $rootScope.$broadcast("updatePortfolios");
                //redirect using the portfolio
                reload(null, portfolio.Id);
            });
        }
        else {
            Notification.show('Duplicate portfolio name');
        }
       
    }

    ctrl.showAlertsNotification = function (result) {

        var instance = ModalService.open({
            templateUrl: window.globals.extViews.get('client/transaction/notification-modal/notification-modal.html'),
            controller: 'NotificationsModalController',
            controllerAs: 'modal',
            backdrop: 'static',
            size: 'md',
            keyboard: false,
            resolve: {
                record: function () {
                    return { Id: result.TransactionId, TransactionIdentifier: result.PoolId };
                }
            }
        });

    }

    ctrl.showDeletePortfolio = function (portfolio) {

        var instance = ModalService.open({
            templateUrl: window.globals.extViews.get('client/shared/views/delete-portfolio.html'),
            controller: ['$uibModalInstance', function ($uibModalInstance) {
                var dltCtrl = this;

                dltCtrl.portfolio = portfolio;

                dltCtrl.deletePortfolio = function () {
                    console.log('delete portfolio', dltCtrl.portfolio.Id);

                    WebService.deletePromise('portfolio/' + dltCtrl.portfolio.Id).then(function (response) {
                        Notification.show(dltCtrl.portfolio.Name + ' has been deleted');

                        $rootScope.$broadcast('updatePortfolios');
                        //refresh the search using the searchModel                        
                        delete ctrl.searchModel['PortfolioId'];
                        reload(ctrl.searchModel, '');
                    });

                    $uibModalInstance.close(dltCtrl.portfolio.Id);
                };

                dltCtrl.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }],
            controllerAs: 'modal',
            backdrop: 'static',
            size: 'md',
            keyboard: false
        });

        instance.result.then(function (response) {
            //remove that portfolio from the list
            if (response > 0) {
                ctrl.portfolios = $.grep(ctrl.portfolios, function (item) {
                    return item.Id != response;
                });
            }
        });
    }

    ctrl.deletePortfolio = function (portfolio) {
        ctrl.refineSearch();
        ctrl.showDeletePortfolio(portfolio);
    }

    ctrl.getGALabel = function () {        
        return 'Total Record: ' + ctrl.portfolios.length;
    };

    ctrl.getGACategory = function () {
        return 'SearchResult';
    };

    ctrl.downloadSearchResults = function () {
        
        if (ctrl.searchModel.TransactionIdsCusips) {
            ctrl.searchModel.TransactionIdsCusips = formatIdsCusips(ctrl.searchModel.TransactionIdsCusips, ',');
        }

        ctrl.encodedModel = btoa(encodeURIComponent(JSON.stringify(ctrl.searchModel || {}).replace(/\./g, '|')));
        
        Notification.show('Search results download request is processing...', 'warning', 3000);
    };

    ctrl.hasStratsData = function () {
        return ctrl.searchResults.MBS.StratTypeList != null;
    };

    ctrl.stratsOverLimit = function () {
        return ctrl.searchResults.MBS.MBSTableResultList.length > 200;
    };
    
    ctrl.load();
}]);