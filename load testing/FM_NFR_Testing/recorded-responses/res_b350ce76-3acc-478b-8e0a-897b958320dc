﻿'use strict';

angular.module('dusdisclose').controller('NavbarController',
    ['$state', '$timeout', '$http', 'ModalService', 'Notification', 'AccountService', 'WebService', 'AppConstants', '$scope', 'AgreementService', 'PortfolioService', 'NotificationBarService', '$rootScope', '$filter', '$interval',
    function ($state, $timeout, $http, ModalService, Notification, AccountService, WebService, AppConstants, $scope, AgreementService, PortfolioService, NotificationBarService, $rootScope, $filter, $interval) {

        var ctrl = this;
        ctrl.template = window.globals.extViews.get('client/navbar/navbar.html');
        ctrl.AcctSrv = AccountService;
        ctrl.userInfo = AccountService.getUserInfo();
        ctrl.searchInput = "";
        ctrl.searchSuggestions = [];

        ctrl.showIssuanceData = true;
        ctrl.showAlerts = true;
        ctrl.notificationUnreadCount = 0;
        ctrl.alerts = [];
        ctrl.issuanceDataChanges = [];
        ctrl.filteredNotifications = [];
        ctrl.issuanceDataChangesCount = 0;
        ctrl.alertsCount = 0;

        ctrl.advancedSearchClicked = false;

        ctrl.AdvSearch = new AdvancedSearch();
        ctrl.portfolios = [];

        // if Refresh browser
        AccountService.isLoggedInUser().then(function (response) {

            if (response.data) {
                AccountService.getUserInfo(true).then(function (response) {
                    var userData = response.data;

                    if (userData.EmailAddress) {
                        AccountService.setUserInfo(userData);
                        AccountService.setLoggedIn(true);
                        ctrl.getNotifications();
                        ctrl.getPortfolios();
                    } else {
                        AccountService.setLoggedIn(false);
                    }
                })
            }
        });

        ctrl.getPortfolios = function () {
            PortfolioService.getPortfolios().then(function (response) {
                ctrl.portfolios = response.data;
            });
        }

        ctrl.onClickedOutside = function (useApply) {
            if (!useApply) {
                ctrl.advancedSearchClicked = false;
            } else {
                $scope.$apply(function () {
                    ctrl.advancedSearchClicked = false;
                });
            }
        }

        $rootScope.$on("updatePortfolios", function () {
            ctrl.getPortfolios();
            ctrl.getNotifications();
        });

        ctrl.advancedSearch = function () {
            ctrl.advancedSearchClick();
            if (ctrl.AdvSearch.PortfolioId == 0 || !ctrl.AdvSearch.PortfolioId)
                $state.go('search', { searchOptions: ctrl.AdvSearch, portfolioId: null }, { reload: true });
            else
                $state.go('search', { searchOptions: null, portfolioId: ctrl.AdvSearch.PortfolioId }, { reload: true });
            ctrl.AdvSearch = new AdvancedSearch();
        }

        ctrl.advancedSearchClick = function () {
            ctrl.advancedSearchClicked = !ctrl.advancedSearchClicked;
        }
        
        ctrl.daysAgo = function (date) {
            return moment(date).fromNow();
        }

        var updateUnreadCounts = function () {

            ctrl.unreadIssuanceDataChangesCount = $.grep(ctrl.allIssuanceDataChanges, function (item) {
                return item.Read === 0;
            }).length;

            ctrl.unreadAlertsCount = $.grep(ctrl.allAlerts, function (item) {
                return item.Read === 0;
            }).length;

            ctrl.notificationUnreadCount = ctrl.unreadIssuanceDataChangesCount + ctrl.unreadAlertsCount;
        };

        var getNextOfNotificationType = function (list, allList) {
            if (list.length >= allList.length) return;

            var visibleThreshold = list.length;

            return allList.slice(visibleThreshold, visibleThreshold + 20);
        }

        ctrl.loadNextNotifications = function () {
            if (ctrl.showAlerts) {
                var nextAlerts = getNextOfNotificationType(ctrl.alerts, ctrl.allAlerts);
                if (nextAlerts) ctrl.alerts = ctrl.alerts.concat(nextAlerts);
            }

            if (ctrl.showIssuanceData) {
                var nextChanges = getNextOfNotificationType(ctrl.issuanceDataChanges, ctrl.allIssuanceDataChanges);
                if (nextChanges) ctrl.issuanceDataChanges = ctrl.issuanceDataChanges.concat(nextChanges);
            }

            if (nextAlerts || nextChanges)
                ctrl.filterNotifications();
        };

        ctrl.getNotifications = function () {
            NotificationBarService.getNotifications().then(
                function (response) {
                    ctrl.allNotifications = response.data;

                    ctrl.allIssuanceDataChanges = $.grep(ctrl.allNotifications, function (item) {
                        return item.NotificationType === 0;
                    });

                    ctrl.allAlerts = $.grep(ctrl.allNotifications, function (item) {
                        return item.NotificationType === 1;
                    });

                    ctrl.issuanceDataChangesCount = ctrl.allIssuanceDataChanges.length;

                    ctrl.alertsCount = ctrl.allAlerts.length;

                    updateUnreadCounts();

                    ctrl.alerts = [];
                    ctrl.issuanceDataChanges = [];

                    ctrl.loadNextNotifications();

                    if (!ctrl.notificationCountInterval)
                    ctrl.notificationCountInterval = $interval(function () {
                        getNotificationUnreadCount();
                    }, 60000);
                }
            );
        }

        var getNotificationUnreadCount = function () {
            NotificationBarService.getNotificationUnreadCount().then(
                function (response) {
                    if (response.data != ctrl.notificationUnreadCount)
                        ctrl.getNotifications();
                }
            );
        };

        var applyNotificationScrollEvent = function () {
            if (!$('#scroll-to-bottom-test').length) return;

            ctrl.notificationScrollApplied = true;

            $('#scroll-to-bottom-test').on('scroll', function () {
                var scrollTop = $(this).scrollTop();

                //If the bottom of the notification scroll has been reached
                if (scrollTop + $(this).innerHeight() >= this.scrollHeight) { 
                    ctrl.loadNextNotifications(event);
                    $scope.$apply();
                }
            });
        };

        ctrl.refreshNotifications = function (open) {
            if (!ctrl.notificationScrollApplied) applyNotificationScrollEvent();
            if (!open) {
                ctrl.showIssuanceData = true;
                ctrl.showAlerts = true;
                ctrl.filterNotifications();
            }
            
            if (!ctrl.notificationUnreadCount) return;

            if (open)
                NotificationBarService.setToRead();
            else {
                ctrl.allNotifications.forEach(function (item) { item.Read = 1; });
                updateUnreadCounts();
            }
        };

        ctrl.notificationMessage = function () {
            if (!ctrl.hasNotificationData()) {
                if (!ctrl.portfolios || ctrl.portfolios.length < 1)
                    return "Build an advanced search and create a portfolio to receive notifications.";
                else
                    return "You currently don't have any notifications.";
            }

            if (!ctrl.showIssuanceData && !ctrl.showAlerts) return "No notifications selected. Toggle the options above to display them.";

            if (!ctrl.showAlerts && !ctrl.issuanceDataChangesCount) return "You currently don't have any Issuance Data Changes.";

            if (!ctrl.showIssuanceData && !ctrl.alertsCount) return "You currently don't have any Alerts.";

            return "";
        };

        $rootScope.$on("loggedIn", function () {
            ctrl.getNotifications();
        });

        $rootScope.$on("refreshNotifications", function () {
        });
       
        ctrl.filterNotifications = function (event) {
            if (event)
                event.stopPropagation();

            ctrl.filteredNotifications = [];

            if (ctrl.showAlerts && ctrl.showIssuanceData) {
                ctrl.filteredNotifications = ctrl.issuanceDataChanges.concat(ctrl.alerts);
                return;
            }

            if (ctrl.showAlerts) {
                ctrl.filteredNotifications = ctrl.alerts;
                return;
            }

            if (ctrl.showIssuanceData)
                ctrl.filteredNotifications = ctrl.issuanceDataChanges;
        }

        $rootScope.$on("displayAdvSearch", function () {
            ctrl.advancedSearchClick();
        });

        ctrl.isUserLoggedIn = function () {
            return AccountService.getLoggedIn().val;
        }

        ctrl.getUserFullName = function () {
            var userData = AccountService.getUserInfo();

            if (userData.EmailAddress)
                return '{0} {1}'.format(userData.FirstName, userData.LastName);
            else
                return '';
        }

        ctrl.template = window.globals.extViews.get('client/navbar/navbar.html');

        ctrl.isUserLoggedIn = function () {
            return AccountService.getLoggedIn().val;
        }

        ctrl.resetIsAccountValid = function () {
            AccountService.getActivateStatus().val = true;
        }

        ctrl.isAccountValid = function () {
            return AccountService.getActivateStatus().val;
        }

        ctrl.getUserFullName = function () {
            var userData = AccountService.getUserInfo();

            if (userData.EmailAddress)
                return '{0} {1}'.format(userData.FirstName, userData.LastName);
            else
                return '';
        }
               
        ctrl.logout = function () {
            var instance = ModalService.open({
                templateUrl: window.globals.extViews.get('client/shared/views/logout-confirm.html'),
                controller: ['$uibModalInstance', function ($uibModalInstance) {
                    var ctrl = this;
                    ctrl.confirm = function () {
                        AccountService.logOut().then(function (response) {
                            var userData = response.data;
                            $http.defaults.headers.common[AppConstants.AntiForgeryToken.HeaderName] = userData.Token || ":";
                            AccountService.setLoggedIn(false);
                            AccountService.setUserInfo(new UserInfo());
                            $uibModalInstance.close();
                            $state.go('home');
                        });
                    };
                    ctrl.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }],
                controllerAs: 'modal',
                backdrop: 'static',
                size: 'sm',
                keyboard: false
            });

            instance.result.then(function () {
                ctrl.loggedIn = false;
            });
        }
        
        ctrl.showAccountInfo = function () {
            var instance = ModalService.open({
                templateUrl: window.globals.extViews.get('client/shared/views/account-info-modal.html'),
                controller: 'AccountInfoModalController',
                controllerAs: 'modal',
                backdrop: 'static',
                size: 'md',
                keyboard: false
            });
        };

        ctrl.showRegistrationModal = function () {

            var instance = ModalService.open({
                templateUrl: window.globals.extViews.get('client/registration/registration-modal.html'),
                controller: 'RegistrationController',
                controllerAs: 'modal',
                backdrop: 'static',
                size: 'md',
                keyboard: false
            });

            instance.result.then(function (returnData) {
                ctrl.userInfo = returnData;
                $timeout(function () {
                    Notification.show(AppConstants.Template.ConfirmEmailMessage.format('registration confirmation',
                        ctrl.userInfo.EmailAddress, 'complete your registration'), 'warning', 8000);
                }, 20);
            });
        }

        var goToTransaction = function () {
            $state.go('transaction', { id: ctrl.searchInput.Id });
            ctrl.searchInput = "";
        };

        ctrl.transactionSelected = function () {
            if (ctrl.isUserLoggedIn()) goToTransaction();

            else {

                AgreementService.hasUnRegisteredAcceptedLatestUsageAgreement().then(function (response) {
                    goToTransaction();
                }, function (rejection) {
                    var errorCode = rejection.data.Message;
                    if (errorCode.indexOf('_MUST_ACCEPT_AGREEMENT_FAILED') > 0) {

                        var instance = ModalService.open({
                            templateUrl: window.globals.extViews.get('client/shared/views/usage-agreement-modal.html'),
                            controller: 'UsageAgreementController',
                            controllerAs: 'modal',
                            backdrop: 'static',
                            size: 'md',
                            keyboard: false
                        });

                        instance.result.then(function () {
                            goToTransaction();
                        });
                    }
                });
            }
        };

        ctrl.getSearchSuggestions = function (val) {
            return WebService.getPromise('search/quick/' + val).then(
                function (response) {
                    ctrl.searchSuggestions = response.data;
                    if (ctrl.searchSuggestions.length > 0 && ctrl.searchSuggestions[0].Type.toLowerCase() == 'remic')
                        ctrl.searchSuggestions = $filter('unique')(ctrl.searchSuggestions, 'TransactionIdentifier');
                    
                    return ctrl.searchSuggestions.filter(function (item) { return item.Status != 'Collapsed'; });
                }
            );
        };

        ctrl.lookingForCollapsed = function () {
            return ctrl.searchSuggestions.length == 1 && ctrl.searchInput === ctrl.searchSuggestions[0].TransactionIdentifier;
        };

        ctrl.hasNotificationData = function () {
            return ctrl.issuanceDataChangesCount > 0 || ctrl.alertsCount > 0;
        }

    }]);