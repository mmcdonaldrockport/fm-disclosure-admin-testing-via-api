﻿'use strict'

angular.module('dusdisclose').directive('dusLoan', [function () {
    return {
        restrict: 'E',
        templateUrl: window.globals.extViews.get('client/transaction/loan/dus-loan.html'),
        scope: {
            loan: '<',
            displayArm: '<',
            isCreditFacility: '<'
        },
        replace: true,
        controller: ['$scope', '$filter', 'WebService', function ($scope, $filter, WebService) {

            $scope.prepaymentHeaders = [{ label: 'Prepayment Type', attribute: 'PrepaymentType' }, { label: 'Term', attribute: 'PrepaymentTerm' }, { label: 'End Date', attribute: 'PrepaymentEndDate', format: 'date' }];            

            var populatePanels = function (loan) {
                var issuanceColumnSizes = { xs: 18, sm: 18, md: 9, lg: 9};

                if (!$scope.isCreditFacility) {

                    $scope.atIssuanceFields = [
                        new PanelRow([
                            new PanelData('UW NCF DSCR', $filter('number')(loan.UWNCFDSCR, 2), 'x', null, issuanceColumnSizes),
                            new PanelData('UW NCF DSCR (I/O)', $filter('number')(loan.UWNCFDSCRIO, 2), 'x', null, issuanceColumnSizes),
                            new PanelData('LTV', $filter('number')(loan.LTV, 2), '%', null, issuanceColumnSizes)
                        ])
                    ];

                    $scope.DSCRFields = [
                        new PanelRow([
                            new PanelData(loan.CurrentDSCRYTDDate ? ('Q' + loan.CurrentDSCRYTDDate.format('Q YYYY') + ' (YTD)') : 'YTD', $filter('number')(loan.CurrentDSCRYTD, 2), 'x', null, { xs: 18 }),
                            new PanelData(loan.CurrentDSCRPrecedingDate ? (loan.CurrentDSCRPrecedingDate.format('YYYY') + ' (Preceding)') : 'Preceding', $filter('number')(loan.CurrentDSCRPreceding, 2), 'x', null, { xs: 18 }),
                            new PanelData(loan.CurrentDSCR2ndPrecedingDate ? (loan.CurrentDSCR2ndPrecedingDate.format('YYYY') + ' (2nd Preceding)') : '2nd Preceding', $filter('number')(loan.CurrentDSCR2ndPreceding, 2), 'x', null, { xs: 18 }),
                            new PanelData(loan.CurrentDSCR3rdPrecedingDate ? (loan.CurrentDSCR3rdPrecedingDate.format('YYYY') + ' (3rd Preceding)') : '3rd Preceding', $filter('number')(loan.CurrentDSCR3rdPreceding, 2), 'x', null, { xs: 18 })
                        ])
                    ];
                }

                var monthUnit = { unit: ' month', plural: ' months' };

                $scope.loanTermsFields = [
                    new PanelRow([
                        new PanelData('Original', loan.OriginalTerm, monthUnit),
                        new PanelData('Remaining', loan.RemainingTerm, monthUnit, { atIssuance: loan.AtIssuanceRemainingTerm }),
                        new PanelData('Amortization', loan.Amortization, monthUnit),
                        new PanelData('Original I/O Term', loan.AtIssuanceRemainingIO, monthUnit)
                    ], 'Term'),
                ];

                if ($scope.displayArm) {

                    var ARMsize = { xs: 12, sm: 12, md: 12, lg: 12 };

                    $scope.loanTermsFields.unshift(new PanelRow([
                        new PanelData('Convertible', loan.Convertible, null, null, ARMsize),
                        new PanelData('Accrual Method', loan.AccrualMethod, null, null, ARMsize)
                    ]));

                    $scope.loanTermsFields.unshift(new PanelRow([
                        new PanelData('Rate Reset Frequency', loan.RateResetFrequency, monthUnit, null, ARMsize),
                        new PanelData('Payment Reset Frequency', loan.PaymentResetFrequency, monthUnit, null, ARMsize),
                        new PanelData('Lookback Period', loan.LookbackPeriod, { unit: ' day', plural: ' days' }, null, ARMsize)
                    ], null, true));

                    $scope.loanTermsFields.unshift(new PanelRow([
                        new PanelData('Paying PTR', $filter('number')(loan.PayingPTR, 3), '%', { atIssuance: $filter('number')(loan.AtIssuancePayingPTR, 3) }, ARMsize),
                        new PanelData('Note Rate Cap', $filter('number')(loan.NoteRateCap, 3), '%', null, ARMsize),
                        new PanelData('Note Rate Floor', $filter('number')(loan.NoteRateFloor, 3), '%', null, ARMsize)
                    ], null, true));

                    $scope.loanTermsFields.unshift(new PanelRow([
                        new PanelData('Paying Note Rate', $filter('number')(loan.PayingNoteRate, 3), '%', { atIssuance: $filter('number')(loan.AtIssuancePayingNoteRate, 3) }, ARMsize),
                        new PanelData('ARM Index', loan.ARMIndex, null, null, ARMsize),
                        new PanelData('ARM Margin', $filter('number')(loan.ARMMargin, 3), '%', null, ARMsize)
                    ], 'ARM', true));

                    if (!$scope.isCreditFacility) {
                        var UWNCFDSCRCap = new PanelData('UW NCF DSCR at Cap', $filter('number')(loan.UWNCFDSCRCap, 2), 'x', null, issuanceColumnSizes);
                        $scope.atIssuanceFields[0].dataList.splice(2, 0, UWNCFDSCRCap);
                    }
                    
                }
                else {
                    $scope.loanTermsFields.unshift(new PanelRow([
                        new PanelData('Paying Note Rate', $filter('number')(loan.PayingNoteRate, 3), '%', { atIssuance: $filter('number')(loan.AtIssuancePayingNoteRate, 3) }),
                        new PanelData('Paying PTR', $filter('number')(loan.PayingPTR, 3), '%', { atIssuance: $filter('number')(loan.AtIssuancePayingPTR, 3) }),
                        new PanelData('Accrual Method', loan.AccrualMethod)
                    ], 'Fixed Rate'));
                }
            };

            populatePanels($scope.loan);

            $scope.atIssuanceTitle = 'At Issuance DSCR & LTV';
            $scope.DSCRTitle = 'Current DSCR';
            $scope.loanTermsTitle = 'Loan Terms';

            $scope.timelineLabels = {
                NoteDate: 'Note Date',
                FirstPaymentDate: 'First Payment',
                LockoutEndDate: 'Lockout End',
                IOPeriodEndDate: 'I/O Period End',
                PrepaymentEndDate: 'Prepayment End',
                MaturityDate: 'Maturity'
            };
        }]
    }
}]);