﻿'use strict';

angular.module('dusdisclose').controller('DetailModalController', ['$uibModalInstance', 'WebService', 'AccountService', 'record', 'TransactionService', '$filter', function ($uibModalInstance, WebService, AccountService, record, TransactionService, $filter) {
    var ctrl = this;

    ctrl.record = record;

    ctrl.transaction = TransactionService.getTransaction();

    ctrl.Details = {};

    ctrl.fields = [];

    ctrl.labels = {};

    ctrl.userAuthenticated = function () {
        return AccountService.getLoggedIn().val;
    };

    ctrl.load = function () {
        switch (ctrl.record.Type) {
            case 'Security':
                ctrl.labels = securityLabels;
                break;
            case 'Loan':
                ctrl.labels = loanLabels;
                break;
            case 'Property':
                ctrl.labels = propertyLabels;
                break;
        }

        WebService.getPromise('transaction/' + ctrl.record.Type + '/' + ctrl.record.Id + '/details', null, {useMoments: true}).then(function (response) {
            if (ctrl.record.Type === 'Loan') {
                ctrl.Details = response.data.Details;
                if (response.data.Debts.length == 1) {
                    ctrl.Details = angular.extend(ctrl.Details, response.data.Debts[0]);
                    ctrl.labels = angular.extend(ctrl.labels, debtLabels);
                }
                else if (response.data.Debts.length > 1) {
                    var debts = {};
                    var labels = {};
                    response.data.Debts.forEach(function (debt, i) {
                        Object.keys(debtLabels).forEach(function (k) {
                            debts[k + i] = debt[k];
                            labels[k + i] = debtLabels[k]
                        });
                    });
                    ctrl.Details = angular.extend(ctrl.Details, debts);
                    ctrl.labels = angular.extend(ctrl.labels, labels);
                }
            }
            else {
                ctrl.Details = response.data;
            }
            ctrl.fields = Object.keys(ctrl.labels);
        });
    };

    ctrl.getGALabel = function () {
        return 'TransactionID: ' + ctrl.transaction.TransactionIdentifier;
    };

    ctrl.getGACategory = function () {
        return 'Transaction: ' + ctrl.transaction.ExecutionType;
    }

    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    var securityLabels = {
        SeasoningAtIssuance: new DetailModalField('Seasoning – At Issuance', 'months'),
        SeasoningCurrent: new DetailModalField('Seasoning – Current', 'months'),
        MinLoanPTR: new DetailModalField('Min. Loan PTR', 'percent', 3),
        MaxLoanPTR: new DetailModalField('Max. Loan PTR', 'percent', 3),
        IssuanceUPB: new DetailModalField('Issuance UPB','currency'),
        MinLoanIssuanceUPB: new DetailModalField('Min. Loan Issuance UPB','currency'),
        MaxLoanIssuanceUPB: new DetailModalField('Max. Loan Issuance UPB','currency'),
        TotalInterestDistribution: new DetailModalField('Total Interest Distribution','currency'),
        LifetimePTRFloor20179: new DetailModalField('Lifetime PTR Floor - At Issuance', 'percent', 3),
        LifetimePTRFloor20950: new DetailModalField('Lifetime PTR Floor - Current', 'percent', 3),
        TotalPrincipalDistribution: new DetailModalField('Total Principal Distribution','currency'),
        CumulativePrepaymentAllocation: new DetailModalField('Cumulative Prepayment Allocation','currency'),
        PrepaymentAllocation: new DetailModalField('Prepayment Allocation','currency'),
        PrepaidLoanCount: new DetailModalField('# of Prepaid Loans', ''),
        PrepaidLoansPrincipalAmount: new DetailModalField('Prepaid Loans Principal Amount','currency')
    };

    var loanLabels = {
        NextRateChangeDate: new DetailModalField('Next Rate Change Date','date'),
        NextPaymentChangeDate: new DetailModalField('Next Payment Change Date','date'),
        ConversionEligibilityStartDate: new DetailModalField('Conversion Eligibility Start Date','date'),
        ConversionEligibilityEndDate: new DetailModalField('Conversion Eligibility End Date','date'),
        FirstScheduledPaymentChangeDateTrust: new DetailModalField('First Scheduled Payment Change Date in Trust','date'),
        FirstScheduledRateChangeDateTrust: new DetailModalField('First Scheduled Rate Change Date in Trust', 'date'),
        InitialFixedTermEndDate: new DetailModalField('Initial Fixed Term End Date','date'),
        ServicerNameAtIssuance: new DetailModalField('Servicer Name – At Issuance', ''),
        ServicerNameCurrent: new DetailModalField('Servicer Name – Current', ''),
        TierDropEligible: new DetailModalField('Tier Drop Eligible', ''),
        AmortizationType: new DetailModalField('Amortization Type', ''),
        PercentageInitialPoolBalance: new DetailModalField('% of Initial Pool Balance', 'percent', 2),
        OriginalNoteRate: new DetailModalField('Original Note Rate', 'percent', 3),
        PeriodicPaymentChangeIncrease: new DetailModalField('Periodic Payment Change Increase', 'percent', 3),
        PeriodicPaymentChangeDecrease: new DetailModalField('Periodic Payment Change Decrease', 'percent', 3),
        PeriodicRateChangeIncrease: new DetailModalField('Periodic Rate Change Increase', 'percent', 3),
        PeriodicRateChangeDecrease: new DetailModalField('Periodic Rate Change Decrease', 'percent', 3),
        SeasoningAtIssuance: new DetailModalField('Seasoning – At Issuance', 'months'),
        SeasoningCurrent: new DetailModalField('Seasoning – Current', 'months'),
        InitialFixedTerm: new DetailModalField('Initial Fixed Term', ''),
        BalancePerUnitAtIssuance: new DetailModalField('Balance per Unit – At Issuance','currency'),
        BalancePerUnitCurrent: new DetailModalField('Balance per Unit – Current','currency'),
        FixedPrincipalPaymentAmount: new DetailModalField('Fixed Principal Payment Amount', 'currency'),
        AllInUWLTV: new DetailModalField('All-In UW LTV', 'percent', 2),
        AllInUWNCFDSCR: new DetailModalField('All-In UW NCF DSCR','ratio'),
        AllInCurrentNCFDSCR: new DetailModalField('All-In Current NCF DSCR','ratio'),
        DSCRMaximumPayment: new DetailModalField('DSCR at Maximum Payment','ratio'),
        MonthlyDebtService: new DetailModalField('Monthly Debt Service','currency'),
        MonthlyDebtServiceAmountPartialIO: new DetailModalField('Monthly Debt Service Amount - Partial IO','currency'),
        UWNCFDebtYieldAtIssuance: new DetailModalField('UW NCF Debt Yield – At Issuance', 'percent', 3),
        UWNCFDebtYieldCurrent: new DetailModalField('UW NCF Debt Yield – Current', 'percent', 3),
        TotalScheduledPAndIDue: new DetailModalField('Total Scheduled P&I Due','currency'),
        ScheduledInterestAmount: new DetailModalField('Scheduled Interest Amount','currency'),
        ScheduledPrincipalAmount: new DetailModalField('Scheduled Principal Amount','currency'),
        DefeasanceStatus: new DetailModalField('Defeasance Status', ''),
        LiquidationPrepaymentCode: new DetailModalField('Liquidation/Prepayment Code', ''),
        LiquidationPrepaymentDate: new DetailModalField('Liquidation/Prepayment Date','date'),
        PrepaymentPremiumYieldMaintenanceReceived: new DetailModalField('Prepayment Premium/Yield Maintenance (YM) Received', 'currency'),
        TotalDebtCurrentBalance: new DetailModalField('Total Debt Current Balance', 'currency')
    };

    var debtLabels = {
        AddDebtPoolNumberAndLoanNumber: new DetailModalField('Add. Debt Pool Number & Loan Number', ''),
        AddDebtType: new DetailModalField('Add. Debt Type', ''),
        AddDebtAmortizationTerm: new DetailModalField('Add. Debt Amortization Term', 'months'),
        AddDebtARMMargin: new DetailModalField('Add. Debt ARM Margin', 'percent', 3),
        AddDebtBalance: new DetailModalField('Add. Debt Balance','currency'),
        AddDebtBalloon: new DetailModalField('Add. Debt Balloon (Y/N)', ''),
        AddDebtCurrentInterestRate: new DetailModalField('Add. Debt Current Interest Rate', 'percent', 3),
        AddDebtLastInterestOnlyEndDate: new DetailModalField('Add. Debt Last Interest Only End Date','date'),
        AddDebtLienHolder: new DetailModalField('Add. Debt Lien Holder', ''),
        AddDebtLienPriority: new DetailModalField('Add. Debt Lien Priority', ''),
        AddDebtLineOfCreditFullAmount: new DetailModalField('Add. Debt Line of Credit Full Amount', 'currency'),
        AddDebtMaturityDate: new DetailModalField('Add. Debt Maturity Date','date'),
        AddDebtMaximumMonthlyPayment: new DetailModalField('Add. Debt Maximum Monthly Payment','currency'),
        AddDebtMinimumInterestRate: new DetailModalField('Add. Debt Minimum Interest Rate', 'percent', 3),
        AddDebtMonthlyPaymentAmount: new DetailModalField('Add. Debt Monthly Payment Amount','currency'),
        MezzLoanPrepaymentProvisionTerm: new DetailModalField('Mezz. Loan Prepayment Provision Term', ''),
        MezzLoanInterestAccrualMethod: new DetailModalField('Mezz. Loan Interest Accrual Method', ''),
        MezzLoanAmortizationTerm: new DetailModalField('Mezz. Loan Amortization Term', 'months'),
        MezzLoanBalance: new DetailModalField('Mezz. Loan Balance','currency'),
        MezzLoanFirstMonthlyPaymentDueDate: new DetailModalField('Mezz. Loan First Monthly Payment Due Date','date'),
        MezzLoanInitialTermInterestRate: new DetailModalField('Mezz. Loan Initial Term Interest Rate', 'percent', 3),
        MezzLoanInitialTermMaturityDate: new DetailModalField('Mezz. Loan Initial Term Maturity Date','date'),
        MezzLoanInterestOnlyEndDate: new DetailModalField('Mezz. Loan Interest Only End Date','date'),
        MezzLoanPrepaymentProvision: new DetailModalField('Mezz. Loan Prepayment Provision', ''),
        MezzLoanPrepaymentProvisionEndDate: new DetailModalField('Mezz. Loan Prepayment Provision End Date','date'),
        MezzLoanProvider: new DetailModalField('Mezz. Loan Provider', ''),
        MezzLoanMonthlyPaymentAmount: new DetailModalField('Mezz. Loan Monthly Payment Amount','currency')
    };

    var propertyLabels = {
        PropertyStatus: new DetailModalField('Property Status', ''),
        County: new DetailModalField('County', ''),
        UnitOfMeasure: new DetailModalField('Unit of Measure', ''),
        PropertyValueAsOfDate: new DetailModalField('Property Value As of Date','date'),
        TaxEscrow: new DetailModalField('Tax Escrow', ''),
        TerrorismInsurance: new DetailModalField('Terrorism Insurance', ''),
        UWReplacementReserves: new DetailModalField('UW Replacement Reserves','currency'),
        HAPRemainingTerm: new DetailModalField('HAP Remaining Term', 'months'),
        EnergyStarScore: new DetailModalField('ENERGY STAR® Score', ''),
        EnergyStarScoreDate: new DetailModalField('ENERGY STAR® Score Date', 'dateNoDay'),
        SourceEnergyUseIntensity: new DetailModalField('Source Energy Use Intensity', ''),
        SourceEnergyUseIntensityDate: new DetailModalField('Source Energy Use Intensity Date', 'dateNoDay'),
        MostRecentDebtServiceAmount: new DetailModalField('Most Recent Debt Service Amount','currency'),
        PrecedingFiscalYearDebtServiceAmount: new DetailModalField('Preceding Fiscal Year Debt Service Amount','currency'),
        SecondPrecedingFiscalYearDebtServiceAmount: new DetailModalField('Second Preceding Fiscal Year Debt Service Amount','currency'),
        NoteADebtService: new DetailModalField('Note A-Debt Service','currency'),
        NoteBDebtService: new DetailModalField('Note B-Debt Service','currency'),
        NoteCDebtService: new DetailModalField('Note C-Debt Service', 'currency'),
        NetChangeBetweenPrecedingBaseYearOccupancy: new DetailModalField('Net Change Between Preceding & Base Year - % Occupancy','percent', 2),
        NetChangeBetweenPrecedingBaseYearTotalRevenue: new DetailModalField('Net Change Between Preceding & Base Year - % Total Revenue', 'percent', 2),
    };

    ctrl.load();

}]);




