﻿'use strict'

angular.module('dusdisclose').directive('dusTimeline', [function () {
    return {
        restrict: 'E',
        templateUrl: window.globals.extViews.get('client/transaction/structures/dus-timeline.html'),
        scope: {
            timelineLabels: '<',
            transaction: '=',
            timelineType: '@'
        },
        replace: true,
        controller: ['$scope', function ($scope) {

            $scope.timeline = [];

            $scope.isPast = function (date) {
                var today = moment();
                return date.isBefore(today, 'day');
            };

            $scope.allPast = function (timeline) {
                for (var index in timeline) {
                    if (!$scope.isPast(timeline[index].value))
                        return false;
                }
                return true;
            };

            var createTimeline = function (transaction, timelineLabels) {
                
                var timeline = [];

                if (!transaction) return [];

                if (transaction.Resecuritizations) {
                    transaction.Resecuritizations.forEach(function (item) {
                        var date = { label: "Resecuritized", value: item.ResecuritizationDate, id: item.ResecuritizedId, identifier: item.ResecuritizedIdentifier };
                        timeline.push(date);
                    });
                }

                Object.keys(timelineLabels).forEach(function (key) {
                    if (transaction[key]) {
                        var date = { label: timelineLabels[key], value: transaction[key] };
                        timeline.push(date);
                    }
                });

                var issueAndMaternityTimeline = timeline.filter(function (item) {
                    return item.label == "Issue" || item.label == "Maturity";
                });

                var withoutIssueAndMaternityTimeline = timeline.filter(function (item) {
                    return item.label != "Issue" && item.label != "Maturity";
                });

                withoutIssueAndMaternityTimeline.sort(function (a, b) {
                    return a.value.isAfter(b.value, 'day');
                });

                for (var i = 0; i < issueAndMaternityTimeline.length; i++) {
                    if (issueAndMaternityTimeline[i].label == 'Issue') {
                        withoutIssueAndMaternityTimeline.splice(0, 0, issueAndMaternityTimeline[i]);
                    }
                    if (issueAndMaternityTimeline[i].label == 'Maturity') {
                        withoutIssueAndMaternityTimeline.splice(withoutIssueAndMaternityTimeline.length, 0, issueAndMaternityTimeline[i]);
                    }
                }

                timeline = withoutIssueAndMaternityTimeline;

                return timeline;
            };

            $scope.$watch('transaction', function () {
                $scope.timeline = createTimeline($scope.transaction, $scope.timelineLabels);
            });
        }]
    }
}]);