﻿'use strict';

angular.module('dusdisclose').controller('RegistrationController',
    ['$scope', '$uibModalInstance', 'AccountService', 'ValidationService', 'AppConstants', 'Notification', 'ApiFormValidationService', '$timeout', 'vcRecaptchaService', 'KeyService',
        function ($scope, $uibModalInstance, AccountService, ValidationService, AppConstants, Notification, ApiFormValidationService, $timeout, vcRecaptchaService, KeyService) {

            var ctrl = this;
            ctrl.userInfo = new UserInfo();
            ctrl.usageAgreement = new UsageAgreement();
            ctrl.companyTypeOptions = AppConstants.CompanyType;
            ctrl.reCaptchaWidgetId = null;
            ctrl.reCaptchaResponse = null;

            var clearedReCAPCHA = false;

            ctrl.initForm = function (form) {
                ApiFormValidationService.initialize(form);
            }

            ctrl.load = function () {

                clearedReCAPCHA = false;
                ctrl.dismissError = false;
                AccountService.getUserAgreement().then(function (response) {
                    ctrl.usageAgreement = response.data;
                })
                KeyService.getReCaptchaKey().then(function (response) {
                    ctrl.reCaptchaKey = response.data;
                });
            }

            ctrl.setReCaptchaWidgetId = function (widgetId) {
                //console.info('Created widget ID: %s', widgetId);
                ctrl.reCaptchaWidgetId = widgetId;
            };

            ctrl.setReCaptchaResponse = function (response) {                
                ctrl.reCaptchaResponse = response;
            };

            ctrl.cbExpiration = function () {
                console.info('Captcha expired. Resetting response object');
                vcRecaptchaService.reload(ctrl.reCaptchaWidgetId);
                ctrl.reCaptchaResponse = null;
                clearedReCAPCHA = false;
            };

            ctrl.resendRegisteredEmail = function () {

                AccountService.recoverAccount(ctrl.userInfo.EmailAddress).then(function (response) {
                    $uibModalInstance.close(response.data);
                }, function (rejected) {
                    var errorCode = rejected.data;
                    ApiFormValidationService.applyErrorCode(form, errorCode);
                });

            }

            ctrl.validateEmail = function (form) {
                AccountService.verifyEmail(ctrl.userInfo.EmailAddress).then(function (response) { }, function (rejected) {
                    var errorCode = rejected.data;
                    ApiFormValidationService.applyErrorCode(form, errorCode);
                });
            }

            ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }

            var saveUserInfo = function (form) {

                var registerModel = angular.extend({ "AgreementId": ctrl.usageAgreement.Id }, ctrl.userInfo);
                AccountService.fieldValidating($scope, ctrl.userInfo, form).then(function (dataValid) {
                    if (dataValid) {
                        AccountService.saveUserInfo(registerModel).then(function () {
                            $uibModalInstance.close(ctrl.userInfo);
                        }, function (rejected) {
                            var errorCode = rejected.data;
                            ApiFormValidationService.applyErrorCode(form, errorCode);
                        });
                    }
                })
            }

            ctrl.register = function (form) {

                // clear errors
                ctrl.dismissError = false;
                ApiFormValidationService.initialize(form);

                if (ctrl.selectedCompanyType == 'select')
                    ctrl.selectedCompanyType = '';

                ctrl.userInfo.CompanyType = ctrl.selectedCompanyType;

                AccountService.fieldValidating($scope, ctrl.userInfo, form);
                        
                if (clearedReCAPCHA) {
                    saveUserInfo(form);
                } else {
                    AccountService.verifyReCAPTCHA(ctrl.reCaptchaResponse).then(function () {
                        clearedReCAPCHA = true;
                        saveUserInfo(form);
                    }, function (rejected) {
                        var errorCode = rejected.data;
                        ApiFormValidationService.applyErrorCode(form, errorCode);
                        vcRecaptchaService.reload(ctrl.reCaptchaWidgetId);
                    })
                }
            }

            ctrl.load();
        }]);
