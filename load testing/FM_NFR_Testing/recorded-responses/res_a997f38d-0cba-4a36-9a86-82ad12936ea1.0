'use strict';

angular.module('dusdisclose').controller('ResourcesController', 
    ['$stateParams', 'AppConstants', 'WebService', '$scope', function ($stateParams, AppConstants, WebService, $scope) {
    var ctrl = this;

    ctrl.selectedResource;
    ctrl.resources;
    ctrl.topLevel;
    ctrl.marketConsumables;

    switch ($stateParams.resourcetype) {
        case 'productinformation':
            ctrl.selectedResource = AppConstants.ResourceTypes.productinformation;
            break;
        case 'learningcenter':
            ctrl.selectedResource = AppConstants.ResourceTypes.learningcenter;
            break;
        case 'datacollections':
            ctrl.selectedResource = AppConstants.ResourceTypes.datacollections;
            break;
    }

    ctrl.Title = ctrl.selectedResource.label;

    ctrl.load = function () {
        ctrl.getResources();        
    }

    ctrl.getResources = function () {
        WebService.getPromise('resources/' + ctrl.selectedResource.resourceType).then(function (response) {
            
            ctrl.resources = response.data;

            if (ctrl.resources.length > 0) {
                ctrl.topLevel = $.grep(ctrl.resources, function (item) {
                    return !item.Name;
                })[0];

                ctrl.categories = $.grep(response.data, function (item) {
                    return item.Name && !item.ParentId;
                });

                if (hasSystemDocuments())
                    ctrl.loadMarketConsumables();
            }
        });
    }

    ctrl.loadMarketConsumables = function () {
        WebService.getPromise('resources/market/files').then(function (response) {
            ctrl.marketConsumables = response.data;
        });
    }

    ctrl.getSubCategories = function (categoryId) {
        if (!categoryId) return;
        return $.grep(ctrl.resources, function (item) {
            return item.ParentId == categoryId;
        });
    }

    ctrl.getContents = function (categoryId) {
        if (!categoryId) return;
        
        var contents = $.grep(ctrl.resources, function (item) {
            return item.ResourceCategoryId == categoryId;
        });

        return contents;
    }

    ctrl.getMarketConsumables = function (content) {
        if (!ctrl.marketConsumables)
            return [];

        var data = ctrl.marketConsumables.filter(function (item) {
            return item.ResourceCategoryId === content.ResourceCategoryId &&
                item.SystemDocumentId === content.SystemDocumentId;
        });

        return data;
    }

    ctrl.getConsumableDateUrl = function (mcItem) {
        return mcItem.ConsumableDate.replace(/-/g, '/');
    }

    $scope.$on("loggedIn", function () { ctrl.load(); });
    $scope.$on("loggedOut", function () { ctrl.load(); });

    var hasSystemDocuments = function () {
        var sysDocs = ctrl.resources.filter(function (item) {
            return item.ContentType === 2
        });

        return sysDocs.length > 0;
    };

    ctrl.load();
}]);