﻿'use strict';

angular.module('app').controller('ReportFiltersController', ['$scope', '$state', 'Report', 'ReportFilters', 'AppConstants', 'BuildReportValidator',
    function ($scope, $state, Report, ReportFilters, AppConstants, BuildReportValidator) {
    var ctrl = this;

    ctrl.currentState = $state.current;
    var report = Report.getCurrentReport();
    var validator = BuildReportValidator.newValidator();

    ctrl.filters = report.reportFilters.filtersList;
    ctrl.filtersView = {};

    ctrl.fieldsSelected = report.reportFields.selectedList;

    ctrl.fieldsSelectedDictionary = report.reportFields.selectedDictionary;
    ctrl.fieldsDict = report.dataSetDefinition.fieldsDictionary;
    ctrl.operatorsDict = AppConstants.OPERATORS_DICT;

    var clearAddFilter = function () {
        ctrl.addFilter = ReportFilters.initFilter({ FieldName: '' });
    };
    clearAddFilter();

    ctrl.allFields = report.dataSetDefinition.allFields.map(function (field) {
        if (ctrl.fieldsSelectedDictionary[field.Name]) {
            field.CaptionUser = ctrl.fieldsSelectedDictionary[field.Name].CaptionUser || field.Caption;
        }
        else
            field.CaptionUser = field.Caption;
        return field;
    });

        //ctrl.defaultDropDown = [{ CaptionUser: 'Type for more fields', _isDisabled: true }].concat(ctrl.fieldsSelected);
    ctrl.defaultDropDown = ctrl.fieldsSelected;
    ctrl.dropdownLimit = AppConstants.SEARCH_INFO.dropdownLimit;

    ctrl.getDefaultDropDown = function (currentFieldName) {
        var result = angular.copy(ctrl.fieldsSelected);
        if (currentFieldName && !ctrl.fieldsSelectedDictionary[currentFieldName]) {
            result.push(ctrl.fieldsDict[currentFieldName]);
        }
        result.push({ CaptionUser: 'Type for more fields', _isDisabled: true });
        return result;
    };

    var refreshFilter = function (vFilter, reset) {
        vFilter._dataType = ctrl.fieldsDict[vFilter.FieldName].Type;
        vFilter._operators = ReportFilters.getOperators(vFilter._dataType);
        if (reset) vFilter.OperatorId = vFilter._operators[0].Id;
        vFilter._showCriteria = true;
        vFilter._disableValueInput = true;
        ReportFilters.getValueOptions(vFilter._dataType, report.datasetId, vFilter.FieldName).then(function (options) {
            vFilter._valueOptions = options || reset ? options : vFilter._valueOptions;
            vFilter._valueInputType = ReportFilters.getValueInputType(ctrl.operatorsDict[vFilter.OperatorId].Type, vFilter._valueOptions.length);
            var defaultValue = vFilter._valueOptions[0] ? vFilter._valueOptions[0].Id : '';
            vFilter.Values = ReportFilters.getValues(vFilter.Values, ctrl.operatorsDict[vFilter.OperatorId].MaxNumberOfValues, reset, defaultValue);
            vFilter._disableValueInput = false;
        });
    };

    ctrl.getFieldNameDisplay = function (filter) {
        if (ctrl.fieldsSelectedDictionary[filter.FieldName])
            return ctrl.fieldsSelectedDictionary[filter.FieldName].CaptionUser;
        else
            return ctrl.fieldsDict[filter.FieldName].Caption;
    };

    ctrl.selectFilterField = function (vFilter) {
        if (vFilter.FieldName) {
            refreshFilter(vFilter, true);
            validator.setValidator('editFilter', false, "Click 'Set Filter' or 'Cancel' to continue.");
        }
        else {
            vFilter['_showCriteria'] = false;
        }
    };

    ctrl.addToFilters = function () {
        report.reportFilters.addFilter(ctrl.addFilter, ctrl.addFilter._dataType, ctrl.addFilter._valueOptions);
        clearAddFilter();
        validator.setValidator('editFilter', true);
        report.changeSaveStatus(false);
    };

    ctrl.cancelAddFilter = function () {
        validator.setValidator('editFilter', true);
        clearAddFilter();
    };

    ctrl.updateValueInput = function (vFilter) {
        vFilter._valueInputType = ReportFilters.getValueInputType(ctrl.operatorsDict[vFilter.OperatorId].Type, vFilter._valueOptions.length);
        vFilter.Values = ReportFilters.getValues(vFilter.Values, ctrl.operatorsDict[vFilter.OperatorId].MaxNumberOfValues, false);
    };

    ctrl.editFilter = function (filter, index) {
        var vFilter = angular.copy(filter);
        refreshFilter(vFilter, false);
        vFilter._editing = true;
        ctrl.filtersView[index] = vFilter;
        validator.setValidator('editFilter', false, "Click 'Set Filter' or 'Cancel' to continue.");
    };

    ctrl.saveEditFilter = function (filter, vFilter) {
        filter.update(vFilter, vFilter._dataType, vFilter._valueOptions);
        vFilter._editing = false;
        validator.setValidator('editFilter', true);
        report.changeSaveStatus(false);
    };

    ctrl.cancelEditFilter = function (vFilter) {
        validator.setValidator('editFilter', true);
        vFilter._editing = false;
    };

    ctrl.deleteFilter = function (index) {
        report.reportFilters.deleteFilter(index);
        report.changeSaveStatus(false);
    };

    ctrl.getDropDownList = function (useDefault) {
        return useDefault ? ctrl.fieldsSelected : ctrl.allFields;
    };

    ctrl.showValidationMessage = function () {
        return !validator.isValid();
    };

    ctrl.getErrors = function () {
        return validator.getErrors();
    };

    //console.log('ReportFiltersController');
}]);