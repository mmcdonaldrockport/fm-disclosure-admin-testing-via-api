@ECHO OFF
 REM usage: testrunner [options] <project-file>
 REM -a         Turns on exporting of all results
 REM -A         Turns on exporting of all results using folders instead of long filenames
 REM -c <arg>   Sets the testcase
 REM -d <arg>   Sets the domain
 REM -D <arg>   Sets system property with name=value
 REM -e <arg>   Sets the endpoint
 REM -E <arg>   Sets the environment
 REM -f <arg>   Sets the output folder to export results to
 REM -F <arg>   Report format. Used with -R. Valid options PDF, XLS, HTML, RTF, CSV, TXT, and XML (comma-separated)
 REM -G <arg>   Sets global property with name=value
 REM -g         Sets the output to include Coverage HTML reports
 REM -h <arg>   Sets the host
 REM -H <arg>   Adds a custom HTTP Header to all outgoing requests (name=value), can be specified multiple times
 REM -i         Enables Swing UI for scripts
 REM -I         Do not stop if error occurs, ignore them
 REM -j         Sets the output to include JUnit XML reports
 REM -J         Sets the output to include JUnit XML reports adding test properties to the report
 REM -l <arg>   Installs an activated license file
 REM -M         Creates a Test Run Log Report in XML format
 REM -m         Sets the maximum number of TestStep errors to save for each testcase
 REM -O         Do not send usage statistics
 REM -o         Opens generated report(s) in a browser
 REM -p <arg>   Sets the password
 REM -P <arg>   Sets or overrides project property with name=value
 REM -r         Prints a small summary report
 REM -R <arg>   Report to Generate
 REM -s <arg>   Sets the testsuite
 REM -S         Saves the project after running the tests
 REM -t <arg>   Sets the soapui-settings.xml file to use
 REM -T <arg>   Runs only test cases that have the specified tags. Format: -T"TestCase tag1[, tag2 ...]".
 REM -u <arg>   Sets the username
 REM -v <arg>   Sets password for soapui-settings.xml file
 REM -w <arg>   Sets the WSS password type, either 'Text' or 'Digest'
 REM -x <arg>   Sets project password for decryption if project is encrypted

"c:\Program Files\SmartBear\ReadyAPI-1.8.5\bin\testrunner.bat" -D runoptions=C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/runoptions.properties -s "FM Data Transfer" -c "FM Data Transfer Test" -E "Dev" C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/FMDataTransfers-soapui-project.xml
