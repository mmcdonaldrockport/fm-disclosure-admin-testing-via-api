// clean up error codes and turn into a list of quoted strings for error message query
// https://mvnrepository.com/artifact/com.google.guava/guava
@Grapes(
    @Grab(group='com.google.guava', module='guava', version='r05')
)

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

def filepath = 'C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/datafiles/SIT Data Transfer Files 20161216'

xpath02 = 'PropertyFinancialDataReadyRequestPayload.PropertyFinancial/Collaterals/mfcdf:Collateral/mfcdf:CollateralProperty/mfcdf:PropertyIdentifier'

def namespaceStr = '''
xmlns:ns0="http://www.fanniemae.com/multifamily/dataexchange/interface/v1.x"
xmlns:ns1="http://www.fanniemae.com/enterprisedata/core/serviceTypes/v1.1"
xmlns:ns1="http://www.fanniemae.com/multifamily/integrated/cdf/v1.x"
'''

def wrapper = "'";
def separator = " ";
    
def splitter = Splitter.on(separator).omitEmptyStrings().trimResults();
//def joiner = Joiner.on(wrapper + separator + wrapper).skipNulls();
    
// parse namespaces
Map nameSpaces = [:];
/*
splitter.split(namespaceStr).each { nsStr ->
    def nsParts = nsStr.split(':');
    if (nsStr.split(':')[0] == 'xmlns') {
        nsPrefix = nsStr.split(':')[1].split('=')[0];
        nsValue = nsStr.split('=')[1][1..-2]  // trim quotes;
        nameSpaces.put(nsPrefix, nsValue);
        println nameSpaces;
    }
}
*/
// reformat xpath to gpath

new File(filepath).eachFile() { file->  
    def filename = file.getName();
    def fileContent = new File(filepath + '/' + filename).text;
    def xml = new XmlSlurper().parseText(fileContent);
    //.declareNamespace(nameSpaces);

    //DebtInstrument.AssetSecuritizedInstrument.MortgageBackedSecurity.MBSPoolPrefixType
    //DebtInstrument.AssetSecuritizedInstrument.MortgageBackedSecurity.MBSSecurityARMSubType
    ////AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.Agreements.Agreement.Deal.Loans.*.LoanAcquisition.LoanDUSDiscloseDerivedProductType

    def secValues = [:];
    //println secValues.isEmpty()

    xml.AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.CollateralPools.CollateralPool.Security.each() { security ->
        secValues['cusip'] = security.SecurityCUSIPIdentifier;
        secValues['prefix'] = security.DebtInstrument.AssetSecuritizedInstrument.MortgageBackedSecurity.MBSPoolPrefixType;
        secValues['secType'] = security.ProductGroupType;
        secValues['intRateType'] = security.SecurityInterestRateTerm.SecurityInterestRateType
        secValues['armSubtype'] = security.DebtInstrument.AssetSecuritizedInstrument.MortgageBackedSecurity.MBSSecurityARMSubType;
        secValues['issueDate'] = security.SecurityIssueDate;
        secValues['passthruRate'] = security.SecurityInterestRates.SecurityInterestRate.SecurityInterestRate;
        //println "$filename: $secType $intRateType $prefix $armSubtype";
        secValues['dealId'] = xml.AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.Agreements.Agreement.Deal.DealAgreementIdentifier;
    }
    def loanInfo = [:];
    xml.AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.Agreements.Agreement.Deal.Loans.'*'.each { loan ->
        loanInfo[loan.LoanIdentifier] = loan.LoanAcquisition.LoanDUSDiscloseDerivedProductType;
    } 

    def printStr = '';
    secValues.each { k, v ->
        printStr += (v != "" ? "$v, " : "");
    };
    
    loanInfo.each { k, v ->
        printStr += "$k:$v, ";
    }   

    if (printStr != "") {
        println "$filename: $printStr";
    }        
    
//    ns1:AtIssuanceDisclosureDataReadyRequest.AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.CollateralPools.mfcdf:CollateralPool.mfcdf:Security.mfcdf:ProductGroupType    //
    //AtIssuanceDisclosureDataReadyRequest.AtIssuanceDisclosureDataReadyRequestPayload.AtIssuanceMBSDisclosure.CollateralPools.mfcdf:CollateralPool.mfcdf:Security
    
}  

return;

/**

import org.xml.sax.InputSource;
import java.io.InputStream;
import javax.xml.xpath.*

def records = '''<records>
      <car name='HSV Maloo' make='Holden' year='2006'>
        <country>Australia</country>
        <record type='speed'>Production Pickup Truck with speed of 271kph</record>
      </car>
      <car name='P50' make='Peel' year='1962'>
        <country>Isle of Man</country>
        <record type='size'>Smallest Street-Legal Car at 99cm wide and 59 kg in weight</record>
      </car>
      <car name='Royale' make='Bugatti' year='1931'>
        <country>France</country>
        <record type='price'>Most Valuable Car at $15 million</record>
      </car>
    </records>''';

def xpath = XPathFactory.newInstance().newXPath();

InputStream inpFile = new FileInputStream(new File('C:\\Users\\mmcdonald.ROCKPORT\\projects\\FM_DataTransfer\\datafiles\\11-propfin\\SE_MSVG.PROP_FNCL_STSFD_20140512151515.xml'));
InputSource is = new InputSource(new InputStreamReader(inpFile));

def nodes = xpath.evaluate( '//PropertyFinancialDataReadyRequest/PropertyFinancialDataReadyRequestPayload/PropertyFinancial/Collaterals/mfcdf:Collateral/mfcdf:CollateralProperty/mfcdf:PropertyIdentifier', is, XPathConstants.NODESET )
def messages = [];
nodes.each{ 
  def make = xpath.evaluate( '@make', it )
  def country = xpath.evaluate( 'country/text()', it )
  def type = xpath.evaluate( 'record/@type', it )
  messages << "$make of $country has a $type record"
  println it;

}
  println messages;
 return 0;


*/