import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class GUIUtils {

    static listDialog(String title) {
        return new GUIUtils.ListDialog(null, title);
    }

    public class ListDialog extends JDialog {
        public ListDialog(String title) {
            //super();
            setBounds(100, 100, 450, 300);
            setLocationRelativeTo(null); // center on screen
            setModal(true); 
            getContentPane().setLayout( new BorderLayout());
            setTitle(title);
            setVisible( true );
        }
    }

}

GUIUtils.listDialog('test');