@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET me=%~n0
SET parent=%~dp0

SET saxonjar=C:\local\saxon\saxon9he.jar
SET xquery=ExtractRequiredFields.xq
SET output=alljobs.reqFlds.xml
SET qversion=3.1

java -cp "%saxonjar%" net.sf.saxon.Query -q:%xquery% -o:%output% -qversion:%qversion%
IF %ERRORLEVEL% NEQ 0 (
  ECHO *** Extract failed ***
  EXIT /B 1
)
ECHO *** Result written to '%output%' ***