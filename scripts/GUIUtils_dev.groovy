import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class GUIUtils {

    static listDialog(def title, def itemList) {
        return new GUIUtils.ListDialog(null, title, itemList);
    }

    public class ListDialog extends JDialog {
        def selectedItemList = [];
        def itemList = [];
        JList jList = null;
    
        public ListDialog(def title, def itemList) {
            //super();
            this.itemList = itemList;
            setBounds(100, 100, 450, 300);
            setLocationRelativeTo(null); // center on screen
            setModal(true); 
            getContentPane().setLayout( new BorderLayout());
            setTitle(title);
            
            jList = new JList();

            jList.setModel(new AbstractListModel() {
                def values = itemList;
                
                public int getSize() {
                    return values.size;
                }
                public Object getElementAt(int index) {
                    return values[index];
                }
            });
        
            jList.setLayoutOrientation(JList.VERTICAL);
            
            JButton okBtn = new JButton( "OK" );
            okBtn.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e ) {
                    selectedItemList = jList.getSelectedValuesList();
                    dispose();
                }
            });
    
            JButton cancelBtn = new JButton( "Cancel" );
            cancelBtn.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e ) {
                   selectedItemList = [];
                   dispose();
                }
            });
            
            JPanel contentPanel = new JPanel();
            contentPanel.setLayout( new BorderLayout() );
            contentPanel.add( new JScrollPane( jList ) );
    
            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout( new FlowLayout(FlowLayout.CENTER) );
            buttonPanel.add( okBtn );
            buttonPanel.add( cancelBtn );
    
            getContentPane().add( contentPanel, BorderLayout.CENTER ); 
            getContentPane().add( buttonPanel, BorderLayout.SOUTH);
            //setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
             
            //pack();
            //setVisible( true );
            //return selectedList;
        }
        
        ArrayList getSelectedItems() {
            //setVisible( true );
            setVisible( true );
            dispose();
            return selectedItemList;
        }
        
    }

}

println GUIUtils.listDialog('Select Lakes to Delete', ['Huron','Ontario','Michigan','Erie','Superior']).getSelectedItems();
//def dlg2 = GUIUtils.listDialog('test', [4,5,6]);
println GUIUtils.listDialog('Select Numbers', [1,2,3]).getSelectedItems();