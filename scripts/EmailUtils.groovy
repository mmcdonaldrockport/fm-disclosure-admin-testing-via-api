//@Grab(group='javax.mail', module='mail', version='1.4')
//@Grab(group='com.sun.mail', module='javax.mail', version='1.5.5')

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.*;

import javax.mail.*;
import javax.mail.search.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import com.sun.mail.util.MailSSLSocketFactory;


class EmailUtils {

    public static void main(String[] args) {
        def smtpHost = "smtp.office365.com";
        def smtpPort = "587";
        def from = "mmcdonald@rockportllc.com";
        def to = "mmcdonald@rockportllc.com";
        def username = "mmcdonald@rockportllc.com";
        def encodedPassword = "bSRyMGNrNU1jZDBuQDFk";
        //String encoded = s.bytes.encodeBase64().toString();
        def subject = "Test Email";
        def body = "This is the body.";
    
        //def mu = new MailUtils();
       // MailUtils.getEmail();
        EmailUtils.sendTextEmail(smtpHost, smtpPort, from , to, username, new String(encodedPassword.decodeBase64()), subject, body);
        
    }

    def static getImapEmail() {
    
        def host = "outlook.office365.com"
        def port = "993"
        def username = "mmcdonald@rockportllc.com"
        def encodedPassword = "bSRyMGNrNU1jZDBuQDFk"
        //String encoded = s.bytes.encodeBase64().toString()
        
        // set properties
        //IMAP properties : https://javaee.github.io/javamail/docs/api/com/sun/mail/imap/package-summary.html
        Properties props = new Properties()
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.imaps.starttls.enable", "true");
        props.put("mail.imaps.ssl.socketFactory", sf);
        props.put("mail.store.protocol", "imap")
        props.put("mail.imaps.host", host)
        props.put("mail.imaps.port", port)
        props.put("mail.imaps.connectiontimeout", 15000);
        props.put("mail.imaps.timeout", 15000);
        

       
        // connect
        def session = Session.getDefaultInstance(props, null)
        def store = session.getStore("imaps")
        store.connect(host, username, new String(encodedPassword.decodeBase64()));
        
        // open folder
        def folder = store.getFolder("Inbox");
        //def clutterFolder = store.getFolder("Clutter");
        folder.open(Folder.READ_WRITE);
        //draftsFolder.open(Folder.READ_WRITE)
        
        //LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        def yesterday = Date.from(LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant());
        SearchTerm sender = new FromTerm(new InternetAddress("mmcdonald@qamentor.com"));
        SearchTerm newerThan = new ReceivedDateTerm(ComparisonTerm.GT, yesterday);
        SearchTerm allTerms = new AndTerm(sender, newerThan);
        
        def msgCount = folder.getMessageCount();
        def msgs = folder.search(allTerms);
        
        def printFmt = '%1$-15s %2$-60s\n';
        
        def searchMsgCount = msgs.length;
        if (searchMsgCount > 0) { 
            def msg = msgs[searchMsgCount - 1];
            def flags = [];    
            msg.isSet(Flags.Flag.ANSWERED) ? flags << "ANSWERED " : "";
            msg.isSet(Flags.Flag.DELETED) ? flags << "DELETED " : ""; 
            msg.isSet(Flags.Flag.DRAFT) ? flags << "DRAFT " : "";  
            msg.isSet(Flags.Flag.FLAGGED) ? flags << "FLAGGED " : "";  
            msg.isSet(Flags.Flag.RECENT) ? flags << "RECENT " : "";  
            msg.isSet(Flags.Flag.SEEN) ? flags << "SEEN " : "";  
            msg.isSet(Flags.Flag.USER) ? flags << "USER " : "";  
        
            printf(printFmt, "From:", msg.from[0]);
            printf(printFmt, "To:", msg.allRecipients[0]);
            printf(printFmt, "Subject: ", msg.subject);
            printf(printFmt, "Sent On: ", msg.sentDate);
            printf(printFmt, "Received On: ", msg.receivedDate);
            printf(printFmt, "Message #:", msg.messageNumber);
            printf(printFmt, "Reply To: ", msgs.replyTo.address[0]);
            printf(printFmt, "Flags: ", flags);
            printf(printFmt, "Folder: ", msgs.folder);
            printf(printFmt, "Content Type: ", msg.contentType);
            // GetContent???
        } else {
            println("No messages found");
        }
        
        folder.close(false);
        
        //folder.getMessages(msgCount, msgCount).each { msg ->
        //msgs.each { msg ->
        //    println msg.subject
            //javax.mail.Message[] msgsToCopy = [msg];
            //msgsToCopy.add(msg);
            //draftsFolder.copyMessages(msgsToCopy, clutterFolder);
            //msg.setFlag(Flags.Flag.DELETED, true);
        //}
    }
    
    def static sendTextEmail(
        String host,
        String port,
        String from,
        String to,
        String username,
        String password,
        String subject,
        String body
    ) {
        
    
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
 
        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
 
        Session session = Session.getInstance(properties, auth);
 
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
 
        msg.setFrom(new InternetAddress(from));
        InternetAddress[] toAddresses = [ new InternetAddress(to) ];
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        // set plain text message
        msg.setContent(body,"text/plain");
 
        // sends the e-mail
        Transport.send(msg);
    }

} 
