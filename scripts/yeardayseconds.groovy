import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.DateTime;

DateTime dt = new DateTime();

DateTimeFormatter fmt = DateTimeFormat.forPattern("yyD") ;
String str = fmt.print(dt) + (dt.secondOfDay + '').padLeft(5,'0');