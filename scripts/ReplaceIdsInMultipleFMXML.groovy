/*
* Replace identifiers in FM message so it can be reused
* Only works with PreClose, AtIssuance without PreClose, not for REMIC.
*/

@Grapes(
    @Grab(group='joda-time', module='joda-time', version='2.9.4')
)
import groovy.util.XmlSlurper;
import groovy.xml.XmlUtil;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.DateTime;
import java.util.Random;
import javax.swing.filechooser.FileFilter
import javax.swing.JFileChooser
import groovy.swing.SwingBuilder
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ReplaceIdsInFMXML {

    
    
// creates the replacement values
    public static void main(String[] args) {
        
       
        if (args) {
            println("-------------------------------------------------------------------------------");
            println("Replaces identifiers in FM message so it can be reused.");
            println("Only works with PreClose, AtIssuance without PreClose, not for REMIC.\n");
            println("USAGE: groovy ReplaceIdsInFMXML.groovy");
            println("-------------------------------------------------------------------------------");
            
            System.exit(0);
        }
        
        def rand = new Random();
        
        DateTimeFormatter yrDaySecsFmt = DateTimeFormat.forPattern("yyDDD");
        DateTime dt = new DateTime();
        def yrDaySecs = yrDaySecsFmt.print(dt) + (dt.secondOfDay + '').padLeft(5,'0');
        def newYrDaySecs = yrDaySecs;
        
        DateTimeFormatter fmTimestampFmt = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss.SSS");

        // get the last used directory if found
        def lastDir = '';
        // get last directory if it exists
        def userHome = System.getProperty("user.home");
        File lastDirFile = new File(userHome + '/Rockport/FannieMae/dev/.lastDir');
        if (lastDirFile.exists()) {
            lastDir = lastDirFile.text;
        } else {
            lastDir = userHome;
            File d = new File(userHome + '/Rockport/FannieMae/dev');
            d.mkdirs();
            //ld = new File(d.canonicalPath + '/.lastDir');
            lastDirFile.write(lastDir);
        }
          
        // filter for xml files
        def filter = [getDescription: {-> "*.xml"}, accept:{file -> file.toString() ==~ /.*?\.xml/  || file.isDirectory() }] as FileFilter;
        
        def inFiles = [];
        def outDir = null;
        def outFile = null;
        def outFileName = '';
        def fileOutCount = 0;
        
        def swing = new SwingBuilder()
        
        // get input files
        
        def dialog = swing.fileChooser(dialogTitle: "Select Input File(s)", 
            currentDirectory: new File(lastDir),
            multiSelectionEnabled: true,
            fileFilter: filter);
        if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
            println "input: " +  dialog.selectedFiles;
            inFiles = dialog.selectedFiles;
        } else {
            println("User canceled, exiting.");
            System.exit(1);
        }
        
        lastDir = inFiles[0].canonicalPath;

        // get output directory
        dialog = swing.fileChooser(dialogTitle: "Select Output Directory", 
            currentDirectory: new File(lastDir),
            fileSelectionMode:JFileChooser.DIRECTORIES_ONLY, 
            fileFilter: filter);
        if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
            println "Output: " +  dialog.selectedFile;
            outDir = dialog.selectedFile;
        } else {
            println("User canceled, exiting.");
            System.exit(1);
        }
        
        //lastDir = outFile.canonicalPath;
      
        /*
        inFileName = args[0];
        if (args.size() < 2) {
            ArrayList a = args[0].split('\\.')
            a.push(a[-1]);
            a[-2] = 'new-' + uniquePrefix;
            outFileName = a.join('.');
            println("No output file specified, will write to " + outFileName);
        } else {
           outFileName = args[1];
        }
        */
       
        def sb = new SwingBuilder();
        
        try {

           inFiles.each { inFile ->
                /* pattern for replacement:
                    [unique number string, arbitrary string, increment start number, places to left-fill with zeroes]
                    if zero-fill is 0, the incrementing number is ignored
                */
                while ((newYrDaySecs = yrDaySecsFmt.print(dt) + (dt.secondOfDay + '').padLeft(5,'0')) == yrDaySecs) {
                    dt = new DateTime();
                }
                yrDaySecs = newYrDaySecs;
                def uniquePrefix = (rand.nextInt(8) + 1) + yrDaySecs[5..6] + yrDaySecs[1] + yrDaySecs[2..4] + yrDaySecs[7..9];
    
                def fmTimestamp = fmTimestampFmt.print(dt);
                
                ArrayList a = inFile.name.split('\\.')
                a.push(a[-1]);
                a[-2] = 'new-' + uniquePrefix;
                outFileName = a.join('.');
                outFile = new File(outDir.canonicalPath + '/' + outFileName);
                //println("No output file specified, will write to " + outFile.canonicalPath);
                
                def params = [:]       
                params["RequestCreationTimestamp"]  = ['', fmTimestamp, 0, 0];
                params["RequestPostTimestamp"]      = ['', fmTimestamp, 1, 0];
                params["RequestID"]                 = [uniquePrefix, '', 0, 0];
                params["DealAgreementIdentifier"]   = [uniquePrefix, '', 1, 3];
                params["DealIdentifier"]            = [uniquePrefix, '', 1001, 3];
                params["LoanIdentifier"]            = [uniquePrefix, '', 2001, 3];
                params["FannieMaeLoanNumber"]       = [uniquePrefix, '', 0, 0];
                params["CollateralIdentifier"]      = [uniquePrefix, '', 5001, 3];
                params["CollateralReferenceNumber"] = [uniquePrefix, '', 6001, 3];
                params["PropertyIdentifier"]        = [uniquePrefix, '', 7001, 3];
                params["MBSPoolIdentifier"]         = [uniquePrefix, '', 8001, 3];
                params["SecurityCUSIPIdentifier"]  =  [uniquePrefix, 'C', 1, 3];
                
                 // there can be duplicate collateral/property ids in a file, so check for these and reuse already generated ids
                def repeatingValues = [:];
                
                //def inFile = new File('C:/local/AtIssuanceMBSDisclosure_MSFMS_65_AN0457.xml');
                //def inFile = new File(inFileName);
                def text = inFiles[0].getText();
                
                def xml = new XmlSlurper().parseText(text)
                
                println('About to replace values in ' + inFile.name);
                params.each { key, value ->
                    println('Replacing ' + key);
                    xml.'**'.findAll{it.name() == key}.each { node ->
                        println(key + '_' + node.localText());
                            if (repeatingValues[key + '_' + node.localText()]) {
                            println('Duplicate');
                            node.replaceBody(repeatingValues[key + '_' + node.localText()]);
                        } else {
                            def newId = evalParam(value);
                            repeatingValues.put(key + '_' + node.localText(), newId);
                            node.replaceBody(newId);
                        }
                    }
                }
                
                //def outFile = new File('C:/local/testreplace.xml');
                //def outFile = new File(outFileName);
                outFile.write(XmlUtil.serialize(xml));
                //lastDirFile.write(lastDir);
                println('Saved file to ' + outFile.name);
                fileOutCount++;
            }
        } catch (Exception e) {
            // something bad happened
            sb.optionPane().showMessageDialog(null, "Error occurred, " + e.getMessage(), "Message", JOptionPane.ERROR_MESSAGE)
            System.exit(-1);
        }

        // we're good!
        sb.optionPane().showMessageDialog(null, "$fileOutCount out of ${inFiles.size()} files written to " + outDir.canonicalPath, "Message", JOptionPane.INFORMATION_MESSAGE)
    
        System.exit(0);
    
    }
    
    public static String evalParam(param) {
        def val = param[0] + param[1] + '';
        if (param[3] + 0 > 0) {
            val += (param[2]++ + '').padLeft(param[3],'0');
        }
        return val;
    }
    
    
   
    /*
    RequestCreationTimestamp
    RequestPostTimestamp
    RequestID
    
    DealAgreementIdentifier
    DealIdentifier
    LoanIdentifier
    FannieMaeLoanNumber
    CollateralIdentifier
    CollateralReferenceNumber
    PropertyIdentifier
    
    PropertyActivityDatetime
    
    MBSPoolIdentifier
    SecurityCUSIPIdentifier
    
    SecurityIssueDate
    SecurityFederalBookEntryDate
    
    SecurityStatusDatetime
    SecurityFactorEffectiveDate
    SecurityInterestRateEffectiveDate
    SecurityFirstPaymentDate
    */
    
    
    /*
    def getNodes = { doc, path ->
        def nodes = doc
        path.split("\\.").each { nodes = nodes."${it}" }
        return nodes
    }
    
    
        getNodes(xml, key).each { node ->
          println("node");
          node.replaceBody(evaluate(value));
        }
    
    }
    */
    /*
    param.each { key,value ->
        def node = xml
        key.split("\\.").each { it ->
          node = node."${it}"
        }
        node.replaceBody(evaluate(value));  
    }
    */
    
    //println XmlUtil.serialize(xml)

}