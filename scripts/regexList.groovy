import java.util.regex.Pattern;
import java.util.regex.Matcher;

def expErrs = [
        [
        'regex':"There should be 0 or 1 NOT more than 1 Financial Instrument Collateral Group Balance level node-set per message", 
        'msg':"2.4: There should be 0 or 1 (NOT more than 1) Financial Instrument Collateral Group Balance level node-set per message"
        ], 
        [
        'regex':".*", 
        'msg':"1.11: MBSPoolIdentifier (PreClose level) already exists in the system"
        ]
];

def actErrs = ["MBSPoolIdentifier (PreClose level) already exists in the system"]

//def actErrs = ['This is error one','This is error (two)'];
//def expErrs = [['regex':'(two)','msg':'1.two'], ['regex':'three','msg':'1.three']];


def missErrs = expErrs.collect{it};
def unmatchedErrs = actErrs.collect{it};

def me = 0;
println 'Before:';
//println missErrs;
//println unexpErrs;

while (me < missErrs.size()) {
        
    println("Missing Error: " + missErrs[me]['regex']);
    //def pattern = Pattern.compile(Pattern.quote(missErrs[me]['regex']));
    def pattern = Pattern.compile(missErrs[me]['regex']);
    boolean found = false;
    
    def ue = 0;
    while (ue < unmatchedErrs.size()) {    
         
         println("UnmatchedError: " + unmatchedErrs[ue]);
         def matcher = pattern.matcher(unmatchedErrs[ue]);
       
         if (matcher.find()) {
            found = true;
            unmatchedErrs.remove(ue);
            println("FOUND");
         } else {
            ue++;
         }
    }
    
    if (found) { 
        missErrs.remove(me);
    } else {
        me++;
    }
}

/*
println 'After:';
println missErrs;
println unmatchedErrs;*/