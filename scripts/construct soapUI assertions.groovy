import static java.util.UUID.randomUUID 
 
/*
def sourceId = '';



*/


def scriptAssertion = '''<con:entry>
                        <con:id>$uuid1</con:id>
                        <con:isGroup>false</con:isGroup>
                        <con:sourceId>dee164d7-b3b1-4042-91c5-11fe8ef444cb</con:sourceId>
                        <con:propertyName>ResponseAsXml</con:propertyName>
                        <con:assertion type="GroovyScriptAssertion" name="$name" id="$uuid2">
                            <con:configuration>
                                <scriptText>import static com.jayway.jsonpath.JsonPath.parse;
import com.rockportllc.soapui.utils.AssertionUtils;

String storedStr  = context.expand( '${Get DB Property Details#ResponseAsXml#//$dbpath}' )

String msgStr = context.expand(\'\'\'
    ${#TestCase#message#//*:Loans/*:Loan[*:LoanIdentifier=${#TestCase#loanIdentifier}]
        /*:LoanCollaterals/*:LoanCollateral/*:Collateral
        /*:CollateralProperty[*:PropertyIdentifier=${Get Property Info#ResponseAsXml#//PROPERTYIDENTIFIER}]
        /*:Property/*:PropertyActivities/*:PropertyActivity[*:PropertyActivityType='Property Occupancy']
        /*:PropertyOccupancy/*:PropertyPhysicalOccupancyPercent}\'\'\');

assert AssertionUtils.compareStringsAsDecimals(storedStr, msgStr, '=', ['':'0.0']);</scriptText>
                            </con:configuration>
                        </con:assertion>
                    </con:entry>''';

def xpathAssertion = '''<con:entry>
                        <con:id>$uuid1</con:id>
                        <con:isGroup>false</con:isGroup>
                        <con:sourceId>dee164d7-b3b1-4042-91c5-11fe8ef444cb</con:sourceId>
                        <con:propertyName>ResponseAsXml</con:propertyName>
                        <con:assertion type="XPath Match" name="$name" id="$uuid2">
                            <con:configuration>
                                <path>//$dbpath</path>
                                <content>${#TestCase#message#
//*:Agreements/*:Agreement/*:Deal/*:Loans/*:Loan[*:LoanIdentifier=${#TestCase#loanIdentifier}]
/*:LoanCollaterals/*:LoanCollateral/*:Collateral
/*:CollateralProperty[*:PropertyIdentifier=${Get Property Info#ResponseAsXml#//PROPERTYIDENTIFIER}]
/*:Property/*:PropertyAddresses/*:PropertyAddress/*:PropertyAddressCityName}</content>
                                <allowWildcards>false</allowWildcards>
                                <ignoreNamspaceDifferences>false</ignoreNamspaceDifferences>
                                <ignoreComments>false</ignoreComments>
                            </con:configuration>
                        </con:assertion>
                    </con:entry>''';

//println scriptAssertion.replace('$uuid1', uuid1).replace('$uuid2', uuid2);

def fields = [
["Property Acquisition Name","X","PROPERTYACQUISITIONNAME"],
["Property Built Year","X","PROPERTYBUILTYEAR"],
["Property Acquisition Total Unit Count","X","PROPERTYACQUISITIONTOTALUNITCOUNT"],
["Property Green Building Certification Type","X","PROPERTYGREENBUILDINGCERTIFICATIONTYPE"],
["Property Land Ownership Right Type","X","PROPERTYLANDOWNERSHIPRIGHTTYPE"],
["Property Tax Escrowed Indicator","X","PROPERTYTAXESCROWEDINDICATOR"],
["Property Age Restricted Indicator","X","PROPERTYAGERESTRICTEDINDICATOR"],
["Property Affordable Housing Type","X","PROPERTYAFFORDABLEHOUSINGTYPE"],
["Property Dwelling Unit Income or Rent Restriction Percent","D","PROPERTYDWELLINGUNITINCOMEORRENTRESTRICTIONPERCENT"],
["Property Housing Assistance Program Remaining Term","X","PROPERTYHOUSINGASSISTANCEPROGRAMREMAININGTERM"],
["Property Address Identifier","X","PROPERTYADDRESSIDENTIFIER"],
["Property Acquisition Street Address Text","X","PROPERTYACQUISITIONSTREETADDRESSTEXT"],
["Property Acquisition Address City Name","X","PROPERTYACQUISITIONADDRESSCITYNAME"],
["Property Acquisition Address County Name","X","PROPERTYACQUISITIONADDRESSCOUNTYNAME"],
["Property Acquisition Address State Code","X","PROPERTYACQUISITIONADDRESSSTATECODE"],
["Property Acquisition Address Postal Code","X","PROPERTYACQUISITIONADDRESSPOSTALCODE"],
["Property Geocode Longitude Number","D","PROPERTYGEOCODELONGITUDENUMBER"],
["Property Geocoded Latitude Number","D","PROPERTYGEOCODEDLATITUDENUMBER"],
["Property Primary Address Indicator","X","PROPERTYPRIMARYADDRESSINDICATOR"],
["Property Geographic Metropolitan Statistical Area Desc","X","PROPERTYGEOGRAPHICMETROPOLITANSTATISTICALAREADESC"],
["Geographic Metropolitan Statistical Area Code","X","GEOGRAPHICMETROPOLITANSTATISTICALAREACODE"],
["Property Acquisition Property Type","X","PROPERTYACQUISITIONPROPERTYTYPE"],
["Property Phase Number","X","PROPERTYPHASENUMBER"],
["Property Phase Constructed Year","X","PROPERTYPHASECONSTRUCTEDYEAR"],
["Property Phase Constructed Unit Count","X","PROPERTYPHASECONSTRUCTEDUNITCOUNT"],
["Property Activity Type (Financial Statement)","X","PROPERTYACTIVITYTYPE(FINANCIALSTATEMENT)"],
["Property Activity Datetime (Financial Statement)","X","PROPERTYACTIVITYDATETIME(FINANCIALSTATEMENT)"],
["Property Financial Statement Fiscal Year","X","PROPERTYFINANCIALSTATEMENTFISCALYEAR"],
["Property Financial Activity Type (Financial Income)","X","PROPERTYFINANCIALACTIVITYTYPE(FINANCIALINCOME)"],
["Property Financial Activity Source Type (Financial Income)","X","PROPERTYFINANCIALACTIVITYSOURCETYPE(FINANCIALINCOME)"],
["Property Underwritten Commercial Income Amount","D","PROPERTYUNDERWRITTENCOMMERCIALINCOMEAMOUNT"],
["Property Underwritten Gross Potential Rent Amount","D","PROPERTYUNDERWRITTENGROSSPOTENTIALRENTAMOUNT"],
["Property Underwritten Laundry and Vending Income Amount","D","PROPERTYUNDERWRITTENLAUNDRYANDVENDINGINCOMEAMOUNT"],
["Property Underwritten Less Bad Debt Income Amount","D","PROPERTYUNDERWRITTENLESSBADDEBTINCOMEAMOUNT"],
["Property Underwritten Less Concession Revenue Amount","D","PROPERTYUNDERWRITTENLESSCONCESSIONREVENUEAMOUNT"],
["Property Underwritten Meal Income Amount","D","PROPERTYUNDERWRITTENMEALINCOMEAMOUNT"],
["Property Underwritten Medicare and Medicaid Income Amount","D","PROPERTYUNDERWRITTENMEDICAREANDMEDICAIDINCOMEAMOUNT"],
["Property Underwritten Nursing Medical Income Amount","D","PROPERTYUNDERWRITTENNURSINGMEDICALINCOMEAMOUNT"],
["Property Underwritten Parking Income Amount","D","PROPERTYUNDERWRITTENPARKINGINCOMEAMOUNT"],
["Property Underwritten Other Income Amount","D","PROPERTYUNDERWRITTENOTHERINCOMEAMOUNT"],
["Property Underwritten Secondary Residential Income Amount","D","PROPERTYUNDERWRITTENSECONDARYRESIDENTIALINCOMEAMOUNT"],
["Property Underwritten Vacancy and Collection Loss Amount","D","PROPERTYUNDERWRITTENVACANCYANDCOLLECTIONLOSSAMOUNT"],
["Property Financial Activity Type (Financial Expense)","X","PROPERTYFINANCIALACTIVITYTYPE(FINANCIALEXPENSE)"],
["Property Financial Activity Source Type (Financial Expense)","X","PROPERTYFINANCIALACTIVITYSOURCETYPE(FINANCIALEXPENSE)"],
["Property Underwritten Advertising and Marketing Expense Amount","D","PROPERTYUNDERWRITTENADVERTISINGANDMARKETINGEXPENSEAMOUNT"],
["Property Underwritten General and Administrative Expense Amount","D","PROPERTYUNDERWRITTENGENERALANDADMINISTRATIVEEXPENSEAMOUNT"],
["Property Underwritten Ground Rent Amount","D","PROPERTYUNDERWRITTENGROUNDRENTAMOUNT"],
["Property Underwritten Management Fees Expense Amount","D","PROPERTYUNDERWRITTENMANAGEMENTFEESEXPENSEAMOUNT"],
["Property Underwritten Meal Expense Amount","D","PROPERTYUNDERWRITTENMEALEXPENSEAMOUNT"],
["Property Underwritten Payroll and Benefits Expense Amount","D","PROPERTYUNDERWRITTENPAYROLLANDBENEFITSEXPENSEAMOUNT"],
["Property Underwritten Professional Fee Amount","D","PROPERTYUNDERWRITTENPROFESSIONALFEEAMOUNT"],
["Property Underwritten Insurance Expense Amount","D","PROPERTYUNDERWRITTENINSURANCEEXPENSEAMOUNT"],
["Property Underwritten Real Estate Taxes Expense Amount","D","PROPERTYUNDERWRITTENREALESTATETAXESEXPENSEAMOUNT"],
["Property Underwritten Repairs and Maintenance Expense Amount","D","PROPERTYUNDERWRITTENREPAIRSANDMAINTENANCEEXPENSEAMOUNT"],
["Property Underwritten Housekeeping Amount","D","PROPERTYUNDERWRITTENHOUSEKEEPINGAMOUNT"],
["Property Underwritten Utilities Expense Amount","D","PROPERTYUNDERWRITTENUTILITIESEXPENSEAMOUNT"],
["Property Underwritten Water and Sewer Expense Amount","D","PROPERTYUNDERWRITTENWATERANDSEWEREXPENSEAMOUNT"],
["Property Underwritten Other Expense Amount","D","PROPERTYUNDERWRITTENOTHEREXPENSEAMOUNT"],
["Property Financial Activity Type (Financial Summary)","X","PROPERTYFINANCIALACTIVITYTYPE(FINANCIALSUMMARY)"],
["Property Financial Activity Source Type (Financial Summary)","X","PROPERTYFINANCIALACTIVITYSOURCETYPE(FINANCIALSUMMARY)"],
["Property Underwritten Effective Gross Income Amount","D","PROPERTYUNDERWRITTENEFFECTIVEGROSSINCOMEAMOUNT"],
["Property Underwritten Total Operating Expense Amount","D","PROPERTYUNDERWRITTENTOTALOPERATINGEXPENSEAMOUNT"],
["Property Underwritten Total Operating Capital Expenditure (Replacement Reserves) Amount","D","PROPERTYUNDERWRITTENTOTALOPERATINGCAPITALEXPENDITURE(REPLACEMENTRESERVES)AMOUNT"],
["Property Underwritten NCF Amount","D","PROPERTYUNDERWRITTENNCFAMOUNT"],
["Property Activity Type (Property Energy)","X","PROPERTYACTIVITYTYPE(PROPERTYENERGY)"],
["Property Activity Datetime (Property Energy)","X","PROPERTYACTIVITYDATETIME(PROPERTYENERGY)"],
["Property Energy Star Score Date","X","PROPERTYENERGYSTARSCOREDATE"],
["Property Energy Star Score Number","D","PROPERTYENERGYSTARSCORENUMBER"],
["Property Source Energy Use Intensity Rate","D","PROPERTYSOURCEENERGYUSEINTENSITYRATE"],
["Property Source Energy Use Intensity Date","D","PROPERTYSOURCEENERGYUSEINTENSITYDATE"],
["Property Activity Type (Property Occupancy)","X","PROPERTYACTIVITYTYPE(PROPERTYOCCUPANCY)"],
["Property Activity Datetime (Property Occupancy)","X","PROPERTYACTIVITYDATETIME(PROPERTYOCCUPANCY)"],
["Property Underwritten Physical Occupancy As of Date","X","PROPERTYUNDERWRITTENPHYSICALOCCUPANCYASOFDATE"],
["Property Underwritten Physical Occupancy Percent","D","PROPERTYUNDERWRITTENPHYSICALOCCUPANCYPERCENT"],
["Property Underwritten Economic Occupancy Percent","D","PROPERTYUNDERWRITTENECONOMICOCCUPANCYPERCENT"],
["Property Activity Type (Property Valuation)","X","PROPERTYACTIVITYTYPE(PROPERTYVALUATION)"],
["Property Activity Datetime (Property Valuation)","X","PROPERTYACTIVITYDATETIME(PROPERTYVALUATION)"],
["Property Valuation As of Date","X","PROPERTYVALUATIONASOFDATE"],
["Property Underwritten Value Amount","D","PROPERTYUNDERWRITTENVALUEAMOUNT"],
["Property Terrorism Insurance Coverage Indicator","X","PROPERTYTERRORISMINSURANCECOVERAGEINDICATOR"],
["Property Affordable Unit AMI Type","X","PROPERTYAFFORDABLEUNITAMITYPE"],
["Property Affordable Unit 50 Percent Area Median Income Percent","D","PROPERTYAFFORDABLEUNIT50PERCENTAREAMEDIANINCOMEPERCENT"],
["Property Affordable Unit 60 Percent Area Median Income Percent","D","PROPERTYAFFORDABLEUNIT60PERCENTAREAMEDIANINCOMEPERCENT"],
["Property Affordable Unit 80 Percent Area Median Income Percent","D","PROPERTYAFFORDABLEUNIT80PERCENTAREAMEDIANINCOMEPERCENT"],
["Collateral Structured Facility Collateral Substituted Indicator","X","COLLATERALSTRUCTUREDFACILITYCOLLATERALSUBSTITUTEDINDICATOR"]

];

def assertions = "";

fields.each { f ->
    def uuid1 = randomUUID() as String;
    def uuid2 = randomUUID() as String;
    def name = f[0];
    def dbpath = f[2].replace('(','').replace(')','');

    if (f[1] == 'D') {
        assertions += scriptAssertion.replace('$uuid1', uuid1).replace('$uuid2', uuid2).replace('$name', name).replace('$dbpath', dbpath);
    
    } else if (f[1] == 'X') {
        assertions += xpathAssertion.replace('$uuid1', uuid1).replace('$uuid2', uuid2).replace('$name', name).replace('$dbpath', dbpath);
    }
        
}

new File('C:\\local\\soapuiassertions.xml') << assertions;
return 0;

return assertions;