import java.io.File;
import java.io.IOException;

import jxl.*;

import jxl.read.biff.BiffException;


import jxl.write.*;

Workbook wb = Workbook.getWorkbook(new File("C:\\Users\\mmcdonald.ROCKPORT\\projects\\FM_DataTransfer\\reports\\DataTransferTestResults_20160831_091558.xls"));
WritableWorkbook wwb = Workbook.createWorkbook(new File("C:\\Users\\mmcdonald.ROCKPORT\\projects\\FM_DataTransfer\\reports\\test.xls"));

createSheetCopy(wb, 0, 0, wwb);
public static WritableSheet createSheetCopy(Workbook w, int from, int to, WritableWorkbook writeableWorkbook) throws WriteException {
        Sheet sheet = w.getSheet(from);
        WritableSheet newSheet = writeableWorkbook.getSheet(to);
        // Avoid warning
        // "Maximum number of format records exceeded. Using default format."
        Map<CellFormat, WritableCellFormat> definedFormats = new HashMap<CellFormat, WritableCellFormat>();
        for (int colIdx = 0; colIdx < sheet.getColumns(); colIdx++) {
            newSheet.setColumnView(colIdx, sheet.getColumnView(colIdx));
            for (int rowIdx = 0; rowIdx < sheet.getRows(); rowIdx++) {
                if (colIdx == 0) {
                    newSheet.setRowView(rowIdx, sheet.getRowView(rowIdx));
                }
                Cell readCell = sheet.getCell(colIdx, rowIdx);
                Label label = new Label(colIdx, rowIdx, readCell.getContents());
                CellFormat readFormat = readCell.getCellFormat();
                if (readFormat != null) {
                    if (!definedFormats.containsKey(readFormat)) {
                        definedFormats.put(readFormat, new WritableCellFormat(
                                readFormat));
                    }
                    label.setCellFormat(definedFormats.get(readFormat));
                }
                newSheet.addCell(label);
            }
        }
        return newSheet;
    }
   