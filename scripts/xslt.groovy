import net.sf.saxon.TransformerFactoryImpl;
import net.sf.saxon.TransformerFactory;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

def xslt = '''<?xml version="1.0" encoding="UTF-8"?>
<!--
java  -jar dir/saxon7.jar   [options]   source-document   stylesheet   [ params�]
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:srvtypes="http://www.fanniemae.com/enterprisedata/core/serviceTypes/v1.1"  version="2.0">
    <xsl:output method="text"/>
    <xsl:template match="/">
        <xsl:text>FILENAME HEADER INFO</xsl:text><xsl:text>&#10;</xsl:text>
        <xsl:for-each select="collection('file:///C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/datafiles/01-preclose?select=*.xml')">
            
            <xsl:value-of select="tokenize(document-uri(.), '/')[last()]"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="//srvtypes:RequestID/text()"/>
              <xsl:text>,</xsl:text>
            <xsl:value-of select="//srvtypes:RequestPostTimestamp/text()"/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>''';

def xml = new File('C:\\Users\\mmcdonald.ROCKPORT\\projects\\FM_DataTransfer\\datafiles\\01-preclose\\PrecloseMBSAdminSetup_CANDD_670_873567.xml').getText();

TransformerFactory fact = new net.sf.saxon.TransformerFactoryImpl();
def transformer = fact.newInstance().newTransformer(new StreamSource(new StringReader(xslt)))

// Set output file
def html = new FileOutputStream("C:/Users/mmcdonald.ROCKPORT/Desktop/output.html")

// Perform transformation
transformer.transform(new StreamSource(new StringReader(xml)), new StreamResult(html))