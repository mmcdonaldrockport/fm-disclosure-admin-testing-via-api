// Using Poi to write to already existing spreadsheet.
//package com.rockportllc.soapui.utils;

import java.io.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;


def testRunName = '';
def scenarioFilePath = "C:/local/test.xls";
    
testRunName = ExcelUtils.readCellByName(scenarioFilePath, 0, "A3");
println(testRunName);


class ExcelUtils {
    private static String[] splitAlphaNumeric(String str) {
        return str.split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");
    }
    
    public static String readCell(String filePath, int sheetIdx, int rowIdx, int colIdx) {
        DataFormatter formatter = new DataFormatter(Locale.US);
    
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filePath))
        HSSFSheet sheet = workbook.getSheetAt(sheetIdx);
        //String cellValue = sheet.getRow(rowIdx).getCell(colIdx).getStringCellValue();
        String cellValue = formatter.formatCellValue(sheet.getRow(rowIdx).getCell(colIdx));
        return cellValue;
    }

    public static String writeTableValues(String filePath, int sheetIdx, String startCell, ArrayList infoArray ) {
        // read in existing file
        def stuff = "";
        File wsFile = new File(filePath);
        FileInputStream fIpStream= new FileInputStream(wsFile); 
        HSSFWorkbook wb = new HSSFWorkbook(fIpStream);

        // add info on second tab, starting in row 3
        HSSFSheet worksheet = wb.getSheetAt(sheetIdx);

        int rowNum = splitAlphaNumeric(startCell)[1].toInteger() - 1;
        String colName = splitAlphaNumeric(startCell)[0];
        int colIdx = CellReference.convertColStringToIndex(colName)
    
        stuff = rowNum;
        infoArray.each() { info ->
            Row row = worksheet.createRow(rowNum);
    
            for (def i = 0; i < info.size(); i++) {
                def descCell = row.createCell(colIdx + i);
                descCell.setCellValue(info[i]);
            }
            rowNum++;
        }
           
        fIpStream.close(); 
        
        // write out the file
        wsFile = new File(filePath);
        FileOutputStream output_file = new FileOutputStream(wsFile);  
        wb.write(output_file);
        output_file.close();
        
        return stuff;
    }
    
    public static String writeNameValuePairs(String filePath, int sheetIdx, String startCell, Map pairs ) {
        // read in existing file
        def stuff = "";
        
        
        File wsFile = new File(filePath);
        FileInputStream fIpStream= new FileInputStream(wsFile); 
        HSSFWorkbook wb = new HSSFWorkbook(fIpStream);

        // add info on second tab, starting in row 3
        HSSFSheet worksheet = wb.getSheetAt(sheetIdx);

        int rowNum = splitAlphaNumeric(startCell)[1].toInteger() - 1;
        String colName = splitAlphaNumeric(startCell)[0];
        int colIdx = CellReference.convertColStringToIndex(colName)
        
        stuff = rowNum;
        pairs.each() { key, value ->
            Row row = worksheet.createRow(rowNum);
    
            def descCell = row.createCell(colIdx);
            descCell.setCellValue(key);
            def valueCell = row.createCell(colIdx + 1);
            valueCell.setCellValue(value);
            rowNum++;
            
            //stuff += key + ":" + value;
        }
        
        fIpStream.close(); 
        
        // write out the file
        wsFile = new File(filePath);
        FileOutputStream output_file = new FileOutputStream(wsFile);  
        wb.write(output_file);
        output_file.close();
        
        return stuff;
        
        /*
        def timeout = 5000;
        
        def fileClosed = false;
        while (timeout > 0 && !fileClosed ) {
            try {
                output_file = new FileOutputStream(wsFile);  
                fileClosed = true;
            } catch (Exception e) {    
                timeout -= 1000;
                sleep(1000);
            }
        }
        
        if (fileClosed) {
            //log.info("File available ${timeout} ms before timeout");
            wb.write(output_file);
            output_file.close(); 
        } else {
            throw new Exception('Unable to write file.');
        }
        */
        
        
        
    }
    
    public static String readCellByName(String filePath, int sheetIdx, String cellName) {
        DataFormatter formatter = new DataFormatter(Locale.US);
    
        int rowIdx = splitAlphaNumeric(cellName)[1].toInteger() - 1;
        String colName = splitAlphaNumeric(cellName)[0];
        int colIdx = CellReference.convertColStringToIndex(colName);
    
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filePath))
        HSSFSheet sheet = workbook.getSheetAt(sheetIdx);
        String cellValue = formatter.formatCellValue(sheet.getRow(rowIdx).getCell(colIdx));
        return cellValue;
    }
    
    
    public static String getCellValue(String filePath, int sheetIdx, String cellName) {
        // not working yet
        File wsFile = new File(filePath);
        FileInputStream fIpStream= new FileInputStream(wsFile); 
        Workbook wb = new HSSFWorkbook(fIpStream); //or new XSSFWorkbook("/somepath/test.xls")
        Sheet sheet = wb.getSheetAt(sheetIdx);
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

        // suppose your formula is in B3
        CellReference cellReference = new CellReference(cellName); 
        Row row = sheet.getRow(cellReference.getRow());
        println(row);
        Cell cell = row.getCell(cellReference.getCol()); 
        def value = null;
        
        if (cell!=null) {
            switch (evaluator.evaluateFormulaCell(cell)) {
                case Cell.CELL_TYPE_BOOLEAN:
                    return cell.getBooleanCellValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    return cell.getNumericCellValue();
                    break;
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();
                    break;
                case Cell.CELL_TYPE_BLANK:
                    break;
                case Cell.CELL_TYPE_ERROR:
                    value = cell.getErrorCellValue();
                    break;

                // CELL_TYPE_FORMULA will never occur
                case Cell.CELL_TYPE_FORMULA: 
                    break;
            }    
        }
        return value;
    
    }
    
    
    

}