package com.rockportllc.soapui.utils;

import static com.jayway.jsonpath.JsonPath.parse;

class JsonUtils {

    static getJsonValue(def json, def path, def nullReplacement) {
        return (parse(json).read(path) ?: nullReplacement) as String;
    }

}