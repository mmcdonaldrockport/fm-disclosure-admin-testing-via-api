package com.rockportllc.soapui.utils;
//http://www.soderstrom.se/?p=32

// https://mvnrepository.com/artifact/com.itextpdf/itextpdf
@Grapes(
    @Grab(group='com.itextpdf', module='itextpdf', version='5.5.10')
)

import com.lowagie.text.pdf.PdfDate
import com.lowagie.text.pdf.PdfReader
 
// Read the document properties of a PDF document.
class PdfProps {
    // The file where this document is stored.
    File file
 
    // Hash map containing document properties
    Map properties
 
    // Construct: opens a document, reads its info, then closes the file.
    PdfProps(File file) {
        this.file = file
        def reader = new PdfReader(file.absolutePath)
        properties = scan(reader.info)
        reader.close()
    }
 
    static boolean isPdf(File file) {
        file.withReader {reader -> reader.readLine() ==~ /^%PDF-\d.*/}
    }
 
    // Map from our own property names to values
    private Map scan(Map info) {
        properties = [:]
        properties['fname'] = file.name
        properties['fpath'] = file.absolutePath
        properties['fsize'] = (file.size()>> 10) // kilobytes
        mapNonEmpty('app', info['Creator'])
        mapNonEmpty('title', info['Title'])
        mapNonEmpty('author', info['Author'])
        mapDate('created', info['CreationDate'])
        mapDate('mod', info['ModDate'])
        mapNonEmpty('subject', info['Subject'])
        mapNonEmpty('keywords', info['Keywords'])
        return properties
    }
 
    // Convert PDF date to java.util.Date
    private void mapDate(String key, pdfDate) {
        Calendar cal = PdfDate.decode(pdfDate)
        if (cal) properties[key] = new Date(cal.time.time)
    }
 
    private void mapNonEmpty(String key, String value) {
        if (value?.trim()) properties[key] = value
    }
}