package com.rockportllc.soapui.utils;

import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;
import ar.com.hjg.pngj.IImageLine;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.chunks.ChunkCopyBehaviour;
import ar.com.hjg.pngj.chunks.PngChunkTextVar;
import ar.com.hjg.pngj.ImageLineHelper;
import ar.com.hjg.pngj.chunks.PngChunkTextVar.PngTxtInfo;
import ar.com.hjg.pngj.chunks.PngChunk;
import java.util.UUID;

import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.sanselan.formats.jpeg.exifRewrite.ExifRewriter;
import org.apache.sanselan.formats.tiff.TiffImageMetadata;
import org.apache.sanselan.formats.tiff.constants.TiffConstants;
import org.apache.sanselan.formats.tiff.write.TiffOutputDirectory;
import org.apache.sanselan.formats.tiff.write.TiffOutputField;
import org.apache.sanselan.formats.tiff.write.TiffOutputSet;


class ImageUtils {

    //public static setPNGDescription(String pngFilePath, String description) {
    public static writePNGDescription(String pngFilePath, String description) {
        PngReader pngr = new PngReader(new File(pngFilePath));
        def tempFilePath = pngFilePath + UUID.randomUUID();
        int channels = pngr.imgInfo.channels;
        if (channels < 3 || pngr.imgInfo.bitDepth != 8)
            throw new RuntimeException("This method is for RGB8/RGBA8 images");
        
        PngWriter pngw = new PngWriter(new File(tempFilePath), pngr.imgInfo, true);
        pngw.copyChunksFrom(pngr.getChunksList(), ChunkCopyBehaviour.COPY_ALL_SAFE);
        pngw.getMetadata().setText(PngChunkTextVar.KEY_Description, description);
        pngw.getMetadata().setText(PngChunkTextVar.KEY_Author, "SoapUI");
        for (int row = 0; row < pngr.imgInfo.rows; row++) { // also: while(pngr.hasMoreRows()) 
            IImageLine l1 = pngr.readRow();
            /*
            int[] scanline = ((ImageLineInt) l1).getScanline(); // to save typing
            for (int j = 0; j < pngr.imgInfo.cols; j++) {
                scanline[j * channels] /= 2;
                scanline[j * channels + 1] = ImageLineHelper.clampTo_0_255(scanline[j * channels + 1] + 20);
            }
            */
            pngw.writeRow(l1);
        }
        pngr.end(); // it's recommended to end the reader first, in case there are trailing chunks to read
        pngw.end();
        
        // overwrite original file with temp file
        def input = new File(tempFilePath).newInputStream();
        def output = new File(pngFilePath).newOutputStream();

        output << input; 

        input.close();
        output.close();
        new File(tempFilePath).delete();

   }
   
  
   public static writePNGDescription(String pngFilePath, String newPngFilePath, String description) {
        PngReader pngr = new PngReader(new File(pngFilePath));
        //def tempFilePath = pngFilePath + UUID.randomUUID();
        int channels = pngr.imgInfo.channels;
        if (channels < 3 || pngr.imgInfo.bitDepth != 8)
            throw new RuntimeException("This method is for RGB8/RGBA8 images");
        
        PngWriter pngw = new PngWriter(new File(newPngFilePath), pngr.imgInfo, true);
        pngw.copyChunksFrom(pngr.getChunksList(), ChunkCopyBehaviour.COPY_ALL_SAFE);
        pngw.getMetadata().setText(PngChunkTextVar.KEY_Description, description);
        pngw.getMetadata().setText(PngChunkTextVar.KEY_Author, "SoapUI");
        for (int row = 0; row < pngr.imgInfo.rows; row++) { // also: while(pngr.hasMoreRows()) 
            IImageLine l1 = pngr.readRow();
            /*
            int[] scanline = ((ImageLineInt) l1).getScanline(); // to save typing
            for (int j = 0; j < pngr.imgInfo.cols; j++) {
                scanline[j * channels] /= 2;
                scanline[j * channels + 1] = ImageLineHelper.clampTo_0_255(scanline[j * channels + 1] + 20);
            }
            */
            pngw.writeRow(l1);
        }
        pngr.end(); // it's recommended to end the reader first, in case there are trailing chunks to read
        pngw.end();

   }
   

   public static readPNGDescription(String pngFilePath) {
        PngReader pngr = new PngReader(new File(pngFilePath));
    
        int channels = pngr.imgInfo.channels;
        if (channels < 3 || pngr.imgInfo.bitDepth != 8)
            throw new RuntimeException("This method is for RGB8/RGBA8 images");
        pngr.end(); 
        def desc = pngr.getMetadata().getTxtForKey(PngChunkTextVar.KEY_Description);
        return desc;
    }
    
   // http://stackoverflow.com/questions/36868013/editing-jpeg-exif-data-with-java 
   // https://commons.apache.org/proper/commons-imaging/javadocs/api-release/org/apache/sanselan/Sanselan.html
   public static writeJPGDescription(String jpgFilePath, String desc) {
       def tempJpgFilePath = jpgFilePath + UUID.randomUUID();
       writeJPGDescription(jpgFilePath, tempJpgFilePath, desc);
       def input = new File(tempJpgFilePath).newInputStream();
       def output = new File(jpgFilePath).newOutputStream();

       output << input; 

       input.close();
       output.close();
       new File(tempJpgFilePath).delete();
       return;
   }
   
   public static writeJPGDescription(String jpgFilePath, String newJpgFilePath, String desc) {
        BufferedOutputStream os = null;
        try
        {
            def jpgFile = new File(jpgFilePath);
            def newJpgFile = new File(newJpgFilePath);
            TiffOutputSet outputSet = null;
 
            // note that metadata might be null if no metadata is found.
            IImageMetadata metadata = Sanselan.getMetadata(jpgFile);
            JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
            if (jpegMetadata != null)
            {
                // note that exif might be null if no Exif metadata is found.
                TiffImageMetadata exif = jpegMetadata.getExif();
 
                if (exif != null) { outputSet = exif.getOutputSet(); }
            }
 
            // if file does not contain any exif metadata, we create an empty set of exif metadata. Otherwise, we keep all of the other existing tags.
            if (null == outputSet) { outputSet = new TiffOutputSet(); }

            def descFld = new TiffOutputField(TiffConstants.EXIF_TAG_USER_COMMENT, TiffConstants.FIELD_TYPE_ASCII, desc.size() , desc.getBytes());

            TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
            
            // make sure to remove old value if present (this method will
            // not fail if the tag does not exist).
            exifDirectory.removeField(TiffConstants.EXIF_TAG_USER_COMMENT);
            exifDirectory.add(descFld);

            os = new BufferedOutputStream(new FileOutputStream(newJpgFile));
            new ExifRewriter().updateExifMetadataLossless(jpgFile, os, outputSet);

            os.close();
            os = null;
        } finally {
            if (os != null) { try { os.close(); } catch (IOException e) { } }
        }
   }    
    
    
    public static readJPGDescription(String jpgFilePath) {
        def desc = ''
        TiffOutputSet outputSet = null;

         // note that metadata might be null if no metadata is found.
        IImageMetadata metadata = Sanselan.getMetadata(new File(jpgFilePath));
        JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
        if (jpegMetadata != null) {
            // note that exif might be null if no Exif metadata is found.
            TiffImageMetadata exif = jpegMetadata.getExif();

            if (exif != null) { outputSet = exif.getOutputSet(); }
        }

        // if file does not contain any exif metadata, return '' 
        if (outputSet == null) { return desc; }

        TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
        
        def tag = exifDirectory.findField(TiffConstants.EXIF_TAG_USER_COMMENT); 

        if (tag != null) {
            def field = jpegMetadata.findEXIFValue(TiffConstants.EXIF_TAG_USER_COMMENT);
            desc = field.getValueDescription();
        }
        return desc[1..-2];
    }
}