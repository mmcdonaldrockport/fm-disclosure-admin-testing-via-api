package com.rockportllc.soapui.utils;

import com.rockportllc.soapui.utils.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.DateTime;
import org.apache.commons.lang3.RandomUtils as ApacheRandomUtils

class RandomUtils {

    public static firstNameList = [
        "Nell", "U", "Efrain", "Adina", "Edmund", "Adelia", "Dot", "Veta", "Barabara-Jean", "Dayle", "Malvina", "Andra", "Alonzo", 
        "Meggan", "Clara", "Brook", "Karren", "Jasmin", "Millicent", "Summer", "Richard", "Regena", "Hugh", "Jalisa", 
        "Jeannette", "Vasiliki", "Wilson", "Lucio", "Avery", "Donnette", "Ahmed", "Debera", "Alejandrina", "Isidra", 
        "Maynard", "Silas", "Vincent", "Mitzie", "Dwight", "Sima", "Aurore", "Kaila", "Renea", "Jay", "Sylvie", 
        "Omer", "Meri", "Dionne", "Norma", "Chanell", "Lashay", "Bruce", "Albert", "Isaac", "Stephen"];

    public static lastNameList = [
        "Wayne", "Eaton", "Gentry", "Hensley", "Elliott", "O'Shaugnessy", "Watts", "Carey", "Lin", "Garza", "Shannon", "Michael", "Moses", 
        "Henderson", "Fisher", "Dixon", "Hinton", "Rich", "Savage", "Pittman", "Conway", "Kirk", "Boyer", "Ortega", "Hancock", 
        "Franco", "Stevenson", "Rosales", "Flowers", "Saunders", "Mcfarland", "Orozco", "Hatfield", "Stone", "Mack", "Leblanc", 
        "Blake", "Wolfeschlegelsteinhausenbergerdorff", "Harper", "Jenkins", "Allison", "Mc Gamble", "Mcmahon", "Hopkins", "Sullivan", 
        "Fischer", "Cherry", "Sandoval", "Vincent", "Holt", "Newman-Ailes", "Newton", "Einstein", "Hawking"];

    public static companyList = [
        "CHOAM", "Acme Corp", "Sirius Cybernetics Corp", "MomCorp", "Rich Industries", "Soylent Corporation", "Very Big Corp of America", 
        "Frobozz Magic Co", "Warbucks Industries", "Tyrell Corp", "Wayne Enterprises", "Virtucon", "Globex", "Umbrella Corp", 
        "Wonka Industries", "Stark Industries", "Clampett Oil", "Oceanic Airlines", "Yoyodyne Propulsion Systems", 
        "Cyberdyne Systems Corp", "dAnconia Copper", "Gringotts", "Oscorp", "Nakatomi Trading Corp", "Spacely Space Sprockets", 
        "Stay Puft Corporation", "Dunder Miflin", "Pendant Publishing", "Initech", "Initrode", "Lomax Industries", "Hudsucker Industries",
        "Los Pollos Hermanos", "Mugatu Industries", "Soylent Corporation", "Wayne Enterprises", "Career Transitions Corporation",
        "Buy-N-Large", "Michael Scott Paper Company", "Ewing Oil", "Vandelay Industries", "Sterling Cooper Draper Pryce",
        "Dewey Cheatham and Howe"];    
        
    public static char[] ALPHANUMERIC_CHARS = 'BCDFGHJKLMNPQRSTVWXYZ0123456789'.toCharArray();

    public static getRandomFirstName() {
        return firstNameList[ApacheRandomUtils.nextInt(0,firstNameList.size-1)];
    }
    
    public static getRandomLastName() {
        return lastNameList[ApacheRandomUtils.nextInt(0,lastNameList.size-1)];
    }
    
    public static getRandomCompanyName() {
        return companyList[ApacheRandomUtils.nextInt(0,companyList.size-1)];
    }
    
    public static getRandomEmailAddress() {}
    
    public static getRandomPhoneNumber() {
        return (ApacheRandomUtils.nextInt(100,999) as String) + '-' + 
            (ApacheRandomUtils.nextInt(100,999) as String) + '-' + 
            (ApacheRandomUtils.nextInt(0,9999) as String).padLeft(4,'0');
    }
    
    public static getRandomInt(int from, int to) {
        ApacheRandomUtils.nextInt(from,to)
    }
    
    public static String getRandomAlphaNumericStringWithDateTime(int length) {
        def dt = DateUtils.getCurrentDateTimeDateTime("UTC");
        
        def dtStr = DateUtils.getFormattedDateTimeString(dt, "YYYYMMddHHmmssSSS");

        def randStr = RandomStringUtils.random(length - dtStr.size(), 0, ALPHANUMERIC_CHARS.length-1, false, false, ALPHANUMERIC_CHARS) + dtStr;
        
        return randStr;
    }
    
    
    public static String getRandomNumericStringWithDateTime(int length) {
        def dt = DateUtils.getCurrentDateTime("UTC");
        
        def dtStr = DateUtils.getFormattedDateTimeString(dt, "YYYYMMddHHmmssSSS");

        def randStr = RandomStringUtils.randomNumeric(length - dtStr.size()) + dtStr;
        
        return randStr;
    }
    
    public static String getUniqueFMPrefix() {
        String initialDigit = (ApacheRandomUtils.nextInt(1,9)) as String;
        return getUniqueFMPrefix(initialDigit);
    }
    
    public static String getUniqueFMPrefix(def initialDigit) {
        String initDig = Integer.parseInt(initialDigit as String);
        DateTime dt = new DateTime();
        DateTimeFormatter yrDaySecsFmt = DateTimeFormat.forPattern("yyDDD");
        def yrDaySecs = yrDaySecsFmt.print(dt) + (dt.secondOfDay + '').padLeft(5,'0');
        def uniquePrefix = initDig + yrDaySecs[5..6] + yrDaySecs[1] + yrDaySecs[2..4] + yrDaySecs[7..9];
        return uniquePrefix;
    }
    
    
}