package com.rockportllc.soapui.utils;

import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.openxml4j.util.Nullable;
class DOCXUtils {

    public static writeDOCXDescription(String docxFilePath, String newDocxFilePath, String descContent) {
        def opc = OPCPackage.open(docxFilePath);
		pp.setDescriptionProperty("Descriptiom");
        opc.close();
	}
    
    public static String getDOCXDescription(String docxFilePath) {
		def opc = OPCPackage.open(docxFilePath);
        Nullable<String> desc = pp.getDescriptionProperty() ?: "";
        opc.close();
		return desc;
    }
      
} 
