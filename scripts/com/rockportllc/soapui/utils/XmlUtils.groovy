package com.rockportllc.soapui.utils;

import javax.xml.xpath.*;
import javax.xml.parsers.DocumentBuilderFactory;

class XmlUtils {

	// execute xpath string against an XML string and return result
	public static evaluateXpath( String xml, String xpathQuery ) {
		def xpath = XPathFactory.newInstance().newXPath()
		def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
		def inputStream = new ByteArrayInputStream( xml.bytes )
		def records     = builder.parse(inputStream).documentElement
		return xpath.evaluate( xpathQuery, records )
	}
	
	
}