package com.rockportllc.soapui.utils;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class GUIUtils {

    static int MULTIPLE = 0;
    static int SINGLE = 1;

    static listDialog(def title, def itemList, def selectionMode = GUIUtils.SINGLE) {
        return new GUIUtils.ListDialog(null, title, itemList, selectionMode);
    }

    public class ListDialog extends JDialog {
        def selectedItemList = [];
        def selectedIndicesList = [];
        def itemList = [];
        JList jList = null;
    
        public ListDialog(def title, def itemList, def selectedItem, def selectionMode) {
            //super();
            this.itemList = itemList;
            setBounds(100, 100, 450, 300);
            setLocationRelativeTo(null); // center on screen
            setModal(true); 
            getContentPane().setLayout( new BorderLayout());
            setTitle(title);
            
            jList = new JList();

            jList.setModel(new AbstractListModel() {
                def values = itemList;
                
                public int getSize() {
                    return values.size;
                }
                public Object getElementAt(int index) {
                    return values[index];
                }
            });
        
            jList.setLayoutOrientation(JList.VERTICAL);
            if (selectionMode == GUIUtils.SINGLE) {
                jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            }
            if (itemList.contains(selectedItem)) {
                jList.setSelected(selectedItem);
            }
            JButton okBtn = new JButton( "OK" );
            okBtn.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e ) {
                    selectedItemList = jList.getSelectedValuesList();
                    selectedIndicesList = jList.getSelectedIndices();
                    dispose();
                }
            });
    
            JButton cancelBtn = new JButton( "Cancel" );
            cancelBtn.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e ) {
                   selectedItemList = [];
                   dispose();
                }
            });
            
            JPanel contentPanel = new JPanel();
            contentPanel.setLayout( new BorderLayout() );
            contentPanel.add( new JScrollPane( jList ) );
    
            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout( new FlowLayout(FlowLayout.CENTER) );
            buttonPanel.add( okBtn );
            buttonPanel.add( cancelBtn );
    
            getContentPane().add( contentPanel, BorderLayout.CENTER ); 
            getContentPane().add( buttonPanel, BorderLayout.SOUTH);
            //setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
             
            //pack();
            //setVisible( true );
            //return selectedList;
        }
        
        ArrayList getSelectedItems() {
            //setVisible( true );
            setVisible( true );
            dispose();
            return selectedItemList;
        }
        
        ArrayList getSelectedIndices() {
            //setVisible( true );
            setVisible( true );
            dispose();
            return selectedIndicesList;
        }
        
    }

}