package com.rockportllc.soapui.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.Seconds;

class DateUtils {

	public static final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS";


	public static DateTime getCurrentDateTime(String zoneId) {
		DateTimeZone tz = DateTimeZone.forID(zoneId);
		return new DateTime(tz);
	};


	public static String getFormattedDateTimeString(DateTime dt, String pattern) {
		//DateTime dt = new DateTime(zone);
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern(pattern);
		
		return dtfOut.print(dt);
	};
	
	public static String getCurrentFormattedDateTimeString(String pattern) {
		DateTime dt = new DateTime();
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern(pattern);
		
		return dtfOut.print(dt);
	};
	
	public static boolean timeoutExceeded(def startTimeStr, def checkTimeStr, def timeout, def dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS") {
	
		DateTimeFormatter formatter = DateTimeFormat.forPattern(dateTimeFormat);

		def startTime = formatter.parseDateTime(startTimeStr);
		def checkTime = formatter.parseDateTime(checkTimeStr);

		def secsBtw = Seconds.secondsBetween(startTime, checkTime);
		return secsBtw.getValue() > (timeout as Integer)/1000;
	}
	
}