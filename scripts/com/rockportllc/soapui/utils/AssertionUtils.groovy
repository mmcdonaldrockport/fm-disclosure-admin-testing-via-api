package com.rockportllc.soapui.utils;

//import static com.jayway.jsonpath.JsonPath.parse;

class AssertionUtils {
	/*
	com.rockportllc.soapui.utils.AssertionUtils.compareStringsAsDoubles(storedNetRateStr, messageNetRateStr, '=', ['':'0.0']);
	*/

	static compareStringsAsIntegers(def strArg1, def strArg2, def operator, def equivalents = [:]) {

        def str1 = strArg1 ?: '';
        def str2 = strArg2 ?: '';
        
        equivalents.each { k, v ->
            if (str1 == k) { str1 = v }
            if (str2 == k) { str2 = v }
        }
        
        
        BigInteger bi1 = new BigInteger(str1);
        BigInteger bi2 = new BigInteger(str2);
        switch (operator) {
            case '<':
                return bi1 < bi2;
                break;
            case '<=':
                return bi1 <= bi2;
                break;            
            case '=' :
            case '==' :
                return bi1 == bi2;
                break;
            case '>=':
                return bi1 >= bi2;
                break;
            case '>':
                return bi1 > bi2;
                break;            
            default:
                return false;
        }
        
   }
	
	static compareStringsAsDecimals(def strArg1, def strArg2, def operator, def equivalents = [:]) {

        def str1 = strArg1 ?: '';
        def str2 = strArg2 ?: '';
        
        equivalents.each { k, v ->
            if (str1 == k) { str1 = v }
            if (str2 == k) { str2 = v }
        }
        
        
        BigDecimal bd1 = new BigDecimal(str1);
        BigDecimal bd2 = new BigDecimal(str2);
        switch (operator) {
            case '<':
                return bd1 < bd2;
                break;
            case '<=':
                return bd1 <= bd2;
                break;            
            case '=' :
            case '==' :
                return bd1 == bd2;
                break;
            case '>=':
                return bd1 >= bd2;
                break;
            case '>':
                return bd1 > bd2;
                break;            
            default:
                return false;
        }
        
   }
	
	
	static compareStringsAsDoubles(def strArg1, def strArg2, def operator, def equivalents = [:]) {
        def str1 = strArg1 ?: '';
        def str2 = strArg2 ?: '';
        
        equivalents.each { k, v ->
            if (str1 == k) { str1 = v }
            if (str2 == k) { str2 = v }
        }
        
        
        def dbl1 = Double.parseDouble(str1)
        def dbl2 = Double.parseDouble(str2)
        switch (operator) {
            case '<':
                return dbl1 < dbl2;
                break;
            case '<=':
                return dbl1 <= dbl2;
                break;            
            case '=' :
            case '==' :
                return dbl1 == dbl2;
                break;
            case '>=':
                return dbl1 >= dbl2;
                break;
            case '>':
                return dbl1 > dbl2;
                break;            
            default:
                return false;
        }
        
    }
	
	static compareStrings(def strArg1, def strArg2, def operator, def equivalents = [:]) {
		def str1 = strArg1 ?: '';
        def str2 = strArg2 ?: '';
        
        equivalents.each { k, v ->
            if (str1 == k) { str1 = v }
            if (str2 == k) { str2 = v }
        }

        switch (operator) {
            case '<':
                return str1 < str2;
                break;
            case '<=':
                return str1 <= str2;
                break;            
            case '=' :
            case '==' :
                return str1 == str2;
                break;
            case '>=':
                return str1 >= str2;
                break;
            case '>':
                return str1 > str2;
                break;            
            default:
                return false;
        }
        
	
	}

}