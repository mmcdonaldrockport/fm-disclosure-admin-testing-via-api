package com.rockportllc.soapui.utils;

import com.eviware.soapui.model.testsuite.TestProperty;
import groovy.sql.Sql;
import java.sql.DatabaseMetaData;
import org.hsqldb.jdbc.JDBCDriver;

public class DBUtils {

    /**
    * Add a table to a database referenced by the dtConnName of the SoapUI Connection object
    *
    */
    
    static String createDataTable(def tableName, def columnDefStr, def dtConnName, def testRunner, def context, def log) {
        def clause = '';
        def columnDefs = Eval.me(columnDefStr);
        columnDefs.each { name, prototype ->
            Object val = null;
            if (prototype.contains('.')) { 
                try {
                    val = new java.math.BigDecimal(prototype);  
                    String[] parts = prototype.value.split('\\.');
                    //log.info parts.class;
                    if (parts.length < 2) { parts += ''; }
                    def precision = parts[0].length() + parts[1].length();
                    def scale = parts[1].length();
                    clause += name + ' DECIMAL (' + precision + ',' + scale + '),\n';
                }    
                catch (Exception e) {
                    // not a valid decimal
                }
            }
            if (val == null) {
                // try Integer
                try { 
                    val = new java.lang.Integer(prototype);
                    clause += name  + ' INTEGER,\n'
                }
                catch (Exception e) {
                    // not a valid integer
                }
            }
            if (val == null) {
                // try Boolean
                try { 
                    val = prototype == 'true' || prototype == 'false' ? new java.lang.Boolean(prototype) : null; 
                    if (val != null ) { clause += name  + ' BOOLEAN,\n'; }
                }
                catch (Exception e) {
                    //not a valid boolean
                }
            }
             if (val == null) {
                // let's use a String then!
                val = prototype;
                clause += name  + ' CLOB,\n'
            }
            //log.info val.class
        }
        //log.info clause
            
        def conn = testRunner.testCase.testSuite.project.databaseConnectionContainer.getDatabaseConnectionByName(dtConnName);
        def connString = context.expand(conn.getConnectionString()); 
        def driver = conn.getDriver();
        def sql = Sql.newInstance(connString, driver);

        sql.execute("DROP TABLE IF EXISTS " + tableName);
        
        String sqlText = new String("""
             CREATE TABLE IF NOT EXISTS ${tableName} ( 
                 id IDENTITY PRIMARY KEY,
                 ${clause.reverse().drop(2).reverse()}
             );""");
             
        sql.execute(sqlText);
        //sql.execute("INSERT INTO " + tableName + " (name ) VALUES ('hello')");
        //sql.execute("INSERT INTO " + tableName + " (name) VALUES ('goodbye')");

        return sqlText;
    }            

    static String createDataTable(def dtPropStep, def dtConnName, def testRunner, def context, def log) {
        def dtProps = testRunner.testCase.getTestStepByName(dtPropStep);

        def tableName = context.expand(dtProps.getPropertyAt(0).value);
        def clause = '';
        for (int c = 1; c < dtProps.getPropertyCount(); c++) {
            def prop = dtProps.getPropertyAt(c) ;
            
            Object val = null;
            if (prop.value.contains('.')) { 
                //try bigdecimal
                try {
                    val = new java.math.BigDecimal(prop.value);  
                    String[] parts = prop.value.split('\\.');
                    //log.info parts.class;
                    if (parts.length < 2) { parts += ''; }
                    def precision = parts[0].length() + parts[1].length();
                    def scale = parts[1].length();
                    clause += prop.name + ' DECIMAL (' + precision + ',' + scale + '),\n';
                }    
                catch (Exception e) {
                    // not a valid decimal
                }
            }
            if (val == null) {
                // try Integer
                try { 
                    val = new java.lang.Integer(prop.value);
                    clause += prop.name  + ' INTEGER,\n'
                }
                catch (Exception e) {
                    // not a valid integer
                }
            }
            if (val == null) {
                // try Boolean
                try { 
                    val = prop.value == 'true' || prop.value == 'false' ? new java.lang.Boolean(prop.value) : null; 
                    if (val != null ) { clause += prop.name  + ' BOOLEAN,\n'; }
                }
                catch (Exception e) {
                    //not a valid boolean
                }
            }
            if (val == null) {
                // let's use a String then!
                val = prop.value;
                clause += prop.name  + ' CLOB,\n'
            }
            //log.info val.class
        }
        //log.info clause
            
        def conn = testRunner.testCase.testSuite.project.databaseConnectionContainer.getDatabaseConnectionByName(dtConnName);
        def connString = context.expand(conn.getConnectionString()); 
        def driver = conn.getDriver();
        def sql = Sql.newInstance(connString, driver);

        sql.execute("DROP TABLE IF EXISTS " + tableName);
        
        String sqlText = new String("""
             CREATE TABLE IF NOT EXISTS ${tableName} ( 
                 id IDENTITY PRIMARY KEY,
                 ${clause.reverse().drop(2).reverse()}
             );""");
             
        sql.execute(sqlText);
        //sql.execute("INSERT INTO " + tableName + " (name ) VALUES ('hello')");
        //sql.execute("INSERT INTO " + tableName + " (name) VALUES ('goodbye')");

        return sqlText;
            
    }

	static String dropDataTable(def tableName, def dtConnName, def testRunner, def context, def log) {
		def conn = testRunner.testCase.testSuite.project.databaseConnectionContainer.getDatabaseConnectionByName(dtConnName);
        def connString = context.expand(conn.getConnectionString()); 
        def driver = conn.getDriver();
        def sql = Sql.newInstance(connString, driver);

        sql.execute("DROP TABLE IF EXISTS " + tableName);
	}
	
	static String clearDataTable(def tableName, def dtConnName, def testRunner, def context, def log) {
		def conn = testRunner.testCase.testSuite.project.databaseConnectionContainer.getDatabaseConnectionByName(dtConnName);
        def connString = context.expand(conn.getConnectionString()); 
        def driver = conn.getDriver();
        def sql = Sql.newInstance(connString, driver);

        sql.execute("DELETE FROM TABLE IF EXISTS " + tableName);
	}

}
