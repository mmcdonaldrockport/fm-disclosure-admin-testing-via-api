@GrabConfig(systemClassLoader=true) 
@Grab('org.hsqldb:hsqldb:2.2.9') 
import groovy.sql.Sql
import java.sql.DatabaseMetaData;

def params = [:];
params.put('id',['integer', 0, 0]);
params.put('str_col',['varchar', 100, 0]);
params.put('dec_col',['decimal', 14, 7]);
// name, type, length, precision
// mystring, varchar, 200, n/a
// myint, integer, n/a, n/a, n/a
// mydecimal, decimal, 14, 10


def sql = Sql.newInstance("jdbc:hsqldb:mem:database", "sa", "", "org.hsqldb.jdbcDriver")

sql.execute("DROP TABLE IF EXISTS sample_table");
sql.execute("CREATE TABLE sample_table ()");
params.each { key, value ->
    //println(key);
    def clause = '';
    switch (value[0].toUpperCase()) {
        case 'INTEGER': clause = "$key INTEGER"; break;
        case 'VARCHAR': clause = "$key VARCHAR(${value[1]})"; break;
        case 'DECIMAL': clause = "$key DECIMAL(${value[1]}, ${value[2]})"; break;
    }
    //println(clause);
    sql.execute("ALTER TABLE sample_table ADD COLUMN "+ clause); 
}
sql.execute("INSERT INTO sample_table (id, str_col, dec_col) VALUES (1,'hello',2.5)");
sql.execute("INSERT INTO sample_table (id, str_col, dec_col) VALUES (2,'goodbye',4.75)");
sql.eachRow('SELECT * FROM SAMPLE_TABLE') { row ->
    println row
}
sql.execute("DROP TABLE IF EXISTS sample_table");