import javax.xml.xpath.*
import javax.xml.parsers.DocumentBuilderFactory

def f = new File('C:/local/megas/AtIssuanceMEGADisclosure_31418M2U5.xml');
def xml = f.text;

def cusips = processXml(xml, '//*[local-name()="SecurityRelatedCUSIPIdentifier"]/text()');




def processXml( String xml, String xpathQuery ) {
  def xpath = XPathFactory.newInstance().newXPath()
  def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
  def inputStream = new ByteArrayInputStream( xml.bytes )
  def records     = builder.parse(inputStream).documentElement
  def nodes       = xpath.evaluate( xpathQuery, records, XPathConstants.NODESET )
  nodes.collect { node -> node.textContent }
}

