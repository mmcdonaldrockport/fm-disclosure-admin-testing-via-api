package com.rockportllc.soapui.utils;

import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

class PDFUtils {

    public static writePDFSubject(String pdfFilePath, String newPdfFilePath, String subjectContent) {
        PDDocument document = null;
        try {
            document = PDDocument.load(new File(pdfFilePath));
            PDDocumentInformation info = document.getDocumentInformation();
            info.setSubject(subjectContent);
            document.setDocumentInformation(info);
            //println( "Subject=" + info.getSubject() );
            document.save(newPdfFilePath);
        } finally {
            if (document != null) { document.close(); }
        }
    }
    
    public static String getPDFSubject(String pdfFilePath) {
        PDDocument document = null;
        String subjectContent = '';
        try {
            document = PDDocument.load(new File(pdfFilePath));
            PDDocumentInformation info = document.getDocumentInformation();
            subjectContent = info.getSubject();
        } finally {
            if (document != null) { document.close(); }
        }
        return subjectContent;
    }
      
} 

/*def doc = new PDDocument();
   doc.load(new File('C:/Users/mmcdonald.ROCKPORT/Downloads/pdf-sample.pdf'));
    PDDocumentInformation info = doc.getDocumentInformation();
    info.setTitle("This is the title");
    doc.setDocumentInformation(info);
    doc.save('C:/Users/mmcdonald.ROCKPORT/Downloads/pdf-sample-2.pdf');
    doc.close();
  */ //println( "Subject=" + info.getTitle() );
  /*  PDDocumentInformation info = document.getDocumentInformation();
System.out.println( "Page Count=" + document.getNumberOfPages() );
System.out.println( "Title=" + info.getTitle() );
System.out.println( "Author=" + info.getAuthor() );
System.out.println( "Subject=" + info.getSubject() );
System.out.println( "Keywords=" + info.getKeywords() );
System.out.println( "Creator=" + info.getCreator() );
System.out.println( "Producer=" + info.getProducer() );
System.out.println( "Creation Date=" + info.getCreationDate() );
System.out.println( "Modification Date=" + info.getModificationDate());
System.out.println( "Trapped=" + info.getTrapped() ); 
*/