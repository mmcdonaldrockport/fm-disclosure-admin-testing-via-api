import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

PDDocument document = new PDDocument();
PDDocumentInformation info = document.getDocumentInformation();
info.setTitle("This is the title");
document.setDocumentInformation(info);

PDPage page = new PDPage();
document.addPage( page );


// Create a new font object selecting one of the PDF base fonts
PDFont font = PDType1Font.HELVETICA_BOLD;

// Start a new content stream which will "hold" the to be created content
PDPageContentStream contentStream = new PDPageContentStream(document, page);

// Define a text content stream using the selected font, moving the cursor and drawing the text "Hello World"
contentStream.beginText();
contentStream.setFont( font, 12 );
contentStream.moveTextPositionByAmount( 100, 700 );
contentStream.drawString( "Hello World" );
contentStream.endText();

// Make sure that the content stream is closed:
contentStream.close();


// Save the results and ensure that the document is properly closed:
document.save( "C:/Users/mmcdonald.ROCKPORT/Downloads/HelloWorld.pdf");
document.close();


/*def doc = new PDDocument();
   doc.load(new File('C:/Users/mmcdonald.ROCKPORT/Downloads/pdf-sample.pdf'));
    PDDocumentInformation info = doc.getDocumentInformation();
    info.setTitle("This is the title");
    doc.setDocumentInformation(info);
    doc.save('C:/Users/mmcdonald.ROCKPORT/Downloads/pdf-sample-2.pdf');
    doc.close();
  */ //println( "Subject=" + info.getTitle() );