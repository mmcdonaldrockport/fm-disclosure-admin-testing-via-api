(: 
Extract required fields from IMP with Saxon

C:\local\saxon>java -cp "C:\local\saxon\saxon9he.jar" net.sf.saxon.Query 
	-s:01-PreCloseMBSAdminSetup.imp 
	-q:ExtractRequiredFields.xq  
	-o:reqFields.xml 
	-qversion:3.1
	
:)
xquery version "3.1"; 
declare option saxon:output "indent=yes";
declare option saxon:output "method=xml";

<jobs> {
for $doc in collection("file:///C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/IMP/?select=*.imp")
return 
	<job name="{$doc/job/@name/string()}">
	<fields>
	{ for $f at $c in $doc//field[@required='true']
		return (<field><number>{$c}</number><name>{$f/@name/string()}</name><type>{$f/@dataType/string()}</type></field>,'&#xa;')
	}
	</fields>
	</job>
} </jobs>
