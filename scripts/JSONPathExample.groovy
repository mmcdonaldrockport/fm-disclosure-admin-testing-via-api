// https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path
@Grapes(
    @Grab(group='com.jayway.jsonpath', module='json-path', version='2.2.0')
)

import static com.jayway.jsonpath.JsonPath.parse

def json = '''{
   "Id": 143502,
   "TransactionId": "22763159078001",
   "Prefix": "HA",
   "TransactionType": 1,
   "CUSIP": "2276315907C001",
   "Product": "DUS",
   "ExecutionType": "MBS",
   "CutOffDateBalance": 4789000.0,
   "IssueDate": "2015-04-01T00:00:00",
   "MaturityDate": "2022-04-01T00:00:00",
   "WeightedAVGCoupon": 2.608,
   "WAPassThroughRate": 2.75,
   "WAOriginalLoanTerm": 360,
   "AmortizationWAM": 360,
   "WeightedAVGLTV": 80.0,
   "InterestType": "Adjustable",
   "PublicationType": null,
   "SettlementDate": "2015-04-29T00:00:00",
   "LastUpdateDate": null,
   "LastUpdateUser": null,
   "HoldReason": null,
   "OpsStatus": null,
   "STStatus": null,
   "ADStatus": null,
   "DocStatus": null,
   "Status": true,
   "Eligible": 0,
   "IRPSuppressed": null,
   "AdditionalDisclosureRequired": 0,
   "PropertyName": "Greater Smials, Park View Estates MHP",
   "SecurityStatusType": "Active",
   "DealId": 143501,
   "Loans": [   {
      "Id": 143508,
      "LoanId": 22763159072001,
      "PropertyCount": 2
   }],
   "Footnotes": [],
   "IssuanceDocuments":    [
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 1,
         "DisplayOrder": 1,
         "TypeName": "Annex A",
         "Format": "EXCEL",
         "Status": 1
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 2,
         "DisplayOrder": 2,
         "TypeName": "Annex A - Corrected",
         "Format": "EXCEL",
         "Status": 2
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 3,
         "DisplayOrder": 3,
         "TypeName": "Annex A - Final",
         "Format": "EXCEL",
         "Status": 3
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 11,
         "DisplayOrder": 4,
         "TypeName": "Issue Supplement",
         "Format": "PDF",
         "Status": 1
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 12,
         "DisplayOrder": 5,
         "TypeName": "Trust Agreement",
         "Format": "PDF",
         "Status": 1
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 8,
         "DisplayOrder": 6,
         "TypeName": "Unitary Prospectus",
         "Format": "PDF",
         "Status": 1
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 9,
         "DisplayOrder": 7,
         "TypeName": "Unitary Prospectus - Corrected",
         "Format": "PDF",
         "Status": 2
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 10,
         "DisplayOrder": 8,
         "TypeName": "Unitary Prospectus - Final",
         "Format": "PDF",
         "Status": 3
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 4,
         "DisplayOrder": 9,
         "TypeName": "Unitary Prospectus Cover Page",
         "Format": "PDF",
         "Status": 1
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 5,
         "DisplayOrder": 10,
         "TypeName": "Unitary Prospectus Cover Page - Corrected",
         "Format": "PDF",
         "Status": 2
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 6,
         "DisplayOrder": 11,
         "TypeName": "Unitary Prospectus Cover Page - Final",
         "Format": "PDF",
         "Status": 3
      },
            {
         "Id": null,
         "RecordId": null,
         "CreatedOn": null,
         "IssuanceDocumentType": 7,
         "DisplayOrder": 12,
         "TypeName": "Unitary Prospectus Template",
         "Format": "PDF",
         "Status": 1
      }
   ],
   "IRPDocuments": [   {
      "Id": 148,
      "TransactionId": 143502,
      "DateReceived": "2016-11-08T00:00:00",
      "Date": null
   }],
   "Comments": [],
   "AdditionalDisclosures": [   {
      "TransactionId": 143502,
      "Message": null
   }],
   "showAdditional": true,
   "Corrections": [],
   "Banners": [],
   "Waiver": "waiver"
}'''

/*
log.info parse(json).read('$.quote.id')
log.info parse(json).read('$.quote.am ount')
log.info parse(json).read('$.quote.links[0].href')

def quoteId = parse(json).read('$.quote.id')
log.info quoteId
assert quoteId==12345

assert parse(json).read('$.quote.id')==12345
*/

println( parse(json).read('$..Loans.*.Id'));