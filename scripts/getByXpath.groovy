import javax.xml.xpath.*;
import org.xml.sax.InputSource;
import javax.xml.namespace.NamespaceContext;

NamespaceContext ctx = new NamespaceContext() {
    public String getNamespaceURI(String prefix) {
        return prefix.equals("mfcdf") ? "http://www.fanniemae.com/multifamily/integrated/cdf/v1.x" : null; 
    }
    public Iterator getPrefixes(String val) {
        return null;
    }
    public String getPrefix(String uri) {
        return null;
    }
};


def xpathOuter = XPathFactory.newInstance().newXPath()
def xpathInner = XPathFactory.newInstance().newXPath()
xpathOuter.setNamespaceContext(ctx);
xpathInner.setNamespaceContext(ctx);

def records = new InputSource(new FileInputStream(new File("C:/Users/mmcdonald.ROCKPORT/projects/FM_DataTransfer/datafiles/14-collateralchange/CollateralChange_469451_9999061909-001_REL_PROP.xml")));

def dealNodes = xpathOuter.evaluate( '//mfcdf:Agreement//mfcdf:Deal', records, XPathConstants.NODESET )
//mfcdf:Loans/mfcdf:Loan/mfcdf:LoanIdentifier', records, XPathConstants.NODESET )
def output = ""
dealNodes.each { outer ->
//  def make = xpath.evaluate( '//mfcdf:LoanIdentifier/text()', it )
  output += xpathOuter.evaluate( 'DealIdentifier/text()', outer ) + '|';
  def loanNodes = xpathInner.evaluate('//mfcdf:Agreement//mfcdf:Deal//mfcdf:Loans//mfcdf:Loan', records, XPathConstants.NODESET);
  loanNodes.each { inner ->
      output += xpathInner.evaluate( 'LoanIdentifier/text()', inner ) + '|'
  }
}
println output;