package com.javacodegeeks.snippets.desktop;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

public class GetSelectedJRadioButtonFromButtonGroup extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;

    private JRadioButton restartRB;
    private JRadioButton newRB;
//    private JRadioButton net;
    private JButton button;
    private ButtonGroup restartOrNewGroup;

    public GetSelectedJRadioButtonFromButtonGroup() {

        // set flow layout for the frame
        this.getContentPane().setLayout(new FlowLayout());

        

        restartRB = new JRadioButton("Restart");
        restartRB.setActionCommand("Restart");

        newRB = new JRadioButton("New");
        newRB.setActionCommand("New");

        //net = new JRadioButton(".NET");
       // net.setActionCommand("net");

        restartRB.setSelected(true);

        button = new JButton("Check");

        button.addActionListener(this);

        restartOrNewGroup = new ButtonGroup();

        //add radio buttons
        restartOrNewGroup.add(restartRB);
        restartOrNewGroup.add(newRB);
        //restartOrNewGroup.add(net);

        add(restartRB);
        add(newRB);
        //add(net);
        add(button);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Check")) {
            System.out.println("Selected Radio Button: " + restartOrNewGroup.getSelection().getActionCommand());
        }
    }

    private static void createAndShowGUI() {

  //Create and set up the window.

  JFrame frame = new GetSelectedJRadioButtonFromButtonGroup();

  //Display the window.

  frame.pack();

  frame.setVisible(true);

  frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    public static void main(String[] args) {

  //Schedule a job for the event-dispatching thread:

  //creating and showing this application's GUI.

  javax.swing.SwingUtilities.invokeLater(new Runnable() {

public void run() {

    createAndShowGUI(); 

}

  });
    }

}