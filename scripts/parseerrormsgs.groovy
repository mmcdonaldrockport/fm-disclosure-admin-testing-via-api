@Grab('org.apache.commons:commons-csv:1.2')
import org.apache.commons.csv.CSVParser
import static org.apache.commons.csv.CSVFormat.*

import java.nio.file.Paths

Paths.get('C:/Users/mmcdonald.ROCKPORT/Desktop/errormsgs.csv').withReader { reader ->
    CSVParser csv = new CSVParser(reader, DEFAULT.withHeader())
    println "INSERT INTO public.Validation (EVENTID, EVENTERRORID, ERRORMSG) VALUES ";

    for (record in csv.iterator()) {
        print "(";
        def ids = record.get(1).split('\\.');
        def errMsg = record.get(2)
        println ids[0] + "," + ids[1] + ",'" + errMsg + "'),"
    }
}