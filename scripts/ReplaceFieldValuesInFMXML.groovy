/*
* Replace identifiers in FM message so it can be reused
* Only works with PreClose, AtIssuance without PreClose, not for REMIC.
*/

@Grapes(
    @Grab(group='joda-time', module='joda-time', version='2.9.4')
)
import groovy.util.XmlSlurper;
import groovy.xml.XmlUtil;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.DateTime;
import java.util.Random;
import javax.swing.filechooser.FileFilter
import javax.swing.JFileChooser
import groovy.swing.SwingBuilder
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ReplaceIdsInFMXML {

    
    
// creates the replacement values
    public static void main(String[] args) {
        
        String inFileName = '';
        String outFileName = '';
        
        if (args) {
            println("-------------------------------------------------------------------------------");
            println("Replaces identifiers in FM message so it can be reused.");
            println("Only works with PreClose, AtIssuance without PreClose, not for REMIC.\n");
            println("USAGE: groovy ReplaceIdsInFMXML.groovy");
            println("-------------------------------------------------------------------------------");
            
            System.exit(0);
        }
        
        def rand = new Random();
        
         // generate unique number, rearrangement of the current 2-digit year, day number of the year and number of seconds so far this day
        DateTime dt = new DateTime();
        DateTimeFormatter yrDaySecsFmt = DateTimeFormat.forPattern("yyDDD");
        def yrDaySecs = yrDaySecsFmt.print(dt) + (dt.secondOfDay + '').padLeft(5,'0');
        def uniquePrefix = (rand.nextInt(8) + 1) + yrDaySecs[5..6] + yrDaySecs[1] + yrDaySecs[2..4] + yrDaySecs[7..9];
        
        // create request timestamp
        DateTimeFormatter fmTimestampFmt = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss.SSS");
        def fmTimestamp = fmTimestampFmt.print(dt);
        
        // get the last used directory if found
        def lastDir = '';
        // get last directory if it exists
        def userHome = System.getProperty("user.home");
        File lastDirFile = new File(userHome + '/Rockport/FannieMae/dev/.lastDir');
        if (lastDirFile.exists()) {
            lastDir = lastDirFile.text;
        } else {
            lastDir = userHome;
            File d = new File(userHome + '/Rockport/FannieMae/dev');
            d.mkdirs();
            //ld = new File(d.canonicalPath + '/.lastDir');
            lastDirFile.write(lastDir);
        }
          
        // filter for xml files
        def filter = [getDescription: {-> "*.xml"}, accept:{file -> file.toString() ==~ /.*?\.xml/  || file.isDirectory() }] as FileFilter;
        
        def inFile = null;
        def outFile = null;
        def swing = new SwingBuilder()
        
        // get input file
        def dialog = swing.fileChooser(dialogTitle: "Select Input File", 
            currentDirectory: new File(lastDir),
            fileFilter: filter);
        if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
            println "input: " +  dialog.selectedFile;
            inFile = dialog.selectedFile;
        } else {
            println("User canceled, exiting.");
            System.exit(1);
        }
        
        lastDir = inFile.canonicalPath;

        // get output file or directory
        dialog = swing.fileChooser(dialogTitle: "Select Output File (select directory for default filename)", 
            currentDirectory: new File(lastDir),
            fileSelectionMode:JFileChooser.FILES_AND_DIRECTORIES, 
            fileFilter: filter);
        if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
            println "Output: " +  dialog.selectedFile;
            if (dialog.selectedFile.isDirectory()) { 
                println('Directory'); 
                ArrayList a = inFile.name.split('\\.')
                a.push(a[-1]);
                a[-2] = 'new-' + uniquePrefix;
                outFileName = a.join('.');
                outFile = new File(dialog.selectedFile.canonicalPath + '/' + outFileName);
                println("No output file specified, will write to " + outFile.canonicalPath);
            } else {
                outFile = dialog.selectedFile;
            }
        } else {
            println("User canceled, exiting.");
            System.exit(1);
        }
        
        lastDir = outFile.canonicalPath;

        /*
        inFileName = args[0];
        if (args.size() < 2) {
            ArrayList a = args[0].split('\\.')
            a.push(a[-1]);
            a[-2] = 'new-' + uniquePrefix;
            outFileName = a.join('.');
            println("No output file specified, will write to " + outFileName);
        } else {
           outFileName = args[1];
        }
        */
        
        def sb = new SwingBuilder();
        
        try {
       
            /* pattern for replacement:
                [unique number string, arbitrary string, increment start number, places to left-fill with zeroes]
                if zero-fill is 0, the incrementing number is ignored
            */
            def params = [:]       
params['CollateralPoolWeightedAverageNetMarginRate'] = ['','1.0123456789',0,0];
params['CollateralPoolWeightedAverageCouponRate'] = ['','3.012345679',0,0];
params['CollateralPoolWeightedAverageLoanToValueRatioFactor'] = ['','5.0123456791',0,0];
params['CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor'] = ['','7.0123456792',0,0];
params['SecurityLifetimeFloorRate'] = ['','9.0123456793',0,0];
params['SecurityLifetimeCapRate'] = ['','11.0123456794',0,0];
params['SecurityPaydownFactor'] = ['','13.0123456795',0,0];
params['SecurityInterestRate'] = ['','15.0123456796',0,0];
params['LoanOriginationInterestRate'] = ['','17.0123456797',0,0];
params['LoanInterestRate'] = ['','19.0123456798',0,0];
params['LoanFannieMaeParticipationPercent'] = ['','21.0123456799',0,0];
params['LoanPassThroughRatePercent'] = ['','23.01234568',0,0];
params['LoanCutOffDateAllInLoanToValuePercent'] = ['','25.0123456801',0,0];
params['LoanCutOffDateLoanToValuePercent'] = ['','27.0123456802',0,0];
params['LoanUnderwrittenNCFDebtServiceCoverageRatioFactor'] = ['','29.0123456803',0,0];
params['LoanUnderwrittenNCFInterestOnlyDebtServiceCoverageRatioFactor'] = ['','31.0123456804',0,0];
params['LoanUnderwrittenNCFAllInDebtServiceCoverageRatioFactor'] = ['','33.0123456805',0,0];
params['LoanUnderwrittenNCFCapRateDebtServiceCoverageRatioFactor'] = ['','35.0123456806',0,0];
params['LoanUnderwrittenNCFDebtYieldFactor'] = ['','37.0123456807',0,0];
params['LoanLifetimeCapInterestRate'] = ['','39.0123456808',0,0];
params['LoanLifetimeFloorInterestRate'] = ['','41.0123456809',0,0];
params['LoanMortgageMarginPercent'] = ['','43.012345681',0,0];
params['LoanPeriodicPaymentChangeDecreasePercent'] = ['','45.0123456811',0,0];
params['LoanPeriodicPaymentChangeMaximumIncreasePercent'] = ['','47.0123456812',0,0];
params['LoanPeriodicRateChangeMaximumDecreasePercent'] = ['','49.0123456813',0,0];
params['LoanPeriodicRateChangeMaximumIncreasePercent'] = ['','51.0123456814',0,0];
params['LoanAdditionalDebtMinimumInterestRatePercent'] = ['','53.0123456815',0,0];
params['LoanAdditionalDebtCurrentInterestRate'] = ['','55.0123456816',0,0];
params['LoanAdditionalDebtAdjustableRateMortgageMarginPercent'] = ['','57.0123456817',0,0];
params['LoanMezzanineInitialTermInterestRatePercent'] = ['','59.0123456818',0,0];
params['LoanOtherDecliningPremiumPercent'] = ['','61.0123456819',0,0];
params['PropertyUnitIncomeorRentRestrictionPercent'] = ['','63.012345682',0,0];
params['PropertyAffordableUnitAMIPercent'] = ['','65.0123456821',0,0];
params['PropertySourceEnergyUseIntensityRate'] = ['','67.0123456822',0,0];
params['PropertyPhysicalOccupancyPercent'] = ['','69.0123456823',0,0];
params['PropertyEconomicOccupancyPercent'] = ['','71.0123456824',0,0];
            
            //def inFile = new File('C:/local/AtIssuanceMBSDisclosure_MSFMS_65_AN0457.xml');
            //def inFile = new File(inFileName);
            def text = inFile.getText();
            
            def xml = new XmlSlurper().parseText(text)
            
            params.each { key, value ->
                xml.'**'.findAll{it.name() == key}.each { node ->
                  //if (node.size() <=1) {
                      println('Replacing ' + key);
                      node.replaceBody(evalParam(value));
                  //}
                }
            }
            
            //def outFile = new File('C:/local/testreplace.xml');
            //def outFile = new File(outFileName);
            outFile.write(XmlUtil.serialize(xml));
            lastDirFile.write(lastDir);
        } catch (Exception e) {
            // something bad happened
            sb.optionPane().showMessageDialog(null, "Error occurred, " + e.getMessage(), "Message", JOptionPane.ERROR_MESSAGE)
            System.exit(-1);
        }

        // we're good!
        sb.optionPane().showMessageDialog(null, "File written to " + outFile.canonicalPath, "Message", JOptionPane.INFORMATION_MESSAGE)
    
        System.exit(0);
    }
    
    public static String evalParam(param) {
        def val = param[0] + param[1] + '';
        if (param[3] + 0 > 0) {
            val += (param[2]++ + '').padLeft(param[3],'0');
        }
        return val;
    }
    
    
   
    /*
    RequestCreationTimestamp
    RequestPostTimestamp
    RequestID
    
    DealAgreementIdentifier
    DealIdentifier
    LoanIdentifier
    FannieMaeLoanNumber
    CollateralIdentifier
    CollateralReferenceNumber
    PropertyIdentifier
    
    PropertyActivityDatetime
    
    MBSPoolIdentifier
    SecurityCUSIPIdentifier
    
    SecurityIssueDate
    SecurityFederalBookEntryDate
    
    SecurityStatusDatetime
    SecurityFactorEffectiveDate
    SecurityInterestRateEffectiveDate
    SecurityFirstPaymentDate
    */
    
    
    /*
    def getNodes = { doc, path ->
        def nodes = doc
        path.split("\\.").each { nodes = nodes."${it}" }
        return nodes
    }
    
    
        getNodes(xml, key).each { node ->
          println("node");
          node.replaceBody(evaluate(value));
        }
    
    }
    */
    /*
    param.each { key,value ->
        def node = xml
        key.split("\\.").each { it ->
          node = node."${it}"
        }
        node.replaceBody(evaluate(value));  
    }
    */
    
    //println XmlUtil.serialize(xml)

}