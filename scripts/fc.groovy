import javax.swing.filechooser.FileFilter
import javax.swing.JFileChooser
import groovy.swing.SwingBuilder
import java.awt.BorderLayout as BL

def swing = new SwingBuilder()
def dialog = swing.fileChooser(dialogTitle: "Select Input File")
if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
    println "Input: " +  dialog.selectedFile
     
}

dialog = swing.fileChooser(dialogTitle: "Select Output File (select directory for default filename)", fileSelectionMode:JFileChooser.FILES_AND_DIRECTORIES)
if (dialog.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
    println "Output: " +  dialog.selectedFile
     //def outfile = new File(dialog.selectedFile);
     if (dialog.selectedFile.isDirectory()) { println('Directory'); } 
     else { println('File'); }
} 