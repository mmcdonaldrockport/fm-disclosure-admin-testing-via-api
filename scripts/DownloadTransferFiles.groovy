@Grapes(
@Grab(group='commons-codec', module='commons-codec', version='1.10')
)
import groovy.sql.*;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;

/*****************************************************************************************************************/

def env = 'LIVERECOVERY';

/*****************************************************************************************************************/

def exportLocation = "C:/Users/mmcdonald/Documents/FMDataFiles/";

def dbInfo = [
    'DEV': [server:'DVFM01-D03', instance:'DVFM01_D03', dbName:'DisclosureAdmin', integrated:true],
    'QA1': [server:'DVFM01-D03', instance:'QAFM01_D04', dbName:'DisclosureAdmin', integrated:true],
    'QA2': [server:'DVFM01-D03', instance:'QAFM01_D04', dbName:'DisclosureAdmin2', integrated:true],
    'QA3': [server:'DVFM01-D03', instance:'QAFM01_D04', dbName:'DisclosureAdmin3', integrated:true],
    'SIT': [server:'789785-DB12', instance:'SITSQLDB401', dbName:'DisclosureAdmin', integrated:false, user:'sit_fnma_soapui_svc' , encPassword:'MjZLbkYqSjheZXVu' ], 
    'LIVE': [server:'853029-SQLCLUS5', instance:'LIVESQLDB101', dbName:'DisclosureAdmin', integrated:true],     
    'LIVERECOVERY': [server:'868233-DFSDR01', instance:'DRSQLDB901', dbName:'DisclosureAdmin', integrated:true]    //172.28.19.160
];

def dt = new Date().format('yyyyMMdd_HHmmss')
def instance = dbInfo[env]['instance'];
def dbServer = dbInfo[env]['server'] + '\\' + instance;
def dbName = dbInfo[env]['dbName'];
// DEV & QA use integrated security - SIT needs user and password
def integratedSecurity = dbInfo[env]['integrated'];
def driver = 'com.microsoft.sqlserver.jdbc.SQLServerDriver';
def sql = null;

// get sql connection
try {
    if (integratedSecurity) {
        sql = Sql.newInstance("jdbc:sqlserver://${dbServer};databaseName=${dbName};integratedSecurity=true", driver);
    } else {
        def user = dbInfo[env]['user'];
        def password = decrypt(dbInfo[env]['encPassword']);
        sql = Sql.newInstance("jdbc:sqlserver://${dbServer};databaseName=${dbName}", user, password, driver);
    }
} catch (Exception e) {
    println(e);
    return 1;
}

def exportDir = "$exportLocation/${env}_${instance}_${dbName}_$dt";

// create directory from the date-time
try {
    def dir = new File(exportDir).mkdirs();
    println("Created directory $exportDir");    
} catch (Exception e) {
    println(e);
    return 1;
}

def fileList = [];

def filesCreated = 0;
def rowsReturned = 0;

// get file names
try {
    sqlStr = 'SELECT name, tf.transferQueueId from adm.TransferFile tf JOIN adm.Transferqueue tq ON tf.TransferQueueId = tq.Id WHERE tq.Status in (3, 4);'
    //sqlStr = 'select Name, TransferQueueId from adm.TransferFile;'
    sql.execute(sqlStr) { isResultSet, resultSet ->
        rowsReturned = resultSet.size();
        if (rowsReturned < 1) {
            println('No files returned from query.');
            return 1;
        } else {
            //println(rowsReturned + ' rows returned');
            resultSet.each { row ->
                def name = row['Name'];
                def jobId = row['TransferQueueId'];
                fileList.add(['name':name, 'jobId':jobId]);
            }
        }
    }
} catch (Exception e) {
    println(e);
    return 1;
}

println("$rowsReturned rows returned.");

//return 0;

// get file contents
fileList.each { fileItem ->
    try {
        def name = fileItem['name'];
        def jobId = fileItem['jobId'];
        sql.execute("select Contents from adm.TransferFile where TransferQueueId = $jobId;") { isResultSet, resultSet ->
            row = resultSet;             
            def fileName = fileItem['name'];
            def file = new File("$exportDir/$fileName");
            file << row['Contents'][0];
            filesCreated++;
            println("$fileName");
        }
    } catch (Exception e) {
        println(e);
        //return 1;
    }
}

println("\n$filesCreated files created.");

return 0;

// helper functions
String encrypt(text) {
   def encrypted = '';
   try {
       byte[] encodedBytes = Base64.encodeBase64(text.getBytes());
       encrypted = new String(encodedBytes) 
   } catch (Exception e) {
       println("failed to encrypt password." + e.getMessage())
   }
   return encrypted;
};   

String decrypt(text) {
    String textData = text;
    byte[] encodedBytes = textData.getBytes();
    String decrypted = new String(Base64.decodeBase64(encodedBytes));
    return decrypted; 
};