@Grapes(
@Grab(group='commons-codec', module='commons-codec', version='1.10')
)
import groovy.sql.*;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;

def env = 'UAT2';

def exportLocation = 'C:/local/FMDataFiles/';

def dbInfo = [
    'DEV': [server:'DVFM01-D03', instance:'DVFM01_D03', dbName:'DisclosureAdmin', integrated:true],
    'QA1': [server:'DVFM01-D03', instance:'QAFM01_D04', dbName:'DisclosureAdmin', integrated:true],
    'QA2': [server:'DVFM01-D03', instance:'QAFM01_D04', dbName:'DisclosureAdmin2', integrated:true],
    'SIT': [server:'789785-DB12', instance:'SITSQLDB401', dbName:'DisclosureAdmin', integrated:false, user:'sit_fnma_soapui_svc' , encPassword:'MjZLbkYqSjheZXVu' ],
    'UAT2': [server:'789760-DB10', instance:'UATSQLDB201', dbName:'DisclosureAdmin2', integrated:true]
];

def dt = new Date().format('yyyyMMdd_HHmmss')
def instance = dbInfo[env]['instance'];
def dbServer = dbInfo[env]['server'] + '\\' + instance;
def dbName = dbInfo[env]['dbName'];
// DEV & QA use integrated security - SIT needs user and password
def integratedSecurity = dbInfo[env]['integrated'];
def driver = 'com.microsoft.sqlserver.jdbc.SQLServerDriver';
def sql = null;

// get sql connection
try {
    if (integratedSecurity) {
        sql = Sql.newInstance("jdbc:sqlserver://${dbServer};databaseName=${dbName};integratedSecurity=true", driver);
    } else {
        def user = dbInfo[env]['user'];
        def password = decrypt(dbInfo[env]['encPassword']);
        sql = Sql.newInstance("jdbc:sqlserver://${dbServer};databaseName=${dbName}", user, password, driver);
    }
} catch (Exception e) {
    println(e);
    return 1;
}

def exportDir = "$exportLocation/${env}_${instance}_${dbName}_$dt";

// create directory from the date-time
try {
    def dir = new File(exportDir).mkdirs();
    println("Created directory $exportDir");    
} catch (Exception e) {
    println(e);
    return 1;
}

def fileList = [];

def filesCreated = 0;
def rowsReturned = 0;

// get file names
try {
    //sql.execute('SELECT Name, TransferQueueId FROM adm.TransferFile WHERE TransferQueueId > 85904 OR TransferQueueId IN (85555,85671,85682,85681,85781,85780,85782,85783,85784,85785,85786,85787,85788,85790,85789);') { isResultSet, resultSet ->
    sql.execute('SELECT Name, TransferQueueId FROM adm.TransferFile;') { isResultSet, resultSet ->
        rowsReturned = resultSet.size();
        if (rowsReturned < 1) {
            println('No files returned from query.');
            return 1;
        } else {
            //println(rowsReturned + ' rows returned');
            resultSet.each { row ->
                def name = row['Name'];
                def jobId = row['TransferQueueId'];
                fileList.add(['name':name, 'jobId':jobId]);
            }
        }
    }
} catch (Exception e) {
    println(e);
    return 1;
}

println("$rowsReturned rows returned.");

// get file contents
fileList.each { fileItem ->
    try {
        def name = fileItem['name'];
        def jobId = fileItem['jobId'];
        sql.execute("select Contents from adm.TransferFile where TransferQueueId = $jobId;") { isResultSet, resultSet ->
            row = resultSet;             
            def fileName = fileItem['name'];
            def file = new File("$exportDir/$fileName");
            def xml = row['Contents'].toString()[1..-2];
            file << xml;
            filesCreated++;
            println("File $filesCreated of $rowsReturned: " + fileName);
        }
    } catch (Exception e) {
        println(e);
        //return 1;
    }
}

println("\n$filesCreated files created.");

return 0;

// helper functions
String encrypt(text) {
   def encrypted = '';
   try {
       byte[] encodedBytes = Base64.encodeBase64(text.getBytes());
       encrypted = new String(encodedBytes) 
   } catch (Exception e) {
       println("failed to encrypt password." + e.getMessage())
   }
   return encrypted;
};   

String decrypt(text) {
    String textData = text;
    byte[] encodedBytes = textData.getBytes();
    String decrypted = new String(Base64.decodeBase64(encodedBytes));
    return decrypted; 
};



