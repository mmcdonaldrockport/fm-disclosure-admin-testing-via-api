/* get average execution time */
SELECT 
	LEFT(te.Name, 2) As MessageType, 
	Avg(DateDiff(Second, DateTimeStarted, DateTimeEnded)) As AvgSeconds
FROM 
	adm.TransferExecution te JOIN adm.TransferFile tf ON te.TransferQueueId = tf.TransferQueueId
GROUP By 
	LEFT(te.Name, 2)
ORDER BY MessageType

SELECT * FROM adm.BEEQueue