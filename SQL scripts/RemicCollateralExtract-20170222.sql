
SELECT 
--DealId, 
--DL.Id, 
cp.MBSPoolIdentifier,
SE.SecurityCUSIPIdentifier as SecurityCUSIPNumber,
stcg.DealGroupNumber AS SecurityREMICGroupIdentifier,
GAB.SecurityCollateralUnpaidPrincipalBalanceAmount As SecurityAtIssuanceCollateralParticipationUPBAmount,
DL.Name AS TrustName,
--SE.Id AS SecurityId,
GAB.FinancialInstrumentCollateralGroupParticipationPercent AS SecurityParticipationPercent,
GAB.FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate AS FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate
FROM import.StructuredTransactionCollateralGroup STCG
LEFT JOIN import.Deal DL ON DL.Id = STCG.DealId
LEFT JOIN import.FinancialInstrumentStructuredTransactionGroupAssociationBalance GAB ON GAB.StructuredTransactionCollateralGroupId = STCG.Id
LEFT JOIN import.Security SE ON SE.Id = GAB.SecurityId
INNER JOIN FmData.CollateralPool cp ON SE.Id = cp.Id
WHERE SE.Id is not null

