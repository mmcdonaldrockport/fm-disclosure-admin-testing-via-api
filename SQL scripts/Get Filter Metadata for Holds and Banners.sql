-- Get Filter Metadata values for Issuance Holds and Banners

--SELECT CONCAT('SELECT ',Id, ' AS MDId, ', ' ''', Caption,''' AS Caption, ', RIGHT(OptionsSource,LEN(OptionsSource)-7), ' UNION ') FROM adm.FilterMetadata WHERE OptionsSource IS NOT NULL

SELECT 5 AS MDId,  'Execution Type' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 21213  UNION 
SELECT 8 AS MDId,  'ARM Plan' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 20793  UNION 
SELECT 9 AS MDId,  'MSA Area' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 20143  UNION 
SELECT 10 AS MDId,  'MSA' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 20143  UNION 
SELECT 11 AS MDId,  'Prefix' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 20309  UNION 
SELECT 12 AS MDId,  'Prepayment Type' AS Caption, code as Id, Value as Name FROM adm.v_AllowableValues WHERE TermId = 20701  UNION 
SELECT 13 AS MDId,  'Product Type' AS Caption, code as Id, Value as Name from adm.v_AllowableValues where TermId = 21212 UNION
SELECT 1 AS MDId,  'Pool Number' AS Caption, '', ''  UNION 
SELECT 2 AS MDId,  'Deal ID' AS Caption, '', ''  UNION
SELECT 3 AS MDId,  'CUSIP' AS Caption, '', ''  UNION
SELECT 4 AS MDId,  'Trust ID' AS Caption , '', '' UNION
SELECT 6 AS MDId,  'Issue Date' AS Caption, '', ''   

order by MDId, Value 

select concat(Caption,'=',id) from adm.FilterMetadata