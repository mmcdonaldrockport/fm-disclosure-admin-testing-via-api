USE DisclosureAdmin;

BEGIN
		BEGIN TRAN
		BEGIN TRY
			--THROW 51001, 'Testing the Catch clause', 1;
			DELETE FROM FmData.LoanCrossedRelationship;
			DELETE FROM FmData.SecurityAtIssuance ;
			DELETE FROM FmData.LoanFinancialActivity ;
			DELETE FROM FmData.SecurityFactor ;
			DELETE FROM FmData.LoanMezzaninePrepaymentProtectionTerm ;
			DELETE FROM FmData.SecurityInterestAccrualSchedule ;
			DELETE FROM FmData.LoanOtherDecliningPrepaymentSchedule ;
			DELETE FROM FmData.SecurityInterestRate ;
			DELETE FROM FmData.LoanPrepaymentPremiumTerm ;
			DELETE FROM FmData.SecurityInterestRateAdjustmentLifetimeTerm ;
			DELETE FROM FmData.LoanPrepaymentProtectionTerm ;
			DELETE FROM FmData.SecurityPayment ;
			DELETE FROM FmData.LoanServicingActivity ;
			DELETE FROM FmData.SecurityRelationship ;
			DELETE FROM FmData.LoanServicingReclassificationActivity ;
			DELETE FROM FmData.SecurityStatus ;
			DELETE FROM FmData.Property ;
			DELETE FROM FmData.StructuredTransactionCollateralGroup ;
			DELETE FROM FmData.PropertyAcquisition ;
			DELETE FROM FmData.PropertyAddress ;
			DELETE FROM FmData.PropertyAffordableUnit ;
			DELETE FROM FmData.PropertyCashFlowActivity ;
			DELETE FROM FmData.PropertyEnergy ;
			DELETE FROM FmData.PropertyFinancialExpenseActivity ;
			DELETE FROM FmData.PropertyFinancialIncomeActivity ;
			DELETE FROM FmData.PropertyFinancialStatement ;
			DELETE FROM FmData.PropertyFinancialSummaryActivity ;
			DELETE FROM FmData.Loan ;
			DELETE FROM FmData.PropertyHousing ;
			DELETE FROM FmData.Deal ;
			DELETE FROM FmData.PropertyInspection ;
			DELETE FROM FmData.DealMonitoring ;
			DELETE FROM FmData.PropertyInsurance ;
			DELETE FROM FmData.DealStructuredFacilityCollateral ;
			DELETE FROM FmData.PropertyMonitoring ;
			DELETE FROM FmData.FinancialInstrumentCollateralGroupBalance ;
			DELETE FROM FmData.PropertyOccupancy ;
			DELETE FROM FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance ;
			DELETE FROM FmData.PropertyPhasedConstruction ;
			DELETE FROM FmData.PropertyState ;
			DELETE FROM FmData.LoanAcquisition ;
			DELETE FROM FmData.PropertyValuation ;
			DELETE FROM FmData.LoanAdditionalDebt ;
			DELETE FROM FmData.[Security] ;
			DELETE FROM FmData.LoanCollateral ;
			DELETE FROM FmData.Collateral ;
			DELETE FROM FmData.CollateralPool ; 
			COMMIT TRAN
			
			SELECT
				'OK' as Result, 
				ERROR_NUMBER() AS ErrorNumber, 
				ERROR_MESSAGE() as ErrorMessage, 
				ERROR_LINE() as ErrorLine;
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT
				'FAILED' as Result, 
				ERROR_NUMBER() AS ErrorNumber, 
				ERROR_MESSAGE() as ErrorMessage, 
				ERROR_LINE() as ErrorLine;
		END CATCH
	END

BEGIN TRAN
	BEGIN TRY
		DELETE adm.TransferExecution;
		DELETE adm.TransferQueue;
		COMMIT TRAN;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SELECT
			'FAILED' as Result, 
			ERROR_NUMBER() AS ErrorNumber, 
			ERROR_MESSAGE() as ErrorMessage, 
			ERROR_LINE() as ErrorLine;
	END CATCH
