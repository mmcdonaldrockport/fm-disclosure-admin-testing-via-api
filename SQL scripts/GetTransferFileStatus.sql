SELECT * 
FROM 
	adm.TransferFile tf 
	JOIN adm.TransferQueue tq on tf.TransferQueueId = tq.Id 
	LEFT OUTER JOIN adm.TransferExecution tx on tx.TransferQueueId = tq.Id;