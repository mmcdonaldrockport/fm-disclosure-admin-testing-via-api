/* run this and save results as csv */
SELECT 
	name, 
	'|', 
	REPLACE(REPLACE(CONVERT(VARCHAR(MAX),contents), CHAR(13), ''),CHAR(10),' ') AS contents
FROM 
	adm.TransferFile tf JOIN adm.Transferqueue tq ON tf.TransferQueueId = tq.Id 
WHERE 
	tq.Status in (3,4) 
	AND CONVERT(VARCHAR(MAX),contents) NOT LIKE '%DataPayload%'
	AND tq.id > 1854
	-- AND LEFT(name,2) = '02'

