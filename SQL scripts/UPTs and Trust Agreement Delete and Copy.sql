-- UPTs and Trust Agreeement documents in DocumentHistory table.

use DisclosureAdmin2;
--delete from adm.documenthistory where DocumentId in (10,11,12,13,14,16,17);
select * from adm.documenthistory;
select * from adm.documents

INSERT INTO adm.DocumentHistory (DocumentId, CreatedOn, Content, FileName, FileSize, CreatedBy, BasisEffDate)
select dh.DocumentId, CreatedOn, Content, FileName, FileSize, CreatedBy, dh.BasisEffDate
from DisclosureAdmin.adm.documenthistory dh JOIN (SELECT DocumentId, Min(BasisEffDate) as BasisEffDate FROM DisclosureAdmin.adm.documenthistory  WHERE DocumentId in (10,11,12,13,14,16) GROUP BY DocumentId) dh2 ON 
dh.DocumentId = dh2.DocumentId and dh.BasisEffDate = dh2.BasisEffDate;

INSERT INTO adm.DocumentHistory (DocumentId, CreatedOn, Content, FileName, FileSize, CreatedBy, BasisEffDate)
select dh.DocumentId, CreatedOn, Content, FileName, FileSize, CreatedBy, dh.BasisEffDate
from DisclosureAdmin.adm.documenthistory dh JOIN (SELECT DocumentId, Min(BasisEffDate) as BasisEffDate FROM DisclosureAdmin.adm.documenthistory  WHERE DocumentId in (17) GROUP BY DocumentId) dh2 ON 
dh.DocumentId = dh2.DocumentId and dh.BasisEffDate = dh2.BasisEffDate;


