use DisclosureAdmin;

SELECT
	concat('[',idt.DocumentTypeId,', ',dh.DocumentId,', ',idt.DocumentVersionId,']'),
	concat('[',dt.Name,'] [',dv.Name,'] [', d.Name,']'), 
	idt.Id as IssuanceDocumentTypeId,
	idt.DocumentTypeId, 
	dh.DocumentId,
	idt.DocumentVersionId,
	dt.Name as DocumentTypeName,
	dv.Name as DocumentVersionName,
	d.Name as DocumentName, 
	(SELECT filename FROM adm.DocumentHistory WHERE id = dh.DocumentHistoryId) AS filename,
	dh.DocumentHistoryId,
	Max(d.BasisEffDate) as D_BasisEffectiveDate,
	dh.BasisEffDate,
	d.OnHold,
	d.DocConfig,
	d.Basis,
	d.ProductType,
	dt.DocumentFormat
FROM
	(SELECT 
		MAX(id) as DocumentHistoryId,
		DocumentId,
		MAX(BasisEffDate) as BasisEffDate
	FROM 
		adm.DocumentHistory
	--WHERE BasisEffDate <= '2011-10-01'
	GROUP BY
		DocumentId) dh 
		JOIN adm.Documents d ON dh.DocumentId = d.Id 
		JOIN adm.IssuanceDocumentTypes idt ON idt.DocumentTypeId = d.DocumentTypeId
		JOIN adm.DocumentTypes dt ON idt.DocumentTypeId = dt.id
		JOIN adm.DocumentVersions dv on dv.Id = idt.DocumentVersionId
	--Where dh.BasisEffDate < '2012-01-01'
--WHERE
	--(idt.DocumentTypeId = 1 AND idt.DocumentVersionId = 1 AND dh.DocumentId = 1) OR  (idt.DocumentTypeId = 1 AND idt.DocumentVersionId = 1 AND dh.DocumentId = 2) OR  1=0
GROUP BY
	idt.Id,
	dh.DocumentHistoryId,
	dh.DocumentId, 
	idt.DocumentTypeId,
	idt.DocumentVersionId,
	--FileName, 
	dh.BasisEffDate,
	d.Name,
	d.OnHold,
	d.Basis,
	d.ProductType,
	d.DocConfig,
	dt.Name, 
	dt.DocumentFormat,
	dv.Name
ORDER BY
	dh.DocumentId,
	DocumentHistoryId,
	idt.DocumentVersionId

