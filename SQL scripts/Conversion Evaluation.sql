/*
TransferMessageDetail Message Type: 
Error = 0,
Warning = 1
ValidationError = 2,
ValidationWarning = 3,
TransferDefinitionError = 4,
Information = 5
*/

SELECT MAX(TransferExecutionId) FROM adm.TransferMessageDetail
SELECT ROWIndex, COUNT(*) FROM adm.TransferMessageDetail WHERE TransferExecutionId = 251 GROUP BY ROWINDEX
SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId > 262 ORDER BY TransferExecutionId 
--SELECT ActionName, ContainerName, MessageSource, MessageType FROM adm.TransferMessageDetail

SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId = 279

SELECT * FROM adm.TransferExecution ORDER BY Id  --294

SELECT * FROM fmdata.Loan
SELECT * FROM import.Loan_Source1 WHERE LoanIdentifier NOT IN (SELECT LoanIdentifier FROM FmData.Loan )

SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId = 279 AND PrimaryValue <>''

SELECT TransferExecutionId, ActionName, SourceColumnName, MessageType, MessageSource, Message, Count(1)
FROM adm.TransferMessageDetail 
WHERE 
		TransferExecutionId > 239
GROUP BY TransferExecutionId, ActionName, SourceColumnName, MessageType, MessageSource, Message 
ORDER BY TransferExecutionId


--Get DMBS records
SELECT  
DealAgreementIdentifier
,MBSPoolIdentifier
,LoanIdentifier
,FannieMaeLoanNumber
,LoanPropertyCurrentCount
,LoanActivityType
,LoanActivityDatetime
FROM import.LoanDMBS_Source1  
WHERE LoanActivityType IS NOT NULL AND LoanActivityDatetime IS NOT NULL

SELECT * FROM import.RemicCollateral_Source1 rc_s1 
	JOIN fmData.Security s ON rc_s1.SecurityCUSIPNumber = s.SecurityCUSIPIdentifier
	JOIN fmData.CollateralPool cp ON s.Id = cp.Id
WHERE s.ProductGroupType in ('MBS', 'Mega')

SELECT * FROM fmData.Security s JOIN fmData.CollateralPool cp ON s.Id = cp.Id
	WHERE ProductGroupType = 'Mega'

SELECT * FROM fmData.Security s JOIN fmData.CollateralPool cp ON s.Id = cp.Id
	WHERE ProductGroupType = 'Mega'



SELECT * from import.Security_SOURCE1 where DealIdentifier IS NULL

SELECT 	TransferExecutionId, Name,  count(1) 
FROM adm.TransferExecution te JOIN adm.TransferMessageDetail tmd ON te.Id = tmd.TransferExecutionId
WHERE TransferExecutionId > 294 -- AND TransferExecutionId < 288
GROUP BY TransferExecutionId, Name
ORDER BY TransferExecutionId

SELECT * FROM FmData.Loan WHERE LoanIdentifier in (
	SELECT LEFT(PrimaryValue,5)  FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283
)
SELECT PrimaryValue, count(*)/2 FROM adm.TransferMessageDetail WHERE TransferExecutionId = 273
	GROUP BY PrimaryValue

SELECT * FROM adm.TransferExecution
DELETE FROM import.LoanPrepaymentPremiumTerm

SELECT * FROM fmdata.Security WHERE SecurityCUSIPIdentifier IN (SELECT  SecurityCUSIPNumber FROM import.Security_Source1 WHERE MBSPoolIdentifier IN (SELECT left(PrimaryValue,6) FROM adm.TransferMessageDetail WHERE TransferExecutionId = 263))
SELECT  *  FROM import.Security_Source1 WHERE MBSPoolIdentifier IN (SELECT left(PrimaryValue,6) FROM adm.TransferMessageDetail WHERE TransferExecutionId = 263)

SELECT MBSPoolIdentifier, TimePeriod FROM import.PoolFactorPlus_Source1 WHERE MBSPoolIdentifier IN (
	SELECT PrimaryValue FROM adm.TransferMessageDetail WHERE TransferExecutionId = 273) 
	ORDER BY MBSPoolIdentifier, TimePeriod

SELECT * FROM fmdata.Security WHERE Id in (
SELECT ID FROM fmdata.CollateralPool WHERE MBSPoolIdentifier IN(
SELECT DISTINCT MBSPoolIdentifier FROM import.PoolFactorPlus_Source1 WHERE MBSPoolIdentifier IN (
	SELECT PrimaryValue FROM adm.TransferMessageDetail WHERE TransferExecutionId = 273))) 

	

SELECT COUNT(*) FROM import.Security_Source1 WHERE SecurityCUSIPNumber NOT IN 
	(SELECT SecurityCUSIPNumber FROM import.Security  WHERE ProductGroupType NOT IN ('REMIC','Mega'))
SELECT * FROM import.Security_Source1

SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId = 284 AND ContainerName <> 'LoanFinancialActivity_Source'


SELECT * FROM import.RemicCollateral_Source1 WHERE SecurityCUSIPNumber = '3138EKTH8'
SELECT * FROM import.Security WHERE SecurityCUSIPIdentifier = '3138EKTH8'

SELECT DISTINCT PrimaryValue FROM adm.TransferMessageDetail WHERE TransferExecutionId = 249
SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId = 279 GROUP BY Message

SELECT DISTINCT SecurityCUSIPIdentifier FROM import.Security WHERE ProductGroupType NOT IN ('REMIC','Mega')

SELECT * FROM import.Property_Source1 WHERE PropertyAddressIdentifier IS NULL
SELECT * FROM import.


1886757 17234

SELECT * FROM import.Security_Source1 where MBSPoolIdentifier = '109149'

SELECT * FROM import.Security s JOIN import.Security_Source1 ss on s.SecurityCUSIPIdentifier = ss.SecurityCUSIPNumber
 WHERE SecurityCUSIPIdentifier LIKE '313637EW6'

 SELECT * FROM import.LoanAdditionalDebt_Source WHERE FannieMaeLoanNumber = 8000991773

SELECT count(*) FROM import.LoanAdditionalDebt
SELECT count(*) FROM  import.LoanMezzaninePrepaymentProtectionTerm

SELECT * FROM fmdata.Security s JOIN fmdata.SecurityRelationship sr 
ON s.Id = sr.SecurityRelatedId  
WHERE s.ProductGroupType = 'MEGA'

SELECT * FROM fmdata.SecurityRelationship sr WHERE sr.SecurityId in (
SELECT Id FROM fmdata.Security s WHERE s.ProductGroupType = 'MEGA')

SELECT * FROM fmdata.Security WHERE ProductGroupType = 'MEGA'


SELECT Id, SecurityCUSIPIdentifier , ProductGroupType
FROM FmData.Security WHERE SecurityCUSIPIdentifier  IN ('3138EKTH8', '31402CYW6','31418MDV1')

SELECT * FROM fmdata.CollateralPool WHERE MBSPoolIdentifier IN (  '745506')
SELECT * FROM fmdata.Security WHERE ID = 419116



SELECT * FROM 
	fmdata.CollateralPool cp 
	JOIN fmdata.Security s ON s.Id = cp.Id where ProductGroupType = 'Mega'

WHERE LoanIdentifier in (
	SELECT *  FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283

SELECT * FROM FmData.Loan WHERE LoanIdentifier in (
	SELECT LEFT(PrimaryValue,5)  FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283
)

SELECT * FROM import.LoanToPropertyRelationship_Source1 WHERE 
	CollateralIdentifier in (
		SELECT LEFT(PrimaryValue, CHARINDEX('|', PrimaryValue)-1) 
		FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283) 
	AND
	PropertyIdentifier in (
		SELECT RiGHT(PrimaryValue,LEN(PrimaryValue)-CHARINDEX('|', PrimaryValue)) 
		FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283)

SELECT 
	ltpr.FannieMaeLoanNumber,
	ltpr.CollateralIdentifier,
	ltpr.AssetID,
	ltpr.LoanIdentifier,
	ltpr.PropertyIdentifier
 FROM 
	import.LoanToPropertyRelationship_Source1 ltpr JOIN (
	SELECT 
		LEFT(PrimaryValue, CHARINDEX('|', PrimaryValue)-1) AS LoanIdentifier, 
		RiGHT(PrimaryValue,LEN(PrimaryValue)-CHARINDEX('|', PrimaryValue)) AS CollaterelIdentifier
	FROM 
		adm.TransferMessageDetail WHERE TransferExecutionId = 283) t0
	ON ltpr.LoanIdentifier = t0.LoanIdentifier AND
		ltpr.CollateralIdentifier = t0.CollaterelIdentifier
ORDER By 1,2,3


SELECT * FROM fmdata.Loan WHERE LoanIdentifier in (
	SELECT LEFT(PrimaryValue, CHARINDEX('|', PrimaryValue)-1) FROM adm.TransferMessageDetail WHERE TransferExecutionId = 283)


SELECT * FROM fmdata.Security s JOIN fmdata.CollateralPool cp ON cp.Id = s.ID WHERE ProductGroupType = 'Mega'
SELECT * FROM fmdata.Security s WHERE ProductGroupType = 'Mega'
SELECT * FROM import.CollateralPool WHERE MBSPoolIdentifier = '745629'

SELECT * FROM adm.TransferMessageDetail where Message = 'PropertyFinancialStatementId is required'
SELECT * FROM adm.TransferMessageDetail where ActionName = 'Load_PropertyFinancialSummaryActivity'


SELECT * FROM adm.TransferMessageDetail WHERE TransferExecutionId = 284 

SELECT * FROM fmdata.LoanFinancialActivity

SELECT * FROM import.PropertyOngoingFinancials_Source1 where PropertyFinancialStatementId is null

SELECT * FROM import.LoanPrepayment_Source1

SELECT * FROM fmdata.Security s 
JOIN fmdata.CollateralPool cp ON s.ID = cp.Id where ProductGroupType = 'DMBS'

SELECT * from adm.TransferExecution
SELECT * FROM adm.TransferMessageDetail



SELECT Count(1) FROM import.LoanDMBS_Source1 lds 
JOIN import.LoanToPropertyRelationship_Source ltp 
ON lds.LoanIdentifier = ltp.LoanIdentifier

SELECT * FROM import.Loan


select * from import.LoanDMBS_Source1