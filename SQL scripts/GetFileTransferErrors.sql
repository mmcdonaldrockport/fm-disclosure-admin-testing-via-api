USE DisclosureAdmin;

DECLARE @startDate VARCHAR(10);
SET @startDate = '2016-12-07';

DECLARE @endDate VARCHAR(10);
SET @endDate = '2016-12-07';


BEGIN TRAN;
SELECT * FROM (
SELECT
	@@SERVERNAME as serverName,
	db_name() as DBName,
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	te.Name as ServiceName,
	te.DateTimeStarted,
	te.DateTimeEnded,
	--convert(varchar,(te.DateTimeEnded - te.DateTimeStarted), 108) as ExecutionTime, 
	datediff(second, te.DateTimeStarted, te.DateTimeEnded) as ExecutionTime,
-- LEAVE LINE RETURN IN SO THAT MESSAGES ARE SEPARATED --
	IsNull((select IsNUll(tmd.Message, '[Error message absent]') + '
' AS 'data()' 
		FROM adm.TransferMessageDetail tmd --join adm.TransferExecution te on te.id = tmd.TransferExecutionId
		WHERE te.id = tmd.TransferExecutionId
		FOR XML PATH('')
		),'') as Message
FROM
	adm.TransferExecution te 
WHERE DateTimeEnded >= @startDate and DateTimeEnded < DATEADD (d , 1 , @endDate)
) 
a Where a.Message <> ''
ORDER BY DateTimeStarted;

SELECT ServerName, DBName, StartDate, Message, Count(*) as Count FROM (
SELECT * FROM (
SELECT
	@@SERVERNAME as serverName,
	db_name() as DBName,
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	te.Name as ServiceName,
	FORMAT(te.DateTimeStarted,'yyyy-MM-dd') as StartDate,
	te.DateTimeStarted,
	te.DateTimeEnded,
	--convert(varchar,(te.DateTimeEnded - te.DateTimeStarted), 108) as ExecutionTime, 
	datediff(second, te.DateTimeStarted, te.DateTimeEnded) as ExecutionTime,
-- LEAVE LINE RETURN IN SO THAT MESSAGES ARE SEPARATED --
	IsNull((select IsNUll(tmd.Message, '[Error message absent]') + '
' AS 'data()' 
		FROM adm.TransferMessageDetail tmd --join adm.TransferExecution te on te.id = tmd.TransferExecutionId
		WHERE te.id = tmd.TransferExecutionId
		FOR XML PATH('')
		),'') as Message
FROM
	adm.TransferExecution te 
WHERE DateTimeEnded >= @startDate and DateTimeEnded < DATEADD (d , 1 , @endDate)
) 
a Where a.Message <> ''

) A GROUP BY ServerName, DBName, StartDate, Message
ORDER BY StartDate;
COMMIT TRAN;