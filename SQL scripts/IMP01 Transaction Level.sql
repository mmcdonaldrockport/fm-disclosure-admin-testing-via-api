WITH IMP01 AS (SELECT

--Deal Table
[DealAgreementIdentifier] = D.AgreementIdentifier,
[DealIdentifier] = D.DealIdentifier,
[DealName] = D.Name,

--Collateral Pool Table
[MBSPoolIdentifier] = CP.MBSPoolIdentifier,

--Security Table
[SecurityCUSIPNumber] = S.SecurityCUSIPIdentifier,
[SecurityType] = S.ProductGroupType,
[SecuritySettlementDate] = S.SecurityFederalBookEntryDate,
[SecurityIssueDate] =S.SecurityIssueDate

--Loan Table
--[LoanIdentifier] = L.LoanIdentifier,

--Loan Acquisition Table
--[LoanDUSDiscloseDerivedExecutionType] = LA.LoanDUSDiscloseDerivedExecutionType,
--[LoanDUSDiscloseDerivedProductType] =LA.LoanDUSDiscloseDerivedProductType,
--[LoanDUSSplitIndicator] = LA.LoanDUSSplitIndicator,
--[LoanAdditionalDisclosureIndicator] = LA.LoanAdditionalDisclosureIndicator,
--[LoanAdditionalDisclosureComment] = LA.LoanAdditionalDisclosureComment,
--[LoanBifurcatedStructureIndicator] = LA.LoanBifurcatedStructureIndicator,
--[LoanDeliveryType] = LA.LoanDeliveryType,
--[LoanAcquisitionExecutionType] = LA.LoanExecutionType,
--[LoanNegotiatedTransactionType] = LA.LoanNegotiatedTransactionType,
--[LoanARMPlanNumber] = LA.LoanAdjustableRateMortgagePlanNumber,
--[LoanSellerName] = LA.LoanSellerFullName,

--Loan Prepayment Protection Term Table
--[LoanPrepaymentProtectionType] = LP.LoanPrepaymentProtectionType,

--Loan Prepayment Premium Term Table
--[LoanPrepaymentPremiumType] = LPP.LoanPrepaymentPremiumType,

--Property Table
--[PropertyIdentifier] = P.PropertyIdentifier,
--[PropertyAcquisitionName] = PA.PropertyName,

--Collateral Table
--[CollateralIdentifier] = C.CollateralIdentifier,
--[CollateralType] = C.CollateralType,
--[CollateralReferenceNumber] = C.CollateralReferenceNumber

FROM FmData.Security S
--Security Level Joins, Security to Collateral Pools is 1 -1
LEFT JOIN FmData.CollateralPool CP ON CP.Id = S.Id


--Loan Level Joins, every security will have at least one loan, need not have prepayment terms
INNER JOIN FmData.Loan L ON L.CollateralPoolId = CP.Id
LEFT JOIN FmData.Deal D ON D.Id = L.DealId
--LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
--FULL OUTER JOIN FmData.LoanPrepaymentProtectionTerm LP ON LP.LoanId = L.Id
--FULL OUTER JOIN FmData.LoanPrepaymentPremiumTerm LPP ON LPP.LoanPrepaymentProtectionTermId = LP.Id

--Collateral Level Joins, loans will have at least one property
LEFT JOIN FmData.LoanCollateral LC ON LC.LoanId = L.Id
LEFT JOIN FmData.Collateral C ON C.Id = LC.CollateralId
--LEFT JOIN FmData.Property P ON P.Id = C.Id
--LEFT JOIN FmData.PropertyAcquisition PA ON PA.Id = P.Id)
GROUP BY MBSPoolIdentifier, AgreementIdentifier, DealIdentifier, Name, SecurityCUSIPIdentifier, ProductGroupType, SecurityFederalBookEntryDate, SecurityIssueDate

)
SELECT * FROM IMP01 WHERE [MBSPoolIdentifier] = 'AN0457'

