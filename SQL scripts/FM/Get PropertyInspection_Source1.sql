SELECT 
ISNULL(CONVERT(varchar(MAX),PropertyIdentifier),'') AS PropertyIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyInspectionEffectiveDate),'') AS PropertyInspectionEffectiveDate,
ISNULL(CONVERT(varchar(MAX),PropertyDeferredMaintenanceIndicator),'') AS PropertyDeferredMaintenanceIndicator,
ISNULL(CONVERT(varchar(MAX),PropertyConditionRatingType),'') AS PropertyConditionRatingType,
ISNULL(CONVERT(varchar(MAX),PropertyInspectionSubmissionStatusType),'') AS PropertyInspectionSubmissionStatusType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityType),'') AS PropertyActivityType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityDatetime),'') AS PropertyActivityDatetime,
ISNULL(CONVERT(varchar(MAX),PropertyActivityAcquisitionIndicator),'') AS PropertyActivityAcquisitionIndicator

FROM import.PropertyInspection_SOURCE1
 ORDER BY 
PropertyIdentifier,
PropertyActivityType,
PropertyActivityDatetime



