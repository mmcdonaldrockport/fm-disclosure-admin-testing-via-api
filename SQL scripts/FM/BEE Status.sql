select Distinct 
    q.Id,
    q.TransferQueueId,
    q.RecordId,
    ISNULL(sdp.[12958_MBSPoolIdentifier], sdp.DealName) As PoolIdentifier,
    q.Context,
    q.ErrorMessage,
    q.ErrorStack,
    [Queue Type] = CASE 
                        WHEN q.QueueType = 1 THEN 'Pre Closed Message'
                        WHEN q.QueueType = 2 THEN 'Issuance Closed Message'                 
                        WHEN q.QueueType = 3 THEN 'Correction Message'
                        WHEN q.QueueType = 4 THEN 'Mega Issuance Closed Message'
                        WHEN q.QueueType = 5 THEN 'Remic Issuance Closed Message'
                        WHEN q.QueueType = 6 THEN 'Security Collapsed Message'
                        WHEN q.QueueType = 7 THEN 'Property Inspection Message'
                        WHEN q.QueueType = 8 THEN 'Property Financials Message'
                        WHEN q.QueueType = 9 THEN 'Credit Facility Property Financials Message'
                        WHEN q.QueueType = 10 THEN 'Credit Facility Deal Financials Message'
                        WHEN q.QueueType = 11 THEN 'Collateral Change Message'
                        WHEN q.QueueType = 12 THEN 'Loan Servicing Message'
                        WHEN q.QueueType = 13 THEN 'Security Servicing Message'
                        WHEN q.QueueType = 14 THEN 'Security Dissolve Message'
                        WHEN q.QueueType = 15 THEN 'Pool Terminated Message'
                        WHEN q.QueueType = 16 THEN 'Property Identifier Updated Message'
                        WHEN q.QueueType = 17 THEN 'Credit Facility Deal Update Message'
                        WHEN q.QueueType = 18 THEN 'Credit Facility Deal Update Correction Message'
                        WHEN q.QueueType = 19 THEN 'Credit Facility Property Update Message'
                        WHEN q.QueueType = 20 THEN 'Doc Hold Criteria Upsert'
                        WHEN q.QueueType = 21 THEN 'Hold Criteria Upsert'
                        WHEN q.QueueType = 22 THEN 'Doc Hold Cleared'
                        WHEN q.QueueType = 23 THEN 'Hold Criteria Cleared'
                        WHEN q.QueueType = 24 THEN 'Additional Disclosure Required'
                        WHEN q.QueueType = 25 THEN 'Return To Hold'
                        WHEN q.QueueType = 26 THEN 'Document Version Upsert'
                        WHEN q.QueueType = 27 THEN 'Correction Review Complete'
                        WHEN q.QueueType = 28 THEN 'Refresh Stickering Queue'
                        WHEN q.QueueType = 29 THEN 'Banner Upsert'
                        WHEN q.QueueType = 30 THEN 'Banner Cleared'
                        WHEN q.QueueType = 31 THEN 'Finish Event Handler'
                        WHEN q.QueueType = 32 THEN 'Finish CF Event Handler'
                        WHEN q.QueueType = 33 THEN 'Publish At Issuance'
                        WHEN q.QueueType = 34 THEN 'Process CREFC'
                        WHEN q.QueueType = 35 THEN 'Publish Ongoing CREFC'
                        WHEN q.QueueType = 36 THEN 'Publish Correction'
                        WHEN q.QueueType = 37 THEN 'Publish On Demand 14'
                        WHEN q.QueueType = 38 THEN 'Publish On Demand 17'
                        WHEN q.QueueType = 39 THEN 'Publish On Demand 19'
                        WHEN q.QueueType = 40 THEN 'Issue Supplement'
                        
                        ELSE 'UNKNOWN'
                    END,
    [Status] =  CASE
                    WHEN q.[Status] = 0 THEN 'Pending'
                    WHEN q.[Status] = 1 THEN 'InProgress'
                    WHEN q.[Status] = 2 THEN 'Cancelled'
                    WHEN q.[Status] = 3 THEN 'Error'
                    WHEN q.[Status] = 4 THEN 'Fatal'
                    WHEN q.[Status] = 5 THEN 'Completed'
                    WHEN q.[Status] = 6 THEN 'Stopped'
                    ELSE 'UNKOWN'
                END,
    [Completed On] = q.CompletedOn, 
    [Created On] = q.CreatedOn,
    [Internal State Id] = s.Id,
    [Internal Step] = s.Name,
    [Internal Step Id] = s.InternalStateId,
    [Internale Step RecordId] = s.RecordId,
    [Internale Step PoolId] = sdc.[12958_MBSPoolIdentifier],
    [Status] =  CASE 
                    WHEN s.[Status] = 0 THEN 'Pending'
                    WHEN s.[Status] = 1 THEN 'InProgress'
                    WHEN s.[Status] = 2 THEN 'Canceled'
                    WHEN s.[Status] = 3 THEN 'Error'
                    WHEN s.[Status] = 4 THEN 'Fatal'
                    WHEN s.[Status] = 5 THEN 'Completed'
                    ELSE 'UNKOWN'
                END,
    [Error Message] = s.ErrorMessage,
    [Error Stack] = s.ErrorStack,
    [Completed on] = s.CompletedOn,
    [Created On] = s.CreatedOn
from
    adm.BEEQueue q
    LEFT OUTER JOIN adm.BEInternalState s on s.BEEQueueId = q.Id
    LEFT OUTER JOIN FmData.v_Security_Denormalized sdp ON sdp.SecurityId = q.RecordId
    LEFT OUTER JOIN FmData.v_Security_Denormalized sdc ON sdc.SecurityId = s.RecordId
WHERE
    q.[Status] in (0,1,2,3,4,5) /*AND q.QueueType = 2 and (q.RecordId in (917)) OR s.RecordId in (412262,412263,412264,412259))*/
ORDER BY
    q.CreatedOn DESC, q.id, s.Id