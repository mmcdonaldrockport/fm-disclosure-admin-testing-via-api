SELECT TOP 1 * FROM import.MegaCollateral_Source1

SELECT * FROM import.Security_Source1 ss JOIN (
	SELECT s.MBSPoolIdentifier
	FROM 
		import.RemicCollateral_Source s 
		LEFT JOIN import.RemicCollateral_Source1 s1
			ON s.MBSPoolIdentifier = s1.MBSPoolIdentifier 
			AND s.SecurityCUSIPNumber = s1.SecurityCUSIPNumber
	WHERE
		s1.MBSPoolIdentifier IS NULL
	) rc ON ss.MBSPoolIdentifier = rc.MBSPoolIdentifier



SELECT * FROM import.Security_Source1 ss JOIN (
	SELECT s.MBSPoolIdentifier
	FROM 
		import.MegaCollateral_Source s 
		LEFT JOIN import.MegaCollateral_Source1 s1
			ON s.MBSPoolIdentifier = s1.MBSPoolIdentifier 
			AND s.SecurityCUSIPNumber = s1.SecurityCUSIPNumber
			AND s.SecurityMEGACUSIPNumber = s1.SecurityMEGACUSIPNumber
	WHERE
		s1.MBSPoolIdentifier IS NULL
	) mc ON ss.MBSPoolIdentifier = mc.MBSPoolIdentifier

SELECT * FROM import.Security_Source1 ss JOIN (
	SELECT s.SecurityCUSIPNumber
	FROM 
		import.MegaCollateral_Source s 
		LEFT JOIN import.MegaCollateral_Source1 s1
			ON s.MBSPoolIdentifier = s1.MBSPoolIdentifier 
			AND s.SecurityCUSIPNumber = s1.SecurityCUSIPNumber
			AND s.SecurityMEGACUSIPNumber = s1.SecurityMEGACUSIPNumber
	WHERE
		s1.SecurityCUSIPNumber IS NULL
	) mc ON ss.SecurityCUSIPNumber = mc.SecurityCUSIPNumber



SELECT TOP 1 * FROM import.Security_Source1
	SELECT 
		s.SecurityCUSIPNumber, s.MBSPoolIdentifier   
	FROM 
		import.Security_Source s 
		LEFT JOIN import.Security_Source1 s1
			ON s.MBSPoolIdentifier = s1.MBSPoolIdentifier 
			AND s.SecurityCUSIPNumber = s1.SecurityCUSIPNumber
	WHERE
		s1.SecurityCUSIPNumber IS NULL

SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Security'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'LoanPrepayment'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Deal'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'LoanDelinquency'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'RemicCollateral' AND LastUpdateDateTime < '2017-05-03'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'LoanToPropertyRelationship'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Mega'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Remic'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'PropertyOngoingFinancial'
SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Deal'

SELECT * FROM import.Security_Source WHERE MBSPoolIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Security')
SELECT * FROM import.LoanPrepayment_Source WHERE LoanIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Loan')
SELECT FannieMaeLoanNumber, LoanIdentifier  FROM import.Loan_Source WHERE LoanIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Loan')
SELECT DealIdentifier, *  FROM import.Deal_Source WHERE DealIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Deal')
SELECT FannieMaeLoanNumber, LoanIdentifier, DataAsOfDate  FROM import.LoanDelinquency_Source WHERE LoanIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'LoanDelinquency')
SELECT * FROM import.LoanToPropertyRelationship_Source WHERE LoanIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'LoanToPropertyRelationship')
SELECT * FROM import.Mega_Source WHERE SecurityMEGACUSIPNumber IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Mega')
SELECT DISTINCT SecurityCusipNumber, SecurityREMICGroupIdentifier, TrustName 
	FROM import.RemicCollateral_Source WHERE MBSPoolIdentifier IN (
		SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'RemicCollateral'
			AND LastUpdateDateTime < '2017-05-03'
		)

SELECT * FROM import.DealMonitoring_Source WHERE DealIdentifier IN (SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Deal')

SELECT 
	PropertyIdentifier,
	CollateralIdentifier,
	PropertyFinancialStatementFiscalYear, 
	PropertyFiscalPeriodType
 FROM import.PropertyOngoingFinancials_Source WHERE PropertyIdentifier IN ( SELECT IdentifierValue FROM import.SanityChecksResults WHERE ValidationSource = 'Property')

SELECT * FROM import.Loan_Source WHERE LoanIdentifier = 100541

SELECT * FROM import.Security_Source WHERE MBSPoolIdentifier = 'AN3525'

SELECT * FROM import.Deal_SOURCE WHERE DealIdentifier = 30216

SELECT * FROM adm.TransferMessageDetail WHERE ActionName Like 'Load_Security%'
SELECT ActionName, ContainerName, Message, count(*) FROM adm.TransferMessageDetail
GROUP BY ActionName, ContainerName, Message

SELECT * FROM import.SanityChecksResults WHERE ValidationSource = 'Loan'

SELECT COUNT(*) FROM import.PropertyOngoingFinancials_Source1;
SELECT TOP 1 * FROM import.PropertyOngoingFinancials_Source1;
SELECT DISTINCT 
	PropertyIdentifier,
	CollateralIdentifier,
	PropertyFinancialStatementFiscalYear,
	PropertyFiscalPeriodType


FROM import.PropertyOngoingFinancials_Source1;
	