SELECT 
ISNULL(CONVERT(varchar(MAX),PropertyIdentifier),'') AS PropertyIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyMonitoringFinancialStatementFiscalYear),'') AS PropertyMonitoringFinancialStatementFiscalYear,
ISNULL(CONVERT(varchar(MAX),PropertyMonitoringFiscalPeriodType),'') AS PropertyMonitoringFiscalPeriodType,
ISNULL(CONVERT(varchar(MAX),PropertyMonitoringLevelTrailingTwelveNetOperatingIncomeAmount),'') AS PropertyMonitoringLevelTrailingTwelveNetOperatingIncomeAmount,
ISNULL(CONVERT(varchar(MAX),PropertyMonitoringLevelTrailingThreeNetOperatingIncomeAmount),'') AS PropertyMonitoringLevelTrailingThreeNetOperatingIncomeAmount,
ISNULL(CONVERT(varchar(MAX),PropertyMonitoringFinancialFormTypeCode),'') AS PropertyMonitoringFinancialFormTypeCode,
ISNULL(CONVERT(varchar(MAX),PropertyActivityType),'') AS PropertyActivityType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityDatetime),'') AS PropertyActivityDatetime,
ISNULL(CONVERT(varchar(MAX),PropertyActivityAcquisitionIndicator),'') AS PropertyActivityAcquisitionIndicator


FROM import.PropertyMonitoring_SOURCE1
 ORDER BY 
PropertyIdentifier,
PropertyMonitoringFinancialStatementFiscalYear,
PropertyMonitoringFiscalPeriodType,
PropertyActivityDatetime




