-- File new in FRD 3.15
SELECT 
ISNULL(CONVERT(varchar(MAX),DealIdentifier),'') AS DealIdentifier,
ISNULL(CONVERT(varchar(MAX),FannieMaeLoanNumber),'') AS FannieMaeLoanNumber, --new in FRD 3.15
ISNULL(CONVERT(varchar(MAX),DefeasedStatus),'') AS DefeasedStatus --new in FRD 3.15
 FROM import.Defeasance_Source1 
 ORDER BY 
FannieMaeLoanNumber