SELECT 
ISNULL(CONVERT(varchar(MAX),PropertyIdentifier),'') AS PropertyIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyMostRecentNCFAmount),'') AS PropertyMostRecentNCFAmount,
ISNULL(CONVERT(varchar(MAX),CollateralIdentifier),'') AS CollateralIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialStatementPeriodStartDate),'') AS PropertyFinancialStatementPeriodStartDate,
ISNULL(CONVERT(varchar(MAX),PropertyPrecedingFiscalYearPhysicalOccupancyPercent),'') AS PropertyPrecedingFiscalYearPhysicalOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),PropertyMostRecentNCFtoDSCRFactor),'') AS PropertyMostRecentNCFtoDSCRFactor,
ISNULL(CONVERT(varchar(MAX),PropertyPrecedingFiscalYearDSCRNCFFactor),'') AS PropertyPrecedingFiscalYearDSCRNCFFactor,
ISNULL(CONVERT(varchar(MAX),PropertyPrecedingFiscalYearNCFAmount),'') AS PropertyPrecedingFiscalYearNCFAmount,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialSecondPrecedingFiscalYearDSCRNCFFactor),'') AS PropertyFinancialSecondPrecedingFiscalYearDSCRNCFFactor,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialSecondPrecedingFiscalYearNCFAmount),'') AS PropertyFinancialSecondPrecedingFiscalYearNCFAmount,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialSecondPrecedingFiscalYearPhysicalOccupancyPercent),'') AS PropertyFinancialSecondPrecedingFiscalYearPhysicalOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialStatementPeriodEndDate),'') AS PropertyFinancialStatementPeriodEndDate,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialStatementFiscalYear),'') AS PropertyFinancialStatementFiscalYear,
ISNULL(CONVERT(varchar(MAX),PropertyFiscalPeriodType),'') AS PropertyFiscalPeriodType,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialFormTypeCode),'') AS PropertyFinancialFormTypeCode,
ISNULL(CONVERT(varchar(MAX),PropertyCurrentPhysicalOccupancyPercent),'') AS PropertyCurrentPhysicalOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),PropertyActivityType),'') AS PropertyActivityType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityDatetime),'') AS PropertyActivityDatetime,
ISNULL(CONVERT(varchar(MAX),PropertyActivityAcquisitionIndicator),'') AS PropertyActivityAcquisitionIndicator 
FROM import.PropertyFinancialsTerminated_SOURCE1
 ORDER BY 
PropertyIdentifier,
CollateralIdentifier,
PropertyFinancialStatementFiscalYear,
PropertyFiscalPeriodType,
PropertyActivityDateTime


