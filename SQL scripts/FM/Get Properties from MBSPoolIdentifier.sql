-- USE DisclosureAdmin4;

SELECT p.* FROM fmdata.CollateralPool cp 
	JOIN fmdata.Loan l ON cp.Id = l.CollateralPoolId
	JOIN fmdata.LoanCollateral lc ON l.Id = lc.LoanId
	JOIN fmdata.Property p ON lc.CollateralId = p.Id
WHERE MBSPoolIdentifier = 'AN0097';