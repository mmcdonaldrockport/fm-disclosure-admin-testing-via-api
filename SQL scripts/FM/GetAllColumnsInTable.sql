DECLARE 
	@tableName VARCHAR(MAX),
	@schemaName VARCHAR(MAX);

SET @tableName = 'PropertyOngoingFinancials_SOURCE1';
SET @schemaName = 'import';

SELECT s.[name] 'Schema'
	,t.[name] 'Table'
	,c.[name] 'Column'
	,d.[name] 'Data Type'
	,c.[is_nullable] 'Is Nullable'
FROM sys.schemas s
INNER JOIN sys.tables t ON s.schema_id = t.schema_id
INNER JOIN sys.columns c ON t.object_id = c.object_id
INNER JOIN sys.types d ON c.user_type_id = d.user_type_id
WHERE t.NAME LIKE @tableName AND s.name LIKE @schemaName
--AND c.is_nullable = 0
ORDER BY s.NAME
	,t.NAME
	,c.column_id
	,c.NAME