SELECT 
ISNULL(CONVERT(varchar(MAX),FannieMaeLoanIdentifier),'') AS FannieMaeLoanIdentifier,
ISNULL(CONVERT(varchar(MAX),LoanIdentifier),'') AS LoanIdentifier,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentProtectionType),'') AS LoanPrepaymentProtectionType,
ISNULL(CONVERT(varchar(MAX),LoanDecliningPrepaymentPremiumType),'') AS LoanDecliningPrepaymentPremiumType,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentProtectionEndDate),'') AS LoanPrepaymentProtectionEndDate,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentPremiumType),'') AS LoanPrepaymentPremiumType,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentProtectionTerm),'') AS LoanPrepaymentProtectionTerm,
ISNULL(CONVERT(varchar(MAX),LoanOtherPrepaymentPremiumDescription),'') AS LoanOtherPrepaymentPremiumDescription,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentPremiumTerm),'') AS LoanPrepaymentPremiumTerm,
ISNULL(CONVERT(varchar(MAX),LoanPrepaymentPremiumEndDate),'') AS LoanPrepaymentPremiumEndDate


FROM import.LoanPrepayment_Source1
--WHERE LoanOtherPrepaymentPremiumDescription LIKE '180%'
ORDER BY
FannieMaeLoanIdentifier,
LoanIdentifier,
LoanPrepaymentProtectionType,
LoanPrepaymentProtectionEndDate

