SELECT 
ISNULL(CONVERT(varchar(MAX),PropertyAffordableHousingType),'') AS PropertyAffordableHousingType,
ISNULL(CONVERT(varchar(MAX),PropertyAcquisitionAddressCountyName),'') AS PropertyAcquisitionAddressCountyName,
ISNULL(CONVERT(varchar(MAX),PropertyAcquisitionStreetAddressText),'') AS PropertyAcquisitionStreetAddressText,
ISNULL(CONVERT(varchar(MAX),PropertyAgeRestrictedIndicator),'') AS PropertyAgeRestrictedIndicator,
ISNULL(CONVERT(varchar(MAX),PropertyDwellingUnitIncomeorRentRestrictionPercent),'') AS PropertyDwellingUnitIncomeorRentRestrictionPercent,
ISNULL(CONVERT(varchar(MAX),PropertyHousingAssistanceProgramRemainingTerm),'') AS PropertyHousingAssistanceProgramRemainingTerm,
ISNULL(CONVERT(varchar(MAX),PropertyIdentifier),'') AS PropertyIdentifier,
ISNULL(CONVERT(varchar(MAX),GeographicMetropolitanStatisticalAreaCode),'') AS GeographicMetropolitanStatisticalAreaCode,
ISNULL(CONVERT(varchar(MAX),PropertyBuiltYear),'') AS PropertyBuiltYear,
ISNULL(CONVERT(varchar(MAX),PropertyAddressStateCode),'') AS PropertyAddressStateCode,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenPhysicalOccupancyPercent),'') AS PropertyUnderwrittenPhysicalOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),PropertyTaxEscrowedIndicator),'') AS PropertyTaxEscrowedIndicator,
ISNULL(CONVERT(varchar(MAX),PropertyGreenBuildingCertificationType),'') AS PropertyGreenBuildingCertificationType,
ISNULL(CONVERT(varchar(MAX),PropertyTerrorismInsuranceCoverageIndicator),'') AS PropertyTerrorismInsuranceCoverageIndicator,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenValueAmount),'') AS PropertyUnderwrittenValueAmount,
ISNULL(CONVERT(varchar(MAX),CollateralIdentifier),'') AS CollateralIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyAddressPostalCode),'') AS PropertyAddressPostalCode,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenTotalOperatingExpenseAmount),'') AS PropertyUnderwrittenTotalOperatingExpenseAmount,
ISNULL(CONVERT(varchar(MAX),PropertyFinancialStatementTypeCode),'') AS PropertyFinancialStatementTypeCode,
ISNULL(CONVERT(varchar(MAX),PropertyAffordableUnit50PercentAreaMedianIncomePercent),'') AS PropertyAffordableUnit50PercentAreaMedianIncomePercent,
ISNULL(CONVERT(varchar(MAX),PropertyAffordableUnit60PercentAreaMedianIncomePercent),'') AS PropertyAffordableUnit60PercentAreaMedianIncomePercent,
ISNULL(CONVERT(varchar(MAX),PropertyAffordableUnit80PercentAreaMedianIncomePercent),'') AS PropertyAffordableUnit80PercentAreaMedianIncomePercent,
ISNULL(CONVERT(varchar(MAX),PropertyGeographicMetropolitanStatisticalAreaDesc),'') AS PropertyGeographicMetropolitanStatisticalAreaDesc,
ISNULL(CONVERT(varchar(MAX),PropertyLandOwnershipRightType),'') AS PropertyLandOwnershipRightType,
ISNULL(CONVERT(varchar(MAX),PropertyValuationAsofDate),'') AS PropertyValuationAsofDate,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenEconomicOccupancyPercent),'') AS PropertyUnderwrittenEconomicOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenEffectiveGrossIncomeAmount),'') AS PropertyUnderwrittenEffectiveGrossIncomeAmount,
ISNULL(CONVERT(varchar(MAX),PropertyUnderwrittenNCFAmount),'') AS PropertyUnderwrittenNCFAmount,
ISNULL(CONVERT(varchar(MAX),PropertyAddressCityName),'') AS PropertyAddressCityName,
ISNULL(CONVERT(varchar(MAX),PropertyAcquisitionTotalUnitCount),'') AS PropertyAcquisitionTotalUnitCount,
ISNULL(CONVERT(varchar(MAX),CollateralReferenceNumber),'') AS CollateralReferenceNumber,
ISNULL(CONVERT(varchar(MAX),PropertyCurrentName),'') AS PropertyCurrentName,
ISNULL(CONVERT(varchar(MAX),PropertyLinkageIdentifier),'') AS PropertyLinkageIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertySpecificPropertyType),'') AS PropertySpecificPropertyType,
ISNULL(CONVERT(varchar(MAX),PropertyIdentifier2),'') AS PropertyIdentifier2,
ISNULL(CONVERT(varchar(MAX),PropertyAddressIdentifier),'') AS PropertyAddressIdentifier,
ISNULL(CONVERT(varchar(MAX),PropertyAffordableUnitAMIType),'') AS PropertyAffordableUnitAMIType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityType),'') AS PropertyActivityType,
ISNULL(CONVERT(varchar(MAX),PropertyActivityDatetime),'') AS PropertyActivityDatetime,
ISNULL(CONVERT(varchar(MAX),PropertyActivityAcquisitionIndicator),'') AS PropertyActivityAcquisitionIndicator,
'x' as X
FROM import.Property_Source1


