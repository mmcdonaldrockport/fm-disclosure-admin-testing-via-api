SELECT 
ISNULL(CONVERT(varchar(MAX),DealIdentifier),'') AS DealIdentifier,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelEconomicOccupancyPercent),'') AS DealMonitoringLevelEconomicOccupancyPercent,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelCurrentLoanToValueFactor),'') AS DealMonitoringLevelCurrentLoanToValueFactor,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelTrailingTwelveDebtServiceCoverageRatioRate),'') AS DealMonitoringLevelTrailingTwelveDebtServiceCoverageRatioRate,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelTrailingTwelveNetCashFlowAmount),'') AS DealMonitoringLevelTrailingTwelveNetCashFlowAmount,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelTrailingThreeDebtServiceCoverageRatioRate),'') AS DealMonitoringLevelTrailingThreeDebtServiceCoverageRatioRate,
ISNULL(CONVERT(varchar(MAX),DealMonitoringLevelTrailingThreeNetCashFlowAmount),'') AS DealMonitoringLevelTrailingThreeNetCashFlowAmount,
ISNULL(CONVERT(varchar(MAX),DealMonitoringFinancialStatementFiscalYear),'') AS DealMonitoringFinancialStatementFiscalYear,
ISNULL(CONVERT(varchar(MAX),DealMonitoringFiscalPeriodType),'') AS DealMonitoringFiscalPeriodType,
ISNULL(CONVERT(varchar(MAX),DealMonitoringFinancialFormTypeCode),'') AS DealMonitoringFinancialFormTypeCode,
ISNULL(CONVERT(varchar(MAX),DealAgreementIdentifier),'') AS DealAgreementIdentifier,
ISNULL(CONVERT(varchar(MAX),DealActivityType),'') AS DealActivityType,
ISNULL(CONVERT(varchar(MAX),DealActivityDatetime),'') AS DealActivityDatetime

 FROM import.DealMonitoring_Source1 
 ORDER BY 
DealIdentifier,
DealAgreementIdentifier,
--DealMonitoringFinancialStatementFiscalYear,
--DealMonitoringFiscalPeriodType,
--DealActivityType,
DealActivityDatetime


