SELECT 
ISNULL(CONVERT(varchar(MAX),SecurityREMICCUSIPNumber),'') AS SecurityREMICCUSIPNumber,
ISNULL(CONVERT(varchar(MAX),SecurityREMICGroupIdentifier),'') AS SecurityREMICGroupIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityREMICClassName),'') AS SecurityREMICClassName,
ISNULL(CONVERT(varchar(MAX),SecuritySettlementDate),'') AS SecuritySettlementDate,
ISNULL(CONVERT(varchar(MAX),SecurityCutOffDateBalanceAmount),'') AS SecurityCutOffDateBalanceAmount,
ISNULL(CONVERT(varchar(MAX),SecurityRemittanceRateorPassThroughRate),'') AS SecurityRemittanceRateorPassThroughRate,
ISNULL(CONVERT(varchar(MAX),SecurityIssueDate),'') AS SecurityIssueDate,
ISNULL(CONVERT(varchar(MAX),SecurityPrefixType),'') AS SecurityPrefixType,
ISNULL(CONVERT(varchar(MAX),SecurityMaturityDate),'') AS SecurityMaturityDate,
ISNULL(CONVERT(varchar(MAX),TrancheCurrentPoolStatusType),'') AS TrancheCurrentPoolStatusType,
ISNULL(CONVERT(varchar(MAX),TrustName),'') AS TrustName,
ISNULL(CONVERT(varchar(MAX),AgreementIdentifier),'') AS AgreementIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityStatusDatetime),'') AS SecurityStatusDatetime

FROM import.Remic_Source1
--WHERE LoanOtherPrepaymentPremiumDescription LIKE '180%'
ORDER BY
SecurityREMICCUSIPNumber,
AgreementIdentifier

SELECT * FROM import.Remic_source1
SecurityRemittanceRateorPassThroughRate
