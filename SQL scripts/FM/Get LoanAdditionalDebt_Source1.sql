SELECT 
ISNULL(CONVERT(varchar(MAX),FannieMaeLoanNumber),'') AS FannieMaeLoanNumber,
ISNULL(CONVERT(varchar(MAX),LoanIdentifier),'') AS LoanIdentifier,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtType),'') AS LoanAdditionalDebtType,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtBalanceAmount),'') AS LoanAdditionalDebtBalanceAmount,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtOriginalMonthlyPaymentAmount),'') AS LoanAdditionalDebtOriginalMonthlyPaymentAmount,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtMezzanineFinancingType),'') AS LoanAdditionalDebtMezzanineFinancingType,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineAcquisitionUPBAmount),'') AS LoanMezzanineAcquisitionUPBAmount,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtLienPriorityType),'') AS LoanAdditionalDebtLienPriorityType,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtFannieMaePoolNumber),'') AS LoanAdditionalDebtFannieMaePoolNumber,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtFannieMaeLoanNumber),'') AS LoanAdditionalDebtFannieMaeLoanNumber,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineMonthlyPaymentAmount),'') AS LoanMezzanineMonthlyPaymentAmount,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtLienHolderName),'') AS LoanAdditionalDebtLienHolderName,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtCurrentInterestRate),'') AS LoanAdditionalDebtCurrentInterestRate,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtMaximumMonthlyPayment),'') AS LoanAdditionalDebtMaximumMonthlyPayment, -- new in FRD 3.15
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtARMMarginPercent),'') AS LoanAdditionalDebtARMMarginPercent,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtMinimumInterestRatePercent),'') AS LoanAdditionalDebtMinimumInterestRatePercent,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtMaturityDate),'') AS LoanAdditionalDebtMaturityDate,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtOriginalAmortizationTerm),'') AS LoanAdditionalDebtOriginalAmortizationTerm,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtBalloonIndicator),'') AS LoanAdditionalDebtBalloonIndicator,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtLastInterestOnlyEndDate),'') AS LoanAdditionalDebtLastInterestOnlyEndDate,
ISNULL(CONVERT(varchar(MAX),LoanAdditionalDebtLineOfCreditFullAmount),'') AS LoanAdditionalDebtLineOfCreditFullAmount, -- new in FRD 3.15
ISNULL(CONVERT(varchar(MAX),LoanMezzanineProviderName),'') AS LoanMezzanineProviderName,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineInterestAccrualMethodType),'') AS LoanMezzanineInterestAccrualMethodType,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineInitialTermMaturityDate),'') AS LoanMezzanineInitialTermMaturityDate,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineFirstPaymentDueDate),'') AS LoanMezzanineFirstPaymentDueDate,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineInitialTermInterestRatePercent),'') AS LoanMezzanineInitialTermInterestRatePercent,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineAmortizationTerm),'') AS LoanMezzanineAmortizationTerm,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineInterestOnlyEndDate),'') AS LoanMezzanineInterestOnlyEndDate,
ISNULL(CONVERT(varchar(MAX),LoanMezzanineDecliningPrepaymentPremiumType),'') AS LoanMezzanineDecliningPrepaymentPremiumType,
ISNULL(CONVERT(varchar(MAX),LoanMezzaninePrepaymentEndDate),'') AS LoanMezzaninePrepaymentEndDate,
ISNULL(CONVERT(varchar(MAX),LoanMezzaninePrepaymentTerm),'') AS LoanMezzaninePrepaymentTerm,
ISNULL(CONVERT(varchar(MAX),CDSAdditionalLoanIdentifier),'') AS CDSAdditionalLoanIdentifier,
ISNULL(CONVERT(varchar(MAX),LoanMezzaninePrepaymentProtectionType),'') AS LoanMezzaninePrepaymentProtectionType



FROM import.LoanAdditionalDebt_Source1
--WHERE LoanOtherPrepaymentPremiumDescription LIKE '180%'
--ORDER BY





