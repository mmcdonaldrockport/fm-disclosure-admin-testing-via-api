
SELECT * FROM import.PropertyFinancialsTerminated_Source1

SELECT 
	MBSPoolIdentifier, 
	SecurityCUSIPIdentifier,
	ProductGroupType,
	p.PropertyIdentifier
FROM 
	fmdata.Property p 
	JOIN fmdata.LoanCollateral lc ON lc.CollateralId = p.Id
	JOIN fmdata.Loan l ON l.Id = lc.LoanId
	JOIN fmdata.CollateralPool cp ON l.CollateralPoolId = cp.Id
	JOIN fmdata.Security s ON s.Id = cp.Id
	JOIN fmdata.ConvertedTerminatedPropertyFinancialStatement ctpfs ON ctpfs.PropertyIdentifier = p.PropertyIdentifier
WHERE s.ProductGroupType = 'MBS'
	
