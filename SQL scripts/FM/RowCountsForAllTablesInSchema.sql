DECLARE @schemaName VARCHAR(MAX);

SET @schemaName = 'ext';

SELECT      
	DISTINCT SCHEMA_NAME(A.schema_id) + '.' + A.Name as Name, 
	SUM(B.rows) AS Rows
	--, RIGHT(A.Name,LEN(A.Name) - CHARINDEX('_',A.Name,CHARINDEX('_',A.Name,0)+1))
FROM        sys.objects A
INNER JOIN sys.partitions B ON A.object_id = B.object_id
WHERE       A.type = 'U' 
	AND  SCHEMA_NAME(A.schema_id) = @schemaName
	AND B.index_id = 1
	AND RIGHT(A.Name,7) <> '_Source'
	AND RIGHT(A.Name,8) <> '_Source1'
	--AND len(A.Name)-len(replace(A.Name,'_','')) > 1
	--AND LEN(RIGHT(A.Name,LEN(A.Name) - CHARINDEX('_',A.Name,CHARINDEX('_',A.Name,0)+1))) = 32
GROUP BY    A.schema_id, A.Name;

SELECT * FROM (
SELECT      
	DISTINCT SCHEMA_NAME(A.schema_id) + '.' + A.Name as Name, 
	SUM(B.rows) AS Rows
	--, RIGHT(A.Name,LEN(A.Name) - CHARINDEX('_',A.Name,CHARINDEX('_',A.Name,0)+1))
FROM        sys.objects A
INNER JOIN sys.partitions B ON A.object_id = B.object_id
WHERE       A.type = 'U' 
	AND B.index_id = 1
	AND  SCHEMA_NAME(A.schema_id) = @schemaName
	AND RIGHT(A.Name,7) <> '_Source'
	AND RIGHT(A.Name,8) <> '_Source1'
	--AND len(A.Name)-len(replace(A.Name,'_','')) > 1
	--AND LEN(RIGHT(A.Name,LEN(A.Name) - CHARINDEX('_',A.Name,CHARINDEX('_',A.Name,0)+1))) = 32
GROUP BY    A.schema_id, A.Name) B
WHERE B.ROWs > 0

