
SELECT 
	td.Name as Message, 
	--TransferDefinitionId, 
	Min(DATEDIFF(MS, DateTimeStarted, DateTimeEnded)) as minMS, 
	AVG(DATEDIFF(MS, DateTimeStarted, DateTimeEnded)) as avgMS,
	Max(DATEDIFF(MS, DateTimeStarted, DateTimeEnded)) as maxMS,
	FORMAT(ROUND(AVG(CONVERT(NUMERIC(14,0), DATEDIFF(MS, DateTimeStarted, DateTimeEnded)))/3600,2),'0.00') as HoursPer1000
FROM 
	adm.TransferExecution te 
	JOIN adm.TransferQueue tq on tq.Id = te.TransferQueueId
	JOIN adm.TransferDefinition TD on TD.Id = tq.TransferDefinitionId
WHERE Status = 4 AND tq.TransferDefinitionId in (3, 8, 9, 13, 14)
GROUP BY 
	td.Name
ORDER BY
	TD.Name
	--TransferDefinitionId


