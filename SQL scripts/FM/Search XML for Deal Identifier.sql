USE DisclosureAdmin;

WITH XMLNAMESPACES( 'http://www.fanniemae.com/multifamily/integrated/cdf/v1.x' AS "ns1")
select * 
from adm.TransferFile
where cast(contents.query('//ns1:DealIdentifier/text()') as nvarchar(20)) = '7041'