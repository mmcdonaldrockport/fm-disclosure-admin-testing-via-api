USE QA_77_Rockport;

DECLARE @colName VARCHAR(MAX);

SET @colName = '%footnote%'
SELECT s.[name] 'Schema'
	,t.[name] 'Table'
	,c.[name] 'Column'
	,d.[name] 'Data Type'
	,d.[max_length] 'Max Length'
	,d.[precision] 'Precision'
	,c.[is_identity] 'Is Id'
	,c.[is_nullable] 'Is Nullable'
	,c.[is_computed] 'Is Computed'
	,d.[is_user_defined] 'Is UserDefined'
	,t.[modify_date] 'Date Modified'
	,t.[create_date] 'Date created'
FROM sys.schemas s
INNER JOIN sys.tables t ON s.schema_id = t.schema_id
INNER JOIN sys.columns c ON t.object_id = c.object_id
INNER JOIN sys.types d ON c.user_type_id = d.user_type_id
WHERE c.NAME LIKE @colName 

--order by t.NAME

UNION ALL

SELECT s.[name] 'Schema'
	,t.[name] 'Table'
	,c.[name] 'Column'
	,d.[name] 'Data Type'
	,d.[max_length] 'Max Length'
	,d.[precision] 'Precision'
	,c.[is_identity] 'Is Id'
	,c.[is_nullable] 'Is Nullable'
	,c.[is_computed] 'Is Computed'
	,d.[is_user_defined] 'Is UserDefined'
	,t.[modify_date] 'Date Modified'
	,t.[create_date] 'Date created'
FROM sys.schemas s
INNER JOIN sys.views t ON s.schema_id = t.schema_id
INNER JOIN sys.columns c ON t.object_id = c.object_id
INNER JOIN sys.types d ON c.user_type_id = d.user_type_id
WHERE c.NAME LIKE @colName
--AND c.is_nullable = 0
ORDER BY s.NAME
	,t.NAME
	,c.NAME

select * from 
INFORMATION_SCHEMA.COLUMNS
where DATA_TYPE in('varchar','nvarchar') AND COLUMN_NAME like '%comment%'

select * from 
INFORMATION_SCHEMA.COLUMNS
where DATA_TYPE in('varchar','nvarchar') AND COLUMN_NAME like '%comment%' AND Character_maximum_length = -1 AND COLUMN_NAME = 'WATCHLISTCOMMENTS'

select 'SELECT ' + TABLE_NAME + '.' + COLUMN_NAME + ', MAX(LEN(' + COLUMN_NAME + ')) FROM ' + TABLE_NAME + ' GROUP BY ' + COLUMN_NAME from 
INFORMATION_SCHEMA.COLUMNS
where DATA_TYPE in('varchar','nvarchar') AND COLUMN_NAME like '%comment%' AND Character_maximum_length = -1 AND TABLE_NAME = 'Loans'

SELECT RiskRatings.RatingsOtherComments, MAX(LEN(RatingsOtherComments)) FROM RiskRatings GROUP BY RatingsOtherComments

SELECT * FROM sys.columns

SELECT indexdescription FROM Loans