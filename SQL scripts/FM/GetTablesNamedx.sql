DECLARE @colName VARCHAR(MAX);

SET @colName = '%AdditionalDisclosureIndicator%'

SELECT s.[name] 'Schema'
	,t.[name] 'Table'
	
FROM sys.schemas s
INNER JOIN sys.tables t ON s.schema_id = t.schema_id
--INNER JOIN sys.columns c ON t.object_id = c.object_id
--INNER JOIN sys.types d ON c.user_type_id = d.user_type_id
WHERE t.NAME LIKE '%master'
--AND c.is_nullable = 0
ORDER BY s.NAME
	,t.NAME
