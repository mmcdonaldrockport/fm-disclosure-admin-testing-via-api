SELECT 
ISNULL(CONVERT(varchar(MAX),MBSPoolIdentifier),'') AS MBSPoolIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityCUSIPNumber),'') AS SecurityCUSIPNumber,
ISNULL(CONVERT(varchar(MAX),SecurityMEGACUSIPNumber),'') AS SecurityMEGACUSIPNumber,
ISNULL(CONVERT(varchar(MAX),SecurityAtIssuanceCollateralParticipationUPBAmount),'') AS SecurityAtIssuanceCollateralParticipationUPBAmount,
ISNULL(CONVERT(varchar(MAX),SecurityParticipationPercent),'') AS SecurityParticipationPercent,
ISNULL(CONVERT(varchar(MAX),FinancialInstrumentCollateralGroupBalanceEffectiveDate),'') AS FinancialInstrumentCollateralGroupBalanceEffectiveDate
 FROM import.MegaCollateral_Source1 
 --WHERE MBSPoolIdentifier = '070854'
 ORDER BY MBSPoolIdentifier
 

 WHERE MBSPoolIdentifier NOT IN (
  SELECT DISTINCT 
	--PrimaryValue,
	LEFT(PrimaryValue,6) AS MBSPoolIdentifier
	--RIGHT(PrimaryValue,9) AS SecurityMEGACUSIPNumber
	--SUBSTRING(PrimaryValue,8,9) AS SecurityCUSIPNumber
FROM adm.TransferMessageDetail 
where DateTimeCreated > '2017-04-24' AND ActionName = 'Load_MegaCollateral')
ORDER BY MBSPoolIdentifier
 



