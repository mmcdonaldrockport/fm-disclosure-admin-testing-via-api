SELECT 
ISNULL(CONVERT(varchar(MAX),SecurityMEGACUSIPNumber),'') AS SecurityMEGACUSIPNumber,
ISNULL(CONVERT(varchar(MAX),SecurityCutOffDateBalanceAmount),'') AS SecurityCutOffDateBalanceAmount,
ISNULL(CONVERT(varchar(MAX),SecurityFirstPaymentDate),'') AS SecurityFirstPaymentDate,
ISNULL(CONVERT(varchar(MAX),SecurityRemittanceRateorPassThroughRate),'') AS SecurityRemittanceRateorPassThroughRate,
ISNULL(CONVERT(varchar(MAX),SecurityInterestRateType),'') AS SecurityInterestRateType,
ISNULL(CONVERT(varchar(MAX),SecurityIssueDate),'') AS SecurityIssueDate,
ISNULL(CONVERT(varchar(MAX),MBSPoolAtIssuanceWeightedAverageRemainingMaturityTerm),'') AS MBSPoolAtIssuanceWeightedAverageRemainingMaturityTerm,
ISNULL(CONVERT(varchar(MAX),SecurityEndingUnpaidInterestBalanceAmount),'') AS SecurityEndingUnpaidInterestBalanceAmount,
ISNULL(CONVERT(varchar(MAX),SecurityRecordDate),'') AS SecurityRecordDate,
ISNULL(CONVERT(varchar(MAX),SecurityPrefixType),'') AS SecurityPrefixType,
ISNULL(CONVERT(varchar(MAX),SecurityMaturityDate),'') AS SecurityMaturityDate,
ISNULL(CONVERT(varchar(MAX),SecurityAtIssuanceCollateralPoolWeightedAverageCouponRate),'') AS SecurityAtIssuanceCollateralPoolWeightedAverageCouponRate,
ISNULL(CONVERT(varchar(MAX),SecurityCurrentPaydownFactor),'') AS SecurityCurrentPaydownFactor,
ISNULL(CONVERT(varchar(MAX),SecurityMEGACurrentPoolStatusType),'') AS SecurityMEGACurrentPoolStatusType,
ISNULL(CONVERT(varchar(MAX),SecurityMEGAPoolIdentifier),'') AS SecurityMEGAPoolIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityStatusDateTime),'') AS SecurityStatusDateTime
 FROM import.Mega_Source1 


