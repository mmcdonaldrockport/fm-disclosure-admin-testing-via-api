SELECT DISTINCT  
ISNULL(CONVERT(varchar(MAX),MBSPoolIdentifier),'') AS MBSPoolIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityCUSIPNumber),'') AS SecurityCUSIPNumber,
ISNULL(CONVERT(varchar(MAX),SecurityREMICGroupIdentifier),'') AS SecurityREMICGroupIdentifier,
ISNULL(CONVERT(varchar(MAX),SecurityAtIssuanceCollateralParticipationUPBAmount),'') AS SecurityAtIssuanceCollateralParticipationUPBAmount,
--'' as TrancheCurrentPoolStatusType,
--ISNULL(CONVERT(varchar(MAX),TrancheCurrentPoolStatusType),'') AS TrancheCurrentPoolStatusType,
ISNULL(CONVERT(varchar(MAX),TrustName),'') AS TrustName,
ISNULL(CONVERT(varchar(MAX),SecurityParticipationPercent),'') AS SecurityParticipationPercent,
ISNULL(CONVERT(varchar(MAX),FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate),'') AS FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate
FROM import.RemicCollateral_Source1 s 


WHERE MBSPoolIdentifier IN (
SELECT DISTINCT s.MBSPoolIdentifier FROM import.RemicCollateral_Source1 s JOIN 
(SELECT DISTINCT 
	--PrimaryValue, 
	LEFT(PrimaryValue,6) AS MBSPoolIdentifier
	--RIGHT(PrimaryValue,9) AS SecurityCUSIPNumber 
FROM adm.TransferMessageDetail WHERE DateTimeCreated > '2017-04-24' AND ActionName LIKE 'Load_RemicCollateral%') e 
ON s.MBSPoolIdentifier = e.MBSPoolIdentifier )



