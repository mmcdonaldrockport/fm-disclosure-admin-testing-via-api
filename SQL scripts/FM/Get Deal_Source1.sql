SELECT 
ISNULL(CONVERT(varchar(MAX),DealIdentifier),'') AS DealIdentifier,
ISNULL(CONVERT(varchar(MAX),CollateralIdentifier),'') AS CollateralIdentifier,
ISNULL(CONVERT(varchar(MAX),DealMaximumPermittedLTVofCreditFacilityPercent),'') AS DealMaximumPermittedLTVofCreditFacilityPercent,
ISNULL(CONVERT(varchar(MAX),DealAggregateCutOffDateBalanceAmount),'') AS DealAggregateCutOffDateBalanceAmount,
ISNULL(CONVERT(varchar(MAX),DealMCFALoanToValueFactor),'') AS DealMCFALoanToValueFactor, -- new in FRD 3.15
ISNULL(CONVERT(varchar(MAX),CollateralNonPropertyCollateralType),'') AS CollateralNonPropertyCollateralType,
ISNULL(CONVERT(varchar(MAX),CollateralNonPropertyCollateralValueAmount),'') AS CollateralNonPropertyCollateralValueAmount,
ISNULL(CONVERT(varchar(MAX),CDSDealIdentifier),'') AS CDSDealIdentifier
 FROM import.Deal_Source1 
 ORDER BY 
CONVERT(Integer,DealIdentifier),
CollateralIdentifier,
CDSDealIdentifier
/*
SELECT COUNT(*) FROM import.Deal_Source1 
SELECT DISTINCT 
DealIdentifier,
CollateralIdentifier,
CDSDealIdentifier
FROM import.Deal_Source1
WHERE CollateralIdentifier IS NOT NULL
*/
SELECT COUNT(*) FROM fmdata.Deal where  DealIdentifier IS NULL
SELECT COUNT(*) FROM import.Deal_Source1 where DealIdentifier IS NULL

SELECT COUNT(*) FROM import.PoolFactorPlus_Source WHERE MBSPoolIdentifier = 'AN3525'