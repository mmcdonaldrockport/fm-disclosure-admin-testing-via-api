--while not 0 or 1 

SELECT 
--bq.Id AS [bq.Id],
--bq.QueueType AS [bq.QueueType],
--bq.RecordId AS [bq.RecordId],
bq.Status AS [bq.Status],
--bq.Context AS [bq.Context],
--bq.CompletedOn AS [bq.CompletedOn],
MAX(bq.CreatedOn) AS [bq.CreatedOn],
--bq.ErrorMessage AS [bq.ErrorMessage],
--bq.ErrorStack AS [bq.ErrorStack],
--bq.TransferQueueId AS [bq.TransferQueueId],
--bq.IsScheduled AS [bq.IsScheduled],
--bq.Priority AS [bq.Priority],
--bis.Id AS [bis.Id],
--bis.BEEQueueId AS [bis.BEEQueueId],
bis.Status AS [bis.Status],
--bis.CompletedOn AS [bis.CompletedOn],
MAX(bis.CreatedOn) AS [bis.CreatedOn],
--bis.ErrorMessage AS [bis.ErrorMessage],
--bis.ErrorStack AS [bis.ErrorStack],
bis.Name AS [bis.Name],
--bis.Description AS [bis.Description],
bis.InternalStateId AS [bis.InternalStateId],
bis.RecordId AS [bis.RecordId]
FROM 
	adm.BEEQueue bq LEFT OUTER JOIN adm.BEInternalState bis ON (bq.RecordId = bis.RecordId OR bis.RecordId = -1) and bq.Id = bis.BEEQueueId
WHERE
	bq.recordId = (SELECT recordId from adm.BEEQueue where TransferQueueId = 9141)
	AND bis.InternalStateId in (0,6,1,2,13,3,4,5)
	AND bq.status NOT IN (0,1) 
	--AND bis.status NOT IN (0,1)
GROUP BY
--bq.Id,
--bq.QueueType,
--bq.RecordId,
bq.Status,
--bq.Context,
--bq.CompletedOn,
--bq.CreatedOn,
--bq.ErrorMessage,
--bq.ErrorStack,
--bq.TransferQueueId,
--bq.IsScheduled,
--bq.Priority,
--bis.Id,
--bis.BEEQueueId,
bis.Status,
--bis.CompletedOn,
--bis.CreatedOn,
--bis.ErrorMessage,
--bis.ErrorStack,
bis.Name,
--bis.Description,
bis.InternalStateId,
bis.RecordId

--select * from adm.BEInternalState