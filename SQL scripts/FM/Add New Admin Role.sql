USE DisclosureAdmin2;

DECLARE @RoleName VARCHAR(200);
DECLARE @RoleId Int;

SET @RoleName = 'Admin3'

BEGIN TRAN
	BEGIN TRY
		INSERT INTO adm.Roles (
			Name, IsActive, IsInternal
		) VALUES (
			@RoleName, 1, 0
		);

		SELECT @RoleId = Id FROM adm.Roles WHERE Name = @RoleName;

		INSERT INTO adm.AccessRights (RoleId, PageFunctionCode, AllowedAction) 
			VALUES 
				(@RoleId, 2,  2),
				(@RoleId, 3,  2),
				(@RoleId, 4,  2),
				(@RoleId, 5,  3),
				(@RoleId, 6,  3),
				(@RoleId, 7,  3),
				(@RoleId, 8,  3),
				(@RoleId, 9,  3),
				(@RoleId, 10, 3),
				(@RoleId, 11, 3),
				(@RoleId, 12, 2),
				(@RoleId, 13, 3),
				(@RoleId, 14, 3),
				(@RoleId, 15, 3),
				(@RoleId, 16, 3),
				(@RoleId, 17, 2),
				(@RoleId, 18, 2),
				(@RoleId, 19, 2),
				(@RoleId, 20, 1),
				(@RoleId, 21, 1);
		COMMIT TRAN;
		SELECT 'Commit'
	END TRY 
	BEGIN CATCH
		ROLLBACK TRAN
		SELECT 'Rollback'
	END CATCH
 -- End Tran
 
 