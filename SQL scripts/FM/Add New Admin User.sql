USE DisclosureAdmin2;

DECLARE @UserId VARCHAR(200);
DECLARE @RoleName VARCHAR(200);
DECLARE @IntUserId Int;
DECLARE @RoleId Int;

SET @UserId = 'NewUser2';
SET @RoleName = 'Admin';

BEGIN TRAN
	BEGIN TRY
		INSERT INTO adm.Users (
			FirstName, LastName, UserId, LastAccess, Email, IsInternal, PasswordHash
		) VALUES (
			'FirstName' -- FirstName, 
			,'LastName' -- LastName, 
			,@UserId -- UserId, 
			, NULL -- LastAccess, 
			,'email@company.com' -- Email, 
			,1 -- IsInternal, 
			,0x22FA651BE3280A4A06F5A54586C1AD4CEE5D84F8182286A2517E8896BDA084515316BDB8436DF3626EA47C404BF051DD7A0E8A486DD1F8384DF5BF8E53625290 --PasswordHash
		)


		SELECT @IntUserId = Id FROM adm.Users WHERE UserId = @UserId
		SELECT @RoleId = Id FROM adm.Roles WHERE Name = @RoleName
 
		INSERT INTO adm.UserRoles (
			UserId, RoleId
		) VALUES (
			@IntUserId, @RoleId
		);

		COMMIT TRAN;
		SELECT 'Commit'
	END TRY 
	BEGIN CATCH
		ROLLBACK TRAN
		SELECT 'Rollback'
	END CATCH


