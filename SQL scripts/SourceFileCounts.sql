SELECT 
	source.SOURCE_TABLE_NAME as 'Subject', 
	source.SOURCE_TABLE_ROWS as Source, 
	source1.SOURCE_TABLE_ROWS As Source1, 
	source.SOURCE_TABLE_ROWS - source1.SOURCE_TABLE_ROWS As 'Difference'
FROM
(SELECT  'Deal' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Deal_Source
UNION ALL
SELECT  'DealMonitoring' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.DealMonitoring_Source
UNION ALL
SELECT  'Loan' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Loan_Source
UNION ALL
SELECT  'LoanAdditionalDebt' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanAdditionalDebt_Source
UNION ALL
SELECT  'LoanDelinquency' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanDelinquency_Source
UNION ALL
SELECT  'LoanDMBS' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanDMBS_Source
UNION ALL
SELECT  'LoanPrepayment' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanPrepayment_Source
UNION ALL
SELECT  'LoanToPropertyRelationship' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanToPropertyRelationship_Source
UNION ALL
SELECT  'Mega' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Mega_Source
UNION ALL
SELECT  'MegaCollateral' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.MegaCollateral_Source
UNION ALL
SELECT  'PoolFactorPlus' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PoolFactorPlus_Source
UNION ALL
SELECT  'Property' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Property_Source
UNION ALL
SELECT  'PropertyConstructionPhase' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyConstructionPhase_Source
UNION ALL
SELECT  'PropertyFinancialsTerminated' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyFinancialsTerminated_Source
UNION ALL
SELECT  'PropertyInspection' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyInspection_Source
UNION ALL
SELECT  'PropertyMonitoring' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyMonitoring_Source
UNION ALL
SELECT  'PropertyOngoingFinancials' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyOngoingFinancials_Source
UNION ALL
SELECT  'Remic' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Remic_Source
UNION ALL
SELECT  'RemicCollateral' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.RemicCollateral_Source
UNION ALL
SELECT  'Security' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Security_Source) Source   JOIN

(SELECT  'Deal' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Deal_Source1
UNION ALL
SELECT  'DealMonitoring' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.DealMonitoring_Source1
UNION ALL
SELECT  'Loan' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Loan_Source1
UNION ALL
SELECT  'LoanAdditionalDebt' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanAdditionalDebt_Source1
UNION ALL
SELECT  'LoanDelinquency' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanDelinquency_Source1
UNION ALL
SELECT  'LoanDMBS' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanDMBS_Source1
UNION ALL
SELECT  'LoanPrepayment' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanPrepayment_Source1
UNION ALL
SELECT  'LoanToPropertyRelationship' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.LoanToPropertyRelationship_Source1
UNION ALL
SELECT  'Mega' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Mega_Source1
UNION ALL
SELECT  'MegaCollateral' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.MegaCollateral_Source1
UNION ALL
SELECT  'PoolFactorPlus' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PoolFactorPlus_Source1
UNION ALL
SELECT  'Property' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Property_Source1
UNION ALL
SELECT  'PropertyConstructionPhase' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyConstructionPhase_Source1
UNION ALL
SELECT  'PropertyFinancialsTerminated' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyFinancialsTerminated_Source1
UNION ALL
SELECT  'PropertyInspection' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyInspection_Source1
UNION ALL
SELECT  'PropertyMonitoring' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyMonitoring_Source1
UNION ALL
SELECT  'PropertyOngoingFinancials' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.PropertyOngoingFinancials_Source1
UNION ALL
SELECT  'Remic' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Remic_Source1
UNION ALL
SELECT  'RemicCollateral' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.RemicCollateral_Source1
UNION ALL
SELECT  'Security' AS SOURCE_TABLE_NAME,
        COUNT(*) AS SOURCE_TABLE_ROWS
FROM    import.Security_Source1) Source1 
 ON Source.SOURCE_TABLE_NAME = Source1.SOURCE_TABLE_NAME