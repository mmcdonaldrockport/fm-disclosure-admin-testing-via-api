/* Security and Deal Group 1-3 */
SELECT [TableName] = 'FmData.Collateral', COUNT(1) FROM FmData.Collateral UNION
SELECT [TableName] = 'FmData.CollateralPool', COUNT(1) FROM FmData.CollateralPool UNION
SELECT [TableName] = 'FmData.Deal', COUNT(1) FROM FmData.Deal UNION
SELECT [TableName] = 'FmData.DealMonitoring', COUNT(1) FROM FmData.DealMonitoring UNION
SELECT [TableName] = 'FmData.DealStructuredFacilityAtIssuance', COUNT(1) FROM FmData.DealStructuredFacilityAtIssuance UNION
SELECT [TableName] = 'FmData.DealStructuredFacilityCollateral', COUNT(1) FROM FmData.DealStructuredFacilityCollateral UNION
SELECT [TableName] = 'FmData.FinancialInstrumentCollateralGroupBalance', COUNT(1) FROM FmData.FinancialInstrumentCollateralGroupBalance UNION
SELECT [TableName] = 'FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance', COUNT(1) FROM FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance UNION
/* Security and Deal Group 1-3 */

/* Security Group */
SELECT [TableName] = 'FmData.Security', COUNT(1) FROM FmData.Security UNION
SELECT [TableName] = 'FmData.SecurityAtIssuance', COUNT(1) FROM FmData.SecurityAtIssuance UNION
SELECT [TableName] = 'FmData.SecurityFactor', COUNT(1) FROM FmData.SecurityFactor UNION
SELECT [TableName] = 'FmData.SecurityInterestAccrualSchedule', COUNT(1) FROM FmData.SecurityInterestAccrualSchedule UNION
SELECT [TableName] = 'FmData.SecurityInterestRate', COUNT(1) FROM FmData.SecurityInterestRate UNION
SELECT [TableName] = 'FmData.SecurityInterestRateAdjustmentLifetimeTerm', COUNT(1) FROM FmData.SecurityInterestRateAdjustmentLifetimeTerm UNION
SELECT [TableName] = 'FmData.SecurityPayment', COUNT(1) FROM FmData.SecurityPayment UNION
SELECT [TableName] = 'FmData.SecurityRelationship', COUNT(1) FROM FmData.SecurityRelationship UNION
SELECT [TableName] = 'FmData.SecurityStatus', COUNT(1) FROM FmData.SecurityStatus UNION
SELECT [TableName] = 'FmData.StructuredTransactionCollateralGroup', COUNT(1) FROM FmData.StructuredTransactionCollateralGroup UNION
/* Security Group */

/* Loan Group */
SELECT [TableName] = 'FmData.Loan', COUNT(1) FROM FmData.Loan UNION
SELECT [TableName] = 'FmData.LoanAcquisition', COUNT(1) FROM FmData.LoanAcquisition UNION
--SELECT [TableName] = 'FmData.LoanAcquisition2', COUNT(1) FROM FmData.LoanAcquisition2 UNION
SELECT [TableName] = 'FmData.LoanAdditionalDebt', COUNT(1) FROM FmData.LoanAdditionalDebt UNION
--SELECT [TableName] = 'FmData.LoanDelinquencyActivity', COUNT(1) FROM FmData.LoanDelinquencyActivity UNION
SELECT [TableName] = 'FmData.LoanServicingActivity', COUNT(1) FROM FmData.LoanServicingActivity UNION
SELECT [TableName] = 'FmData.LoanMezzaninePrepaymentProtectionTerm', COUNT(1) FROM FmData.LoanMezzaninePrepaymentProtectionTerm UNION
SELECT [TableName] = 'FmData.LoanPrepaymentPremiumTerm', COUNT(1) FROM FmData.LoanPrepaymentPremiumTerm UNION
SELECT [TableName] = 'FmData.LoanPrepaymentProtectionTerm', COUNT(1) FROM FmData.LoanPrepaymentProtectionTerm UNION
--SELECT [TableName] = 'FmData.LoanOtherDecliningPrepaymentSchedule', COUNT(1) FROM FmData.LoanOtherDecliningPrepaymentSchedule UNION -- No insert in import
SELECT [TableName] = 'FmData.LoanCollateral', COUNT(1) FROM FmData.LoanCollateral UNION
/* Loan Group */ 

/* Property Group */
SELECT [TableName] = 'FmData.Collateral', COUNT(1) FROM FmData.Collateral UNION
SELECT [TableName] = 'FmData.Property', COUNT(1) FROM FmData.Property UNION
SELECT [TableName] = 'FmData.PropertyAcquisition', COUNT(1) FROM FmData.PropertyAcquisition UNION
SELECT [TableName] = 'FmData.PropertyAddress', COUNT(1) FROM FmData.PropertyAddress UNION
SELECT [TableName] = 'FmData.PropertyAffordableUnit', COUNT(1) FROM FmData.PropertyAffordableUnit UNION
SELECT [TableName] = 'FmData.PropertyFinancialStatement', COUNT(1) FROM FmData.PropertyFinancialStatement UNION
SELECT [TableName] = 'FmData.PropertyFinancialExpenseActivity', COUNT(1) FROM FmData.PropertyFinancialExpenseActivity UNION
SELECT [TableName] = 'FmData.PropertyFinancialIncomeActivity', COUNT(1) FROM FmData.PropertyFinancialIncomeActivity UNION
SELECT [TableName] = 'FmData.PropertyFinancialSummaryActivity', COUNT(1) FROM FmData.PropertyFinancialSummaryActivity UNION
SELECT [TableName] = 'FmData.PropertyHousing', COUNT(1) FROM FmData.PropertyHousing UNION
SELECT [TableName] = 'FmData.PropertyInspection', COUNT(1) FROM FmData.PropertyInspection UNION
SELECT [TableName] = 'FmData.PropertyInsurance', COUNT(1) FROM FmData.PropertyInsurance UNION
SELECT [TableName] = 'FmData.PropertyMonitoring', COUNT(1) FROM FmData.PropertyMonitoring UNION
SELECT [TableName] = 'FmData.PropertyOccupancy', COUNT(1) FROM FmData.PropertyOccupancy UNION
SELECT [TableName] = 'FmData.PropertyValuation', COUNT(1) FROM FmData.PropertyValuation UNION
SELECT [TableName] = 'FmData.PropertyPhasedConstruction', COUNT(1) FROM FmData.PropertyPhasedConstruction UNION

SELECT [TableName] = 'FmData.LoanCollateral', COUNT(1) FROM FmData.LoanCollateral UNION
SELECT [TableName] = 'FmData.LoanFinancialActivity', COUNT(1) FROM FmData.LoanFinancialActivity
/* Property Group */