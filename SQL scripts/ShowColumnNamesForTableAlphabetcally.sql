use [UAT_WalkerDunlopGSE];

DECLARE @tablename VARCHAR(max);
SET @tablename = 'WD_Spreadsheet_Staging'

SELECT 
    c.name 
FROM
    sys.columns c
    JOIN sys.tables t ON c.object_id = t.object_id
WHERE
    t.name = @tablename
ORDER BY
    c.name