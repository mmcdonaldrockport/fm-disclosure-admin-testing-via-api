USE DisclosureAdmin;

SELECT 
	*
FROM 
	adm.IssuanceDocumentTypes idt
	JOIN adm.DocumentTypes dt ON idt.DocumentTypeId = dt.id
	JOIN adm.DocumentVersions dv ON dv.Id = idt.DocumentVersionId
