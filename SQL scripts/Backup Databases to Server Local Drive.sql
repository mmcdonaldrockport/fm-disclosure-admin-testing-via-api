DECLARE cDB CURSOR FOR      
	SELECT name      
		FROM sys.databases      
		WHERE
			name in ('DisclosureAdmin')
			--name in ('DisclosureExternal')

DECLARE      
	@Stmt VARCHAR(150),      
	@DbName VARCHAR(150)      

	PRINT('Backing up to c:\rockport\backups\...')      

OPEN cDB
FETCH cDB INTO @DbName
WHILE @@FETCH_STATUS = 0
BEGIN      
	PRINT(@DbName)      
	SET @Stmt = 'BACKUP DATABASE ' + @DbName + ' To DISK =''c:\rockport\backups\' + @DBName + '_from789785-DB12_' + convert(varchar(500),GetDate(),112) +        
	replace(convert(varchar(30), getdate(),108),':','') + '.bak'' WITH COPY_ONLY'      
	Print(@Stmt)      
	EXEC(@Stmt)            
	-- In-loop data fetch      
	FETCH cDB INTO @DbName
END
CLOSE cDB
DEALLOCATE cDB