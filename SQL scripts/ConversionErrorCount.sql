/*
TransferMessageDetail Message Type: 
Error = 0,
Warning = 1
ValidationError = 2,
ValidationWarning = 3,
TransferDefinitionError = 4,
Information = 5
*/

/* REMIC */
SELECT ProductGroupType, COUNT(*) FROM FMDATA.Security GROUP BY ProductGroupType

/* REMIC COLLATERAL */
SELECT COUNT(*) FROM fmdata.CollateralPool cp JOIN import.RemicCollateral_Source1 rc1 ON cp.MBSPoolIdentifier = rc1.MBSPoolIdentifier


SELECT * FROM fmdata.Security WHERE ProductGroupType IS NULL

SELECT count(*) FROM fmdata.Deal
SELECT Count(*) FROM (SELECT DISTINCT DealIdentifier FROM import.Deal_Source1 WHERE CollateralIdentifier IS NULL) a
SELECT Count(*) FROM (SELECT DISTINCT DealIdentifier FROM import.Deal_Source1 WHERE CollateralIdentifier IS NOT NULL) a


SELECT  [MBSPoolIdentifier] = cp.MBSPoolIdentifier,
        [SecurityCUSIPNumber] = S.SecurityCUSIPIdentifier,
        [SecurityREMICGroupIdentifier] = STCG.DealGroupNumber,
        [SecurityAtIssuanceCollateralParticipationUPBAmount] = FISTGAB.SecurityCollateralUnpaidPrincipalBalanceAmount,
        [TrustName] = D.Name,
        [SecurityParticipationPercent] = FISTGAB.FinancialInstrumentCollateralGroupParticipationPercent,
        [FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate] = FISTGAB.FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate
FROM    FmData.Security S
        INNER JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.SecurityId = S.Id
        LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
        LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId
		INNER JOIN FmData.CollateralPool CP ON CP.Id = S.Id

SELECT * FROM FmData.StructuredTransactionCollateralGroup 
SELECT * FROM FmData.Deal where Name is not null


SELECT DealId, DL.Id, DL.Name FROM import.StructuredTransactionCollateralGroup STCG
LEFT JOIN import.Deal DL ON DL.Id = STCG.DealId

Liang Lee [6:51 PM] 
SELECT DealId, DL.Id, DL.Name FROM import.StructuredTransactionCollateralGroup STCG 
LEFT JOIN import.Deal DL ON DL.Id = STCG.DealId


SELECT DealId, DL.Id, DL.Name 
,RS.SecurityREMICCUSIPNumber
FROM import.StructuredTransactionCollateralGroup STCG 
LEFT JOIN import.Deal DL ON DL.Id = STCG.DealId 
LEFT JOIN import.Remic_Source1 RS ON RS.AgreementIdentifier = DL.AgreementIdentifier


 SELECT * FROM FMData.FinancialInstrumentStructuredTransactionGroupAssociationBalance

 SELECT  [MBSPoolIdentifier] = CP.MBSPoolIdentifier,
       [SecurityCUSIPNumber] = S.SecurityCUSIPIdentifier,
       [SecurityREMICGroupIdentifier] = STCG.DealGroupNumber,
       [SecurityAtIssuanceCollateralParticipationUPBAmount] = FISTGAB.SecurityCollateralUnpaidPrincipalBalanceAmount,
       [TrustName] = D.Name,
       [SecurityParticipationPercent] = FISTGAB.FinancialInstrumentCollateralGroupParticipationPercent,
       [FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate] = FISTGAB.FinancialInstrumentStructuredTransactionGroupAssociationEffectiveDate
FROM    FmData.Security S
        INNER JOIN FmData.CollateralPool CP ON CP.Id = S.Id
       INNER JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.SecurityId = S.Id
        LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.Id = S.StructuredTransactionCollateralGroupId
       LEFT JOIN FmData.Deal D ON D.Id = STCG.DealId


SELECT * FROM FmData.Deal d JOIN import.Deal_Source1 ds1  ON d.DealIdentifier = ds1.DealIdentifier WHERE d.DealIdentifier IS NULL
SELECT COUNT(*) FROM fmdata.Deal

SELECT DISTINCT DealIdentifier FROM import.Deal_Source1 WHERE dealIdentifier NOT IN (SELECT DealIdentifier FROM fmdata.Deal)

SELECT DealIdentifier, CollateralIdentifier, CollateralNonPropertyCollateralType, CollateralNonPropertyCollateralValueAmount, DealMaximumPermittedLTVofCreditFacilityPercent, DealAggregateCutOffDateBalanceAmount FROM import.Deal_Source1 WHERE CollateralIdentifier IS NOT NULL--32894
SELECT DISTINCT DealIdentifier, CollateralIdentifier FROM import.Deal_Source1 WHERE CollateralIdentifier IS NOT NULL--32894

SELECT * FROM FmData.Deal d RIGHT OUTER JOIN import.Deal_Source1 ds1  ON d.DealIdentifier = ds1.DealIdentifier WHERE d.DealIdentifier IS NULL

SELECT * FROM adm.TransferExecution te LEFT OUTER JOIN adm.TransferStatistics ts ON te.Id = ts.TransferExecutionId
where te.Id > 2752 
--and OwnerName = 'Source'

SELECT MAX(Id), Name FROM adm.TransferExecution where Id > 2752 GROUP BY Name

SELECT * FROM adm.TransferMessage

SELECT te.Name, tmd.TransferExecutionId, ActionName, ContainerName, count(*)  
FROM 
	adm.TransferMessageDetail tmd  RIGHT OUTER JOIN
		adm.TransferExecution te ON tmd.TransferExecutionId = te.Id
WHERE te.Id >155
group by te.Name, TransferExecutionId,ActionName,ContainerName

SELECT * FROM adm.TransferStatistics where OwnerName = 'Source'



/*****************************/
SELECT tmd.TransferExecutionId, te.Name, count(*) As ErrorRows FROM
	(SELECT MAX(Id) as Id, Name FROM adm.TransferExecution GROUP BY Name) te 
	LEFT OUTER JOIN 
	(SELECT DISTINCT TransferExecutionId, RowIndex AS Error
		FROM 
			Adm.TransferMessageDetail tmd 
		WHERE TransferExecutionId > 2752
		GROUP BY 
			TransferExecutionId, RowIndex) tmd 
	ON te.Id = tmd.TransferExecutionId
where Id > 2752
GROUP BY tmd.TransferExecutionId, te.Name
ORDER BY TransferExecutionId
		
/*****************************/
-- better
SELECT 
	te.Id, te.Name, MessageType, ISNULL(ErrorCount, 0) As ErrorCount 
FROM
	(SELECT MAX(Id) as Id, Name FROM adm.TransferExecution GROUP BY Name) te 
	LEFT OUTER JOIN 
	(SELECT TransferExecutionId, MessageType, count(*) AS ErrorCount	FROM adm.TransferMessageDetail tmd 
		WHERE TransferExecutionId > 2752 GROUP BY TransferExecutionId, MessageType) tmd 
	ON te.Id = tmd.TransferExecutionId
where Id > 2752
--GROUP BY tmd.TransferExecutionId, te.Name, 
ORDER BY Te.Id

SELECT * FROM adm.TransferMessageDetail where TransferExecutionId = 20

SELECT MAX(Id) as Id, Name FROM adm.TransferExecution where Id > 2752 GROUP BY Name 
SELECT * FROM adm.TransferMessageDetail where TransferExecutionId = 2754

SELECT DISTINCT MessageType from adm.TransferMessageDetail 

SELECT tmd.RowIndex, DealIdentifier, CDSDealIdentifier,Message, * FROM adm.TransferMessageDetail tmd 
JOIN (SELECT ROW_NUMBER() OVER (ORDER BY @@rowcount) as RowIndex, * from import.Deal_Source1) ds1 ON tmd.RowIndex = ds1.RowIndex
where TransferExecutionId = 2754 
and ColumnName = 'CollateralNonPropertyCollateralValueAmount'
ORDER BY tmd.RowIndex

SELECT * FROM import.Deal_Source1 where CollateralIdentifier = 97424
Process condition is not valid for this row. (row 3037) - Expression IsNotNull(MBSPoolCurrentWeightedAverageRemainingMaturityTerm)


SELECT * FROM fmdata.Deal where DealIdentifier = 1008351 

--16|97424





select distinct DealIdentifier from import.Deal_Source1 where CollateralNonPropertyCollateralType is not null and CollateralIdentifier = 97424

SELECT TOP 10 ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as RowIndex, * from import.Deal_Source1
SELECT TOP 10 * from import.Deal_Source1

SELECT * FROM fmdata.Deal

/* Get counts from staging tables */
SELECT 'Deal' as 'Source File', (select count(*) from import.Deal_Source) as 'Source', (select count(*) from import.Deal_Source1) as 'Source1' UNION
SELECT 'DealMonitoring' as 'Source File', (select count(*) from import.DealMonitoring_Source) as 'Source', (select count(*) from import.DealMonitoring_Source1) as 'Source1' UNION
SELECT 'Mega' as 'Source File', (select count(*) from import.Mega_Source) as 'Source', (select count(*) from import.Mega_Source1) as 'Source1' UNION
SELECT 'MegaCollateral' as 'Source File', (select count(*) from import.MegaCollateral_Source) as 'Source', (select count(*) from import.MegaCollateral_Source1) as 'Source1' UNION
SELECT 'PoolFactorPlus' as 'Source File', (select count(*) from import.PoolFactorPlus_Source) as 'Source', (select count(*) from import.PoolFactorPlus_Source1) as 'Source1' UNION
SELECT 'Remic' as 'Source File', (select count(*) from import.Remic_Source) as 'Source', (select count(*) from import.Remic_Source1) as 'Source1' UNION
SELECT 'RemicCollateral' as 'Source File', (select count(*) from import.RemicCollateral_Source) as 'Source', (select count(*) from import.RemicCollateral_Source1) as 'Source1' UNION
SELECT 'Security' as 'Source File', (select count(*) from import.Security_Source) as 'Source', (select count(*) from import.Security_Source1) as 'Source1'
--Process condition is not valid for this row. (row 592) - Expression IsNotNull(MBSPoolCurrentWeightedAverageRemainingMaturityTerm)


SELECT * FROM adm.TransferMessageDetail where TransferExecutionId = 2769



SELECT count(*) FROM fmdata.Loan
SELECT count(*) FROM fmdata.Security
SELECT Distinct id, Name FROM adm.TransferMessageDetail

