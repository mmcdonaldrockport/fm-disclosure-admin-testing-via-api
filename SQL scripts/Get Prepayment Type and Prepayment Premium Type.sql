-- Ordered by security, loan, minimum of LoanPrepaymentProtectionEndDate and LoanPrepaymentPremiumEndDate
-- null dates replaced by maximum date (12-31-9999)
SELECT 
	ProductGroupType,
	ProductARMPlanNumber,
	CP_Id,
	L_Id,
	LP_LoanPrepaymentProtectionEndDate,
	LPP_LoanPrepaymentPremiumEndDate,
	CASE 
		WHEN LP_LoanPrepaymentProtectionEndDate < LPP_LoanPrepaymentPremiumEndDate 
		THEN LP_LoanPrepaymentProtectionEndDate ELSE LPP_LoanPrepaymentPremiumEndDate 
	END as MinEndDate,
	LP_LoanPrepaymentProtectionType,
	LPP_LoanPrepaymentPremiumType
FROM ( 
	SELECT 
		S.ProductGroupType,
		S.ProductARMPlanNumber,
		CP.Id as CP_Id,
		CP.MBSPoolIdentifier,
		L.ID as L_Id,
		L.LoanIdentifier,
		LP.Id AS LP_Id,
		LP.LastUpdateDateTime AS LP_LastUpdateDateTime,
		LP.LastUpdatedPayloadId AS LP_LastUpdatedPayloadId,
		LP.LastUpdatedPayloadTypeCode AS LP_LastUpdatedPayloadTypeCode,
		LP.LastUpdatedUser AS LP_LastUpdatedUser,
		LP.LoanId AS LP_LoanId,
		IsNull(LP.LoanPrepaymentProtectionEndDate, '12-31-9999') AS LP_LoanPrepaymentProtectionEndDate,
		LP.LoanPrepaymentProtectionTerm AS LP_LoanPrepaymentProtectionTerm,
		LP.LoanPrepaymentProtectionType AS LP_LoanPrepaymentProtectionType,
		LPP.Id AS LPP_Id,
		LPP.LastUpdateDateTime AS LPP_LastUpdateDateTime,
		LPP.LastUpdatedPayloadId AS LPP_LastUpdatedPayloadId,
		LPP.LastUpdatedPayloadTypeCode AS LPP_LastUpdatedPayloadTypeCode,
		LPP.LastUpdatedUser AS LPP_LastUpdatedUser,
		LPP.LoanOtherPrepaymentPremiumDescription AS LPP_LoanOtherPrepaymentPremiumDescription,
		LPP.LoanPrepaymentDecliningPremiumType AS LPP_LoanPrepaymentDecliningPremiumType,
		IsNull(LPP.LoanPrepaymentPremiumEndDate,'12-13-9999') AS LPP_LoanPrepaymentPremiumEndDate,
		LPP.LoanPrepaymentPremiumTerm AS LPP_LoanPrepaymentPremiumTerm,
		LPP.LoanPrepaymentPremiumType AS LPP_LoanPrepaymentPremiumType,
		LPP.LoanPrepaymentProtectionTermId AS LPP_LoanPrepaymentProtectionTermId
	FROM 
		FmData.Loan L
		/*Join prepayment info*/
		FULL OUTER JOIN FmData.LoanPrepaymentProtectionTerm LP ON LP.LoanId = L.Id
		FULL OUTER JOIN FmData.LoanPrepaymentPremiumTerm LPP ON LPP.LoanPrepaymentProtectionTermId = LP.Id
		/*Join Security*/
		LEFT JOIN FmData.CollateralPool CP ON CP.Id = L.CollateralPoolId
		LEFT JOIN FMData.Security S on CP.Id = S.Id
--Where CP.ID = 412450 
) t 
ORDER by 
	CP_Id, L_Id, MinEndDate

