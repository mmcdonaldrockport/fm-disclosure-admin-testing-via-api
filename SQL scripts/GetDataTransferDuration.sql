use DisclosureAdmin2;

SELECT 
	--row_number() OVER (ORDER By te.Name) AS rowNum,
	te.Name AS [TransferExecution.Name],
	datediff(s, DateTimeStarted, DateTimeEnded) as DurationInSeconds, 
	tf.Name AS [TransferFile.Name],
	tf.Size AS [TransferFile.Size],
	te.Id AS [TransferExecution.Id],
	CASE WHEN tq.Status = 3 THEN 'Failed' WHEN tq.Status = 4 THEN 'Completed' ELSE CAST(tq.Status AS VARCHAR) END AS StatusText,
	te.TransferQueueId AS [TransferExecution.TransferQueueId],
	te.EngineExecutionId AS [TransferExecution.EngineExecutionId],
	te.Mode AS [TransferExecution.Mode],
	te.DateTimeStarted AS [TransferExecution.DateTimeStarted],
	te.DateTimeEnded AS [TransferExecution.DateTimeEnded],
	tf.Id AS [TransferFile.Id],
	tf.TransferQueueId AS [TransferFile.TransferQueueId],
	tf.FileType AS [TransferFile.FileType],
	--tf.Contents AS [TransferFile.Contents],
	tf.DateTimeCreated AS [TransferFile.DateTimeCreated],
	tq.Id AS [TransferQueue.Id],
	tq.Status AS [TransferQueue.Status],
	tq.TransferDefinitionId AS [TransferQueue.TransferDefinitionId],
	tq.DateTimeCreated AS [TransferQueue.DateTimeCreated],
	tq.CreatedById AS [TransferQueue.CreatedById]
FROM 
	adm.TransferExecution te 
	JOIN adm.TransferFile tf ON te.TransferQueueId = tf.TransferQueueId 
	JOIN adm.TransferQueue tq ON te.TransferQueueId = tq.Id
WHERE
	--tq.Status in (3,4)
	--and 
	te.DateTimeStarted > '2017-08-28'
ORDER BY te.Id

SELECT 
	te.Name AS [TransferExecution.Name],
	count(*) as FileCount,
	MIN(datediff(s, DateTimeStarted, DateTimeEnded)) as MinSeconds,
	MAX(datediff(s, DateTimeStarted, DateTimeEnded)) as MaxSeconds,
	AVG(datediff(s, DateTimeStarted, DateTimeEnded)) as AvgSeconds	
	/*
	tf.Name AS [TransferFile.Name],
	tf.Size AS [TransferFile.Size],
	te.Id AS [TransferExecution.Id],
	te.TransferQueueId AS [TransferExecution.TransferQueueId],
	te.EngineExecutionId AS [TransferExecution.EngineExecutionId],
	te.Mode AS [TransferExecution.Mode],
	te.DateTimeStarted AS [TransferExecution.DateTimeStarted],
	te.DateTimeEnded AS [TransferExecution.DateTimeEnded],
	tf.Id AS [TransferFile.Id],
	tf.TransferQueueId AS [TransferFile.TransferQueueId],
	tf.FileType AS [TransferFile.FileType],
	--tf.Contents AS [TransferFile.Contents],
	tf.DateTimeCreated AS [TransferFile.DateTimeCreated],
	tq.Status AS [TransferQueue.Status],
	tq.TransferDefinitionId AS [TransferQueue.TransferDefinitionId],
	tq.DateTimeCreated AS [TransferQueue.DateTimeCreated],
	tq.CreatedById AS [TransferQueue.CreatedById]
	*/
FROM 
	adm.TransferExecution te 
	JOIN adm.TransferFile tf ON te.TransferQueueId = tf.TransferQueueId 
	JOIN adm.TransferQueue tq ON te.TransferQueueId = tq.Id
WHERE
	tq.Status in (3,4)
GROUP BY 
	te.Name
ORDER BY 
	te.Name


--SELECT CONCAT('te.', c.name, ' AS [',t.name, '.',c.name,'],') FROM syscolumns c JOIN sysobjects t on c.id = t.id WHERE t.name = 'TransferExecution' order by c.colid
--SELECT CONCAT('tf.', c.name, ' AS [',t.name, '.',c.name,'],') FROM syscolumns c JOIN sysobjects t on c.id = t.id WHERE t.name = 'TransferFile' order by c.colid
--SELECT CONCAT('tq.', c.name, ' AS [',t.name, '.',c.name,'],') FROM syscolumns c JOIN sysobjects t on c.id = t.id WHERE t.name = 'TransferQueue' order by c.colid
