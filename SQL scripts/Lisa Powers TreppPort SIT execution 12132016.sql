use DisclosureAdmin;

select s.SecurityCUSIPIdentifier,
	CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor,
	CollateralPoolWeightedAverageAmortizationMaturityTerm,
	CollateralPoolAverageOriginalLoanSizeAmount,
	CollateralPoolWeightedAverageLoanToValueRatioFactor
	CollateralPool
	  from 
	fmdata.FinancialInstrumentCollateralGroupBalance ficgb 
		JOIN fmdata.CollateralPool cp ON ficgb.CollateralPoolId = cp.id
		JOIN fmdata.Security s on s.id = cp.id

SELECT 
S.SecurityCUSIPIdentifier,
SP.SecurityUnpaidPrincipalBalanceAmount
FROM FmData.Security S 
JOIN FmData.SecurityPayment SP ON SP.SecurityId = S.Id
WHERE SecurityUnpaidPrincipalBalanceAmount is not null

/*
WHERE
	CollateralPoolWeightedAverageDebtServiceCoverageRatioFactor is not null OR
	CollateralPoolWeightedAverageAmortizationMaturityTerm is not null or
	CollateralPoolAverageOriginalLoanSizeAmount is not null or
	CollateralPoolWeightedAverageLoanToValueRatioFactor is not null 
	--CollateralPoolWeightedAveragePrepaymentProtectionTerm is not null
	*/
	--select * from syscolumns c JOIN sysobjects t on t.id = c.id where c.name like '%SecurityCollateralUnpaidPrincipalBalanceAmount%'