SELECT
	TransferQueueId as JobId,
	te.Id as TransferExecutionId,
	'tm' as MessageSource,
	tm.MessageType as ErrorType,
	tm.Message as ErrorMessage
FROM
	adm.TransferExecution te 
	JOIN adm.TransferQueue tq on te.TransferQueueId = tq.id 
	JOIN adm.TransferMessage tm ON te.Id = tm.TransferExecutionId
WHERE
	TransferQueueId = 4083
UNION 
SELECT
	TransferQueueId as JobId,
	te.Id as TransferExecutionId,
	'tmd' as MessageSource,
	tmd.MessageType,
	tmd.Message
FROM
	adm.TransferExecution te 
	JOIN adm.TransferQueue tq on te.TransferQueueId = tq.id 
	JOIN adm.TransferMessageDetail tmd ON te.Id = tmd.TransferExecutionId
WHERE
	TransferQueueId = 4083;
COMMIT;



SELECT 
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	tq.Status as StatusCode,
	s.StatusName as Status,
	tm.MessageType as ErrorType,
	tm.Message as Message,
	tf.Name as FileName
FROM
	adm.TransferExecution te 
	join adm.TransferQueue tq on te.TransferQueueId = tq.id 
	join adm.TransferMessage tm on te.id = tm.TransferExecutionId
	join adm.TransferFile tf on te.TransferQueueId = tf.TransferQueueId
	left outer join (
		select 0 as status, 'Pending' as statusName union 
		select 1, 'ValidationFailed' union
		select 2, 'Processing' union
		select 3, 'Failed' union
		select 4, 'Completed' union
		select 5, 'CompletedWithWarnings' union
		select 6, 'Cancelled' ) s on tq.status = s.status
WHERE
	TransferQueueId >= 5572 and TransferQueueId <= 6857
UNION
SELECT 
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	tq.Status as StatusCode,
	s.StatusName as Status,
	tmd.MessageType as ErrorType,
	tmd.Message as Message,
	tf.Name as FileName
FROM
	adm.TransferExecution te 
	join adm.TransferQueue tq on te.TransferQueueId = tq.id 
	join adm.TransferMessageDetail tmd on te.id = tmd.TransferExecutionId
	join adm.TransferFile tf on te.TransferQueueId = tf.TransferQueueId
	left outer join (
		select 0 as status, 'Pending' as statusName union 
		select 1, 'ValidationFailed' union
		select 2, 'Processing' union
		select 3, 'Failed' union
		select 4, 'Completed' union
		select 5, 'CompletedWithWarnings' union
		select 6, 'Cancelled' ) s on tq.status = s.status
WHERE
	TransferQueueId >= 5572 and TransferQueueId <= 6857
ORDER BY 
	TransferExecutionId DESC, JobId;
COMMIT;






SELECT 
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	tq.Status as StatusCode,
	s.StatusName as Status,
	IsNull((select IsNUll(tmd.Message, '[Error message absent]') + '
' AS 'data()' 
		FROM adm.TransferMessageDetail tmd --join adm.TransferExecution te on te.id = tmd.TransferExecutionId
		WHERE te.id = tmd.TransferExecutionId
		FOR XML PATH('')
		),'') as Message
FROM
	adm.TransferExecution te 
	join adm.TransferQueue tq on te.TransferQueueId = tq.id 

	left outer join (
		select 0 as status, 'Pending' as statusName union 
		select 1, 'ValidationFailed' union
		select 2, 'Processing' union
		select 3, 'Failed' union
		select 4, 'Completed' union
		select 5, 'CompletedWithWarnings' union
		select 6, 'Cancelled' ) s on tq.status = s.status
WHERE
	TransferQueueId = 4145
ORDER BY 
	TransferExecutionId DESC, JobId

select te.TransferQueueId from adm.TransferExecution te where te.id in (
select tm.TransferExecutionId from adm.TransferMessage tm join adm.TransferMessageDetail tmd 
	on tm.TransferExecutionId = tmd.TransferExecutionId and tm.MessageType = tmd.MessageType);
commit;



SELECT 
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	tq.Status as StatusCode,
	s.StatusName as Status,
	tm.MessageType as ErrorType,
	tm.Message as Message
FROM
	adm.TransferExecution te 
	join adm.TransferQueue tq on te.TransferQueueId = tq.id 
	join adm.TransferMessage tm on te.id = tm.TransferExecutionId
	left outer join (
		select 0 as status, 'Pending' as statusName union 
		select 1, 'ValidationFailed' union
		select 2, 'Processing' union
		select 3, 'Failed' union
		select 4, 'Completed' union
		select 5, 'CompletedWithWarnings' union
		select 6, 'Cancelled' ) s on tq.status = s.status
WHERE
	TransferQueueId = 4083
UNION
SELECT 
	te.Id as TransferExecutionId,
	te.TransferQueueId as JobId,
	tq.Status as StatusCode,
	s.StatusName as Status,
	tmd.MessageType as ErrorType,
	tmd.Message as Message
FROM
	adm.TransferExecution te 
	join adm.TransferQueue tq on te.TransferQueueId = tq.id 
	join adm.TransferMessageDetail tmd on te.id = tmd.TransferExecutionId
	left outer join (
		select 0 as status, 'Pending' as statusName union 
		select 1, 'ValidationFailed' union
		select 2, 'Processing' union
		select 3, 'Failed' union
		select 4, 'Completed' union
		select 5, 'CompletedWithWarnings' union
		select 6, 'Cancelled' ) s on tq.status = s.status
WHERE
	TransferQueueId = 4083
ORDER BY 
	TransferExecutionId DESC, JobId

