/* Security and Deal Group 1-3 */
SELECT [TableName] = 'Import.Collateral', COUNT(1) FROM import.Collateral UNION
SELECT [TableName] = 'Import.CollateralPool', COUNT(1) FROM import.CollateralPool UNION
SELECT [TableName] = 'Import.Deal', COUNT(1) FROM import.Deal UNION
SELECT [TableName] = 'Import.DealMonitoring', COUNT(1) FROM import.DealMonitoring UNION
SELECT [TableName] = 'Import.DealStructuredFacilityAtIssuance', COUNT(1) FROM import.DealStructuredFacilityAtIssuance UNION
SELECT [TableName] = 'Import.DealStructuredFacilityCollateral', COUNT(1) FROM import.DealStructuredFacilityCollateral UNION
SELECT [TableName] = 'Import.FinancialInstrumentCollateralGroupBalance', COUNT(1) FROM import.FinancialInstrumentCollateralGroupBalance UNION
SELECT [TableName] = 'Import.FinancialInstrumentStructuredTransactionGroupAssociationBalance', COUNT(1) FROM import.FinancialInstrumentStructuredTransactionGroupAssociationBalance UNION
/* Security and Deal Group 1-3 */

/* Security Group */
SELECT [TableName] = 'Import.Security', COUNT(1) FROM import.Security UNION
SELECT [TableName] = 'Import.SecurityAtIssuance', COUNT(1) FROM import.SecurityAtIssuance UNION
SELECT [TableName] = 'Import.SecurityFactor', COUNT(1) FROM import.SecurityFactor UNION
SELECT [TableName] = 'Import.SecurityInterestAccrualSchedule', COUNT(1) FROM import.SecurityInterestAccrualSchedule UNION
SELECT [TableName] = 'Import.SecurityInterestRate', COUNT(1) FROM import.SecurityInterestRate UNION
SELECT [TableName] = 'Import.SecurityInterestRateAdjustmentLifetimeTerm', COUNT(1) FROM import.SecurityInterestRateAdjustmentLifetimeTerm UNION
SELECT [TableName] = 'Import.SecurityPayment', COUNT(1) FROM import.SecurityPayment UNION
SELECT [TableName] = 'Import.SecurityRelationship', COUNT(1) FROM import.SecurityRelationship UNION
SELECT [TableName] = 'Import.SecurityStatus', COUNT(1) FROM import.SecurityStatus UNION
SELECT [TableName] = 'Import.StructuredTransactionCollateralGroup', COUNT(1) FROM import.StructuredTransactionCollateralGroup UNION
/* Security Group */

/* Loan Group */
SELECT [TableName] = 'Import.Loan', COUNT(1) FROM import.Loan UNION
SELECT [TableName] = 'Import.LoanAcquisition', COUNT(1) FROM import.LoanAcquisition UNION
--SELECT [TableName] = 'Import.LoanAcquisition2', COUNT(1) FROM import.LoanAcquisition2 UNION
SELECT [TableName] = 'Import.LoanAdditionalDebt', COUNT(1) FROM import.LoanAdditionalDebt UNION
--SELECT [TableName] = 'Import.LoanDelinquencyActivity', COUNT(1) FROM import.LoanDelinquencyActivity UNION
SELECT [TableName] = 'Import.LoanServicingActivity', COUNT(1) FROM import.LoanServicingActivity UNION
SELECT [TableName] = 'Import.LoanMezzaninePrepaymentProtectionTerm', COUNT(1) FROM import.LoanMezzaninePrepaymentProtectionTerm UNION
SELECT [TableName] = 'Import.LoanPrepaymentPremiumTerm', COUNT(1) FROM import.LoanPrepaymentPremiumTerm UNION
SELECT [TableName] = 'Import.LoanPrepaymentProtectionTerm', COUNT(1) FROM import.LoanPrepaymentProtectionTerm UNION
--SELECT [TableName] = 'Import.LoanOtherDecliningPrepaymentSchedule', COUNT(1) FROM import.LoanOtherDecliningPrepaymentSchedule UNION -- No insert in import
SELECT [TableName] = 'Import.LoanCollateral', COUNT(1) FROM import.LoanCollateral UNION
/* Loan Group */ 

/* Property Group */
SELECT [TableName] = 'Import.Collateral', COUNT(1) FROM import.Collateral UNION
SELECT [TableName] = 'Import.Property', COUNT(1) FROM import.Property UNION
SELECT [TableName] = 'Import.PropertyAcquisition', COUNT(1) FROM import.PropertyAcquisition UNION
SELECT [TableName] = 'Import.PropertyAddress', COUNT(1) FROM import.PropertyAddress UNION
SELECT [TableName] = 'Import.PropertyAffordableUnit', COUNT(1) FROM import.PropertyAffordableUnit UNION
SELECT [TableName] = 'Import.PropertyFinancialStatement', COUNT(1) FROM import.PropertyFinancialStatement UNION
SELECT [TableName] = 'Import.PropertyFinancialExpenseActivity', COUNT(1) FROM import.PropertyFinancialExpenseActivity UNION
SELECT [TableName] = 'Import.PropertyFinancialIncomeActivity', COUNT(1) FROM import.PropertyFinancialIncomeActivity UNION
SELECT [TableName] = 'Import.PropertyFinancialSummaryActivity', COUNT(1) FROM import.PropertyFinancialSummaryActivity UNION
SELECT [TableName] = 'Import.PropertyHousing', COUNT(1) FROM import.PropertyHousing UNION
SELECT [TableName] = 'Import.PropertyInspection', COUNT(1) FROM import.PropertyInspection UNION
SELECT [TableName] = 'Import.PropertyInsurance', COUNT(1) FROM import.PropertyInsurance UNION
SELECT [TableName] = 'Import.PropertyMonitoring', COUNT(1) FROM import.PropertyMonitoring UNION
SELECT [TableName] = 'Import.PropertyOccupancy', COUNT(1) FROM import.PropertyOccupancy UNION
SELECT [TableName] = 'Import.PropertyValuation', COUNT(1) FROM import.PropertyValuation UNION
SELECT [TableName] = 'Import.PropertyPhasedConstruction', COUNT(1) FROM import.PropertyPhasedConstruction UNION

SELECT [TableName] = 'Import.LoanCollateral', COUNT(1) FROM import.LoanCollateral UNION
SELECT [TableName] = 'Import.LoanFinancialActivity', COUNT(1) FROM import.LoanFinancialActivity
/* Property Group */