--Advancded Search Results Inputs
WITH SecurityInterestRateAI AS

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate ASC),
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)

, SecurityInterestRateON AS

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate DESC),
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)

, SecurityStatus AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityStatusDatetime DESC),
		SecurityId,
		SecurityStatusType
	FROM FmData.SecurityStatus)

, FinancialInstrumentCollateralGroupBalanceAI AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate ASC),
		CollateralPoolId,
		CollateralPoolActiveLoanCount,
		CollateralPoolWeightedAverageMaturityNumber,
		CollateralPoolWeightedAverageAmortizationMaturityTerm
	FROM FmData.FinancialInstrumentCollateralGroupBalance)

, FinancialInstrumentCollateralGroupBalanceON AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate DESC),
		CollateralPoolId,
		CollateralPoolActiveLoanCount,
		CollateralPoolWeightedAverageMaturityNumber
	FROM FmData.FinancialInstrumentCollateralGroupBalance)

, LoanServicingActivity AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY LoanId ORDER BY LoanActivityDatetime DESC),
		LoanId,
		LoanInterestRate,
		LoanEndingScheduledBalanceAmount
	FROM FmData.LoanServicingActivity)

, SecurityPayment AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityPaymentEffectiveDate DESC),
		SecurityId,
		SecurityUnpaidPrincipalBalanceAmount 
	FROM FmData.SecurityPayment)

, SecurityRelationship AS 

	(SELECT
	[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityRelatedId ORDER BY SecurityRelationshipBalanceEffectiveDate DESC),
	*
	FROM FmData.SecurityRelationship)

SELECT DISTINCT

	[Mega Transaction Id] = CP2.MBSPoolIdentifier,
	[Transaction Id] = CP1.MBSPoolIdentifier,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[Security Execution Type] = S.ProductGroupType,
	[Status] = SS.SecurityStatusType,
	[Issue Date] = S.SecurityIssueDate,
	[Maturity Date] = S.SecurityMaturityDate,
	[Certificate Balance ($)] = SR.SecurityCollateralParticipationUnpaidPrincipalBalanceAmount,
	[Paying PTR (%)] = COALESCE(S2.SecurityInterestRate, S1.SecurityInterestRate),
	[Loans] = COALESCE(F2.CollateralPoolActiveLoanCount, F1.CollateralPoolActiveLoanCount),
	[WA Remaining Term] = COALESCE(F2.CollateralPoolWeightedAverageMaturityNumber, F1.CollateralPoolWeightedAverageMaturityNumber),
	[WA Amortization Term] = 
		SUM(COALESCE((LA.LoanRemainingAmortizationTerm * (LSA.LoanEndingScheduledBalanceAmount / SP.SecurityUnpaidPrincipalBalanceAmount)), F1.CollateralPoolWeightedAverageAmortizationMaturityTerm)),
	[WA Remaining I/O Term] = 
		SUM(COALESCE(LA.LoanRemainingInterestOnlyPeriod * (LSA.LoanEndingScheduledBalanceAmount / SP.SecurityUnpaidPrincipalBalanceAmount), (LA.LoanRemainingInterestOnlyPeriod * (LA.LoanContributionBalanceAmount / S.SecurityIssueAmount)))),
	[Participation (%)] = SR.SecurityParticipationPercent

FROM FmData.CollateralPool CP1
	LEFT JOIN FMData.Security S ON S.Id = CP1.Id
	LEFT JOIN SecurityPayment SP ON SP.SecurityId = S.Id AND SP.Ranking = 1
	LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceAI F1 ON F1.CollateralPoolId = CP1.Id AND F1.Ranking = 1
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceON F2 ON F2.CollateralPoolId = CP1.Id AND F2.Ranking = 1
	LEFT JOIN SecurityInterestRateAI S1 ON S1.SecurityId = CP1.Id AND S1.Ranking = 1
	LEFT JOIN SecurityInterestRateON S2 ON S2.SecurityId = CP1.Id AND S2.Ranking = 1
	LEFT JOIN SecurityRelationship SR ON SR.SecurityRelatedId = CP1.Id AND SR.Ranking = 1
	LEFT JOIN FmData.CollateralPool CP2 ON CP2.Id = SR.SecurityId
	LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP1.Id
	LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
	LEFT JOIN LoanServicingActivity LSA ON LSA.LoanId = L.Id AND LSA.Ranking = 1
	LEFT JOIN FmData.Security M ON M.Id = CP2.Id

WHERE M.ProductGroupType = 'Mega'

GROUP BY 
	CP2.MBSPoolIdentifier,
	CP1.MBSPoolIdentifier,
	S.SecurityCUSIPIdentifier,
	S.MBSPoolPrefixType,
	S.ProductGroupType,
	SS.SecurityStatusType,
	S.SecurityIssueDate,
	S.SecurityMaturityDate,
	SR.SecurityCollateralParticipationUnpaidPrincipalBalanceAmount,
	COALESCE(S2.SecurityInterestRate, S1.SecurityInterestRate),
	COALESCE(F2.CollateralPoolActiveLoanCount, F1.CollateralPoolActiveLoanCount),
	COALESCE(F2.CollateralPoolWeightedAverageMaturityNumber, F1.CollateralPoolWeightedAverageMaturityNumber),
	SR.SecurityParticipationPercent

ORDER BY 2 DESC