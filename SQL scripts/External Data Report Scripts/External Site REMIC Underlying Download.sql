WITH SecurityInterestRateAI AS

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate ASC),
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)

, SecurityInterestRateON AS

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate DESC),
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)

, SecurityStatus AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityStatusDatetime DESC),
		SecurityId,
		SecurityStatusType
	FROM FmData.SecurityStatus)

, FinancialInstrumentCollateralGroupBalanceAI AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate ASC),
		CollateralPoolId,
		CollateralPoolActiveLoanCount,
		CollateralPoolWeightedAverageMaturityNumber,
		CollateralPoolWeightedAverageAmortizationMaturityTerm
	FROM FmData.FinancialInstrumentCollateralGroupBalance)

, FinancialInstrumentCollateralGroupBalanceON AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY CollateralPoolId ORDER BY FinancialInstrumentCollateralGroupBalanceEffectiveDate DESC),
		CollateralPoolId,
		CollateralPoolActiveLoanCount,
		CollateralPoolWeightedAverageMaturityNumber
	FROM FmData.FinancialInstrumentCollateralGroupBalance)

, LoanServicingActivity AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY LoanId ORDER BY LoanActivityDatetime DESC),
		LoanId,
		LoanInterestRate,
		LoanEndingScheduledBalanceAmount
	FROM FmData.LoanServicingActivity)

, SecurityPayment AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityPaymentEffectiveDate DESC),
		SecurityId,
		SecurityUnpaidPrincipalBalanceAmount 
	FROM FmData.SecurityPayment)

SELECT DISTINCT

	[Trust Name] = D.Name,
	[Group] = STCG.DealGroupNumber,
	[Transaction ID] = CP1.MBSPoolIdentifier,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[Security] = S.ProductGroupType,
	[Status] = SS.SecurityStatusType,
	[Issue Date] = S.SecurityIssueDate,
	[Maturity Date] = S.SecurityMaturityDate,
	[Certificate Balance ($)] = FISTGAB.SecurityCollateralUnpaidPrincipalBalanceAmount,
	[Paying PTR (%)] = COALESCE(SIR2.SecurityInterestRate, SIR1.SecurityInterestRate),
	[Loans] = COALESCE(F2.CollateralPoolActiveLoanCount, F1.CollateralPoolActiveLoanCount),
	[WA Remaining Term] = COALESCE(F2.CollateralPoolWeightedAverageMaturityNumber, F1.CollateralPoolWeightedAverageMaturityNumber),
	[WA Amortization Term] = 
		SUM(COALESCE((LA.LoanRemainingAmortizationTerm * (LSA.LoanEndingScheduledBalanceAmount / SP.SecurityUnpaidPrincipalBalanceAmount)), F1.CollateralPoolWeightedAverageAmortizationMaturityTerm)),
	[WA Remaining I/O Term] = 
		SUM(COALESCE(LA.LoanRemainingInterestOnlyPeriod * (LSA.LoanEndingScheduledBalanceAmount / SP.SecurityUnpaidPrincipalBalanceAmount), (LA.LoanRemainingInterestOnlyPeriod * (LA.LoanContributionBalanceAmount / S.SecurityIssueAmount)))),
	[Participation (%)] = FISTGAB.FinancialInstrumentCollateralGroupParticipationPercent

FROM FmData.Deal D
	LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.DealId = D.Id
	LEFT JOIN FmData.FinancialInstrumentStructuredTransactionGroupAssociationBalance FISTGAB ON FISTGAB.StructuredTransactionCollateralGroupId = STCG.Id
	LEFT JOIN FmData.Security S ON S.Id = FISTGAB.SecurityId
	LEFT JOIN FmData.CollateralPool CP1 ON CP1.Id = S.Id
		
	LEFT JOIN SecurityPayment SP ON SP.SecurityId = S.Id AND SP.Ranking = 1
	LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceAI F1 ON F1.CollateralPoolId = CP1.Id AND F1.Ranking = 1
	LEFT JOIN FinancialInstrumentCollateralGroupBalanceON F2 ON F2.CollateralPoolId = CP1.Id AND F2.Ranking = 1
	LEFT JOIN SecurityInterestRateAI SIR1 ON SIR1.SecurityId = CP1.Id AND SIR1.Ranking = 1
	LEFT JOIN SecurityInterestRateON SIR2 ON SIR2.SecurityId = CP1.Id AND SIR2.Ranking = 1
	LEFT JOIN FmData.Loan L ON L.CollateralPoolId = CP1.Id
	LEFT JOIN FmData.LoanAcquisition LA ON LA.Id = L.Id
	LEFT JOIN LoanServicingActivity LSA ON LSA.LoanId = L.Id AND LSA.Ranking = 1

	LEFT JOIN FmData.Security R ON R.StructuredTransactionCollateralGroupId = STCG.Id

WHERE R.ProductGroupType = 'REMIC'
	
GROUP BY 
	D.Name,
	STCG.DealGroupNumber,
	CP1.MBSPoolIdentifier,
	S.SecurityCUSIPIdentifier,
	S.MBSPoolPrefixType,
	S.ProductGroupType,
	SS.SecurityStatusType,
	S.SecurityIssueDate,
	S.SecurityMaturityDate,
	FISTGAB.SecurityCollateralUnpaidPrincipalBalanceAmount,
	COALESCE(SIR2.SecurityInterestRate, SIR1.SecurityInterestRate),
	COALESCE(F2.CollateralPoolActiveLoanCount, F1.CollateralPoolActiveLoanCount),
	COALESCE(F2.CollateralPoolWeightedAverageMaturityNumber, F1.CollateralPoolWeightedAverageMaturityNumber),
	FISTGAB.FinancialInstrumentCollateralGroupParticipationPercent
ORDER BY 3 DESC