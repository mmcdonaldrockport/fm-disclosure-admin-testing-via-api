WITH SecurityInterestRate AS 

	(SELECT 
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate DESC),	
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)



SELECT

	[Trust Name] = D.Name,
	[Class] = S.SecurityClassName,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Security Type] = S.ProductGroupType,
	[Issue Date] = S.SecurityIssueDate,
	[Issuance UPB ($)] = S.SecurityIssueAmount,
	[Group] = STCG.DealGroupNumber,
	[Interest Rate (%)] = SIR.SecurityInterestRate,
	[Final Distribution Date] = CAST(FORMAT(S.SecurityMaturityDate, 'M/') AS varchar(max)) + CAST(S.SecurityPaymentDayNumber AS varchar(max)) + '/' +CAST(FORMAT(S.SecurityMaturityDate, 'yyyy') AS varchar(max))

FROM FmData.Deal D
	LEFT JOIN FmData.StructuredTransactionCollateralGroup STCG ON STCG.DealId = D.Id
	LEFT JOIN FmData.Security S ON S.StructuredTransactionCollateralGroupId = STCG.Id
	LEFT JOIN SecurityInterestRate SIR ON SIR.SecurityId = S.Id AND SIR.Ranking = 1
	
WHERE S.ProductGroupType= 'REMIC'

ORDER BY 1, 2