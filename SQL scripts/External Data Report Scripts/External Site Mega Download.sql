--FRD 44 - Mega Download

WITH Resecuritized AS 

	(SELECT 
		[SecurityRelatedId] = SR.SecurityRelatedId,
		[SecurityId] = SR.SecurityId,
		S.SecurityIssueDate,
		CP.MBSPoolIdentifier
	FROM FmData.SecurityRelationship SR
		LEFT JOIN FmData.Security S ON S.Id =  SR.SecurityId
		LEFT JOIN FmData.CollateralPool CP  ON CP.Id = SR.SecurityId)

, SecurityInterestRate AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityInterestRateEffectiveDate ASC),
		SecurityId,
		SecurityInterestRate
	FROM FmData.SecurityInterestRate)

, SecurityStatus AS 

	(SELECT
		[Ranking] = ROW_NUMBER() OVER(PARTITION BY SecurityId ORDER BY SecurityStatusDatetime DESC),
		SecurityId,
		SecurityStatusType
	FROM FmData.SecurityStatus)

SELECT DISTINCT
	[Transaction ID] = CP.MBSPoolIdentifier,
	[Prefix] = S.MBSPoolPrefixType,
	[Security Execution Type] = S.ProductGroupType,
	[CUSIP] = S.SecurityCUSIPIdentifier,
	[Interest Type] = S.SecurityInterestRateType,
	[Status] = SS.SecurityStatusType,
	[Issuance UPB ($)] = S.SecurityIssueAmount,
	[Issuance PTR (%)] = SIR.SecurityInterestRate,
	[Resecuritization] = 
		CASE 
			WHEN S.ResecuritizationRestrictedIndicator = 'N' THEN 'Eligible'
			WHEN S.ResecuritizationRestrictedIndicator = 'Y' THEN 'Ineligible'
			ELSE ''
		END,
	[Issue Date] = S.SecurityIssueDate,
	[First Payment Date] = S.SecurityFirstPaymentDate,
	[Resecuritized Date] = R.SecurityIssueDate,
	[Resecuritized Transaction ID] = R.MBSPoolIdentifier,
	[Terminated Date] = S.SecurityPaidOffDate,
	[Dissolved Date] = S.TrustDissolutionDate,
	[Maturity Date] = S.SecurityMaturityDate

FROM FmData.CollateralPool CP
	LEFT JOIN FmData.Security S ON S.Id = CP.Id
	LEFT JOIN Resecuritized R ON R.SecurityRelatedId = S.Id
	LEFT JOIN SecurityInterestRate SIR ON SIR.SecurityId = S.Id AND SIR.Ranking = 1
	LEFT JOIN SecurityStatus SS ON SS.SecurityId = S.Id AND SS.Ranking = 1

WHERE S.ProductGroupType = 'MEGA'